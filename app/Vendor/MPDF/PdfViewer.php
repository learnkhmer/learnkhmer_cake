<?php
require 'vendor/autoload.php';

use \Mpdf\Mpdf;
class PdfViewer extends Mpdf
{
    /*
     *  Use it in controller
     *  App::import('Vendor', 'MPDF/PdfViewer');
        $PdfViewer = new PdfViewer([
            'default_font' => 'khmeros'
        ]);
        $PdfViewer->loadView('Home/test');
        $PdfViewer->Output();
     *
     */
    public function loadView($path = null)
    {
        $root = APP.'View'.DS.$path.'.ctp';
        $content = file_get_contents($root);
        $this->WriteHTML($content);
    }
}

-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 06, 2017 at 03:39 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rachnasc_khmerlearner`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created`, `modified`, `description`) VALUES
('44d65ae0-0e55-11e7-9092-0606248e262c', 'lier', 'lier', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
('44d65ae0-0e55-11e7-9092-0606248e262d', 'reading', 'reading', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(6) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Top', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Footer', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_id` int(11) NOT NULL,
  `external_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `name`, `menu_id`, `external_id`, `parent_id`, `type`, `created`, `modified`, `sort`) VALUES
(1, 'Home', 1, '1', '0', 'page', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 'Beginner', 1, '2', '0', 'page', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
(3, 'Reading K-12', 1, '44d65ae0-0e55-11e7-9092-0606248e262d', '0', 'category', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(4, 'R 1', 1, '44d65ae0-0e55-11e7-9092-0606248e262c', '3', 'category', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5),
(5, 'Still', 1, '1', '4', 'page', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 4),
(6, 'kikio', 1, '44d65ae0-0e55-11e7-9092-0606248e2623', '3', 'category', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3),
(7, 'Math K-12', 1, '44d65ae0-0e55-11e7-9092-0606248e2624', '0', 'category', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2),
(8, 'rimino', 1, '2', '4', 'page', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 9),
(9, 'Contact Us', 1, '2', '4', 'page', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(10, 'Test Preparation', 1, '2', '0', 'page', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(11, 'Resources', 1, '12', '0', 'category', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 11),
(12, 'Conversation', 1, '3', '0', 'category', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 12);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `is_static` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `description`, `is_static`, `created`, `modified`) VALUES
(1, 'Home', 'Something about Home', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'About', 'Something about About', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Vision', 'Something about vision', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Mession', 'Something about Mission', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) UNSIGNED NOT NULL,
  `category_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(10) NOT NULL,
  `page` int(11) NOT NULL,
  `sound` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `title`, `level`, `page`, `sound`, `description`, `model`, `created`, `modified`) VALUES
(5, '44d65ae0-0e55-11e7-9092-0606248e262c', 'ទីបំផុត! ឃ្មឹប និង ហុងដា សម្តែងកំប្លែងរួមគ្នាវិញហើយនៅថ្ងៃនេះ ក្រោយបែកបាក់គ្នាជាច្រើនឆ្នាំ!', 1, 1, 'http://localhost/job/learnkhmeronline/app/webroot/files/filemanager/aromkatha.mp3', '<p style=\"box-sizing: border-box; font-size: 16px; margin-bottom: 1.3rem; color: #333333; font-family: HelveticaNeue-Light, \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, sans-serif, DefaultFont, Khmer;\"><strong style=\"box-sizing: border-box;\">ភ្នំពេញ៖&nbsp;</strong>នៅថ្ងៃនេះអតីតតារាកំប្លែងនាងឃ្មឹប ដែលជាភរិយាដើមរបស់តារាកំប្លែងលោកហុងដា បានវិលចូលត្រឡប់មកលេងកំប្លែងវិញហើយ នៅក្នុងក្រុមរបស់នាយកុយតាមរយៈកម្មវិធី &laquo;មន្តស្នេហ៍សំនៀង&raquo;។</p>\r\n<p style=\"box-sizing: border-box; font-size: 16px; margin-bottom: 1.3rem; color: #333333; font-family: HelveticaNeue-Light, \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, sans-serif, DefaultFont, Khmer;\">នោះបើយោងតាមការ Live Video ផ្តាល់ពីស្ថានីយទូរទស្សន៍ស៊ីធីអិន នៅលើហ្វេសប៊ុកផ្ទាល់របស់លោក ហេង ឡុង អនាគតស្វាមីអ្នកនាង ចន ច័ន្ទលក្ខិណា។</p>\r\n<p style=\"box-sizing: border-box; font-size: 16px; margin-bottom: 1.3rem; color: #333333; font-family: HelveticaNeue-Light, \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, sans-serif, DefaultFont, Khmer;\">ការវិលត្រលប់របស់អ្នកនាងឃ្មឹបនៅលើកនេះ គឺពិតជាបានធ្វើឱ្យមហាជនមានភាពភ្ញាក់ផ្អើល និងរំភើបនឹកស្មានមិនដល់យ៉ាងខ្លាំង។&nbsp;</p>\r\n<p style=\"box-sizing: border-box; font-size: 16px; margin-bottom: 1.3rem; color: #333333; font-family: HelveticaNeue-Light, \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, sans-serif, DefaultFont, Khmer;\">ជាពិសេសអ្វីដែលធ្វើឱ្យទស្សិកជនទ្រាំមិនសរសើរមិនបាននោះគឺ លោកហុងដា និងអ្នកនាងឃ្មឹបបានដើរតួធ្វើជាស្វាមីភរិយានិងគ្នាទៀតផង ទោះបីជាអ្នកទាំងពីរបានបែកបាក់គ្នាអស់រយៈពេលជាច្រើនឆ្នាំកន្លងទៅហើយ។</p>\r\n<p style=\"box-sizing: border-box; font-size: 16px; margin-bottom: 1.3rem; color: #333333; font-family: HelveticaNeue-Light, \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, sans-serif, DefaultFont, Khmer;\"><strong style=\"box-sizing: border-box;\">ទស្សនាការ Live Video ផ្ទាល់ដូចខាងក្រោមនេះ៖&nbsp;</strong></p>', '', '2017-05-21 05:10:00', '2017-05-21 05:12:01');

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` int(255) DEFAULT NULL,
  `audio` int(255) DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT 'CONSONANT, VOWEL',
  `sort` int(11) NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quiz_items`
--

CREATE TABLE `quiz_items` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` int(255) DEFAULT NULL,
  `audio` int(255) DEFAULT NULL,
  `quiz_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `is_corrected` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `readings`
--

CREATE TABLE `readings` (
  `id` int(11) UNSIGNED NOT NULL,
  `level` char(36) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `sound` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8_unicode_ci,
  `status` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `readings`
--

INSERT INTO `readings` (`id`, `level`, `sound`, `content`, `status`, `created`, `modified`) VALUES
(4, '58ce05ee-901c-4859-9f63-4e1fbacf2bfb', '', '<p><img src=\"../../../app/webroot/files/filemanager/Toppage3.jpg\" width=\"151\" height=\"227\" /></p>\r\n<p>&nbsp;</p>\r\n<p>With the script included, initialize TinyMCE on any element (or elements) in your web page.</p>\r\n<p>Since TinyMCE lets you identify replaceable elements via a CSS selector, all you need do is pass an object that contains a <code class=\"prettyprint prettyprinted\"><span class=\"pln\">selector</span></code> to <code class=\"prettyprint prettyprinted\"><span class=\"pln\">tinymce</span><span class=\"pun\">.</span><span class=\"pln\">init</span><span class=\"pun\">()</span></code>.</p>', 'deleted', '2017-03-16 09:06:46', '2017-03-19 00:15:42'),
(5, '1', 'http://localhost/job/learnkhmeronline/app/webroot/files/filemanager/aromkatha.mp3', '<p>jj<img src=\"/job/learnkhmeronline/app/webroot/files/filemanager/fmlogo_bbg.png\" alt=\"\" width=\"41\" height=\"12\" /><img src=\"/job/learnkhmeronline/app/webroot/files/filemanager/Toppage3.jpg\" alt=\"\" width=\"905\" height=\"1024\" /><img src=\"/job/learnkhmeronline/app/webroot/files/filemanager/nj.gif\" alt=\"\" width=\"500\" height=\"750\" /></p>', 'active', '2017-03-17 08:04:56', '2017-05-07 00:03:59');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credential` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `author`, `site_title`, `contact`, `description`, `logo`, `credential`, `facebook`, `twitter`, `gplus`, `created`, `modified`) VALUES
(1, 'Mr. Yanns', 'title', '069909169', 'dd', NULL, 'Copyright © 2017 by LearnKhmerOnline Team. All rights reserved.', 'http://facebook.com/', 'http://twitter.com/', 'http://google.com/', '2017-03-19 00:00:00', '2017-05-24 12:20:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(36) NOT NULL,
  `parent_id` char(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'active',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `parent_id`, `username`, `email`, `password`, `birthdate`, `gender`, `mobile`, `country`, `address`, `social_id`, `role`, `token`, `status`, `created`, `modified`) VALUES
(1, NULL, 'admin', 'admin.info@gmail.com', '203c0468ebbe7e451b5915214f314f3bb9a7e3bc', '1993-03-24', 'Male', '069909169', 'French', 'Phnom Penh, Cambodia', '123445', 'admin', NULL, 'active', '2017-02-28 00:00:00', '2017-02-28 00:00:00'),
(2, NULL, 'editor', 'editor.info@gmail.com', '203c0468ebbe7e451b5915214f314f3bb9a7e3bc', '1993-03-24', 'Male', '069909169', 'Cambodia', 'Phnom Penh, Cambodia', '123445', 'editor', NULL, 'active', '2017-02-28 00:00:00', '2017-06-06 09:25:16'),
(4, NULL, 'rar', '58b9b517-bae4-497b-9788-ba4fbacf2bfb+lflj@gmail.com', '5d60251296ede2a497564e15a291aa416dc8c8be', '0000-00-00', 'Male', '2', 'USA', '', NULL, 'user', NULL, 'deleted', '2017-03-03 13:25:05', '2017-03-03 13:25:27'),
(5, NULL, 'samphors', 'samphors.info@gmail.com', '5d60251296ede2a497564e15a291aa416dc8c8be', '1990-12-12', 'Male', '098989898', 'USA', '234', NULL, 'admin', NULL, 'active', '2017-03-05 10:47:28', '2017-03-05 11:14:54'),
(6, NULL, 'dhlah', 'slfj@gmail.com', '5d60251296ede2a497564e15a291aa416dc8c8be', '1998-12-11', 'Male', '333', 'USA', '33', NULL, 'admin', NULL, 'active', '2017-03-05 11:15:30', '2017-03-05 11:15:30'),
(7, NULL, 'rika', 'admin@uyfc.com', '1407fd7455854df1171a0e6598ad30b7b3cd296c', '1824-01-07', 'Female', '0987654321', 'USA', '5678', NULL, 'admin', NULL, 'active', '2017-05-07 00:22:32', '2017-05-07 00:22:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `slug` (`slug`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `title` (`title`),
  ADD KEY `name` (`name`),
  ADD KEY `name_2` (`name`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `external_id` (`external_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_media_id` (`image`,`audio`);

--
-- Indexes for table `quiz_items`
--
ALTER TABLE `quiz_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_media_id` (`image`,`audio`);

--
-- Indexes for table `readings`
--
ALTER TABLE `readings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`email`,`password`),
  ADD KEY `parent_id` (`parent_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quiz_items`
--
ALTER TABLE `quiz_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `readings`
--
ALTER TABLE `readings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(36) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models/', '/next/path/to/models/'),
 *     'Model/Behavior'            => array('/path/to/behaviors/', '/next/path/to/behaviors/'),
 *     'Model/Datasource'          => array('/path/to/datasources/', '/next/path/to/datasources/'),
 *     'Model/Datasource/Database' => array('/path/to/databases/', '/next/path/to/database/'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions/', '/next/path/to/sessions/'),
 *     'Controller'                => array('/path/to/controllers/', '/next/path/to/controllers/'),
 *     'Controller/Component'      => array('/path/to/components/', '/next/path/to/components/'),
 *     'Controller/Component/Auth' => array('/path/to/auths/', '/next/path/to/auths/'),
 *     'Controller/Component/Acl'  => array('/path/to/acls/', '/next/path/to/acls/'),
 *     'View'                      => array('/path/to/views/', '/next/path/to/views/'),
 *     'View/Helper'               => array('/path/to/helpers/', '/next/path/to/helpers/'),
 *     'Console'                   => array('/path/to/consoles/', '/next/path/to/consoles/'),
 *     'Console/Command'           => array('/path/to/commands/', '/next/path/to/commands/'),
 *     'Console/Command/Task'      => array('/path/to/tasks/', '/next/path/to/tasks/'),
 *     'Lib'                       => array('/path/to/libs/', '/next/path/to/libs/'),
 *     'Locale'                    => array('/path/to/locales/', '/next/path/to/locales/'),
 *     'Vendor'                    => array('/path/to/vendors/', '/next/path/to/vendors/'),
 *     'Plugin'                    => array('/path/to/plugins/', '/next/path/to/plugins/'),
 * ));
 */

/**
 * Custom Inflector rules can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. Make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); // Loads a single plugin named DebugKit
 */

/**
 * To prefer app translation over plugin translation, you can set
 *
 * Configure::write('I18n.preferApp', true);
 */

/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter. By default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *		'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 *		'MyCacheFilter' => array('prefix' => 'my_cache_'), //  will use MyCacheFilter class from the Routing/Filter package in your app with settings array.
 *		'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 *		array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 *		array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
	'AssetDispatcher',
	'CacheDispatcher'
));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
	'engine' => 'File',
	'types' => array('notice', 'info', 'debug'),
	'file' => 'debug',
));
CakeLog::config('error', array(
	'engine' => 'File',
	'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
	'file' => 'error',
));

define('SERVICE_NAME', '​រៀនខ្មែរអនឡាញ');
define('DEBUG_MODE', @$_SERVER['SERVER_NAME']);

define('EMAIL_FROM', 'helloygd1@gmail.com');
define('EMAIL_FROM_PAYPAL', 'info@learnkhmer.online');
define('EMAIL_ADMIN', 'samphors.info@gmail.com');
define('EMAIL_CC', 'samphors.info@gmail.com');

// message
define('LOGIN_SUCCESS', 'Logged in successfully. Awesome!!!');
define('LOGOUT_SUCCESS', 'Logged out successfully. Welcome!!!');
define('LOGIN_ERROR_USER', 'id or password, or if both are wrong.');
define('REGISTRATION_SUCCESS', 'User registration successful, Please login now.');
define('REGISTRATION_FAILURE', 'User registration failed, Please try again.');
define('FORGET_PASSWORD_SUCCESS', 'Password reset successful, and sent to your mail.');
define('FORGET_PASSWORD_FAILURE', 'Password reset failed, Please try again.');
define('PASSWORD_CHANGE_SUCCESS', 'Password changed sucessfully.');
define('PASSWORD_ACCOUNT_NOT_ACTIVATED', 'You don\'t have account yet.');
define('PASSWORD_ACCOUNT_REMOVED', 'Your account has been removed, please contact to administrator.');
define('MESSAGE_RESULT_NOT_FOUND', 'Result is emtpy, Your request not found!');
define('MESSAGE_CONFIRM_DELETE', 'Are you sure you want to delete this?');
define('MESSAGE_FAIL', 'Operation failed.');
define('MESSAGE_UPDATE', 'Record has been successfully updated!');
define('MESSAGE_CREATE', 'Record has been successfully created!');

// validate messaage
define('MESSAGE_REQUIED', 'This field is required.');

// user role
define('USER_ROLE_SUPER', 'super');
define('USER_ROLE_ADMIN', 'admin');
define('USER_ROLE_TEACHER', 'teacher');
define('USER_ROLE_PARENT', 'parent');
define('USER_ROLE_STUDENT', 'student');
define('ROLE', serialize(array(
    USER_ROLE_TEACHER => USER_ROLE_TEACHER,
    USER_ROLE_PARENT => USER_ROLE_PARENT,
    USER_ROLE_STUDENT => USER_ROLE_STUDENT,
)));

// number
define('LIMIT', 10);
define('LIMIT_20', 20);

// options
define('GENDER', serialize(array(
    'Male' => 'Male',
    'Female' => 'Female',
)));
define('COUNTRY', serialize(array(
    'USA' => 'USA',
    'French' => 'French',
    'Cambodia' => 'Cambodia',
)));

// status
define('DELETE_STATUS', 'deleted');
define('STATUS_SUCCESS', 'success');
define('STATUS_FAIL', 'fail');
define('STATUS_ACTIVE', 'active');
define('STATUS_DEACTIVE', 'deactive');

// reading status
define('READING_STATUS_ACTIVE', 'active');
define('READING_STATUS_DISABLED', 'disabled');


// beginner
define('PATH_BEGINNER_THUMBNAIL', '/files/beginners/thumbnail/');
define('PATH_BEGINNER_IMAGE', '/files/beginners/image/');
define('PATH_BEGINNER_DRAW', '/files/beginners/draw/');
define('PATH_BEGINNER_SIMPLE_IMAGE', '/files/beginners/simple_image/');
define('PATH_BEGINNER_SOUND', '/files/beginners/sound/');
define('PATH_BEGINNER_SIMPLE_SOUND', '/files/beginners/simple_sound/');

// worksheet
define('PATH_WORKSHEET_IMG', '/files/worksheets/thumbnail/');
define('PATH_WORKSHEET_PDF', '/files/worksheets/pdf/');

// readings
define('PATH_READING_SOUND', '/files/readings/sound/');
define('PATH_READING_NOVEL', '/files/readings/novel/');

define('PATH_READING', '/files/readings/');
define('PATH_RESOURCE', '/files/resources/');
define('PATH_WORKSHEET', '/files/worksheets/');

// BEGINNER
define('CATEGORY_BEGINNER_TYPE_SOUND', 'sound');
define('CATEGORY_BEGINNER_TYPE_ANIMATION', 'animation');

// MEMBER PATH
define('MEMBER_PATH', '/img/teams');

// Singer
define('SINGER_PATH', '/img/singers');
define('DONATATION_PATH', '/img/donations');
define('SINGER_SONG_PATH', '/img/songs');
define('ADS_PATH', '/img/ads');

define('MUSIC_PAHT_IMAGE', '/files/music/image');
define('MUSIC_PAHT_MP3', '/files/music/mp3');


// CATEGORY MODE
define('CATEGORY_TYPE', serialize(array(
	CATEGORY_BEGINNER_TYPE_ANIMATION => 'ADMIN_CATEGORY_TXT_TYPE_ANIMATION',
	CATEGORY_BEGINNER_TYPE_SOUND => 'ADMIN_CATEGORY_TXT_TYPE_SOUND',
)));

// URL FOR SUB-DOMAIN
define('FILE_DOMAIN', 'https://learnkhmer.online');
define('FILE_DOMAIN_FILES', 'https://learnkhmer.online/files');

// MESSAGE CONTACT
define('CANTACT_ERROR_MESSAGE', 'ឈ្មោះ អ៊ីម៉ែល និង សារ គឺតម្រូវ​អោយ​បញ្ចូល!');
define('CANTACT_SUCCESS_MESSAGE', 'សារ​ត្រូវ​បាន​បញ្ជូន​ដោយ​ជោគ​ជ័យ​! សូម​អគុណ​សម្រាប់​សារ​របស់អ្នក​!');

// UPLOAD API URL
define('UPLOAD_API_URL', 'https://learnkhmer.online/api/media/upload');

define('GOOGLE_VIEW_PATH', 'https://docs.google.com/viewer?url=');

// File Type
define('FILE_TYPE_SOUND', 'sound');
define('FILE_TYPE_SOUND_2', 'sound_2');
define('FILE_TYPE_IMAGE', 'image');
define('FILE_TYPE_THUMBNAIL', 'thumbnail');
define('FILE_TYPE_IMAGE_DRAWING', 'image_drawing');
define('FILE_TYPE_IMAGE_LETTER', 'image_letter');
define('FILE_TYPE_PDF', 'pdf');

define('STATUS_UPDATE', 'update');

define('LOCAL_NAME', 'eng');

define('BIBLE_NEW_BOOK', 'new_book');
define('BIBLE_OLD_BOOK', 'old_book');
define('BIBLE_SONG', 'song');
define('BIBLE', serialize(array(
    BIBLE_NEW_BOOK => 'TXT_BIBLE_NEW_BOOK',
    BIBLE_OLD_BOOK => 'TXT_BIBLE_OLD_BOOK',
    BIBLE_SONG => 'TXT_BIBLE_SONG',
)));


define('CATEGORY_BEGINER', 'beginner');
define('CATEGORY_WORKSHEET', 'worksheet');
define('CATEGORY_SONG', 'song');
define('CATEGORY_CHRIST', 'christ');
define('CATEGORY_ANIMATION', 'animation');
define('CATEGORY_SOUND', 'sound');

define('CATEGORY_MODE', serialize(array(
    CATEGORY_ANIMATION => 'Animation',
    CATEGORY_SOUND => 'Sound',
    CATEGORY_WORKSHEET => 'Worksheet',
    CATEGORY_SONG => 'Song',
    CATEGORY_CHRIST => 'Christ',
)));
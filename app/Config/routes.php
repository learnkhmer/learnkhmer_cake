<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'home', 'action' => 'index'));
    Router::connect('/file', array('controller' => 'pages', 'action' => 'display', 'file'));
    Router::connect('/sitemap', array('controller' => 'sitemap', 'action' => 'index', 'ext' => 'xml'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	Router::connect('/contact', array('controller' => 'pages', 'action' => 'display', 'contact'));
	Router::connect('/conversation', array('controller' => 'pages', 'action' => 'display', 'conversation'));

	Router::connect('/goal-mission', array('controller' => 'pages', 'action' => 'display', 'goal-mission'));
	Router::connect('/report', array('controller' => 'pages', 'action' => 'display', 'report'));
	Router::connect('/our-team', array('controller' => 'pages', 'action' => 'display', 'our-team'));
	Router::connect('/committees', array('controller' => 'pages', 'action' => 'display', 'committees'));
	Router::connect('/our-specialist', array('controller' => 'pages', 'action' => 'display', 'our-specialist'));

	Router::connect('/donate', array('controller' => 'pages', 'action' => 'display', 'donate'));
	Router::connect('/our-sponsor', array('controller' => 'pages', 'action' => 'display', 'our-sponsor'));
	Router::connect('/our-supporter', array('controller' => 'pages', 'action' => 'display', 'our-supporter'));
	Router::connect('/volunteer', array('controller' => 'pages', 'action' => 'display', 'volunteer'));
	Router::connect('/greeting', array('controller' => 'pages', 'action' => 'display', 'greeting'));

	Router::connect('/faq', array('controller' => 'pages', 'action' => 'display', 'faq'));
	Router::connect('/term', array('controller' => 'pages', 'action' => 'display', 'term'));
	Router::connect('/feedback', array('controller' => 'pages', 'action' => 'display', 'feedback'));
    Router::connect('/resources', array('controller' => 'pages', 'action' => 'display', 'resources'));
	//admin route
    Router::connect('/admin', array('controller' => 'users', 'action' => 'index', 'admin' => true));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';

<?php

App::uses('AppModel', 'Model');

class Singer extends AppModel
{
    public $validationDomain = 'validation';

    public $validate = array(
        'name_kh' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'name_en' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
    );
}
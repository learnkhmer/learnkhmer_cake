<?php
App::uses('AppMpdel', 'Model');
App::uses('AuthComponent', 'Controller/Component');

class Permission extends AppModel
{
    public $validationDomain = 'validation';
    public $belongsTo = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id'
        ),
        'Action' => array(
            'className' => 'Action',
            'foreignKey' => 'action_id'
        ),
    );
}

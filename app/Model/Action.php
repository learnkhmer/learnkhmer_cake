<?php
App::uses('AppMpdel', 'Model');
App::uses('AuthComponent', 'Controller/Component');

class Action extends AppModel
{
    public $validationDomain = 'validation';

    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),
        'url' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),
    );
}

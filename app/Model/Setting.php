<?php
App::uses('AppMpdel', 'Model');

class Setting extends AppModel
{
    public function isExist($key = null)
    {
        $data = $this->find('first');
        $converJsonToArray = json_decode($data['Setting']['lang'], TRUE);
        return isset($converJsonToArray[$key]);
    }

    public function getLangColumToArray($arr = array())
    {
        $data = $this->find('first');
        $lang = json_decode($data['Setting']['lang'], TRUE);
        $lang[$arr['Setting']['key']] = [
            'type' => $arr['Setting']['type'],
            'title' => $arr['Setting']['title'],
            'txt_kh' => '',
            'txt_eng' => ''
        ];
        return $lang;
    }
    
    public function getLang()
    {
        $data = $this->find('first');
        return json_decode($data['Setting']['lang'], TRUE);
    }
}

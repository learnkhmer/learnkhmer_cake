<?php
App::uses('AppMpdel', 'Model');

class Reading extends AppModel
{
    public $validationDomain = 'validation';
    public $hasMany = array(
        'ReadingItem' => array(
            'className' => 'ReadingItem',
            'foreignKey' => 'reading_id',
            'dependent' => true,
            'order' => array('ReadingItem.order' => 'asc'),
        )
    );

    public $validate = array(
        'title' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'sound' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'level' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'page' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
    );

    public function get($mode = 'first', $conditions = array(), $page = 1)
    {
        $data = $this->find($mode, array(
            'conditions' => $conditions,
            'page' => $page,
            'order' => array('page' => 'asc'),
        ));
        return $data;
    }
}

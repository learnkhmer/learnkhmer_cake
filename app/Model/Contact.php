<?php
App::uses('AppMpdel', 'Model');

class Contact extends AppModel
{
    public $useTable = false;

    public $validationDomain = 'validation';
    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),
        'email' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
            'email' => array(
                'rule' => 'email',
                'message' => 'Invalid email format!'
            )
        ),
        'description' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),

    );

}

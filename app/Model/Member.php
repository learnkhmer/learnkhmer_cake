<?php
App::uses('AppModel', 'Model');

class Member extends AppModel
{
    public $validationDomain = 'validation';

    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'position' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'link' => array(
            'rule' => 'url',
            'allowEmpty' => true
        ),
    );
}

<?php
App::uses('AppMpdel', 'Model');

class CategoryDetail extends AppModel
{
    public $belongsTo = array(
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'category_id',
        )
    );
}

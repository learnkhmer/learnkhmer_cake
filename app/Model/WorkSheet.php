<?php
App::uses('AppModel', 'Model');

class WorkSheet extends AppModel
{
    public $validationDomain = 'validation';
    public $belongsTo = array(
        'Category',
    );

    public $validate = array(
        'title' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'price' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'category_id' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'thumbnail' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'file' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
    );
}

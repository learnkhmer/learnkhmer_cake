<?php
App::uses('AppController', 'Controller');

class ReadingsController extends AppController
{
    public $title_for_layout = 'Reading';
    public $components = array(
        'Image',
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(array(
            'index',
            'view',
            'view_test',
            'novel'
        ));
        $this->Security->unlockedActions = array(
            'admin_saveAndUpdate',
        );
    }

    public function index($id = null)
    {
        $this->Paginator->settings = array(
            'conditions' => array(
                // 'Reading.category_id' => !$id ? null : $id,
                'Reading.level' => $id,
            ),
            'order' => array(
                'Reading.page' => 'asc',
            ),
            'limit' => 20
        );
        $data = $this->Paginator->paginate('Reading');
        // pr($data);
        $this->set(compact('data'));
        $this->set('title_for_layout', $this->title_for_layout.'');
    }

    public function view($id = null)
    {
        $data = $this->Reading->findById($id);
        $neighbors = $this->Reading->find('neighbors', array(
            'field' => 'id',
            'value' => $id
        ));
        // pr($neighbors);
        $this->set(compact('data', 'neighbors'));
        $this->set('title_for_layout', $this->title_for_layout.'');
    }

    public function admin_index()
    {
        $conditions = array(
            'Reading.type <>' => 2,
        );
        if ($this->request->query('search')) {
            $conditions = array(
                'OR' => array(
                    'Reading.level LIKE' => '%' . trim($this->request->query('search')) . '%',
                    'Reading.title LIKE' => '%' . trim($this->request->query('search')) . '%',
                    'Reading.sound LIKE' => '%' . trim($this->request->query('search')) . '%',
                    'Reading.lesson LIKE' => '%' . trim($this->request->query('search')) . '%',
                ),
            );
        }
        $this->Paginator->settings = array(
            'order' => array(
                'Reading.level' => 'asc',
            ),
            'limit' => 1000,
            'conditions' => $conditions,
        );
        $data = $this->Paginator->paginate('Reading');
        $this->set('data', $data);
        $this->set('title_for_layout', $this->title_for_layout.'');
    }

    public function admin_create()
    {
        if ($this->request->is(array('post'))) {
            $this->Reading->create();
            if ($this->Reading->save($this->request->data)) {
                $this->Session->setFlash(MESSAGE_CREATE, 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(MESSAGE_FAIL, 'error');
            }
        }
        $this->set('title_for_layout', $this->title_for_layout.'Create');
    }

    public function admin_edit($id = null)
    {
        if (!$id) {
            throw new NotFoundException();
        }
        $data = $this->Reading->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->request->data = $data;
        $this->set('title_for_layout', $this->title_for_layout.'Edit');
    }

    public function admin_delete($id = null)
    {
        if (!$id) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $data = $this->Reading->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Reading->id = $data['Reading']['id'];
        if ($this->Reading->delete()) {
            $this->redirect(array('action' => 'index'));
        }
    }

    public function admin_saveAndUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->entity($this->request->data);
        $this->Reading->set($data);
        // Check validation
        if (!$this->Reading->validates()) {
            return json_encode(array(
                'status' => '0',
                'message' => MESSAGE_FAIL,
                'data' => $this->Reading->validationErrors,
            ));
        }
        if ($this->Reading->save($data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }

    private function entity($data = null)
    {
        if ($data === null) {
            return array();
        }
        $data = $data['Reading'];
        if ($data['sound'] === STATUS_UPDATE) {
            $data['sound'] = $this->Image->check_and_upload_file($data['sound_file'], MENU_MODEL_READING, FILE_TYPE_SOUND);
            unset($data['sound_file']);
        }
        return $data;
    }
}

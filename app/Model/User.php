<?php
App::uses('AppMpdel', 'Model');
App::uses('AuthComponent', 'Controller/Component');

class User extends AppModel
{
    public $validationDomain = 'validation';

    public $belongsTo = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id',
        ),
    );

    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
            'min_length' => array(
                'rule' => array('minLength', '5'),
                'message' => 'Password must over 5',
            )
        ),
        'password_confirm' => array(
            'equalToField' => array(
                'rule' => array('equalToField', 'password'),
                'message' => 'Please enter the same value again!',
            )
        ),
        'password_update' => array(
            'min_length' => array(
                'rule' => array('minLength', '5'),
                'message' => MESSAGE_REQUIED,
                'allowEmpty' => true,
                'required' => false,
            )
        ),
    );

    public function beforeSave($options = array())
    {
        // hash a password
        if (isset($this->data[$this->alias]['password']) && ($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }

        // hash a new password
        if (isset($this->data[$this->alias]['password_update']) && ($this->data[$this->alias]['password_update'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password_update']);
        }

        return true;
    }

    public function afterFind($results, $primary = false)
    {
        parent::afterFind($results, $primary);

        foreach ($results as $key => $value) {
        }

        return $results;
    }

    public function equalToField($check, $field)
    {
        $name = '';
        foreach ($check as $key => $value) {
            $name = $key;
            break;
        }

        return $this->data[$this->name][$field] === $this->data[$this->name][$name];
    }

    public function isExistMailAdress($email = null)
    {
        $user = $this->find('first', array('conditions' => array('User.email' => trim($email))));
        return $user ? true : false;
    }

    public function isAdmin($id = null)
    {
        $user = $this->findById($id);
        return $user['User']['role'] === USER_ROLE_ADMIN ? true : false;
    }

    public function isExistToken($token = null)
    {
        $user = $this->find('all', array(
            'conditions' => array(
                'User.token' => $token,
                )
            )
        );

        return $user ? true : false;
    }
}

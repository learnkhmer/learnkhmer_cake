<?php
App::uses('AppMpdel', 'Model');
App::uses('AuthComponent', 'Controller/Component');

class Module extends AppModel
{
    public $validationDomain = 'validation';
    public $belongsTo = array(
        'Parent' => array(
            'className' => 'Module',
            'foreignKey' => 'parent_id'
        ),
    );
    public $hasMany = array(
        'Action' => array(
            'className' => 'Action',
            'foreignKey' => 'module_id',
            'dependent' => true,
            'order' => array('Action.name' => 'asc'),
        )
    );

    public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),
        'url' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),
    );
}

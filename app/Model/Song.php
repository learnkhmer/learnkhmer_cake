<?php

App::uses('AppModel', 'Model');

class Song extends AppModel
{
    public $belongsTo = array(
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'category_id',
        ),
        'Singer' => array(
            'className' => 'Singer',
            'foreignKey' => 'singer_id',
        )
    );
    
    public $validationDomain = 'validation';

    public $validate = array(
        'title' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'category_id' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'written_year' => array(
            'min_length' => array(
                'rule' => array('minLength', '4'),
                'message' => 'Year must over than 3',
            )
        ),
    );
}
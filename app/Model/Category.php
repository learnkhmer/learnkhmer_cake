<?php
App::uses('AppMpdel', 'Model');

class Category extends AppModel
{
    public $validationDomain = 'validation';
    public $hasOne = array(
        'CategoryDetail' => array(
            'className' => 'CategoryDetail',
        ),
    );
    public $belongsTo = array(
        'Parent' => array(
            'className' => 'Category',
            'foreignKey' => 'parent_id'
        ),
    );

    public $validate = array(
        'name_kh' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),
        'name_en' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),
        'model' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),
    );

    public function getAllCategories()
    {
        $data = $this->find('list', array(
            'fields' => array('id', 'name_kh'),
            'order' => array('name_kh' => 'asc'),
        ));
        return $data;
    }

    public function getAllByModelBeginnerAndType($type = null)
    {
        $data = $this->find('all', array(
            'conditions' => array(
                'Category.type' => $type,
                'Category.model' => MENU_MODEL_BEGINNER,
            ),
            'order' => array(
                'Category.order' => 'asc',
            ),
        ));
        return $data;
    }

    public function getAllByModelWorkSheet($model = null)
    {
        $data = $this->find('all', array(
            'conditions' => array(
                'Category.model' => $model,
                'Category.status' => 1,
            ),
            'order' => array(
                'Category.created' => 'asc',
            ),
        ));
        return $data;
    }

    public function getSelectedCategoryAndModel($id = null, $model = null)
    {
        $data = $this->find('first', array(
            'conditions' => array(
                'Category.model' => $model,
                'Category.id' => $id,
                'Category.status' => 1,
            ),
        ));
        return $data;
    }
}

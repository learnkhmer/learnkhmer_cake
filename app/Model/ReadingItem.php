<?php
App::uses('AppMpdel', 'Model');

class ReadingItem extends AppModel
{
    public $belogsTo = array('Reading');
    public $validationDomain = 'validation';

    public $validate = array(
        'text' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),
        'start' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),
        'end' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),
        'reading_id' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            )
        ),
    );
}

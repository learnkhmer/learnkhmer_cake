<?php
App::uses('AppModel', 'Model');

class Beginner extends AppModel
{
    public $validationDomain = 'validation';
    public $belongsTo = array(
        'Category',
    );

    public $validate = array(
        'title' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'category_id' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'thumbnail' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'image' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'draw' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'simple_image' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'sound' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
        'simple_sound' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => MESSAGE_REQUIED,
            ),
        ),
    );

    public function getAllById($id = null)
    {
        $data = $this->find('all', array(
            'conditions' => array(
                'Beginner.category_id' => $id,
            ),
            'order' => array(
                'Beginner.id' => 'asc',
            ),
            'recursive' => -1,
        ));
        return $data;
    }
}

<div class="card">
    <div class="card-body">
        <?php
        echo $this->Form->create('Song', array(
            'type' => 'get',
        )); ?>
        <div class="row">
            <div class="col-md-5 col-sm-6 col-12">
                <div class="form-group">
                    <?php
                    echo $this->Form->text('search', array(
                        'class' => 'form-control',
                        'value' => $this->request->query('search'),
                        'placeholder' => __('COMMON_TXT_SEARCH'),
                    )); ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-6">
                <div class="form-group">
                    <?php
                    echo $this->Form->input('search', array(
                        'class' => 'btn btn-primary',
                        'type' => 'button',
                        'placeholder' => __('COMMON_TXT_SEARCH'),
                        'label' => false,
                    )); ?>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-6">
                <div class="form-group">
                    <?php
                    echo $this->Html->link('<i class="fa fa-plus"></i> Create', array(
                        'action' => 'create',
                        'prefix' => 'admin',
                    ), array(
                        'class' => 'btn btn-primary float-right',
                        'escape' => false,
                    )); ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>


<div class="card">
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Ref Number</th>
                    <th>Written</th>
                    <th>Written Year</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $key => $value) : ?>
                    <tr>
                        <td><?php echo ++$key; ?></td>
                        <td><?php echo $value['Song']['title']; ?></td>
                        <td><?php echo $value['Song']['ref_number']; ?></td>
                        <td><?php echo $value['Song']['written']; ?></td>
                        <td><?php echo $value['Song']['written_year']; ?></td>
                        <td class="project-actions text-right">
                            <?php
                            echo $this->Html->link('<i class="fas fa-pencil-alt"></i> Edit', '/admin/christs/edit/' . $value['Song']['id'], [
                                'class' => 'btn btn-info btn-sm',
                                'escape' => false,
                            ]); ?>

                            <?php
                            echo $this->Form->postLink('<i class="fas fa-trash"></i> Delete', '/admin/christs/delete/' . $value['Song']['id'], [
                                'class' => 'btn btn-danger btn-sm',
                                'escape' => false,
                                'confirm' => 'Are you sure you want to delete?',
                            ]); ?>
                        </td>
                    </tr>
                <?php
                endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <?php
        echo $this->element('/Backend/pagination'); ?>
    </div>
</div>

<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_MUSIC'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_MUSIC'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- <div class="alert alert-primary"> 
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <form action="<?php echo $this->here; ?>" method="get">
                                <input class="form-control keyword" name="keyword" placeholder="<?php echo __('COMMON_TXT_SEARCH'); ?>">
                            </form>
                        </div>
                    </div>
                </div> -->
                <div>
                    <div class="row lyrice">
                        <div class="col-md-12">
                            <div class="alert" style="border: 1px solid #cce5ff;">
                                <h5 class="f-koulen"><?php echo $data['Song']['title']; ?></h5>
                                <hr />
                                <?php if ($data['Song']['audio']) : ?>
                                    <audio preload="auto" controls>
                                        <source src="<?php echo MUSIC_PAHT_MP3.DS.$data['Song']['audio']; ?>">;
                                    </audio>
                                <?php endif; ?>

                                <div id="accordion">
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" data-toggle="collapse" data-target="#chord_image"><i class="fa fa-angle-right"></i> Chord</button>
                                            </h5>
                                        </div>
                                        <div id="chord_image" class="collapse show" data-parent="#accordion">
                                            <div class="card-body">
                                            <?php
                                            if ($data['Song']['chord_image'] && !$data['Song']['lyrice']) : ?>
                                                <?php
                                                echo $this->Html->image(MUSIC_PAHT_IMAGE.DS.$data['Song']['chord_image'], [
                                                    'class' => 'group list-group-image img-fluid',
                                                    'alt' => $data['Song']['title']
                                                ]); ?>
                                            <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" data-toggle="collapse" data-target="#lyric_image"><i class="fa fa-angle-right"></i> Lyric</button>
                                            </h5>
                                        </div>
                                        <div id="lyric_image" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                            <?php
                                            if ($data['Song']['lyric_image'] && !$data['Song']['lyrice']) : ?>
                                                <?php
                                                echo $this->Html->image(MUSIC_PAHT_IMAGE.DS.$data['Song']['lyric_image'], [
                                                    'class' => 'group list-group-image img-fluid',
                                                    'alt' => $data['Song']['title']
                                                ]); ?>
                                            <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="alert" style="border: 1px solid #cce5ff;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-borderless table-sm" style="margin-bottom: 0px">
                                            <tr>
                                                <td><?php echo __('Ref Nuber'); ?></td>
                                                <td><?php echo __('Written'); ?></td>
                                                <td><?php echo __('Written Year'); ?></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php echo $data['Song']['ref_number'] ? $data['Song']['ref_number'] : 'N/A'; ?>
                                                </td>
                                                <td>
                                                    <?php echo $data['Song']['written'] ? $data['Song']['written'] : 'N/A'; ?>
                                                </td>
                                                <td>
                                                    <?php echo $data['Song']['written_year'] ? $data['Song']['written_year'] : 'N/A'; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->script([
    'player/js/audioplayer',
    '//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
    'jquery.matchHeight-min',
]);
echo $this->html->css([
    '/js/player/css/audioplayer',
    '//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css']);
?>
<style>
.lyrice p {
    font-family: 'Hanuman', Fallback, sans-serif !important;
}
.f-hanuman .p1 {
    font-size: 1em !important;
}
.card-header {
    background-color: transparent !important;
}
</style>
<script>
    $(function() {
        $('audio').audioPlayer();
        $('.keyword').autocomplete({
            source: function(request, response) {
              $.ajax({
                type: 'GET',
                dataType: 'json',
                url: BASE_URL + 'songs/search',
                data: 'keyword=' + request.term,
                success: function(data) {
                  response($.map(data, function(item) {
                      var object = new Object();
                      object.label = item.label;
                      object.value = item.value;
                      return object
                  }));
                }
              });
            },
            select: function (event, ui) {
              window.location.href = BASE_URL + 'songs/view/' + ui.item.value;
             }
        });
        //Disable cut copy paste
        $('.lyrice').attr('unselectable','on')
        .css({'-moz-user-select':'-moz-none',
              '-moz-user-select':'none',
              '-o-user-select':'none',
              '-khtml-user-select':'none', /* you could also put this in a class */
              '-webkit-user-select':'none',/* and add the CSS class here instead */
              '-ms-user-select':'none',
              'user-select':'none'
        }).bind('selectstart', function(){ return false; });
    });
    
    document.addEventListener('contextmenu', function(e) {
        e.preventDefault();
    });

    $('img').on('dragstart', function(event) { event.preventDefault(); });

    $('.item').matchHeight({});
    $('.item').on('click', function() {
        window.location.href = BASE_URL + 'songs/view/' + $(this).data('id');
    });
</script>

<div class="card">
    <div class="card-body">
        <?php
        echo $this->Form->create('WorkSheet', array(
            'type' => 'get',
        )); ?>
        <div class="row">
            <div class="col-md-3 col-sm-4 col-12">
                <div class="form-group">
                    <?php
                    echo $this->Form->select('category_id', $categories, array(
                        'empty' => 'Select Category',
                        'class' => 'form-control select2',
                        'value' => $this->request->query('category_id'),
                        'label' => false,
                        'style' => 'width: 100%',
                        'required' => false,
                    )); ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-12">
                <div class="form-group">
                    <?php
                    echo $this->Form->text('search', array(
                        'class' => 'form-control',
                        'value' => $this->request->query('search'),
                        'placeholder' => __('COMMON_TXT_SEARCH'),
                    )); ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-2 col-6">
                <div class="form-group">
                    <?php
                    echo $this->Form->input('search', array(
                        'class' => 'btn btn-primary',
                        'type' => 'button',
                        'placeholder' => __('COMMON_TXT_SEARCH'),
                        'label' => false,
                    )); ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-2 col-6">
                <div class="form-group">
                    <?php
                    echo $this->Html->link('Create', array(
                        'action' => 'create',
                        'prefix' => 'admin',
                    ), array(
                        'class' => 'btn btn-primary float-right',
                        'escape' => false,
                    )); ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<div class="card">
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Discount</th>
                    <th>Sort</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $key => $value) : ?>
                    <tr>
                        <td><?php echo (($this->params['paging']['WorkSheet']['page'] * LIMIT) - LIMIT + 1) + $key; ?></td>
                        <td><?php echo h(ucfirst($value['WorkSheet']['title'])); ?></td>
                        <td><?php echo isset($value['Category']['name_kh']) ? $value['Category']['name_kh'] : '--'; ?></td>
                        <td><?php echo $value['WorkSheet']['price'] ? number_format($value['WorkSheet']['price'], 2) : '0.00'; ?></td>
                        <td><?php echo $value['WorkSheet']['discount'] ? number_format($value['WorkSheet']['discount'], 2) : '0.00'; ?></td>
                        <td><?php echo $value['WorkSheet']['sort']; ?></td>
                        <td class="project-actions text-right">
                            <?php
                            echo $this->Html->link('<i class="fas fa-pencil-alt"></i> Edit', '/admin/worksheets/edit/' . $value['WorkSheet']['id'], [
                                'class' => 'btn btn-info btn-sm',
                                'escape' => false,
                            ]); ?>

                            <?php
                            echo $this->Form->postLink('<i class="fas fa-trash"></i> Delete', '/admin/worksheets/delete/' . $value['WorkSheet']['id'], [
                                'class' => 'btn btn-danger btn-sm',
                                'escape' => false,
                                'confirm' => 'Are you sure you want to delete?',
                            ]); ?>
                        </td>
                    </tr>
                <?php
                endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <?php
        echo $this->element('/Backend/pagination'); ?>
    </div>
</div>
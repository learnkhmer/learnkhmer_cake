
<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->Form->create('Setting',
            array(
            'url' => 'update',
            'role' => 'form',
            'autocomplete' => 'off',
            'novalidate' => true,
            'enctype'=> 'multipart/form-data',
            'class' => 'form-update',
            'inputDefaults' => array(
                'legend' => false,
                'label' => false,
                'div' => false,
            )
        ));
        ?>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right ui-sortable-handle">
              <li class=""><a href="#tab_en" data-toggle="tab" aria-expanded="false">English</a></li>
              <li class="active"><a href="#tab_kh" data-toggle="tab" aria-expanded="true">Khmer</a></li>
              <li class="pull-left header"><i class="fa fa-inbox"></i> Setting</li>
            </ul>
            <div class="box-body">
                <div class="tab-content no-padding">
                    <div class="chart tab-pane active" id="tab_kh">
                        <div class="panel">
                            <?php
                            $data = json_decode($data['Setting']['lang'], TRUE);
                            foreach ($data as $key => $value) : ?>
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        Site Title<code>$this->Language->transtlate('<?php echo $key; ?>')</code>
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <?php echo $this->Input->generate($key, $value, 'kh'); ?>
                                </div>
                            <?php
                            endforeach; ?>
                        </div>
                    </div>
                    <div class="chart tab-pane" id="tab_en">
                        <div class="panel">
                            <?php
                            foreach ($data as $key1 => $value1) : ?>
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        Site Title<code>$languageShare['<?php echo $key1; ?>']</code>
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <?php echo $this->Input->generate($key1, $value1, 'eng'); ?>
                                </div>
                            <?php
                            endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-12">
                    <?php
                    echo $this->Form->button('Save Setting', array(
                        'class' => 'btn btn-primary pull-right btn-update',
                        'type' => 'submit'
                    )); ?>
                </div>
            </div>
        </div>
        <?php 
        echo $this->Form->end(); ?>
    </div>
    <?php
    echo $this->Form->create('Setting',
        array(
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'enctype'=> 'multipart/form-data',
        'class' => 'form-create',
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false,
        )
    ));
    ?>
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="col-md-4">
                    <label>Order</label>
                    <?php
                    echo $this->Form->input('title', array(
                        'type' => 'text',
                        'class' => 'form-control',
                        'placeholder' => 'Setting name ex: Admin Title',
                    )); ?>
                </div>
                <div class="col-md-4">
                    <label>Order</label>
                    <?php
                    echo $this->Form->input('key', array(
                        'type' => 'text',
                        'class' => 'form-control',
                        'placeholder' => 'Setting name ex: admin_title',
                    )); ?>
                </div>
                <div class="col-md-4">
                    <label>Type</label>
                    <div>
                        <?php
                        echo $this->Form->select('type', [
                            'input' => 'input',
                            'textarea' => 'textarea'
                        ], array(
                            'empty' => 'Choose Type',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="col-md-12">
                    <?php
                    echo $this->Form->button('Add New Setting', array(
                        'class' => 'btn btn-primary pull-right btn-save',
                        'type' => 'submit'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo $this->Form->end(); ?>
</div>
<script type="text/javascript">
$(function() {
    $('.form-create').on('submit', function(e) {
        e.preventDefault();
        var that = this;
        var formData = new FormData($(that)[0]);
        var params = {
            type: 'post',
            url: BASE_URL + 'admin/settings/save',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $.LoadingOverlay('show');
                $(that).find('.form-group').removeClass('has-error');
                $(that).find('.help-block').remove();
            }
        };
        ajax_request(params, function(data) {
            if (data.status == 0) {
                alert('Key already exists!');
            } else {
                location.reload();
            }
        });
    });
    // update
    $('.form-update').on('submit', function(e) {
        e.preventDefault();
        var that = this;
        var formData = new FormData($(that)[0]);
        var params = {
            type: 'post',
            url: BASE_URL + 'admin/settings/update',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $.LoadingOverlay('show');
                $(that).find('.form-group').removeClass('has-error');
                $(that).find('.help-block').remove();
            }
        };
        ajax_request(params, function(data) {
            location.reload();
        });
    });

    $('textarea').summernote({
        minHeight: 200,
        placeholder: 'Write here ...',
        focus: false,
        airMode: false,
    });
});
</script>
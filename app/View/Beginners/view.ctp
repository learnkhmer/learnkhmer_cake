<!-- TODO -->
<div class="row">
    <div class="col-12 col-md-6 border-right cursive-writing">

        <div class="i-image" style="display:none">
            <style>
                .text-reprsent {
                    position: absolute;
                    background: red;
                    z-index: 9999999;
                    left: 10px;
                    top: -36px;
                    font-size: 30px;
                    padding: 4px 16px;
                    color: #fff;

                }
            </style>
            <?php if ($data['Beginner']['description']) : ?>
                <div class="text-reprsent rounded rounded-circle text-reprsent">
                    <?php echo $data['Beginner']['description']; ?>
                </div>
            <?php endif; ?>
            <?php
            echo $this->Html->image(FILE_DOMAIN . PATH_BEGINNER_IMAGE . $data['Beginner']['image'], array(
                'class' => 'img-fluid image-letter',
                'alt' => $data['Beginner']['image'],
            )); ?>
        </div>

        <div class="i-drawing">
            <canvas id="tools_sketch" height="300" style="background: url(<?php echo FILE_DOMAIN . PATH_BEGINNER_IMAGE_DRAWING . $data['Beginner']['image_drawing']; ?>);"></canvas>
        </div>
        <audio class="audio-detail">
            <source src="<?php echo FILE_DOMAIN_FILES . '/beginners/sound_2/' . $data['Beginner']['sound_2']; ?>" type="audio/mpeg">
        </audio>
    </div>
    <div class="col-12 col-md-6 position-relative p-write">
        <ul class="list-unstyled btn-wrap text-center">
            <li class="toggle-drawer">
                <a href="#" class="m-btn-detail">
                    <i class="fas fa-eye"></i>
                    <?php echo __d('beginner', 'View'); ?>
                </a>
            </li>
            <li class="i-drawing">
                <a href="#tools_sketch" data-tool="eraser">
                    <i class="fas fa-eraser" ​></i>
                    <?php echo __d('beginner', 'ERASE'); ?>
                </a>
            </li>
            <li class="i-drawing">
                <a href="#tools_sketch" data-download="png">
                    <i class="fas fa-download"></i>
                    <?php echo __d('beginner', 'DOWNLOAD'); ?>
                </a>
            </li>
            <li style="visibility:hidden">
                <a href="#tools_sketch" class="btn-marker" data-tool="marker" style="color: red">
                    <span><i class="fas fa-volume-up"></i></span>
                    <?php echo __d('beginner', 'SOUND'); ?>
                </a>
            </li>
        </ul>
        <ul class="list-unstyled text-center" style="top: 60%;">
            <li style="visibility:hidden" class="btn-sound-detail">
                <a style="color: #fff;
    width: 150px;
    padding-top: 0.5rem;
    padding-bottom: 0.5rem;
    border: 3px solid #da1e8f;" class="btn btn-info m-btn-detail rounded-pill">
                    <span><i class="fas fa-volume-up"></i></span>
                    <?php echo __d('beginner', 'SOUND'); ?>
                </a>
            </li>
        </ul>
    </div>
</div>
<?php
echo $this->Html->script('sketch/sketch.min', array('blog' => false)); ?>
<script>
    $(function() {
        width = 400;
        if ($(window).width() <= 320) {
            width = 200;
        }
        $('#tools_sketch').css({
            'background-size': width,
            'background-repeat': 'round'
        });
        $('#tools_sketch').sketch({
            defaultColor: "#000",
            defaultSize: 25
        });
        $('.m-btn-audio').click(function(e) {
            var content = $('.modal').find('audio');
            $(content)[0].play();
        });

        $('.btn-sound-detail').click(function(e) {
            var content = $('.audio-detail');
            $(content)[0].play();
        });
    });
</script>
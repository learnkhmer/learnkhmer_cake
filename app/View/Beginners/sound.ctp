<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_BEGINNER'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);', array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_BEGINNER'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <div>
                        <span>
                            <i class="fas fa-assistive-listening-systems"></i>
                        </span>
                    </div>
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('beginner', 'CATEGORY'); ?></h4>
                </div>
                <div class="row pt-1">
                    <div class="col">
                        <ul class="list-unstyled cate-list mb-0" id="cat-list">
                            <li>
                                <a href="#animation"
                                data-toggle="collapse"
                                role="button"
                                aria-expanded="true"
                                aria-controls="subject">
                                    <span class="txt-cl-blue">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                                    </span>
                                    <?php echo __d('beginner', 'CATEGORYP_ANIMATION'); ?>
                                </a>
                                <?php if ($animation): ?>
                                    <ul class="pl-4 collapse list-unstyled" id="animation" data-parent="#cat-list">
                                    <?php foreach ($animation as $key => $value): ?>
                                        <li class="<?php echo $this->ActiveCategory->displayAactive($value['Category']['id']); ?>">
                                            <?php
                                            $category_animate_name = Configure::read('Config.language') === 'kh' ? $value['Category']['name_kh'] : $value['Category']['name_en'];
                                            echo $this->Html->link('<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span> ' . $category_animate_name,
                                                '/beginners/animation/'.$value['Category']['id'], array('escape' => false)); ?>
                                        </li>
                                    <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                            <li>
                                <a href="#sound"
                                data-toggle="collapse"
                                role="button"
                                aria-expanded="false"
                                aria-controls="sport"
                                class="collapsed">
                                    <span class="txt-cl-blue">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                                    </span>
                                    <?php echo __d('beginner', 'CATEGORYP_SOUND'); ?>
                                </a>
                                <?php if ($sound): ?>
                                    <ul class="pl-4 collapse show list-unstyled" id="sound" data-parent="#cat-list">
                                    <?php foreach ($sound as $key => $value): ?>
                                        <li class="<?php echo $this->ActiveCategory->displayAactive($value['Category']['id']); ?>">
                                        <?php
                                            $category_sound_name = Configure::read('Config.language') === 'kh' ? $value['Category']['name_kh'] : $value['Category']['name_en'];
                                            echo $this->Html->link('<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span> ' . $category_sound_name,
                                                '/beginners/sound/'.$value['Category']['id'], array('escape' => false)); ?>
                                        </li>
                                    <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row my-3">
                    <div class="col">
                        <button class="btn btn-block bg-gray d-over" disabled><?php echo __d('beginner', 'CATEGORYP_Q_A'); ?></button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9 border-blue entry-content">
                <div class="col b-bottom-blue txt-blue px-0">
                    <?php
                    $CATEGORY_TITLE = '';
                    $category_name = Configure::read('Config.language') === 'kh' ? $category['Category']['name_kh'] : $category['Category']['name_en'] ;
                    if ($category['Category']['description']) {
                        $CATEGORY_TITLE = __d('beginner', 'DESCRIPTION_ABOUT').' '.$category_name . '<span><i class="fas fa-angle-up"></i></span>';
                    } else {
                        $CATEGORY_TITLE = __d('beginner', 'DESCRIPTION_ABOUT').' '.$category_name;
                    }
                    echo $this->Html->link($CATEGORY_TITLE, '#collapseVowel', array(
                        'class' => 'f-koulen vowel-info txt-blue',
                        'data-toggle' => 'collapse',
                        'role' => 'button',
                        'aria-expanded' => 'false',
                        'escape' => false,
                    )); ?>
                    <?php
                    if (!empty($category['Category']['description'])) : ?>
                    <div class="collapse" id="collapseVowel">
                        <p class="border-top mb-0 py-3">
                            <?php echo nl2br($category['Category']['description']); ?>
                        </p>
                    </div>
                    <?php
                    endif; ?>
                </div>
                <div class="letter-list pb-3">
                    <?php if ($data): ?>
                        <div class="row">
                            <?php foreach ($data as $key => $value) : ?>
                                <div class="col-6 col-md-3">
                                    <div class="letter-wrap">
                                        <?php
                                        echo $this->Html->image(FILE_DOMAIN . PATH_BEGINNER_THUMBNAIL . $value['Beginner']['image_thumbnail'], array(
                                            'alt' => 'Letter Kor',
                                            'class' => 'img-fluid',
                                        )); ?>
                                        <ul class="list-unstyled d-flex">
                                            <li class="list-inline-item px-2 w-100">
                                                <?php
                                                echo $this->Form->button('<span><i class="fas fa-volume-up"></i></span> '.__d('beginner', 'SOUND'), array(
                                                    'type' => 'button',
                                                    'class' => 'bg-d-green btn btn-sm btn-audio btn-block',
                                                    'data-toggle' => 'modal',
                                                    'escape' => false,
                                                )); ?>
                                            </li>
                                        </ul>
                                        <audio>
                                            <source src="<?php echo FILE_DOMAIN . PATH_BEGINNER_SOUND . $value['Beginner']['sound']; ?>" type="audio/mpeg">
                                        </audio>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Html->script('f/beginner/sound', array(
    'inline' => false,
));

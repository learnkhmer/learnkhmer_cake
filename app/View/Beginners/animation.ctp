<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_BEGINNER'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);', array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_BEGINNER'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <div>
                        <span>
                            <i class="fas fa-assistive-listening-systems"></i>
                        </span>
                    </div>
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('beginner', 'CATEGORY'); ?></h4>
                </div>
                <div class="row pt-1">
                    <div class="col">
                        <ul class="list-unstyled cate-list mb-0" id="cat-list">
                            <li>
                                <a href="#animation" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="subject">
                                    <span class="txt-cl-blue">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                                    </span>
                                    <?php echo __d('beginner', 'CATEGORYP_ANIMATION'); ?>
                                </a>
                                <?php if ($animation) : ?>
                                    <ul class="pl-4 collapse show list-unstyled" id="animation" data-parent="#cat-list">
                                        <?php foreach ($animation as $key => $value) : ?>
                                            <li class="<?php echo $this->ActiveCategory->displayAactive($value['Category']['id']); ?>">
                                                <?php
                                                $category_animate_name = Configure::read('Config.language') === 'kh' ? $value['Category']['name_kh'] : $value['Category']['name_en'];
                                                echo $this->Html->link(
                                                    '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span> ' . $category_animate_name,
                                                    '/beginners/animation/' . $value['Category']['id'],
                                                    array('escape' => false)
                                                ); ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                            <li>
                                <a href="#sound" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sport" class="collapsed">
                                    <span class="txt-cl-blue">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                                    </span>
                                    <?php echo __d('beginner', 'CATEGORYP_SOUND'); ?>
                                </a>
                                <?php if ($sound) : ?>
                                    <ul class="pl-4 collapse list-unstyled" id="sound" data-parent="#cat-list">
                                        <?php foreach ($sound as $key => $value) : ?>
                                            <li class="<?php echo $this->ActiveCategory->displayAactive($value['Category']['id']); ?>">
                                                <?php
                                                $category_sound_name = Configure::read('Config.language') === 'kh' ? $value['Category']['name_kh'] : $value['Category']['name_en'];
                                                echo $this->Html->link(
                                                    '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span> ' . $category_sound_name,
                                                    '/beginners/sound/' . $value['Category']['id'],
                                                    array('escape' => false)
                                                ); ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row my-3">
                    <div class="col">
                        <button class="btn btn-block bg-gray d-over" disabled><?php echo __d('beginner', 'CATEGORYP_Q_A'); ?></button>
                    </div>
                </div>
                <div class="vdo-wrap">
                    <div class="col">
                        <h4 class="f-koulen text-center txt-blue"><?php echo __d('beginner', 'VIDEO'); ?></h4>
                        <ul class="vdo-list list-unstyled">
                            <li class="vdo-item d-flex">
                                <div class="youtube-ico"></div>
                                <a data-fancybox href="https://www.youtube.com/watch?v=PzpAbZ8tUxs" class="txt-blue"><?php echo __d('beginner', 'Introduction'); ?></a>
                            </li>
                            <li class="vdo-item d-flex" style="margin-bottom: .5rem;">
                                <div class="youtube-ico"></div>
                                <a data-fancybox href="https://www.youtube.com/watch?v=yyuXYmYCjh8" class="txt-blue"><?php echo __d('beginner', 'Read along'); ?></a>
                            </li>
                            <li class="vdo-item d-flex">
                                <div class="youtube-ico"></div>
                                <a data-fancybox href="https://www.youtube.com/watch?v=w8hUw0p15wg" class="txt-blue"><?php echo __d('beginner', 'read along with picture'); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9 border-blue entry-content">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header p-0" id="headingOne">
                            <h5 class="mb-0">
                                <?php
                                $category_name = Configure::read('Config.language') === 'kh' ? $category['Category']['name_kh'] : $category['Category']['name_en'];
                                if ($category['Category']['description']) {
                                    $category_name = '<i class="fa fa-angle-right"></i> '.__d('beginner', 'DESCRIPTION_ABOUT') . ' ' . $category_name;
                                } else {
                                    $category_name = __d('beginner', 'DESCRIPTION_ABOUT') . ' ' . $category_name;
                                } ?>
                                <button class="btn btn-link" data-toggle="collapse" data-target="#chord_image"><?php echo $category_name; ?></button>
                            </h5>
                        </div>
                        <?php
                        if ($category['Category']['description']) : ?>
                        <div id="chord_image" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                <?php echo nl2br($category['Category']['description']); ?>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                
                <div class="letter-list pb-3">
                    <?php if ($data) : ?>
                        <div class="row">
                            <?php foreach ($data as $key => $value) : ?>
                                <div class="col-6 col-md-3">
                                    <div class="letter-wrap">
                                        <?php
                                        echo $this->Html->image(FILE_DOMAIN . PATH_BEGINNER_THUMBNAIL . $value['Beginner']['image_thumbnail'], array(
                                            'alt' => 'Letter Kor',
                                            'class' => 'img-fluid',
                                        )); ?>
                                        <ul class="list-unstyled d-flex">
                                            <li class="list-inline-item">
                                                <?php
                                                echo $this->Form->button('<span><i class="fas fa-eye"></i></span> ' . __d('beginner', 'WRITE'), array(
                                                    'type' => 'button',
                                                    'class' => 'btn bg-sea btn-sm writing-popup',
                                                    'data-id' => $value['Beginner']['id'],
                                                    'escape' => false,
                                                )); ?>
                                            </li>
                                            <li class="list-inline-item">
                                                <?php
                                                echo $this->Form->button('<span><i class="fas fa-volume-up"></i></span> ' . __d('beginner', 'SOUND'), array(
                                                    'type' => 'button',
                                                    'class' => 'bg-d-green btn btn-sm btn-audio',
                                                    'data-toggle' => 'modal',
                                                    'escape' => false,
                                                )); ?>
                                            </li>
                                        </ul>
                                        <audio>
                                            <source src="<?php echo FILE_DOMAIN . PATH_BEGINNER_SOUND . $value['Beginner']['sound']; ?>" type="audio/mpeg">
                                        </audio>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // Drawing Modal 
?>
<div class="modal fade const-modal" id="modal-detail" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom-0 position-relative">
                <div class="close-modal" data-dismiss="modal" aria-label="Close">
                    <span><i class="fas fa-times-circle"></i></span>
                </div>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<?php
echo $this->Html->css('/js/fancybox/jquery.fancybox.min');
echo $this->Html->script(array(
    'sketch/sketch.min.js',
    'fancybox/jquery.fancybox.min',
    'f/beginner/animation',
), array('inline' => false));
?>
<style>
.card-header {
    background-color: transparent !important;
}
</style>
<script>
    $(document).ready(function() {
        $('body').on('click', '.toggle-drawer', function() {
            var $el = $(this).toggleClass('show-i-image');
            if ($el.hasClass('show-i-image')) {
                $('.i-image').show();
                $('.i-drawing').hide();

                $el.find('a').html('<i class="fas fa-backward"></i> Back');
                $('.btn-sound-detail').css({
                    'visibility': 'visible'
                });
            } else {
                $('.i-image').hide();
                $('.i-drawing').show();

                $('.btn-sound-detail').css({
                    'visibility': 'hidden'
                });

                $el.find('a').html('<i class="fas fa-eye"></i> View');
            }
        });
    });
</script>

<div class="row">
<div class="col-sm-3">
            <select class="form-control category_" name="category_id">
                <?php
                
                foreach ($categories as $key => $value) { ?>
                    <option <?php echo ($this->request->query('category_id') == $value['Category']['id']) ? 'selected' : '' ?> value="<?php echo $value['Category']['id']; ?>"><?php echo $value['Category']['name_kh']; ?></option>

                    <?php 
                } ?> 
            </select>
                
                </div>
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
              <i class="fa fa-edit"></i>
                <?php 
                echo $this->Html->link('Create', '/admin/beginners/create?type='.CATEGORY_BEGINNER_TYPE_ANIMATION, [
                    'class' => 'box-title',
                ]); ?>
            </div>
            <div class="box-body">
                <table class="table table-bordered data-list">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo __d('beginner', 'TXT_TITLE'); ?></th>
                            <th><?php echo __d('beginner', 'CATEGORY_TYPE'); ?></th>
                            <th><?php echo __d('beginner', 'TXT_DESCRIPTION'); ?></th>
                            <th><?php echo __('COMMON_TXT_OPERATION'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($data as $key => $value): ?>
                        <tr>
                            <td><?php echo ++$key; ?></td>
                            <td><?php echo $value['Beginner']['title']; ?></td>
                            <td>
                                <?php
                                if (isset($value['Category'])) {
                                    echo $value['Category']['name_kh'];
                                }
                                ?>
                            </td>
                            <td><?php echo $value['Beginner']['description']; ?></td>
                            <td>
                                
                                <?php
                                echo $this->Html->link('<i class="fa fa-edit"></i>'.__d('beginner', 'COMMON_TXT_EDIT'),
                                    [
                                        'action' => 'edit',
                                        $value['Beginner']['id'],
                                        '?' => array(
                                            'type' => CATEGORY_BEGINNER_TYPE_ANIMATION,
                                        )
                                    ], [
                                        'escape' => false,
                                    ]
                                );
                                ?>
                                <?php
                                echo $this->Html->link('<i class="fa fa-trash"></i>'.('Delete '),
                                    array(
                                        'action' => 'delete',
                                        $value['Beginner']['id'],
                                        '?' => array('type' => CATEGORY_BEGINNER_TYPE_ANIMATION)
                                    ), array(
                                        'confirm' => 'Are you sure you wish to delete this recipe?',
                                        'data-id' => $value['Beginner']['id'],
                                        'escape' => false,
                                    )
                                );
                                ?>
                            </td>
                        </tr>
                    <?php
                    endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
  $(function () {
    $('.data-list').DataTable();

    $('.category_').on('change', function() {
        var that = this;
        location.href = BASE_URL + 'admin/beginners/animation?category_id=' + $(that).val();
    })
  });
</script>
<?php
if ($this->request->query('type') === CATEGORY_BEGINNER_TYPE_SOUND) {
    echo $this->element('/Form/beginner_sound');
} else {
    echo $this->element('/Form/beginner_animate');
}

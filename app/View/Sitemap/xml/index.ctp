<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
    <url>
        <loc><?php echo $this->Html->url('/', true); ?></loc>
    </url>
    <url>
        <loc><?php echo $this->Html->url('/beginners/animation/1', true); ?></loc>
    </url>
    <url>
        <loc><?php echo $this->Html->url('/work_sheets/type/13', true); ?></loc>
    </url>
    <url>
        <loc><?php echo $this->Html->url('/readings', true); ?></loc>
    </url>
    <url>
        <loc><?php echo $this->Html->url('/contacts', true); ?></loc>
    </url>
    <url>
        <loc><?php echo $this->Html->url('/pages/about', true); ?></loc>
    </url>
    <url>
        <loc><?php echo $this->Html->url('/pages/report', true); ?></loc>
    </url>
</urlset>

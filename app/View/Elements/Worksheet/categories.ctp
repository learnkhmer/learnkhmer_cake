
<div class="row model-beginner content-sub-category">
    <div class="col-md-3 category">
       <!-- Sound -->
        <h2 class="category">Work Sheet</h2>
        <ul class="list-unstyled category fa-ul">
        <?php
        foreach ($categories as $key => $value) : ?>
            <li>
                <?php echo $this->Html->link($value['Category']['name_en'], '/beginners/sound/'.$value['Category']['id']); ?>
            </li>
        <?php
          endforeach; ?>
        </ul>
    </div>
    
</div>
<script>
    document.addEventListener('touchstart', function(){}, true);
</script>
<?php
echo $this->html->css(array('sass-compiled'), array('inline' => false));

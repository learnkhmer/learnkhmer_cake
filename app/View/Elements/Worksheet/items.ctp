
<div class="row model-beginner">
    <div class="col-md-3 category">
        <?php // categories ?>
        <?php echo $this->element('Front/categories'); ?>
    </div>
    <div class="col-md-9 sub-category">
        <?php if ($data) : ?>
        <h2 class=""><?php echo $data[0]['Post']['associate']['Category']['name_en']; ?></h2>
        <div class="row model-beginner">
            <div class="col-md-4 col-md-offset-8 pdb20">
                <?php
                if (isset($data[0]['Post']['image1'])) {
                    $path = pathinfo($data[0]['Post']['image1']);
                    $full_path = substr($path['dirname'], strrpos($path['dirname'], 'files'));
                    echo $this->Html->link('<i class="fa fa-download"></i> Download all', '/work_sheets/download/'.$data[0]['Post']['associate']['Category']['id'].'?get=all', array(
                        'class' => 'btn btn-info btn-block',
                        'escape' => false,
                    ));
                }
                ?>
            </div>
        </div>
        <?php
        foreach ($data as $key => $value) :
        ?>
        <div class="col-md-4 col-xs-12">
            <div class="pic">
            <?php
            $image = $this->Html->image(DS.PATH_WORKSHEET.'post/'.$value['Post']['image1'], array(
                'class' => '',
                'onError' => 'this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);'
            ));
            $title = $this->Html->tag('h1', $value['Post']['title'], array('class' => 'pic-title'));
            $download = $this->Html->tag('p', '<i class="fa fa-download"></i> Download', array(
                'escape' => false,
                'class' =>  'btn btn-default btn-block open-link',
                'data-link' => $this->Html->url('/work_sheets/download/'.$value['Post']['id']),
            ));
            $view = $this->Html->tag('p', '<i class="fa fa-eye"></i> View More', array(
                'escape' => false,
                'class' =>  'btn btn-info btn-block  open-link',
                'data-link' => $this->Html->url('/work_sheets/view/'.$value['Post']['id']),
            ));
            $span = $this->Html->tag('span', $title.$download, array('class' => 'pic-caption94 come-right'));
            echo $this->Html->link($image.$span, 'javascript: void(0);', array(
                'class' => 'thumbnail '.($this->request->query('id') === $value['Post']['id'] ? 'menu-active' : ''),
                'escape' => false,
            ));
            ?>
        </div>
              </div>
        <?php
        endforeach;
            endif; ?>
    </div>
</div>
<script>
    $(function() {
        $('.open-link').click(function() {
            var link = $(this).data('link');
            window.location.href = link;
        });
    });
</script>
<?php
echo $this->html->css(array('sass-compiled'), array('inline' => false));

<!-- Remove -->
<div id="small_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <h4 class="text-center" style="padding-top:15px;"> <?php echo MESSAGE_CONFIRM_DELETE; ?> </h4>

            <?php if ($this->request->controller === 'users') : ?>
            <div class="modal-body with-padding">
                <div class="text-center">
                    <label>
                        <input type="checkbox" name="delete" value="physical" data-remove-type="physical" />
                        Delete forever
                    </label>
                </div>
            </div>
            <?php endif; ?>
            <div class="modal-footer">
                <input type="hidden" class="delete-id"/>
                <button class="btn btn-primary btn-delete col-sm-5" data-url=""><?php echo LABEL_YES; ?></button>
                <button class="btn btn-warning btn-close col-sm-5 pull-right"><?php echo LABEL_NO; ?></button>
            </div>
        </div>
    </div>
</div>
<!-- /Remove-->
<script type="text/javascript">
    $(".delete").on("click", function() {
        var id = $(this).data('href');
        $('.delete-id').val(id);
        $('#small_modal').modal('show');
    });
    $('.btn-close').click(function() {
        $('#small_modal').modal('hide');
    });
    $('.btn-delete').click(function() {
        var id = $('.delete-id').val();
        var del_type = $('input[name=delete]:checked').val();
        if (del_type === 'physical') {
            if (false === confirm('<?php echo MESSAGE_CONFIRM_DELETE; ?>')) {
                return false;
            }
        }
        $('.btn-delete').text($('.btn-delete').text() + '...');
        if (del_type === undefined || del_type === '') {
            del_type = 'logical';
        }
        $.ajax({
            url: '<?php echo $this->Html->url(array('action' => 'delete')); ?>/' + id,
            dataType: 'json',
            data: { del_type: del_type },
            success:function(respond) {
                if (respond) {
                    $('.row_id' + id).css({background: 'pink'}).fadeOut('slow');
                    $('.btn-delete').text('<?php echo LABEL_DELETE; ?>');
                    $('#small_modal').modal('hide');
                }
            }
        });
    });
</script>

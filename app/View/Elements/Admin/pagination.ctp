<ul class="pagination pagination-lg justify-content-center pt-3">
    <?php
    echo $this->Paginator->first(__('PAGINATION_TXT_FIRST'), array(
        'tag' => 'li',
        ),
        null,
        array(
            'tag' => 'li',
            'class' => 'disabled',
            'disabledTag' => 'a',
        )
    );
    echo $this->Paginator->prev(__('PAGINATION_TXT_PREVIOUS'), array(
        'tag' => 'li',
        ),
        null,
        array(
            'tag' => 'li',
            'class' => 'disabled',
            'disabledTag' => 'a',
        )
    );
    echo $this->Paginator->numbers(array(
        'separator' => '',
        'currentTag' => 'a',
        'currentClass' => 'active',
        'tag' => 'li',
        'first' => 1,
        )
    );
    echo $this->Paginator->next(__('PAGINATION_TXT_NEXT'), array(
        'tag' => 'li',
        'currentClass' => 'disabled',
        ),
        null,
        array(
            'tag' => 'li',
            'class' => 'disabled',
            'disabledTag' => 'a',
        )
    );
    echo $this->Paginator->last(__('PAGINATION_TXT_LAST'), array(
        'tag' => 'li',
        'currentClass' => 'disabled',
        ),
        null,
        array(
            'tag' => 'li',
            'class' => 'disabled',
            'disabledTag' => 'a',
        )
    );
    ?>
</ul>
<script>
    $(function() {
        $('.pagination').find('li').addClass('page-item').find('a').addClass('page-link');
    });
</script>

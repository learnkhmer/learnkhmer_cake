<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="http://learnkhmer.online/"><?php echo SERVICE_NAME; ?></a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 1.0.0
    </div>
</footer>
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <?php echo $this->Html->link(
                'Profile', '/admin/dashboard',
                array(
                    'escape' => false,
                    'class' => 'nav-link '. ($this->request->controller === 'dashboard' ? 'active' : ''),
                )
            ); ?>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <?php echo $this->Html->link(
                '<i class="fas fa-sign-in-alt"></i>',
                '/admin/users/logout',
                array(
                    'escape' => false,
                    'class' => 'nav-link',
                )
            ); ?>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-controlsidebar-slide="true" href="#" role="button">
                <i class="fas fa-th-large"></i>
            </a>
        </li>
    </ul>
</nav>
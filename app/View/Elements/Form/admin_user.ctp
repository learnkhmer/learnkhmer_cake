
<?php
    echo $this->Form->create('User',
        array(
        'class' => 'form-horizontal',
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false)
    ));
?>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo __('E-mail'); ?> : </label>
                <div class="col-sm-9">
                    <?php
                    echo $this->Form->input('email', array(
                        'type' => 'email',
                        'class' => 'form-control',
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo __('Password'); ?> : </label>
                <div class="col-sm-9">
                    <?php
                    echo $this->Form->input('password', array(
                        'class' => 'form-control',
                        'type' => 'password',
                        'required' => false,
                    ));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"> </label>
                <div class="col-sm-9">
                <?php
                    $btn_name = $this->request->action == "admin_create" ? __(LABEL_CREATE) : __(LABEL_UPDATE);
                    echo $this->Form->button($btn_name,
                    array('class' => 'btn btn-primary pull-left', 'type' => 'submit'));
                ?>
                </div>
            </div>
        </div>
    </div>
<?php echo $this->Form->end();

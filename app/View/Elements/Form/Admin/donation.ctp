<?php
echo $this->Form->create(
    'Donation',
    array(
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'enctype' => 'multipart/form-data',
        'class' => 'form-create',
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false,
        )
    )
);
echo $this->Form->hidden('id');
?>
<div class="row">
    <div class="col-md-6 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
            <div class="form-group">
                    <label>Name</label>
                    <div>
                        <?php
                        echo $this->Form->input('name', array(
                            'type' => 'text',
                            'placeholder' => 'Name',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Photo</label>
                    <div class="custom-file">
                       <?php
                        $photo = isset($this->request->data['Donation']['photo']) ? $this->request->data['Donation']['photo'] : 'Choose File';
                        echo $this->Form->input('tmp_photo', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_photo',
                            'accept' => 'image/*'
                        )); ?>
                        <label class="custom-file-label" for="tmp_photo"><?php echo $photo; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Amount</label>
                    <div>
                        <?php
                        echo $this->Form->input('amount', array(
                            'type' => 'number',
                            'placeholder' => 'Amount',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Purpose</label>
                    <div>
                        <?php
                        echo $this->Form->input('purpose', array(
                            'type' => 'text',
                            'placeholder' => 'Purpose',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <div>
                        <?php
                        echo $this->Form->input('description', array(
                            'class' => 'form-control lyrice summernote',
                            'type' => 'textarea',
                            'placeholder' => 'Descritpion',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php
                    $label_action = $this->request->action === 'admin_create' ? 'Create' : 'Update';
                    echo $this->Form->button($label_action, array(
                        'class' => 'btn btn-primary float-right',
                        'type' => 'submit'
                    )); ?>

                    <?php
                    echo $this->Html->link('Back', 'index', array(
                        'class' => 'btn btn-danger float-right mr-2',
                        'type' => 'submit'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Form->end(); ?>
<script>
    $(function() {
        $('.form-create').on('submit', function(e) {
            e.preventDefault();
            var that = this;
            var formData = new FormData($(that)[0]);
            var params = {
                type: 'post',
                url: base_url + 'donations/save',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                    $(that).find('.form-control').removeClass('is-invalid');
                }
            };
            $.ajax(params).done(function(data){
                let json_parse = JSON.parse(data);
                if (json_parse.status == 0) {
                    $.each(json_parse.data, function(index, value){
                        $(that).find('[name="data[Donation][' + index +']"]').addClass('is-invalid')
                    });
                    toastr.error('Data cannot save.')
                } else {
                    toastr.success('Data has been saved successfully.');
                    setTimeout(function() {
                        location.href = base_url + 'donations';
                    }, 1000);
                }
                $.LoadingOverlay('hide');
            });
        });
    });
</script>
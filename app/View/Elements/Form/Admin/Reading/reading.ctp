
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">
        <?php
        if ($this->request->action === 'admin_create') {
            echo __d('reading', 'TXT_REGISTER_FORM');
        } else {
            echo __d('reading', 'TXT_EDIT_FORM');
        } ?>
        </h3>
    </div>
    <?php
    echo $this->Form->create('Reading',
        array(
        'class' => 'form-horizontal',
        'id' => 'reading-form',
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false,
        )
    ));
    if (isset($this->request->data['Reading']['id'])) {
        echo $this->Form->hidden('id');
    }
    ?>
    <div class="box-body">
        <div class="col-md-8">
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    Level Title
                </label>
                <div class="col-sm-9">
                    <?php
                    echo $this->Form->input('level_title', array(
                        'type' => 'text',
                        'class' => 'form-control',
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('reading', 'TXT_TILE'); ?>
                </label>
                <div class="col-sm-9">
                    <?php
                    echo $this->Form->input('title', array(
                        'type' => 'text',
                        'placeholder' => __d('reading', 'TXT_ENTER_TILE'),
                        'class' => 'form-control',
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('reading', 'TXT_LEVEL'); ?>
                </label>
                <div class="col-sm-9">
                    <?php
                    echo $this->Form->input('level', array(
                        'type' => 'number',
                        'placeholder' => __d('reading', 'TXT_ENTER_LEVEL'),
                        'class' => 'form-control',
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('reading', 'TXT_PAGE'); ?>
                </label>
                <div class="col-sm-9">
                    <?php
                    echo $this->Form->input('page', array(
                        'type' => 'number',
                        'placeholder' => __d('reading', 'TXT_ENTER_PAGE'),
                        'class' => 'form-control',
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('reading', 'TXT_SOUND'); ?>:
                </label>
                <div class="col-sm-9">
                    <div class="audio-wrap w-100">
                        <div id="p-play" class="pull-left">
                            <audio controls class="pull-left">
                                <?php
                                $url = '';
                                if(isset($this->request->data['Reading']['sound'])) {
                                    $url = FILE_DOMAIN_FILES . '/readings/sound/' . $this->request->data['Reading']['sound'];
                                }
                                ?>
                                <source src="<?php echo $url; ?>" type="audio/mp3">
                            </audio>
                        </div>
                        <?php
                        $btn_upload = __d('reading', 'COMMON_TXT_UPLOAD');
                        echo $this->Form->button('<i class="fa fa-upload" aria-hidden="true"></i> ' . $btn_upload, array(
                            'type' => 'button',
                            'class' => 'btn btn-primary btn-flat btn-upload pull-right',
                            'escape' => false,
                        )); ?>
                        <?php
                        echo $this->Form->input('sound_file', array(
                            'type' => 'file',
                            'id' => 'sound-file',
                            'class' => 'hide',
                        ));
                        echo $this->Form->hidden('sound'); ?>
                        <div class="clarfix"></div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">&nbsp;</label>
                <div class="col-sm-9">
                    <?php
                    $LABEL_NAME = $this->request->action === 'admin_create' ? __('COMMON_TXT_SAVE') : __('COMMON_TXT_EDIT');
                    echo $this->Form->button($LABEL_NAME, array(
                        'class' => 'btn-save btn btn-primary pull-left btn-flat',
                        'type' => 'button',
                    )); ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo $this->Form->end(); ?>
</div>
<?php
echo $this->Html->script('/admin/js/upload-preview', array('inline' => false)); ?>
<script>
    $(function(e) {
        $('.btn-upload').click(function(e) {
            $('#sound-file').click();
            var content = $(this).closest('.audio-wrap');
            $(content).find('input[type="hidden"]').val('<?= STATUS_UPDATE ?>');
            $.uploadPreview({
                input_field: "#sound-file",
                preview_box: "#p-play",
              });
        });

        $('.btn-save').click(function(e) {
            var form = $('#reading-form');
            var formData = new FormData(form[0]);
            var params = {
                type: 'post',
                url: BASE_URL + 'admin/readings/saveAndUpdate',
                processData: false,
                contentType: false,
                data: formData,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                    $(form).find('.form-group').removeClass('has-error');
                    $(form).find('.help-block').remove();
                }
            };
            ajax_request(params, function(data) {
                if (data === '' && data === 'undefined') {
                    return false;
                }
                if (data.status == 0) {
                    $.each(data.data, function(index, value){
                        var msg = '<span class="help-block">' + value[0] + '</span>';
                        var content = $(form).find('[name="data[Reading][' + index +']"]');
                        $(content).closest('.form-group').addClass('has-error');
                        $(content).parent('div').append(msg);
                    });
                } else {
                    location.href = BASE_URL + 'admin/readings';
                }
            });
        });
    });
</script>

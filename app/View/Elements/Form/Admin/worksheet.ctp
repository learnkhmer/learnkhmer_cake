<?php
echo $this->Form->create(
    'WorkSheet',
    array(
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'enctype' => 'multipart/form-data',
        'class' => 'form-create',
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false,
        )
    )
);
echo $this->Form->hidden('id');
?>
<div class="row">
    <div class="col-md-6 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
            <div class="form-group">
                    <label>Title</label>
                    <div>
                        <?php
                        echo $this->Form->input('title', array(
                            'type' => 'text',
                            'placeholder' => 'Title',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Price</label>
                    <div>
                        <?php
                        echo $this->Form->input('price', array(
                            'type' => 'number',
                            'placeholder' => 'Price',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Discount</label>
                    <div>
                        <?php
                        echo $this->Form->input('discount', array(
                            'type' => 'number',
                            'placeholder' => 'Discount',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Sort</label>
                    <div>
                        <?php
                        echo $this->Form->input('sort', array(
                            'type' => 'number',
                            'placeholder' => 'Sort',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <div>
                        <?php
                        echo $this->Form->input('description', array(
                            'class' => 'form-control summernote',
                            'type' => 'textarea',
                            'placeholder' => 'Descritpion',
                        )); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>Category</label>
                    <div>
                        <?php
                        echo $this->Form->select('category_id', $categories, array(
                            'empty' => 'Select',
                            'class' => 'form-control select2',
                            'style' => 'width: 100%',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Thumbnail</label>
                    <div class="custom-file">
                       <?php
                        $thumbnail = isset($this->request->data['WorkSheet']['thumbnail']) ? $this->request->data['WorkSheet']['thumbnail'] : 'Choose File';
                        echo $this->Form->input('tmp_thumbnail', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_thumbnail',
                            'accept' => 'image/*'
                        )); ?>
                        <label class="custom-file-label" for="tmp_thumbnail"><?php echo $thumbnail; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label>File</label>
                    <div class="custom-file">
                       <?php
                        $file = isset($this->request->data['WorkSheet']['file']) ? $this->request->data['WorkSheet']['file'] : 'Choose File';
                        echo $this->Form->input('tmp_file', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_file',
                            'accept' => 'image/*'
                        )); ?>
                        <label class="custom-file-label" for="tmp_file"><?php echo $file; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <?php
                    $label_action = $this->request->action === 'admin_create' ? 'Create' : 'Update';
                    echo $this->Form->button($label_action, array(
                        'class' => 'btn btn-primary float-right',
                        'type' => 'submit'
                    )); ?>

                    <?php
                    echo $this->Html->link('Back', 'index', array(
                        'class' => 'btn btn-danger float-right mr-2',
                        'type' => 'submit'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Form->end(); ?>
<script>
    $(function() {
        $('.form-create').on('submit', function(e) {
            e.preventDefault();
            var that = this;
            var formData = new FormData($(that)[0]);
            var params = {
                type: 'post',
                url: base_url + 'work_sheets/save',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                    $(that).find('.form-control').removeClass('is-invalid');
                }
            };
            $.ajax(params).done(function(data){
                let json_parse = JSON.parse(data);
                if (json_parse.status == 0) {
                    $.each(json_parse.data, function(index, value) {
                        $(that).find('[name="data[WorkSheet][' + index +']"]').addClass('is-invalid');
                        $(that).find('[name="data[WorkSheet][tmp_' + index +']"]').addClass('is-invalid');
                    });
                    toastr.error('Data cannot save.')
                } else {
                    toastr.success('Data has been saved successfully.');
                    setTimeout(function() {
                        location.href = base_url + 'work_sheets';
                    }, 1000);
                }
                $.LoadingOverlay('hide');
            });
        });
    });
</script>
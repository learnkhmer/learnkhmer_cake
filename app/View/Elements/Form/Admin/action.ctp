<div class="card col-6">
    <div class="card-body">
        <?php
        echo $this->Form->create(
            'Action',
            array(
                'class' => 'form-horizontal',
                'role' => 'form',
                'autocomplete' => 'off',
                'novalidate' => true,
                'inputDefaults' => array(
                    'legend' => false,
                    'label' => false,
                    'div' => false,
                )
            )
        );
        ?>
        <?php if ($this->request->action === 'admin_create') : ?>
            <div class="form-group">
                <label>Parent</label>
                <div>
                    <?php
                    echo $this->Form->text('', array(
                        'value' => ucfirst($module['Module']['name']),
                        'placeholder' => 'Name',
                        'class' => 'form-control',
                        'disabled' => true,
                    )); ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <label>Name</label>
            <div>
                <?php
                echo $this->Form->text('name', array(
                    'placeholder' => 'Name',
                    'class' => 'form-control',
                )); ?>
            </div>
        </div>
        <div class="form-group">
            <label>URL</label>
            <div>
                <?php
                echo $this->Form->text('url', array(
                    'class' => 'form-control',
                    'placeholder' => 'URL',
                )); ?>
            </div>
        </div>
        <div class="form-group">
            <label>Icon</label>
            <div>
                <?php
                echo $this->Form->text('icon', array(
                    'class' => 'form-control',
                    'placeholder' => 'Icon',
                    'value' => 'nav-icon fas fa-tachometer-alt',
                )); ?>
            </div>
        </div>
        <div class="form-group">
            <label>Is Menu</label>
            <div>
                <input type="checkbox" name="data[Action][is_menu]" data-bootstrap-switch <?php echo isset($this->request->data['Action']['is_menu']) && $this->request->data['Action']['is_menu'] ? 'checked' : ''; ?>>
            </div>
        </div>
        <div class="form-group">
            <?php
            $LABEL_NAME = $this->request->action === 'admin_create' ? 'Create' : __('COMMON_TXT_EDIT');
            echo $this->Form->button($LABEL_NAME, array(
                'class' => 'btn btn-primary float-right',
                'type' => 'submit'
            )); ?>

            <?php
            echo $this->Html->link('Back', 'index', array(
                'class' => 'btn btn-danger float-right mr-2',
                'type' => 'submit'
            )); ?>
        </div>
        <?php
        echo $this->Form->end(); ?>
    </div>
</div>
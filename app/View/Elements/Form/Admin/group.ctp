<div class="card col-8">
    <div class="card-body">
        <?php
        echo $this->Form->create(
            'Group',
            array(
                'class' => 'form-horizontal',
                'role' => 'form',
                'autocomplete' => 'off',
                'novalidate' => true,
                'inputDefaults' => array(
                    'legend' => false,
                    'label' => false,
                    'div' => false,
                )
            )
        );
        ?>
        <div class="form-group">
            <label>Name</label>
            <div>
                <?php
                echo $this->Form->text('name', array(
                    'placeholder' => 'Name',
                    'class' => 'form-control col-12 col-sm-6',
                )); ?>
            </div>
        </div>
        <div class="form-group">
            <label>Assign Permission</label>
            <div class="form-module">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Module</th>
                            <th>Access Permission</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="customCheckboxCheckAll">
                                    <label for="customCheckboxCheckAll" class="custom-control-label"> Check / Uncheck all</label>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $permissions = $permissions ? $permissions : [];
                        if ($modules) :
                            foreach ($modules as $key => $value) : ?>
                                <?php
                                if (!$value['Action']) {
                                    continue;
                                } ?>
                                <tr>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input module_check" type="checkbox" id="moduleCheckbox<?php echo $value['Module']['id']; ?>">
                                            <label for="moduleCheckbox<?php echo $value['Module']['id']; ?>" class="custom-control-label"><?php echo ucfirst($value['Module']['name']); ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <?php
                                            foreach ($value['Action'] as $key2 => $value2) : ?>
                                                <div class="col-3">
                                                    <div class="custom-control custom-checkbox">
                                                        <input class="custom-control-input action_check" type="checkbox" name="permissions[]" id="actionCheckbox<?php echo $value2['id']; ?>" value="<?php echo $value2['id']; ?>" <?php echo in_array($value2['id'], $permissions) ? 'checked' : ''; ?>>
                                                        <label for="actionCheckbox<?php echo $value2['id']; ?>" class="custom-control-label"><?php echo ucfirst($value2['name']); ?>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <tr>
                                <td colspan="2" class="text-center">No Mudule, Please create one.</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="form-group">
            <?php
            $LABEL_NAME = $this->request->action === 'admin_create' ? 'Create' : __('COMMON_TXT_EDIT');
            echo $this->Form->button($LABEL_NAME, array(
                'class' => 'btn btn-primary float-right',
                'type' => 'submit'
            )); ?>

            <?php
            echo $this->Html->link('Back', 'index', array(
                'class' => 'btn btn-danger float-right mr-2',
                'type' => 'submit'
            )); ?>
        </div>
        <?php
        echo $this->Form->end(); ?>
    </div>
</div>
<script>
    $(function() {
        $('#customCheckboxCheckAll').change(function() {
            $('input:checkbox').prop('checked', $(this).prop('checked'));
        });

        $('.module_check').change(function() {
            $(this).parents('tr').find('.action_check').prop('checked', $(this).prop('checked'));
        });

        $('.module_check').each(function(key, value) {
            let row_check = $(this).parents('tr').find('.action_check');
            if (row_check.length == row_check.filter(':checked').length) {
                $(this).prop('checked', true);
            }
        });
    });
</script>
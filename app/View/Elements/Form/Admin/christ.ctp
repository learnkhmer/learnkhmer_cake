<?php
echo $this->Form->create(
    'Song',
    array(
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'enctype' => 'multipart/form-data',
        'class' => 'form-create',
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false,
        )
    )
);
echo $this->Form->hidden('id');
?>
<div class="row">
    <div class="col-md-6 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
            <div class="form-group">
                    <label>Name (Kh)</label>
                    <div>
                        <?php
                        echo $this->Form->input('title', array(
                            'type' => 'text',
                            'placeholder' => 'Title',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Ref Number</label>
                    <div>
                        <?php
                        echo $this->Form->input('ref_number', array(
                            'type' => 'number',
                            'placeholder' => 'Ref Number',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Written</label>
                    <div>
                        <?php
                        echo $this->Form->input('written', array(
                            'placeholder' => 'Written',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Written year</label>
                    <div>
                        <?php
                        echo $this->Form->input('written_year', array(
                            'type' => 'number',
                            'placeholder' => 'Year',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Lyrice</label>
                    <div>
                        <?php
                        echo $this->Form->input('lyrice', array(
                            'class' => 'form-control summernote',
                            'type' => 'textarea',
                            'placeholder' => 'Descritpion',
                        )); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>Audio</label>
                    <div class="custom-file">
                       <?php
                        $audio = isset($this->request->data['Song']['audio']) ? $this->request->data['Song']['audio'] : 'Choose File';
                        echo $this->Form->input('tmp_audio', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_audio',
                            'accept' => 'audio/mp3,audio/*;capture=microphone'
                        )); ?>
                        <label class="custom-file-label" for="tmp_audio_id_001"><?php echo  $audio; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Chord</label>
                    <div class="custom-file">
                       <?php
                        $chord_image = isset($this->request->data['Song']['chord_image']) ? $this->request->data['Song']['chord_image'] : 'Choose File';
                        echo $this->Form->input('tmp_chord_image', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_chord_image',
                            'accept' => 'image/*'
                        )); ?>
                        <label class="custom-file-label" for="tmp_chord_image"><?php echo  $chord_image; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Lyrics</label>
                    <div class="custom-file">
                       <?php
                        $lyric_image = isset($this->request->data['Song']['lyric_image']) ? $this->request->data['Song']['lyric_image'] : 'Choose File';
                        echo $this->Form->input('tmp_lyric_image', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_lyric_image',
                            'accept' => 'image/*'
                        )); ?>
                        <label class="custom-file-label" for="tmp_lyric_image"><?php echo  $lyric_image; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Thumnail</label>
                    <div class="custom-file">
                       <?php
                        $thumnail = isset($this->request->data['Song']['thumnail']) ? $this->request->data['Song']['thumnail'] : 'Choose File';
                        echo $this->Form->input('tmp_thumnail', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_thumnail',
                            'accept' => 'image/*'
                        )); ?>
                        <label class="custom-file-label" for="tmp_thumnail"><?php echo  $thumnail; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <?php
                    $label_action = $this->request->action === 'admin_create' ? 'Create' : 'Update';
                    echo $this->Form->button($label_action, array(
                        'class' => 'btn btn-primary float-right',
                        'type' => 'submit'
                    )); ?>

                    <?php
                    echo $this->Html->link('Back', 'index', array(
                        'class' => 'btn btn-danger float-right mr-2',
                        'type' => 'submit'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Form->end(); ?>
<script>
    $(function() {
        $('.form-create').on('submit', function(e) {
            e.preventDefault();
            var that = this;
            var formData = new FormData($(that)[0]);
            var params = {
                type: 'post',
                url: base_url + 'christs/save',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                    $(that).find('.form-control').removeClass('is-invalid');
                }
            };
            $.ajax(params).done(function(data){
                let json_parse = JSON.parse(data);
                if (json_parse.status == 0) {
                    $.each(json_parse.data, function(index, value){
                        $(that).find('[name="data[Song][' + index +']"]').addClass('is-invalid')
                    });
                    toastr.error('Data cannot save.')
                } else {
                    toastr.success('Data has been saved successfully.');
                    setTimeout(function() {
                        location.href = base_url + 'christs';
                    }, 1000);
                }
                $.LoadingOverlay('hide');
            });
        });
    });
</script>
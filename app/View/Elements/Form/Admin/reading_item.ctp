<?php
echo $this->Form->create(
    'ReadingItem',
    array(
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false,
        )
    )
); ?>
<div class="row">
    <div class="col-md-10 col-sm-9 col-7"></div>
    <div class="col-md-2 col-sm-3 col-5">
        <div class="form-group">
            <?php
            echo $this->Html->link('បន្ថែម​ពាក្យ', 'javascript: void(0);', array(
                'class' => 'btn btn-info btn-block add-item',
            ));
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-sm-12 col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Create / Edit</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <?php echo $this->Form->input('Reading.title', array(
                                'placeholder' => __d('reading', 'TXT_ENTER_TILE'),
                                'class' => 'form-control',
                                'disabled' => true,
                            )); ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <?php echo $this->Sound->renderSound(); ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <?php
                            $LABEL_NAME = $this->request->action === 'admin_create' ? 'Create' : __('COMMON_TXT_EDIT');
                            echo $this->Form->button($LABEL_NAME, array(
                                'class' => 'btn btn-primary float-right',
                                'type' => 'submit'
                            )); ?>

                            <?php
                            echo $this->Html->link('Back', '/admin/readings', array(
                                'class' => 'btn btn-danger float-right mr-2',
                                'type' => 'submit',
                                'confirm' => 'Are you sure, you want to leave?',
                            )); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-sm-12 col-12">
        <div class="card">
            <div class="card-body p-0">
                <div class="card-body table-responsive p-0 reading-item-container" style="height: 300px;">
                    <table class="table table-head-fixed text-nowrap">
                        <thead>
                            <tr>
                                <th>Text</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Space</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="table-sortable">
                            <?php echo $this->ReadingItem->render(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end();
echo $this->Html->script('/admin/js/reading_item');

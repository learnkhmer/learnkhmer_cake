
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">បង្កើត/កែប្រែពត៌មានលំអិត</h3>
    </div>
    <?php
    echo $this->Form->create('CategoryDetail',
        array(
        'class' => 'form-horizontal',
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false,
        )
    ));
    ?>
    <div class="box-body">
        <div class="col-md-8">
        <div class="form-group">
                <label class="col-sm-3 control-label">វី​ដេ​អូ​ធ្វើ​បទ​បង្ហាញ​ (URL) </label>
                <div class="col-sm-9">
                    <?php
                    echo $this->Form->input('video1', array(
                        'type' => 'text',
                        'placeholder' => 'បញ្ចូលវី​ដេ​អូ​ធ្វើ​បទ​បង្ហាញ​',
                        'class' => 'form-control',
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">វី​ដេ​អូ​បង្រៀន​ (URL)  </label>
                <div class="col-sm-9">
                    <?php
                    echo $this->Form->input('video2', array(
                        'type' => 'text',
                        'placeholder' => 'បញ្ចូលវី​ដេ​អូ​បង្រៀន​',
                        'class' => 'form-control',
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">ពណនា: </label>
                <div class="col-sm-9">
                    <?php
                    echo $this->Form->input('description', array(
                        'type' => 'textarea',
                        'placeholder' => 'បញ្ចូលការពណនា',
                        'class' => 'form-control',
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">&nbsp;</label>
                <div class="col-sm-9">
                    <?php
                    $LABEL_NAME = $this->request->action === 'admin_create' ? __('COMMON_TXT_SAVE') : __('COMMON_TXT_EDIT');
                    echo $this->Form->button($LABEL_NAME, array(
                        'class' => 'btn btn-primary pull-left btn-flat',
                        'type' => 'submit'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo $this->Form->end(); ?>
</div>

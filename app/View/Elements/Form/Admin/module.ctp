<div class="card col-6">
    <div class="card-body">
        <?php
        echo $this->Form->create(
            'Module',
            array(
                'class' => 'form-horizontal',
                'role' => 'form',
                'autocomplete' => 'off',
                'novalidate' => true,
                'inputDefaults' => array(
                    'legend' => false,
                    'label' => false,
                    'div' => false,
                )
            )
        );
        ?>
        <div class="form-group">
            <label>Parent</label>
            <div>
                <?php
                echo $this->Form->select('parent_id', $parents, array(
                    'class' => 'form-control select2',
                    'empty' => array('' => 'Select Parent'),
                )); ?>
            </div>
        </div>
        <div class="form-group">
            <label>Name</label>
            <div>
                <?php
                echo $this->Form->text('name', array(
                    'placeholder' => 'Name',
                    'class' => 'form-control',
                )); ?>
            </div>
        </div>
        <div class="form-group">
            <label>URL</label>
            <div>
                <?php
                echo $this->Form->text('url', array(
                    'class' => 'form-control',
                    'placeholder' => 'URL',
                )); ?>
            </div>
        </div>
        <div class="form-group">
            <label>Icon</label>
            <div>
                <?php
                echo $this->Form->text('icon', array(
                    'class' => 'form-control',
                    'placeholder' => 'Icon',
                )); ?>
            </div>
        </div>
        <div class="form-group">
            <label>Sort</label>
            <div>
                <?php
                echo $this->Form->input('sort', array(
                    'type' => 'number',
                    'class' => 'form-control',
                    'placeholder' => 'Sort',
                )); ?>
            </div>
        </div>
        <?php
        if ($this->request->action === 'admin_create') : ?>
            <div class="form-group">
                <label><?php echo __('Action'); ?></label>
                <hr />
                <div class="form-module">
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="customCheckboxCheckAll">
                        <label for="customCheckboxCheckAll" class="custom-control-label"> Check / Uncheck all</label>
                    </div>
                    <?php
                    $actions = [
                        'index',
                        'create',
                        'edit',
                        'view',
                        'delete',
                    ]; ?>
                    <?php
                    foreach ($actions as $key => $value) : ?>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="actions[<?php echo $key; ?>][key]" id="customCheckbox<?php echo $key; ?>" <?php echo $value == 'index' ? 'checked' : ''; ?>>
                            <label for="customCheckbox<?php echo $key; ?>" class="custom-control-label"><?php echo ucfirst($value); ?></label>
                            <input type="hidden" name="actions[<?php echo $key; ?>][name]" value="<?php echo $value; ?>" />
                            <input type="hidden" name="actions[<?php echo $key; ?>][is_menu]" value="<?php echo $value == 'index' ? '1' : '0'; ?>" />
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <?php
            $LABEL_NAME = $this->request->action === 'admin_create' ? 'Create' : __('COMMON_TXT_EDIT');
            echo $this->Form->button($LABEL_NAME, array(
                'class' => 'btn btn-primary float-right',
                'type' => 'submit'
            )); ?>

            <?php
            echo $this->Html->link('Back', 'index', array(
                'class' => 'btn btn-danger float-right mr-2',
                'type' => 'submit'
            )); ?>
        </div>
        <?php
        echo $this->Form->end(); ?>
    </div>
</div>
<script>
    $(function() {
        $('#customCheckboxCheckAll').change(function() {
            $('input:checkbox').prop('checked', $(this).prop('checked'));
        });
    });
</script>
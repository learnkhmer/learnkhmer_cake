<?php
echo $this->Form->create(
    'Category',
    array(
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'enctype' => 'multipart/form-data',
        'class' => 'form-create',
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false,
        )
    )
);
echo $this->Form->hidden('id');
?>
<div class="row">
    <div class="col-md-6 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>Name (Kh)</label>
                    <div>
                        <?php
                        echo $this->Form->input('name_kh', array(
                            'type' => 'text',
                            'placeholder' => 'Name (Kh)',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Name (En)</label>
                    <div>
                        <?php
                        echo $this->Form->input('name_en', array(
                            'type' => 'text',
                            'placeholder' => 'Name (En)',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <div>
                        <?php
                        echo $this->Form->input('description', array(
                            'class' => 'form-control summernote',
                            'type' => 'textarea',
                            'placeholder' => 'Descritpion',
                        )); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>Parent</label>
                    <div>
                        <?php
                        echo $this->Form->select('parent_id', $parents, array(
                            'empty' => 'Select',
                            'class' => 'form-control select2',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php
                    echo $this->Form->input('model', array(
                        'options' => unserialize(CATEGORY_MODE),
                        'empty' => 'Choose Model',
                        'class' => 'form-control select2',
                        'label' => false,
                    )); ?>
                </div>
                <div class="form-group">
                    <label>Order</label>
                    <?php
                    echo $this->Form->input('order', array(
                        'type' => 'number',
                        'class' => 'form-control',
                        'value' => 1,
                    )); ?>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <div>
                        <input type="checkbox" name="data[Category][status]" data-bootstrap-switch <?php echo isset($this->request->data['Category']['status']) && $this->request->data['Category']['status'] ? 'checked' : ''; ?>>
                    </div>
                </div>
                <div class="form-group">
                    <?php
                    $label_action = $this->request->action === 'admin_create' ? 'Create' : 'Update';
                    echo $this->Form->button($label_action, array(
                        'class' => 'btn btn-primary float-right',
                        'type' => 'submit'
                    )); ?>
                    <?php
                    echo $this->Html->link('Back', 'index', array(
                        'class' => 'btn btn-danger float-right mr-2',
                        'type' => 'submit'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Form->end(); ?>
<script>
    $(function() {
        $('.form-create').on('submit', function(e) {
            e.preventDefault();
            var that = this;
            var formData = new FormData($(that)[0]);
            var params = {
                type: 'post',
                url: base_url + 'categories/save',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                    $(that).find('.form-control').removeClass('is-invalid');
                }
            };
            $.ajax(params).done(function(data) {
                let json_parse = JSON.parse(data);
                if (json_parse.status == 0) {
                    $.each(json_parse.data, function(index, value) {
                        $(that).find('[name="data[Category][' + index + ']"]').addClass('is-invalid')
                    });
                    toastr.error('Data cannot save.')
                } else {
                    toastr.success('Data has been saved successfully.');
                    setTimeout(function() {
                        location.href = base_url + 'categories';
                    }, 1000);
                }
                $.LoadingOverlay('hide');
            });
        });
    });
</script>
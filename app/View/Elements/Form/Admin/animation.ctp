<?php
echo $this->Form->create(
    'Beginner',
    array(
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'enctype' => 'multipart/form-data',
        'class' => 'form-create',
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false,
        )
    )
);
echo $this->Form->hidden('id');
?>
<div class="row">
    <div class="col-md-6 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>Title</label>
                    <div>
                        <?php
                        echo $this->Form->input('title', array(
                            'type' => 'text',
                            'placeholder' => 'Title',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Category</label>
                    <div>
                        <?php
                        echo $this->Form->select('category_id', $categories, array(
                            'empty' => 'Select',
                            'class' => 'form-control select2',
                            'style' => 'width: 100%',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Thumbnail</label>
                    <div class="custom-file">
                       <?php
                        $thumbnail = isset($this->request->data['Beginner']['thumbnail']) ? $this->request->data['Beginner']['thumbnail'] : 'Choose File';
                        echo $this->Form->input('tmp_thumbnail', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_thumbnail',
                            'accept' => 'image/*'
                        )); ?>
                        <label class="custom-file-label" for="tmp_thumbnail"><?php echo $thumbnail; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Draw</label>
                    <div class="custom-file">
                       <?php
                        $draw = isset($this->request->data['Beginner']['draw']) ? $this->request->data['Beginner']['draw'] : 'Choose File';
                        echo $this->Form->input('tmp_draw', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_draw',
                            'accept' => 'image/*'
                        )); ?>
                        <label class="custom-file-label" for="tmp_draw"><?php echo $draw; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Sort</label>
                    <div>
                        <?php
                        echo $this->Form->input('sort', array(
                            'type' => 'text',
                            'placeholder' => 'Sort',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Descritpion</label>
                    <div>
                        <?php
                        echo $this->Form->input('description', array(
                            'class' => 'form-control summernote',
                            'type' => 'textarea',
                            'placeholder' => 'Descritpion',
                        )); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>Image</label>
                    <div class="custom-file">
                       <?php
                        $image = isset($this->request->data['Beginner']['image']) ? $this->request->data['Beginner']['image'] : 'Choose File';
                        echo $this->Form->input('tmp_image', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_image',
                            'accept' => 'image/*'
                        )); ?>
                        <label class="custom-file-label" for="tmp_image"><?php echo $image; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Simple Image</label>
                    <div class="custom-file">
                       <?php
                        $simple_image = isset($this->request->data['Beginner']['simple_image']) ? $this->request->data['Beginner']['simple_image'] : 'Choose File';
                        echo $this->Form->input('tmp_simple_image', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_simple_image',
                            'accept' => 'image/*'
                        )); ?>
                        <label class="custom-file-label" for="tmp_simple_image"><?php echo $simple_image; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Image Sound</label>
                    <div class="custom-file">
                       <?php
                        $sound = isset($this->request->data['Beginner']['sound']) ? $this->request->data['Beginner']['sound'] : 'Choose File';
                        echo $this->Form->input('tmp_sound', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_sound',
                            'accept' => 'audio/mp3,audio/*;capture=microphone'
                        )); ?>
                        <label class="custom-file-label" for="tmp_sound"><?php echo $sound; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Simple Sound</label>
                    <div class="custom-file">
                       <?php
                        $simple_sound = isset($this->request->data['Beginner']['simple_sound']) ? $this->request->data['Beginner']['simple_sound'] : 'Choose File';
                        echo $this->Form->input('tmp_simple_sound', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_simple_sound',
                            'accept' => 'audio/mp3,audio/*;capture=microphone'
                        )); ?>
                        <label class="custom-file-label" for="tmp_simple_sound"><?php echo $simple_sound; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <?php
                    $label_action = $this->request->action === 'admin_create' ? 'Create' : 'Update';
                    echo $this->Form->button($label_action, array(
                        'class' => 'btn btn-primary float-right',
                        'type' => 'submit'
                    )); ?>

                    <?php
                    echo $this->Html->link('Back', 'index', array(
                        'class' => 'btn btn-danger float-right mr-2',
                        'type' => 'submit'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Form->end(); ?>
<script>
    $(function() {
        $('.form-create').on('submit', function(e) {
            e.preventDefault();
            var that = this;
            var formData = new FormData($(that)[0]);
            var params = {
                type: 'post',
                url: base_url + 'animations/save',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    // $.LoadingOverlay('show');
                    $(that).find('.form-control').removeClass('is-invalid');
                }
            };
            $.ajax(params).done(function(data){
                // let json_parse = JSON.parse(data);
                // if (json_parse.status == 0) {
                //     $.each(json_parse.data, function(index, value){
                //         $(that).find('[name="data[Beginner][' + index +']"]').addClass('is-invalid');
                //         $(that).find('[name="data[Beginner][tmp_' + index +']"]').addClass('is-invalid')
                //     });
                //     toastr.error('Data cannot save.')
                // } else {
                //     toastr.success('Data has been saved successfully.');
                //     setTimeout(function() {
                //         location.href = base_url + 'animations';
                //     }, 1000);
                // }
                // $.LoadingOverlay('hide');
            });
        });
    });
</script>
<?php
echo $this->Form->create('Category',
    array(
    'role' => 'form',
    'autocomplete' => 'off',
    'novalidate' => true,
    'enctype'=> 'multipart/form-data',
    'class' => 'form-create',
    'inputDefaults' => array(
        'legend' => false,
        'label' => false,
        'div' => false,
    )
));
echo $this->Form->hidden('id');
echo $this->Form->hidden('model', array(
    'class' => 'model',
    'value' => $this->request->query('model'),
));
?>
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <label>Name (Kh)</label>
                    <div>
                        <?php
                        echo $this->Form->input('name_kh', array(
                            'type' => 'text',
                            'placeholder' => 'Name (Kh)',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Name (En)</label>
                    <div>
                        <?php
                        echo $this->Form->input('name_en', array(
                            'type' => 'text',
                            'placeholder' => 'Name (En)',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <div>
                        <?php
                        echo $this->Form->input('description', array(
                            'class' => 'form-control description',
                            'type' => 'textarea',
                            'placeholder' => 'Descritpion',
                        )); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <label>Parent</label>
                    <div>
                        <?php
                        echo $this->Form->select('parent_id', $parents, array(
                            'empty' => 'Select',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php
                    echo $this->Form->input('model', array(
                        'options' => unserialize(CATEGORY_MODE),
                        'empty' => 'Choose Model',
                        'class' => 'form-control',
                        'label' => false,
                    )); ?>
                </div>
                <div class="form-group">
                    <label>Order</label>
                    <?php
                    echo $this->Form->input('order', array(
                        'type' => 'number',
                        'class' => 'form-control',
                        'value' => 1,
                    )); ?>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <?php
                    echo $this->Form->input('status', array(
                        'type' => 'checkbox',
                        'value' => 1,
                    )); ?>
                </div>
                <div class="form-group">
                    <?php
                        $LABEL_NAME = $this->request->action === 'admin_create' ? 'Create' : __('COMMON_TXT_EDIT');
                        echo $this->Form->button($LABEL_NAME, array(
                            'class' => 'btn btn-primary btn-block btn-save',
                            'type' => 'submit'
                        )); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function() {
    $('.form-create').on('submit', function(e) {
        e.preventDefault();
        var that = this;
        var formData = new FormData($(that)[0]);
        var params = {
            type: 'post',
            url: BASE_URL + 'admin/categories/save',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $.LoadingOverlay('show');
                $(that).find('.form-group').removeClass('has-error');
                $(that).find('.help-block').remove();
            }
        };
        ajax_request(params, function(data) {
            if (data.status === 0) {
                $.each(data.data, function(index, value){
                    var msg = '<span class="help-block">' + value[0] + '</span>';
                    var content = $(that).find('[name="data[Category][' + index +']"]');
                    $(content).closest('.form-group').addClass('has-error');
                    $(content).parent('div').append(msg);
                });
            } else {
                location.href = BASE_URL + 'admin/categories?model='+$('input[name="data[Category][model]"]').val();
            }
        });
    });
    
    $('.description').summernote({
        minHeight: 200,
        placeholder: 'Write here ...',
        focus: false,
        airMode: false,
    });
});
</script>
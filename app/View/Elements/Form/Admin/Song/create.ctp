<?php
echo $this->Form->create('Song',
    array(
    'role' => 'form',
    'autocomplete' => 'off',
    'novalidate' => true,
    'enctype'=> 'multipart/form-data',
    'class' => 'form-create',
    'inputDefaults' => array(
        'legend' => false,
        'label' => false,
        'div' => false,
    )
));
echo $this->Form->hidden('id');
?>
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <label>Name (Kh)</label>
                    <div>
                        <?php
                        echo $this->Form->input('title', array(
                            'type' => 'text',
                            'placeholder' => 'Title',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Signer</label>
                    <div>
                        <?php
                        echo $this->Form->select('singer_id', $singers, array(
                            'empty' => 'Select',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Artis</label>
                    <div>
                        <?php
                        echo $this->Form->input('artist', array(
                            'placeholder' => 'Artist)',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Lyrice</label>
                    <div>
                        <?php
                        echo $this->Form->input('lyrice', array(
                            'class' => 'form-control lyrice',
                            'type' => 'textarea',
                            'placeholder' => 'Descritpion',
                        )); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <label>Category</label>
                    <div>
                        <?php
                        echo $this->Form->select('category_id', $categories, array(
                            'empty' => 'Select',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Audio</label>
                    <div>
                    <?php
                        $chord = isset($this->request->data['Song']['audio']) ? $this->request->data['Song']['audio'] : '';
                        echo $this->Image->previewUpload($chord, 'tmp_audio'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Chord</label>
                    <div>
                        <?php
                        $chord = isset($this->request->data['Song']['chord_image']) ? $this->request->data['Song']['chord_image'] : '';
                        echo $this->Image->previewUpload($chord, 'tmp_chord_image'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Lyrics</label>
                    <div>
                    <?php
                        $chord = isset($this->request->data['Song']['lyric_image']) ? $this->request->data['Song']['lyric_image'] : '';
                        echo $this->Image->previewUpload($chord, 'tmp_lyric_image'); ?>
                    </div>
                </div>
                <hr />
                <div class="form-group">
                    <label>Thumnail</label>
                    <div>
                    <?php
                        $chord = isset($this->request->data['Song']['thumnail']) ? $this->request->data['Song']['thumnail'] : '';
                        echo $this->Image->previewUpload($chord, 'tmp_thumnail'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php
                        $LABEL_NAME = $this->request->action === 'admin_create' ? 'Create' : __('COMMON_TXT_EDIT');
                        echo $this->Form->button($LABEL_NAME, array(
                            'class' => 'btn btn-primary btn-block btn-save',
                            'type' => 'submit'
                        )); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function() {
    $('select').select2();
    
    $.uploadPreview({
        input_field: "#image-upload",   // Default: .image-upload
        preview_box: "#image-preview",  // Default: .image-preview
        label_field: "#image-label",    // Default: .image-label
        label_default: "Choose File",   // Default: Choose File
        label_selected: "Change File",  // Default: Change File
        no_label: true// Default: false
    });
    
    $('.form-create').on('submit', function(e) {
        e.preventDefault();
        var that = this;
        var formData = new FormData($(that)[0]);
        var params = {
            type: 'post',
            url: BASE_URL + 'admin/songs/save',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function() {
                $.LoadingOverlay('show');
                $(that).find('.form-group').removeClass('has-error');
                $(that).find('.help-block').remove();
            }
        };
        ajax_request(params, function(data) {
            if (data.status === 0) {
                $.each(data.data, function(index, value){
                    var msg = '<span class="help-block">' + value[0] + '</span>';
                    var content = $(that).find('[name="data[Song][' + index +']"]');
                    $(content).closest('.form-group').addClass('has-error');
                    $(content).parent('div').append(msg);
                });
            } else {
                location.href = BASE_URL + 'admin/songs';
            }
        });
    });
    
    $('.btn-file-upload').on('click', function() {
        $(this).prev().click();
    });

    $('.file_upload').on('change', function() {
        $(this).prev().val($(this).val());
    });

    $('.lyrice').summernote({
        minHeight: 200,
        placeholder: 'Write here ...',
        focus: false,
        airMode: false,
        fontNames: ['Roboto', 'Calibri', 'Times New Roman', 'Arial'],
        fontNamesIgnoreCheck: ['Roboto', 'Calibri'],
        dialogsInBody: true,
        dialogsFade: true,
        disableDragAndDrop: false,
        toolbar: [
          // [groupName, [list of button]]
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['para', ['style', 'ul', 'ol', 'paragraph']],
          ['fontsize', ['fontsize']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['height', ['height']],
          ['misc', ['undo', 'redo', 'print', 'fullscreen']],
          ['color', ['color']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ],
        popover: {
          air: [
            ['color', ['color']],
            ['font', ['bold', 'underline', 'clear']]
          ],
        }
    });
});
</script>
<?php
// https://opoloo.github.io/jquery_upload_preview/
echo $this->Form->end();
echo $this->Html->css([
    'imagePreview',
]);
echo $this->Html->script([
    'imagePreview',
], ['inline' => false]);


<?php
echo $this->Form->create(
    'Member',
    array(
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'enctype' => 'multipart/form-data',
        'class' => 'form-create',
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false,
        )
    )
);
echo $this->Form->hidden('id');
?>
<div class="row">
    <div class="col-md-6 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
            <div class="form-group">
                    <label>Name</label>
                    <div>
                        <?php
                        echo $this->Form->input('name', array(
                            'placeholder' => 'Name',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Position</label>
                    <div>
                        <?php
                        echo $this->Form->input('position', array(
                            'placeholder' => 'Position',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Photo</label>
                    <div class="custom-file">
                       <?php
                        $photo = isset($this->request->data['Member']['photo']) ? $this->request->data['Member']['photo'] : 'Choose File';
                        echo $this->Form->input('tmp_photo', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_photo',
                            'accept' => 'image/*'
                        )); ?>
                        <label class="custom-file-label" for="tmp_photo"><?php echo $photo; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Link</label>
                    <div>
                        <?php
                        echo $this->Form->input('link', array(
                            'placeholder' => 'Position',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>About</label>
                    <div>
                        <?php
                        echo $this->Form->input('about', array(
                            'class' => 'form-control summernote',
                            'type' => 'textarea',
                            'placeholder' => 'Descritpion',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Sort</label>
                    <div>
                        <?php
                        echo $this->Form->input('sort', array(
                            'type' => 'number',
                            'placeholder' => 'Sort',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php
                    $label_action = $this->request->action === 'admin_create' ? 'Create' : 'Update';
                    echo $this->Form->button($label_action, array(
                        'class' => 'btn btn-primary float-right',
                        'type' => 'submit'
                    )); ?>

                    <?php
                    echo $this->Html->link('Back', 'index', array(
                        'class' => 'btn btn-danger float-right mr-2',
                        'type' => 'submit'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Form->end(); ?>
<script>
    $(function() {
        $('.form-create').on('submit', function(e) {
            e.preventDefault();
            var that = this;
            var formData = new FormData($(that)[0]);
            var params = {
                type: 'post',
                url: base_url + 'members/save',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                    $(that).find('.form-control').removeClass('is-invalid');
                }
            };
            $.ajax(params).done(function(data){
                let json_parse = JSON.parse(data);
                if (json_parse.status == 0) {
                    $.each(json_parse.data, function(index, value){
                        $(that).find('[name="data[Member][' + index +']"]').addClass('is-invalid')
                    });
                    toastr.error('Data cannot save.')
                } else {
                    toastr.success('Data has been saved successfully.');
                    setTimeout(function() {
                        location.href = base_url + 'members';
                    }, 1000);
                }
                $.LoadingOverlay('hide');
            });
        });
    });
</script>
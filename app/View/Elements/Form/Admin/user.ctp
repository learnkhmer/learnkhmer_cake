<div class="card col-6">
    <div class="card-body">
        <?php
        echo $this->Form->create(
            'User',
            array(
                'class' => 'form-horizontal',
                'role' => 'form',
                'autocomplete' => 'off',
                'novalidate' => true,
                'inputDefaults' => array(
                    'legend' => false,
                    'label' => false,
                    'div' => false,
                )
            )
        );
        ?>
        <div class="form-group">
            <label>Group</label>
            <div>
                <?php
                echo $this->Form->select('group_id', $groups, array(
                    'class' => 'form-control select2',
                    'empty' => array('' => 'Select Group'),
                )); ?>
            </div>
        </div>
        <div class="form-group">
            <label>Username</label>
            <div>
                <?php
                echo $this->Form->text('username', array(
                    'placeholder' => 'Username',
                    'class' => 'form-control',
                )); ?>
            </div>
        </div>
        <div class="form-group">
            <label>Phone</label>
            <div>
                <?php
                echo $this->Form->text('phone', array(
                    'class' => 'form-control',
                    'placeholder' => 'Phone',
                )); ?>
            </div>
        </div>
        <?php if (isset($this->request->data['User']['id']) && $this->request->data['User']['id']) : ?>
            <div class="form-group">
                <label>Password</label>
                <div>
                    <?php
                    echo $this->Form->password('password_update', array(
                        'class' => 'form-control',
                        'placeholder' => 'Password',
                    )); ?>
                </div>
            </div>
        <?php else : ?>
            <div class="form-group">
                <label>Password</label>
                <div>
                    <?php
                    echo $this->Form->password('password', array(
                        'class' => 'form-control',
                        'placeholder' => 'Password',
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <div>
                    <?php
                    echo $this->Form->password('password_confirm', array(
                        'class' => 'form-control',
                        'placeholder' => 'Confirm Password',
                    )); ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <label>Status</label>
            <div>
                <input type="checkbox" name="data[User][status]" data-bootstrap-switch <?php echo isset($this->request->data['User']['status']) && $this->request->data['User']['status'] ? 'checked' : ''; ?>>
            </div>
        </div>
        <div class="form-group">
            <?php
            $LABEL_NAME = $this->request->action === 'admin_create' ? 'Create' : __('COMMON_TXT_EDIT');
            echo $this->Form->button($LABEL_NAME, array(
                'class' => 'btn btn-primary float-right',
                'type' => 'submit'
            )); ?>

            <?php
            echo $this->Html->link('Back', 'index', array(
                'class' => 'btn btn-danger float-right mr-2',
                'type' => 'submit'
            )); ?>
        </div>
        <?php
        echo $this->Form->end(); ?>
    </div>
</div>
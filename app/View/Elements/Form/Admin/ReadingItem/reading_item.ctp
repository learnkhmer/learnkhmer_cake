
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">
        <?php
        if ($this->request->action === 'admin_create') {
            echo 'បង្កើតប្រភេទថ្មី';
        } else {
            echo 'កែប្រែប្រភេទ';
        } ?>
        </h3>
    </div>
    <?php
    echo $this->Form->create('ReadingItem',
        array(
        'class' => 'form-horizontal',
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false,
        )
    ));
    ?>
    <div class="box-body">
        <div class="col-md-8">
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo __d('reading', 'TXT_TILE'); ?></label>
                <div class="col-sm-9">
                    <?php echo $this->Form->input('Reading.title', array(
                        'placeholder' => __d('reading', 'TXT_ENTER_TILE'),
                        'class' => 'form-control',
                        'disabled' => true,
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo __d('reading', 'TXT_SOUND'); ?></label>
                <div class="col-sm-9">
                    <?php echo $this->Sound->renderSound(); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <?php
                     echo $this->Html->link('បន្ថែម​ពាក្យ', 'javascript: void(0);', array(
                        'class' => 'btn btn-info btn-block add-item',
                    ));
                    ?>
                </div>
            </div>
            <?php // Reading Item ?>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo __d('reading', 'TXT_SOUND'); ?></label>
                <div class="col-sm-9">
                    <table class="table table-striped table-bordered table-hovered table-sortable">
                        <thead>
                            <tr>
                                <th>Text</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Space</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php echo $this->ReadingItem->render(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">&nbsp;</label>
                <div class="col-sm-9">
                    <?php
                    $LABEL_NAME = $this->request->action === 'admin_create' ? __('COMMON_TXT_SAVE') : __('COMMON_TXT_EDIT');
                    echo $this->Form->button($LABEL_NAME, array(
                        'class' => 'btn btn-primary pull-left btn-flat',
                        'type' => 'submit'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo $this->Form->end(); ?>
</div>
<?php
echo $this->Html->script('/admin/js/reading_item');
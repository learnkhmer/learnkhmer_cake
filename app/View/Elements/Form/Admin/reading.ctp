<?php
echo $this->Form->create(
    'Reading',
    array(
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'enctype' => 'multipart/form-data',
        'class' => 'form-create',
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false,
        )
    )
);
echo $this->Form->hidden('id');
?>
<div class="row">
    <div class="col-md-6 col-sm-12 col-12">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label>Title</label>
                    <div>
                        <?php
                        echo $this->Form->input('title', array(
                            'type' => 'text',
                            'placeholder' => 'Title',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Sub Title</label>
                    <div>
                        <?php
                        echo $this->Form->input('sub_title', array(
                            'type' => 'text',
                            'placeholder' => 'Sub Title',
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Level</label>
                    <div>
                        <?php
                        echo $this->Form->input('level', array(
                            'type' => 'number',
                            'placeholder' => __d('reading', 'TXT_ENTER_LEVEL'),
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Page</label>
                    <div>
                        <?php
                        echo $this->Form->input('page', array(
                            'type' => 'number',
                            'placeholder' => __d('reading', 'TXT_ENTER_PAGE'),
                            'class' => 'form-control',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Lyrics</label>
                    <div class="custom-file">
                       <?php
                        $sound = isset($this->request->data['Reading']['sound']) ? $this->request->data['Reading']['sound'] : 'Choose File';
                        echo $this->Form->input('tmp_sound', array(
                            'type' => 'file',
                            'class' => 'form-control custom-file-input',
                            'id' => 'tmp_sound',
                            'accept' => 'audio/mp3,audio/*;capture=microphone'
                        )); ?>
                        <label class="custom-file-label" for="tmp_sound"><?php echo $sound; ?></label>
                    </div>
                </div>
                <div class="form-group">
                    <?php
                    $label_action = $this->request->action === 'admin_create' ? 'Create' : 'Update';
                    echo $this->Form->button($label_action, array(
                        'class' => 'btn btn-primary float-right',
                        'type' => 'submit'
                    )); ?>

                    <?php
                    echo $this->Html->link('Back', 'index', array(
                        'class' => 'btn btn-danger float-right mr-2',
                        'type' => 'submit'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Form->end(); ?>
<script>
    $(function() {
        $('.form-create').on('submit', function(e) {
            e.preventDefault();
            var that = this;
            var formData = new FormData($(that)[0]);
            var params = {
                type: 'post',
                url: base_url + 'readings/save',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                    $(that).find('.form-control').removeClass('is-invalid');
                }
            };
            $.ajax(params).done(function(data) {
                let json_parse = JSON.parse(data);
                if (json_parse.status == 0) {
                    $.each(json_parse.data, function(index, value) {
                        $(that).find('[name="data[Reading][' + index + ']"]').addClass('is-invalid')
                    });
                    toastr.error('Data cannot save.')
                } else {
                    toastr.success('Data has been saved successfully.');
                    setTimeout(function() {
                        location.href = base_url + 'readings';
                    }, 1000);
                }
                $.LoadingOverlay('hide');
            });
        });
    });
</script>
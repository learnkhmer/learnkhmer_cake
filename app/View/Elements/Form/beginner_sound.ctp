
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">
            <?php
            echo __d('beginner', 'REGISTER_SOUND_FORM'); ?>
        </h3>
    </div>
    <?php
    echo $this->Form->create('Beginner', array(
        'type' => 'file',
        'id' => 'beginner-form',
        'class' => 'form-horizontal',
        'role' => 'form',
        'autocomplete' => 'off',
        'novalidate' => true,
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false
        ),
    ));
    echo $this->Form->hidden('type', array(
        'value' => $this->request->query('type'),
    ));
    if (isset($this->request->data['Beginner']['id'])) {
        echo $this->Form->hidden('id');
    }
    echo $this->Form->hidden('image', array(
        'value' => $this->request->query('type'),
    ));
    echo $this->Form->hidden('image_letter', array(
        'value' => $this->request->query('type'),
    ));
    echo $this->Form->hidden('image_drawing', array(
        'value' => $this->request->query('type'),
    ));
    ?>
    <div class="box-body">
        <div class="col-md-8">
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('beginner', 'CATEGORY_TYPE'); ?>:
                </label>
                <div class="col-sm-9">
                    <?php
                    $options = array();
                    if ($categories) {
                        foreach ($categories as $key => $value) {
                            $options[$value['Category']['id']] = $value['Category']['name_kh'];
                        }
                    }
                    echo $this->Form->input('category_id', array(
                        'class' => 'form-control',
                        'options' => $options,
                        'empty' => array('' => __d('beginner', 'SELECT_CATEGORY')),
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('beginner', 'TXT_TITLE'); ?>:
                </label>
                <div class="col-sm-9">
                    <?php
                    echo $this->Form->input('title', array(
                        'type' => 'text',
                        'class' => 'form-control',
                        'placeholder' => __d('beginner', 'TXT_ENTER_TITLE')
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('beginner', 'TXT_SOUND'); ?>:
                </label>
                <div class="col-sm-9">
                    <div class="audio-wrap w-100">
                        <div id="p-play" class="pull-left">
                            <audio controls class="pull-left">
                                <?php
                                $url = '';
                                if(isset($this->request->data['Beginner']['sound'])) {
                                    $url = FILE_DOMAIN_FILES . '/beginners/sound/' . $this->request->data['Beginner']['sound'];
                                }
                                ?>
                                <source src="<?php echo $url; ?>" type="audio/mp3">
                            </audio>
                        </div>
                        <?php
                        $btn_upload = __d('beginner', 'COMMON_TXT_UPLOAD');
                        echo $this->Form->button('<i class="fa fa-upload" aria-hidden="true"></i> ' . $btn_upload, array(
                            'type' => 'button',
                            'class' => 'btn btn-primary btn-flat btn-upload pull-right',
                            'escape' => false,
                        )); ?>
                        <?php
                        echo $this->Form->input('sound_file', array(
                            'type' => 'file',
                            'id' => 'sound-file',
                            'class' => 'hide',
                        ));
                        echo $this->Form->hidden('sound'); ?>
                        <div class="clarfix"></div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('beginner', 'TXT_ORDER'); ?>:
                </label>
                <div class="col-sm-9">
                    <?php
                    echo $this->Form->input('order', array(
                        'type' => 'number',
                        'class' => 'form-control',
                        'placeholder' => __d('beginner', 'TXT_ENTER_ORDER')
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('beginner', 'TXT_THUMBNAIL'); ?>:
                </label>
                <div class="col-sm-9">
                    <div class="img-wrap">
                        <?php
                        $img_thumbnail = 'default.png';
                        if($this->request->data('Beginner.image_thumbnail')) {
                            $img_thumbnail = FILE_DOMAIN_FILES . '/beginners/thumbnail/' . $this->request->data['Beginner']['image_thumbnail'];
                        }
                        echo $this->Html->image($img_thumbnail, array(
                            'class' => 'img-fluid img-preview',
                        ));
                        echo $this->Form->input('image_thumbnail_file', array(
                            'type' => 'file',
                            'class' => 'img-file',
                        ));
                        echo $this->Form->hidden('image_thumbnail', array(
                            'value' => $this->request->data ? $this->request->data['Beginner']['image_thumbnail'] : '',
                        )); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">
                    <?php
                    echo __d('beginner', 'TXT_DESCRIPTION'); ?>:
                </label>
                <div class="col-sm-9">
                    <?php
                    echo $this->Form->input('description', array(
                        'type' => 'textarea',
                        'class' => 'form-control',
                        'rows' => 7,
                        'placeholder' => __d('beginner', 'TXT_ENTER_DESCRIPTION')
                    )); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"> </label>
                <div class="col-sm-9">
                    <?php
                    echo $this->Form->button(__d('beginner', 'COMMON_TXT_SAVE'), array(
                        'class' => 'btn btn-primary pull-left btn-flat btn-save',
                        'type' => 'button',
                    )); ?>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<?php
echo $this->Html->script('/admin/js/upload-preview', array('inline' => false)); ?>
<script type="text/javascript">
    $(function() {
        $('.img-file').change(function() {
            var content = $(this).closest('.img-wrap');
            $(content).find('input[type="hidden"]').val('<?= STATUS_UPDATE ?>');
            readURL(this, content);
        });

        $('.img-preview').click(function(e) {
            $(this).closest('.img-wrap')
                .find('input:file').click();
        });

        $('.btn-upload').click(function(e) {
            $('#sound-file').click();
            var content = $(this).closest('.audio-wrap');
            $(content).find('input[type="hidden"]').val('<?= STATUS_UPDATE ?>');
            $.uploadPreview({
                input_field: "#sound-file",
                preview_box: "#p-play",
              });
        });

        $('.btn-save').click(function(e) {
            var form = $('#beginner-form');
            var formData = new FormData(form[0]);
            var params = {
                type: 'post',
                url: BASE_URL + 'admin/beginners/saveAndUpdate',
                processData: false,
                contentType: false,
                data: formData,
                beforeSend: function() {
                    $.LoadingOverlay('show');
                    $(form).find('.form-group').removeClass('has-error');
                    $(form).find('.help-block').remove();
                }
            };
            ajax_request(params, function(data) {
                if (data === '' && data === 'undefined') {
                    return false;
                }
                if (data.status == 0) {
                    $.each(data.data, function(index, value){
                        var msg = '<span class="help-block">' + value[0] + '</span>';
                        var content = $(form).find('[name="data[Beginner][' + index +']"]');
                        $(content).closest('.form-group').addClass('has-error');
                        $(content).parent('div').append(msg);
                    });
                } else {
                    location.href = BASE_URL + 'admin/beginners/' + data.data.type;
                }
            });
        });
    });
</script>

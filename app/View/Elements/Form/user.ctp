
<div class="col-md-12 register-form">
    <?php if ($this->Session->read('Auth.User')) : ?>
        <h3 style="padding-left:15px; margin-bottom: 25px;">Edit</h3>
        <?php echo $this->element('Users/menu'); ?>
    <?php else : ?>
        <?php echo $this->element('Users/login'); ?>
    <?php endif; ?>
    <div class="col-md-8">
        <?php
        echo $this->Form->create('User',
            array(
            'id' => false,
            'class' => 'form-horizontal mgt20',
            'role' => 'form',
            'autocomplete' => 'off',
            'novalidate' => true,
            'inputDefaults' => array(
                'legend' => false,
                'label' => false,
                'div' => false,
                'id' => false,
            )
        ));
        echo $this->Form->hidden('role', array(
            'value' => USER_ROLE_STUDENT,
        ));
        ?>
        <?php if (!$this->Session->read('Auth.User')) : ?>
        <br style="clear:both">
        <div class="form-group">
            <label class="col-sm-1 control-label"></label>
            <div class="col-sm-5">
                <h3 style="margin-bottom: 25px;">Register</h3>
            </div>
        </div>
        <?php endif; ?>
        <div class="form-group">
            <label class="col-sm-1 control-label"></label>
            <div class="col-sm-5">
                <?php
                echo $this->Form->input('first_name', array(
                    'placeholder' => 'First Name',
                    'class' => 'form-control',
                ));
                ?>
            </div>
            <label class="col-sm-1 control-label"></label>
            <div class="col-sm-5">
                <?php
                echo $this->Form->input('last_name', array(
                    'placeholder' => 'Last Name',
                    'class' => 'form-control',
                ));
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-1 control-label"></label>
            <div class="col-sm-5">
                <?php
                echo $this->Form->input('email', array(
                    'type' => 'email',
                    'placeholder' => 'Email',
                    'class' => 'form-control',
                ));
                ?>
            </div>
            <label class="col-sm-1 control-label"></label>
            <div class="col-sm-5">
                <?php
                echo $this->Form->input('password', array(
                    'class' => 'form-control',
                    'type' => 'password',
                    'placeholder' => 'Password',
                ));
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-1 control-label"></label>
            <div class="col-sm-5">
                <?php
                echo $this->Form->input('gender', array(
                    'options' => unserialize(GENDER),
                    'class' => 'form-control',
                    'empty' => '-- Gender --',
                ));
                ?>
            </div>
            <label class="col-sm-1 control-label"></label>
            <div class="col-sm-5">
                <?php
                echo $this->Form->input('address', array(
                    'class' => 'form-control',
                    'placeholder' => 'Address',
                ));
                ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-1 control-label"></label>
            <div class="col-sm-5">
                <?php
                echo $this->Form->inputinput('mobile', array(
                    'class' => 'form-control',
                    'placeholder' => 'Mobile',
                ));
                ?>
            </div>
            <label class="col-sm-1 control-label"></label>
            <div class="col-sm-5">
                <?php
                echo $this->Form->input('company', array(
                    'class' => 'form-control',
                    'placeholder' => 'Company',
                ));
                ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-1 control-label"></label>
            <div class="col-sm-5">
            <?php
                $btn_name = in_array($this->request->action, array('admin_create', 'register')) ? LABEL_CREATE : LABEL_UPDATE;
                echo $this->Form->button($btn_name, array(
                    'class' => 'btn btn-primary btn-lg btn-block',
                    'type' => 'submit',
                ));
            ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<!DOCTYPE HTML>
<html>
<head>
  <title>JavaScript Audio Sync With Text</title>
  <link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta name="description" content="JavaScript Audio Sync With Text - Demo">
  <style>
    body{background:#4BB5C1;font-family:'Lato',sans-serif;font-size:1.5em;line-height:1.1em;letter-spacing:.05em;margin:0}header{background:#46ABB6;padding:20px 0}h1.heading{line-height:1.1em;text-align:center;font-weight:400;font-size:1.7em;color:#EDF7F2}div.naturallanguageform{width:900px;margin:2% auto;max-width:95%;min-height:400px;border-radius:5px;color:#FFF;font-size:1.5em}div.nlfinner{width:96%;padding:2%}p.line{line-height:1.5em;font-weight:300}input.textinput{background:none;border:0;border-bottom:1px dashed #EEE;width:auto;font-size:1em;color:#B5E655;font:inherit}input.textinput:focus{outline:none;border-bottom:1px dashed #D00}::-webkit-input-placeholder{color:#B5E655;font-weight:300}:-moz-placeholder{color:#B5E655;opacity:1}::-moz-placeholder{color:#B5E655;opacity:1}:-ms-input-placeholder{color:#B5E655}select.dropdown{font-size:1em;color:#B5E655;border:none;border-bottom:1px dashed #EFEFEF;-webkit-appearance:none;-moz-appearance:none;background:none;width:auto;text-indent:.01px;box-shadow:none;font:inherit}select.dropdown:focus{outline:none;border-bottom:1px dashed #D00}button.button{background:#388891;border:none;padding:10px 50px;font-size:1em;color:#FFF;display:block;margin:40px auto 0;cursor:pointer;font:inherit}
  </style>
</head>
<body>
  <div style="background: #6D6D6D; padding:2px 20px; text-align: center; position:fixed; top:0px;">
    <a style="color: white; text-decoration: none; font-size: 12px;" href="https://www.codepunker.com/blog/sync-audio-with-text-using-javascript">&laquo; Back to the website</a>
  </div>
  <header>
    <h1 class="heading">JavaScript Audio Sync With Text - Demo</h1>
  </header>

  <div class="naturallanguageform">
    <div class="nlfinner">
      <p class="line">
        <audio style="width:100%;" id="audiofile" src="10bears.mp3" controls></audio>
      </p>
      <p class="line" id="subtitles"></p>
    </div>
  </div>
  <script>
  ( function(win, doc) {
      var audioPlayer = doc.getElementById("audiofile");
      var subtitles = doc.getElementById("subtitles");
      var syncData = [
            {"end": "0.225","start": "0.125","text": "There"},
            {"end": "0.485","start": "0.225","text": "were"},
            {"end": "1.085","start": "0.485","text": "10 in"},
            {"end": "1.325","start": "1.085","text": "his"},
            {"end": "1.685","start": "1.325","text": "bed"},
            {"end": "1.965","start": "1.685","text": "and the"},
            {"end": "2.245","start": "1.965","text": "little"},
            {"end": "2.565","start": "2.245","text": "one"},
            {"end": "2.985","start": "2.565","text": "said"},
            {"end": "3.485","start": "2.985","text": "Roll"},
            {"end": "3.965","start": "3.485","text": "over!"},
            {"end": "4.805","start": "3.965","text": "Roll"},
            {"end": "5.405","start": "4.805","text": "over!"}
          ];
      createSubtitle();

      function createSubtitle()
      {
          var element;
          for (var i = 0; i < syncData.length; i++) {
              element = doc.createElement('span');
              element.setAttribute('id', 'c_' + i);
              element.innerText = syncData[i].text + 'c_';
              subtitles.appendChild(element);
          }
      }

      audioPlayer.addEventListener('timeupdate', function(e){
          syncData.forEach(function(element, index, array){
              if( audioPlayer.currentTime >= element.start && audioPlayer.currentTime <= element.end )
                  subtitles.children[index].style.background = 'yellow';
          });
      });
  }(window, document));
  </script>
</body>
</html>

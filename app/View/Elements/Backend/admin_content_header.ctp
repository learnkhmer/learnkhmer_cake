<section class="content-header">
    <h1>
      <?php
      echo ucfirst($this->request->controller);
      echo '/';
      echo str_replace('admin_', '', $this->request->action); ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo $this->Html->url('/admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?php echo $this->Html->url('/admin/'.$this->request->controller); ?>">Tables</a></li>
      <li class="active">Data tables</li>
    </ol>
</section>


<?php
    $home = $this->Html->link('Home', '/admin/home');
    switch ($this->request->controller) {
        case 'home':
            switch ($this->request->action) {
                case 'admin_index':
                    echo '<ol class="breadcrumb bc-2">';
                    echo '<li>'.$home.'</li>';
                    echo '</ol>';
                break;
            }
            break;

        case 'users':
            $management = $this->Html->link(USER_MANAGEMENT, array( 'action' => 'index'));
            switch ($this->request->action) {
                case 'admin_index':
                    echo '<ol class="breadcrumb bc-2">';
                    echo '<li>'.$home.'</li>';
                    echo '<li>List</li>';
                    echo '</ol>';
                break;

                case 'admin_edit':
                    echo '<ol class="breadcrumb bc-2">';
                    echo '<li>'.$home.'</li>';
                    echo '<li>'.$management.'</li>';
                    echo '<li>Edit</li>';
                    echo '</ol>';
                break;

                default: // create
                    case 'admin_create':
                    echo '<ol class="breadcrumb bc-2">';
                    echo '<li>'.$home.'</li>';
                    echo '<li>'.$management.'</li>';
                    echo '<li>Create</li>';
                    echo '</ol>';
                break;
            }
            break;
            case 'posts':
                $management = $this->Html->link(POST_MANAGEMENT, array( 'action' => 'index'));
                switch ($this->request->action) {
                    case 'admin_index':
                        echo '<ol class="breadcrumb bc-2">';
                        echo '<li>'.$home.'</li>';
                        echo '<li>List</li>';
                        echo '</ol>';
                    break;

                    case 'admin_edit':
                        echo '<ol class="breadcrumb bc-2">';
                        echo '<li>'.$home.'</li>';
                        echo '<li>'.$management.'</li>';
                        echo '<li>Edit</li>';
                        echo '</ol>';
                    break;

                    default: // create
                        case 'admin_create':
                        echo '<ol class="breadcrumb bc-2">';
                        echo '<li>'.$home.'</li>';
                        echo '<li>'.$management.'</li>';
                        echo '<li>Create</li>';
                        echo '</ol>';
                    break;
                }
                break;
                case 'categories':
                    $management = $this->Html->link(CATEGORY_MANAGEMENT, array( 'action' => 'index'));
                    switch ($this->request->action) {
                        case 'admin_index':
                            echo '<ol class="breadcrumb bc-2">';
                            echo '<li>'.$home.'</li>';
                            echo '<li>List</li>';
                            echo '</ol>';
                        break;

                        case 'admin_edit':
                            echo '<ol class="breadcrumb bc-2">';
                            echo '<li>'.$home.'</li>';
                            echo '<li>'.$management.'</li>';
                            echo '<li>Edit</li>';
                            echo '</ol>';
                        break;

                        default: // create
                            case 'admin_create':
                            echo '<ol class="breadcrumb bc-2">';
                            echo '<li>'.$home.'</li>';
                            echo '<li>'.$management.'</li>';
                            echo '<li>Create</li>';
                            echo '</ol>';
                        break;
                    }
                    break;
				case 'sub_categories':
					$category_ = $this->Html->link(CATEGORY_MANAGEMENT, '/admin/categories');
                    $management = $this->Html->link(SUB_CATEGORY_MANAGEMENT, array( 'action' => 'index'));
                    switch ($this->request->action) {
                        case 'admin_index':
                            echo '<ol class="breadcrumb bc-2">';
                            echo '<li>'.$home.'</li>';
							echo '<li>'.$category_.'</li>';
                            echo '<li>List</li>';
                            echo '</ol>';
                        break;

                        case 'admin_edit':
                            echo '<ol class="breadcrumb bc-2">';
                            echo '<li>'.$home.'</li>';
							echo '<li>'.$category_.'</li>';
                            echo '<li>'.$management.'</li>';
                            echo '<li>Edit</li>';
                            echo '</ol>';
                        break;

                        default: // create
                            case 'admin_create':
                            echo '<ol class="breadcrumb bc-2">';
                            echo '<li>'.$home.'</li>';
							echo '<li>'.$category_.'</li>';
                            echo '<li>'.$management.'</li>';
                            echo '<li>Create</li>';
                            echo '</ol>';
                        break;
                    }
                    break;
                case 'settings':
                    $management = $this->Html->link(CONFIGURE_MANAGEMENT, array( 'action' => 'index'));
                    switch ($this->request->action) {
                        case 'admin_index':
                            echo '<ol class="breadcrumb bc-2">';
                            echo '<li>'.$home.'</li>';
                            echo '<li>List</li>';
                            echo '</ol>';
                        break;
                    }
                    break;

        default: // Plans

            break;
    }

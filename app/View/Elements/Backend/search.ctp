
<?php
echo $this->Html->link(__('COMMON_TXT_NEW'), array(
    'action' => 'create',
    'prefix' => 'admin',
), array(
    'class' => 'btn btn-sm btn-flat btn-primary',
    'escape' => false,
)); ?>
<div class="box-tools">
    <?php
    echo $this->Form->create('User', array(
        'type' => 'get',
    )); ?>
    <div class="input-group input-group-sm" style="width: 300px;">
        <?php
        echo $this->Form->text('search', array(
            'class' => 'form-control pull-right',
            'placeholder' => __('COMMON_TXT_SEARCH'),
        )); ?>
        <div class="input-group-btn">
            <?php
            echo $this->Form->button('<i class="fa fa-search"></i>', array(
                'class' => 'btn btn-default',
                'escape' => false,
            )); ?>
        </div>
    </div>
    <?php
    echo $this->Form->end(); ?>
</div>

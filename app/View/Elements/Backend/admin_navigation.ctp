<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="<?php echo $this->request->controller === 'categories' ? 'active' : ''; ?>">
                <?php echo $this->Html->link('<i class="glyphicon glyphicon-user"></i> <span>'.__('Category').'</span>',
                '/admin/categories',
                array(
                    'escape' => false,
                )); ?>
            </li>
            <?php if (false) : ?>
            <li class="treeview <?php echo in_array($this->request->controller, ['categories']) ? 'active' : ''; ?>">
                <?php
                echo $this->Html->link('<i class="fa fa-folder"></i><span>Category</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>',
                'javascript:void(0);', array(
                    'escape' => false,
                )); ?>
                <ul class="treeview-menu">
                    <li class="<?php echo $this->request->controller === 'categories' && $this->request->action === 'admin_music' ? 'active' : ''; ?>">
                        <?php
                        echo $this->Html->link('<i class="fa fa-circle-o"></i><span>Music</span>',
                        [
                            'controller' => 'categories',
                            'action' => 'music'
                        ],
                        array(
                            'escape' => false,
                        )); ?>
                    </li>
                    <li class="<?php echo $this->request->controller === 'categories' && $this->request->action === 'admin_beginner' ? 'active' : ''; ?>">
                        <?php
                        echo $this->Html->link('<i class="fa fa-circle-o"></i><span>Basic</span>',
                        [
                            'controller' => 'categories',
                            'action' => 'beginner'
                        ],
                        array(
                            'escape' => false,
                        )); ?>
                    </li>
                    <li class="<?php echo $this->request->controller === 'categories' && $this->request->action === 'admin_worksheet' ? 'active' : ''; ?>">
                        <?php
                        echo $this->Html->link('<i class="fa fa-circle-o"></i><span>Worksheet</span>',
                        [
                            'controller' => 'categories',
                            'action' => 'worksheet'
                        ],
                        array(
                            'escape' => false,
                        )); ?>
                    </li>
                    <li class="<?php echo $this->request->controller === 'categories' && $this->request->action === 'admin_christ' ? 'active' : ''; ?>">
                        <?php
                        echo $this->Html->link('<i class="fa fa-circle-o"></i><span>Christ</span>',
                        [
                            'controller' => 'categories',
                            'action' => 'christ'
                        ],
                        array(
                            'escape' => false,
                        )); ?>
                    </li>
                </ul>
            </li>
            <?php endif; ?>
            <li class="treeview <?php echo $this->request->controller === 'beginners' ? 'active' : ''; ?>">
                <?php
                echo $this->Html->link('<i class="fa fa-laptop"></i><span>'.__('Alphabet').'</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>',
                'javascript:void(0);', array(
                    'escape' => false,
                )); ?>
                <ul class="treeview-menu">
                    <li class="<?php echo $this->request->action === 'admin_animation' ? 'active' : ''; ?>">
                        
                        <?php
                        echo $this->Html->link('<i class="fa fa-circle-o"></i><span>'.__('ADMIN_MENU_TXT_NAVIGATION_BEGINNER_ANIMATION').'</span>',
                        '/admin/beginners/animation',
                        array(
                            'escape' => false,
                        )); ?>
                    </li>
                    <li class="<?php echo $this->request->action === 'admin_sound' ? 'active' : ''; ?>">
                        <?php
                        echo $this->Html->link('<i class="fa fa-circle-o"></i><span>'.__('ADMIN_MENU_TXT_NAVIGATION_BEGINNER_SOUND').'</span>',
                        '/admin/beginners/sound',
                        array(
                            'escape' => false,
                        )); ?>
                    </li>
                </ul>
            </li>
            <li class="<?php echo $this->request->controller === 'work_sheets' ? 'active' : ''; ?>">
                <?php echo $this->Html->link('<i class="fa fa-files-o"></i> <span>'.__('Homework').'</span>',
                '/admin/work_sheets/',
                array(
                    'escape' => false,
                )); ?>
            </li>
            <li class="treeview <?php echo in_array($this->request->controller, ['readings', 'novels']) ? 'active' : ''; ?>">
                <?php
                echo $this->Html->link('<i class="fa fa-laptop"></i><span>Reading</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>',
                'javascript:void(0);', array(
                    'escape' => false,
                )); ?>
                <ul class="treeview-menu">
                    <li class="<?php echo $this->request->controller === 'readings' ? 'active' : ''; ?>">
                        <?php
                        echo $this->Html->link('<i class="fa fa-circle-o"></i><span>Reading</span>',
                        '/admin/readings',
                        array(
                            'escape' => false,
                        )); ?>
                    </li>
                    <li class="<?php echo $this->request->controller === 'novels' ? 'active' : ''; ?>">
                        <?php
                        echo $this->Html->link('<i class="fa fa-circle-o"></i><span>Novel</span>',
                        '/admin/novels',
                        array(
                            'escape' => false,
                        )); ?>
                    </li>
                </ul>
            </li>
            <li class="<?php echo $this->request->controller === 'singers' ? 'active' : ''; ?>">
                <?php echo $this->Html->link('<i class="fa fa-files-o"></i> <span>'.__('Singer').'</span>',
                '/admin/singers/',
                array(
                    'escape' => false,
                )); ?>
            </li>
            <li class="treeview <?php echo in_array($this->request->controller, ['songs', 'christs']) ? 'active' : ''; ?>">
                <?php
                echo $this->Html->link('<i class="fa fa-laptop"></i><span>Music</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>',
                'javascript:void(0);', array(
                    'escape' => false,
                )); ?>
                <ul class="treeview-menu">
                    <li class="<?php echo $this->request->controller === 'songs' ? 'active' : ''; ?>">
                        <?php
                        echo $this->Html->link('<i class="fa fa-circle-o"></i><span>Regular</span>',
                        '/admin/songs',
                        array(
                            'escape' => false,
                        )); ?>
                    </li>
                    <li class="<?php echo $this->request->controller === 'christs' ? 'active' : ''; ?>">
                        <?php
                        echo $this->Html->link('<i class="fa fa-circle-o"></i><span>Christ</span>',
                        '/admin/christs',
                        array(
                            'escape' => false,
                        )); ?>
                    </li>
                </ul>
            </li>
            <li class="<?php echo $this->request->controller === 'users' ? 'active' : ''; ?>">
                <?php echo $this->Html->link('<i class="glyphicon glyphicon-user"></i> <span>'.__('ADMIN_MENU_TXT_NAVIGATION_USER').'</span>',
                '/admin/users',
                array(
                    'escape' => false,
                )); ?>
            </li>
            <li class="<?php echo $this->request->controller === 'members' ? 'active' : ''; ?>">
                <?php
                echo $this->Html->link('<i class="fa fa-users" aria-hidden="true"></i> <span>'.__('ADMIN_MENU_TXT_NAVIGATION_MEMBER').'</span>',
                '/admin/members', array(
                    'escape' => false,
                )); ?>
            </li>
        </ul>
    </section>
</aside>

<div class="row">
    <div class="col-md-6 col-sm-8 clearfix">
        <ul class="user-info pull-left pull-none-xsm">
            <!-- Profile Info -->
            <li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
                <a href="<?php
                            echo $this->Html->url(array(
                                    'controller' => 'users',
                                    'action' => 'edit',
                                    '?' => array(
                                        'type' => USER_ROLE_ADMIN,
                                    )
                                ));
                            ?>"
                            class="dropdown-toggle en" data-toggle="dropdown">
                    <img src="<?php echo $this->Html->url('/'); ?>img/Testinglogo.png" alt="" class="img-circle" width="44">
                    <?php echo h(ucfirst($this->Session->read('Auth.User.email'))); ?>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-6 col-sm-4 clearfix hidden-xs">
        <ul class="list-inline links-list pull-right">
            <li>
                <?php
                echo $this->Html->link('<i class="entypo-logout right"></i> '. __('Logout'),
                    array(
                        'controller' => 'users',
                        'action' => 'logout',
                    ),
                    array(
                    'escape' => false,
                    )
                );
                ?>
            </li>
        </ul>
    </div>
</div>
<hr>

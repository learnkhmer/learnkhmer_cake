<div class="col-md-4">
    <?php
    echo $this->Form->create('User', array(
        'class' => 'form-horizontal mgt20',
        'role' => 'form',
        'url' => '/users/login',
        'autocomplete' => 'off',
        'novalidate' => true,
        'inputDefaults' => array(
            'legend' => false,
            'label' => false,
            'div' => false)
    ));
    ?>
    <br style="clear:both">

    <div class="form-group">
        <label class="col-sm-1 control-label"></label>
        <div class="col-sm-9">
            <h3 style="margin-bottom: 25px;">Login</h3>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-1 control-label"></label>
        <div class="col-sm-9">
            <?php echo $this->Form->input('email', array(
                'class' => 'form-control',
                'placeholder' => 'Email',
                'error' => false,
            )); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-1 control-label"></label>
        <div class="col-sm-9">
            <?php echo $this->Form->input('password', array(
                'class' => 'form-control',
                'placeholder' => 'Password',
                'type' => 'password',
                'error' => false,
            )); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-1 control-label"></label>
        <div class="col-sm-9">
            <?php echo $this->Form->button('Login', array(
                'class' => 'btn btn-lg btn-primary btn-block',
                'type' => 'submit',
            )); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-1 control-label"></label>
        <div class="col-sm-9">
            <?php echo $this->Html->link('Forget password!', '/users/forget_password'); ?>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

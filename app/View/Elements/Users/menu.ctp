<div class="col-md-3">
    <ul class="list-group">
        <li class="list-group-item"><?php echo $this->Html->link('User', '/users'); ?></li>
        <li class="list-group-item"><?php echo $this->Html->link('Document', '/favorites'); ?></li>
        <li class="list-group-item"><?php echo $this->Html->link('Logout', '/users/logout'); ?></li>
    </ul>
</div>

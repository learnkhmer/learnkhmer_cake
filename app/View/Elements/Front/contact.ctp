<h3 class="s-title text-center pb-4">
    <?php
    echo __d('home', 'TXT_CONTACT_US'); ?>
</h3>
<div class="row">
    <div class="col-12 col-sm-5 col-md-5">
        <ul class="list-unstyled">
            <li class="contact-wrap">
                <div class="row">
                    <div class="col-2">
                        <span class="text-danger">
                            <i class="fas fa-map-marked-alt"></i>
                        </span>
                    </div>
                    <div class="col">
                        <p><?php echo __d('home', 'TXT_MESSAGE_LOCATION'); ?></p>
                        <p>6421 45th Avenue North Crystal, Minnesota 55428</p>
                    </div>
                </div>
            </li>
            <li class="contact-wrap">
                <div class="row">
                    <div class="col-2">
                        <span class="txt-purple">
                            <?php
                            echo $this->Html->image('phone.png', array(
                                'alt' => 'Phone',
                                'id' => 'phone-icon',
                            )); ?>
                        </span>
                    </div>
                    <div class="col">
                        <p><?php echo __d('home', 'TXT_MESSAGE_CONTACT_BY_PHONE'); ?></p>
                        <p><a href="tel:952-393-7802">952-393-7802</a></p>
                    </div>
                </div>
            </li>
            <li class="contact-wrap">
                <div class="row">
                    <div class="col-2">
                        <span class="text-warning">
                            <i class="fas fa-envelope"></i>
                        </span>
                    </div>
                    <div class="col">
                        <p><?php echo __d('home', 'TXT_MESSAGE_ELECTRONIC'); ?></p>
                        <p><a href="mailto:yroeunLKO@gmail.com">yroeunLKO@gmail.com</a></p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="col-12 col-sm-7 col-md-7">
        <p class="mb-0 txt-blue"><?php echo __d('home', 'TXT_MESSAGE_CONACT'); ?></p>
        <div class="contact-form">
            <?php
            echo $this->Form->create('Contact',
                array(
                'class'=>'contact-form-lko',
                'autocomplete' => 'off',
                'novalidate' => true,
                'inputDefaults' => array(
                    'legend' => false,
                    'label' => false,
                    'div' => false)
            )); ?>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <?php
                            echo $this->Form->input('name', array(
                                'class' => 'form-control border border-orange',
                                'placeholder' => __d('home', 'TXT_MESSAGE_SENDER_NAME'),
                            )); ?>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <?php
                            echo $this->Form->input('email', array(
                                'class' => 'form-control border border-orange',
                                'placeholder' => __d('home', 'TXT_MESSAGE_SENDER_EMAIL'),
                            )); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <?php
                    echo $this->Form->input('description', array(
                        'class' => 'form-control border border-orange',
                        'type' => 'textarea',
                        'rows' => '3',
                        'placeholder' => __d('home', 'TXT_MESSAGE_SENDER_DESCRIPTION'),
                    )); ?>
                    <p class="no-error"></p>
                </div>
                <?php
                echo $this->Form->button(__d('home', 'TXT_MESSAGE_BTN_SEND'),
                    array(
                    'class' => 'btn',
                    'type' => 'submit',
                    'escape' => false,
                )); ?>
            <?php
            echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<?php
echo $this->Html->script('f/home/contact');

<nav aria-label="navigation">
    <ul class="pagination justify-content-end">
        <?php
        $data = $this->Paginator->params();
        ?>
        <li class="page-item <?php echo !$data['prevPage'] ? 'disabled' : '' ?>">
            <?php
            echo $this->Html->link(__('PAGINATION_TXT_PREVIOUS'), 'javascript:void(0);', array(
                'class' => 'page-link',
                'aria-disabled' => !$data['prevPage'] ? true : false,
                'data-page' => ($data['page'] - 1),
            )); ?>
        </li>
        <?php
        if ($data['pageCount']):
            for ($i = 1; $i <= $data['pageCount']; $i ++): ?>
                <li class="page-item <?php echo ($data['page'] == $i) ? 'active' : ''?>">
                    <?php
                    echo $this->Html->link($i, 'javascript:void(0);', array(
                        'class' => 'page-link',
                        'data-page' => $i,
                    )); ?>
                </li>
            <?php endfor;
        endif;
        ?>
        <li class="page-item <?php echo !$data['nextPage'] ? 'disabled' : '' ?>">
            <?php
            echo $this->Html->link(__('PAGINATION_TXT_NEXT'), 'javascript:void(0);', array(
                'class' => 'page-link btn-next',
                'aria-disabled' => !$data['nextPage'] ? true : false,
                'data-page' => ($data['page'] + 1),
            )); ?>
        </li>
    </ul>
</nav>

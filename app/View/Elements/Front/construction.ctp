<div class="bg-white text-center const-page">
    <div class="container">
        <div class="row">
            <div class="col">
                <?php echo $this->Html->image('construction.png', [
                    'class' => 'img-fluid',
                    'alt' => 'Under Construction',
                ]); ?>
            </div>
            <div class="w-100 mb-3"></div>
            <div class="col">
                <section class="m-md-auto m-sm-auto">
                    <h3 class="s-title txt-blue"><?php echo __('404_ERROR'); ?></h3>
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </section>
            </div>
        </div>
    </div>
</div>

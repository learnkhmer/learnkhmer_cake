<div class="row">
    <div class="col-12">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <?php
                    echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                    echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                ?>
            </ul>
        </nav>

        <script>

            $(document).ready(function() {
                $('.pagination li').addClass('page-item');
                $('.pagination li a').addClass('page-link');
            });
        </script>
    </div>
</div>
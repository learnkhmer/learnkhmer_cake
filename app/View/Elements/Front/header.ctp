
<?php if ($this->request->controller === "home"): ?>
    <header>
        <?php
        echo $this->Html->image('cloud_top.png', array(
            'alt' => 'cloud',
            'class' => 'cld-top img-fluid',
        )); ?>
        <div class="container">
            <div class="head-top justify-content-between row">
                <div class="col-6 col-md-3 d-inline-flex">
                    <?php
                    echo $this->Html->link('<i class="fas fa-bars"></i>', 'javascript:void(0);', array(
                        'class' => 'm-nav txt-yellow open-nav',
                        'escape' => false,
                    )); ?>
                    <?php
                    echo $this->Html->image('learn_khmer_logo.png', array(
                        'alt' => 'Learn Khmer Online',
                        'class' => 'img-logo img-fluid',
                        'url' => '/',
                    )); ?>
                </div>
                <div class="col-6 col-md-9 text-right">
                    <!--LANGUAGE-->
                    <?php
                    echo $this->element('Front/language'); ?>
                    <!--NAVIGATION-->
                    <?php
                    echo $this->element('Front/navigation'); ?>
                </div>
            </div>
        </div>
        <div class="banner-blog pt-4">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-5">
                        <?php
                        echo $this->Html->image('learning.png', array(
                            'alt' => 'student',
                            'class' => 'img-student img-fluid',
                        )); ?>
                    </div>
                    <div class="col-12 col-md-7">
                        <section id="section-info">
                            <h2 class="txt-blue f-koulen">
                                <?php
                                echo __d('home', 'TXT_LEARN_KHMER_ONLINE', array('html' => 'dafas')); ?>
                            </h2>
                            <p class="txt-yellow">
                                <?php
                                echo __d('home', 'TXT_THE_LANGUAGE'); ?>
                            </p>
                            <div class="row justify-content-center pt-3">
                                <div class="col-12 col-md-4 col-sm-4">
                                    <?php
                                    echo $this->Form->button(__d('home', 'TXT_STUDENT'), array(
                                        'type' => 'button',
                                        'class' => 'btn btn-lg btn-orange w-100',
                                    )); ?>
                                </div>
                                <div class="col-12 col-md-4 col-sm-4">
                                    <?php
                                    echo $this->Form->button(__d('home', 'TXT_TEACHER'), array(
                                        'type' => 'button',
                                        'class' => 'btn btn-lg btn-blue w-100',
                                        'disabled' => true,
                                    )); ?>
                                </div>
                                <div class="col-12 col-md-4 col-sm-4">
                                    <?php
                                    echo $this->Form->button(__d('home', 'TXT_PARENT'), array(
                                        'type' => 'button',
                                        'class' => 'btn btn-lg btn-green w-100',
                                        'disabled' => true,
                                    )); ?>
                                </div>
                            </div>
                            <div class="row justify-content-center pt-3">
                                <div class="col-12 col-md-12 col-sm-12">
                                    <?php
                                    echo $this->Html->link(__d('home', 'TXT_DONATE_FOR_KHMER_LANGUAGE'), 'https://www.givemn.org/organization/Ikare', array(
                                        'class' => 'btn btn-lg btn-donate-bg w-100',
                                        'target' => '__blank',
                                        'style' => 'color:#fff',
                                        'escape' => false,
                                    )); ?>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </header>
<?php else: ?>
    <div class="container pt-2 pb-2">
        <div class="head-top justify-content-between row">
            <div class="col-6 col-md-3 d-inline-flex">
                <?php
                echo $this->Html->link('<i class="fas fa-bars"></i>', 'javascript:void(0);', array(
                    'class' => 'm-nav txt-yellow open-nav',
                    'escape' => false,
                )); ?>
                <?php
                echo $this->Html->image('learn_khmer_logo.png', array(
                    'alt' => 'Learn Khmer Online',
                    'class' => 'img-logo img-fluid',
                    'url' => '/',
                )); ?>
            </div>
            <div class="col-6 col-md-9 text-right">
                <!--LANGUAGE-->
                <?php
                echo $this->element('Front/language'); ?>
                <!--NAVIGATION-->
                <?php
                echo $this->element('Front/navigation'); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

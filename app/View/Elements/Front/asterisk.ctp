<div class="asterisk-wrap">
    <a href="javascript:void(0);" class="download-icon">
        <span><i class="fa fa-download" aria-hidden="true"></i></span>
    </a>
    <ul class="d-inline-flex flex-wrap justify-content-end list-unstyled">
        <li>
            <a href="javascript:void(0);">
                <span class="text-warning"><i class="fa fa-star" aria-hidden="true"></i></span>
            </a>
        </li>
        <li>
            <a href="javascript:void(0);">
                <span class="text-warning"><i class="fa fa-star" aria-hidden="true"></i></span>
            </a>
        </li>
        <li>
            <a href="javascript:void(0);">
                <span class="text-warning"><i class="fa fa-star" aria-hidden="true"></i></span>
            </a>
        </li>
        <li>
            <a href="javascript:void(0);">
                <span class="text-warning"><i class="fa fa-star" aria-hidden="true"></i></span>
            </a>
        </li>
        <li>
            <a href="javascript:void(0);">
                <span class="text-secondary"><i class="fa fa-star" aria-hidden="true"></i></span>
            </a>
        </li>
    </ul>
</div>
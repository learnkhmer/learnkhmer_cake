<h3 class="s-title text-center pb-4">គាំទ្រពួក​យើង​ដើម្បី​ធ្វើ​ឲ​្យ​បានល្អជាងមុន</h3>
<p>ពួក​យើង​​ត្រូវ​ការ​ជំនួយ​ពី​អ្នក​ទាំអស់គ្នា​ ដើម្បី​បន្តរ​ការងារ​នេះ​ឲ្យ​​បាន​គង់វង្សយូរអង្វែង​​ និង​ធ្វើ​ឲ្យ​​អក្សរខ្មែរ​​ កាន់​តែ​ទូលំទូលាយជាងមុនថែម​ទៀត​។ សូមអរគុណ សម្រាប់ការ​ចែក​រំលែក ឬ ជំនួយ​របស់អ្នក។</p>
<div class="row">
    <div class="col-12 col-sm-5 col-md-5">
        <ul class="list-unstyled">
            <li class="contact-wrap">
                <div class="row">
                    <div class="col-2">
                        <span class="text-danger">
                            <i class="fas fa-map-marked-alt"></i>
                        </span>
                    </div>
                    <div class="col">
                        <p>ទីស្នាក់ការណ៍នៅសហរដ្ធអាមេរិក</p>
                        <p>1404 Concordia Ave St. Paul, MN 55104</p>
                    </div>
                </div>
            </li>
            <li class="contact-wrap">
                <div class="row">
                    <div class="col-2">
                        <span class="txt-purple">
                            <?php echo $this->Html->image('phone.png', array(
                                'alt' => 'Phone',
                                'id' => 'phone-icon',
                            )); ?>
                        </span>
                    </div>
                    <div class="col">
                        <p>លេខទូរស័ព្ទទំនាក់ទំនង</p>
                        <p>952-393-7802</p>
                    </div>
                </div>
            </li>
            <li class="contact-wrap">
                <div class="row">
                    <div class="col-2">
                        <span class="text-warning">
                            <i class="fas fa-envelope"></i>
                        </span>
                    </div>
                    <div class="col">
                        <p>សារអេឡិចត្រូនិក</p>
                        <p><a href="mailto:Helloygd1@gmail.com">Helloygd1@gmail.com</a></p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="col-12 col-sm-7 col-md-7">
        <div class="donate-form pt-0">
            <form>
                <div class="form-group">
                    <input type="email" class="form-control border border-orange" placeholder="អ៊ីម៉ែល">
                </div>
                <div class="form-group">
                    <div class="row justify-content-between">
                        <div class="col-3 col-md-3">
                            <button type="button" class="d-amount btn btn-donate-bg w-100" data-amount="10">$10</button>
                        </div>
                        <div class="col-3 col-md-3">
                            <button type="button" class="d-amount btn btn-donate-bg w-100" data-amount="20">$20</button>
                        </div>
                        <div class="col-3 col-md-3">
                            <button type="button" class="d-amount btn btn-donate-bg w-100" data-amount="30">$30</button>
                        </div>
                        <div class="col-3 col-md-3">
                            <button type="button" class="d-amount btn btn-donate-bg w-100" data-amount="50">$50</button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row justify-content-between">
                        <div class="col-3 col-md-3">
                            <button type="button" class="d-amount btn btn-donate-bg w-100" data-amount="100">$100</button>
                        </div>
                        <div class="col-3 col-md-3">
                            <button type="button" class="d-amount btn btn-donate-bg w-100" data-amount="150">$150</button>
                        </div>
                        <div class="col-3 col-md-3">
                            <button type="button" class="d-amount btn btn-donate-bg w-100" data-amount="200">$200</button>
                        </div>
                        <div class="col-3 col-md-3">
                            <button type="button" class="d-amount btn btn-donate-bg w-100" data-amount="0">ផ្សេងៗ</button>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="amount" class="amount form-control border border-orange"/>
                </div>
                <div class="b-dark-blue"></div>
                <div class="justify-content-center row">
                    <div class="col-12 col-md-4 col-sm-4">
                        <button type="submit" class="btn btn-block">យល់​ព្រម​</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-7">
                <p><?php echo __('TXT_COPYRIGHT'); ?></p>
            </div>
            <div class="col-12 col-sm-12 col-md-5 text-right">
                <p>
                    <?php echo __('FOOTER_MENU_TXT_FOLLOW_US'); ?>
                    <?php
                    echo $this->Html->image('footer_arrow.png', array(
                        'alt' => 'arrow',
                        'class' => 'img-fluid',
                        'id' => 'ft-arrow',
                    )); ?>
                    <?php
                    echo $this->Html->link('<span><i class="fab fa-facebook-square"></i></span>', '#', array(
                        'escape' => false,
                    )); ?>
                    <?php
                    echo $this->Html->link('<span><i class="fab fa-twitter-square"></i></span>', '#', array(
                        'escape' => false,
                    )); ?>
                    <?php
                    echo $this->Html->link('<span><i class="fab fa-google-plus-square"></i></span>', '#', array(
                        'escape' => false,
                    )); ?>
                    <?php
                    echo $this->Html->image('go_to_top.png', array(
                        'alt' => 'arrow up',
                        'class' => 'img-fluid go-top',
                    )); ?>
                </p>
            </div>
        </div>
    </div>
</footer>
<!--Navigation Modal-->
<div id="nav-popup"></div>

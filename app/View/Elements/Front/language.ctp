<ul  class="list-unstyled d-inline-block f-koulen lange-wrap">
    <li class="list-inline-item">
        <a href="<?php echo $this->Html->url('/languages/to?local=eng'); ?>">
            <?php
            echo $this->Html->image('en_lang.png', array(
                'alt' => 'English',
            )); ?>
            English
        </a>
    </li>
    <li class="list-inline-item">
        <a href="<?php echo $this->Html->url('/languages/to?local=kh'); ?>">
            <?php
            echo $this->Html->image('kh_lang.png', array(
                'alt' => 'Khmer',
            )); ?>
            <?php echo __('LANG_TXT_KHMER'); ?>
        </a>
    </li>
</ul>
<div class="clearfix"></div>

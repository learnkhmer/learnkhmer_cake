<section class="footer-menu">
    <div class="row">
        <div class="col-12 col-sm-3 col-md-3">
            <h4 class="mb-0 pb-2 txt-yellow f-koulen">
                <?php echo __('FOOTER_MENU_TXT_ABOUT_US'); ?>
            </h4>
            <ul class="list-unstyled">
                <li>
                    <a href="<?php echo $this->Html->url('/#mission-vision'); ?>">
                        <span><i class="fas fa-caret-right"></i></span>
                        <?php echo __('FOOTER_MENU_TXT_GOLD_AND_MISSION'); ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url('/pages/report'); ?>">
                        <span><i class="fas fa-caret-right"></i></span>
                        <?php echo __('FOOTER_MENU_TXT_REPORT'); ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url('/pages/about'); ?>">
                        <span><i class="fas fa-caret-right"></i></span>
                        <?php echo __('FOOTER_MENU_TXT_ABOUT_US'); ?>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-12 col-sm-3 col-md-3">
            <h4 class="mb-0 pb-2 txt-yellow f-koulen">
                <?php echo __('FOOTER_MENU_TXT_NEWS'); ?>
            </h4>
            <ul class="list-unstyled">
                <li>
                    <a href="<?php echo $this->Html->url('/pages/question'); ?>">
                        <span><i class="fas fa-caret-right"></i></span>
                        <?php echo __('FOOTER_MENU_TXT_QA'); ?>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#term-modal" data-whatever="@fat">
                        <span><i class="fas fa-caret-right"></i></span>
                        <?php echo __('FOOTER_MENU_TXT_CONDITION'); ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url('/pages/resource'); ?>">
                        <span><i class="fas fa-caret-right"></i></span>
                        <?php echo __('FOOTER_MENU_TXT_RESOURCE'); ?>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-12 col-sm-3 col-md-3">
            <h4 class="mb-0 pb-2 txt-yellow f-koulen">
                <?php echo __('FOOTER_MENU_TXT_CONTRIBUTION'); ?>
            </h4>
            <ul class="list-unstyled">
                <li>
                    <a href="<?php echo $this->Html->url('https://www.givemn.org/organization/Ikare'); ?>" target="_blank">
                        <span><i class="fas fa-caret-right"></i></span>
                        <?php echo __('FOOTER_MENU_TXT_DONATION'); ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $this->Html->url('/#contact'); ?>">
                        <span><i class="fas fa-caret-right"></i></span>
                        <?php echo __('FOOTER_MENU_TXT_CONTACT'); ?>
                    </a>
                </li>
                <!-- <li>
                    <a href="<?php echo $this->Html->url('/#members'); ?>">
                        <span><i class="fas fa-caret-right"></i></span>
                        <?php echo __('FOOTER_MENU_TXT_OUR_MEMBER'); ?>
                    </a>
                </li> -->
                <li>
                    <a href="http://source.learnkhmer.online/files/note/checklist.pdf">
                        <span><i class="fas fa-caret-right"></i></span>
                        <?php echo __('1-Man Band'); ?>
                    </a>
                </li>
                
            </ul>
        </div>
        <div class="col-12 col-sm-3 col-md-3">
            <h4 class="mb-0 pb-2 txt-yellow text-center f-koulen">
                <?php echo __('FOOTER_MENU_TXT_VISITOR'); ?>
            </h4>
            <p class="mb-0 txt-blue text-center view-counter"><?php $this->Counter->_constructDB(); ?></p>
        </div>
    </div>
</section>

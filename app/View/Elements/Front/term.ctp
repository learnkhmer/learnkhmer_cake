<div class="modal fade const-modal" id="term-modal" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom-0 position-relative">
                <div class="close-modal" data-dismiss="modal" aria-label="Close">
                    <span><i class="fas fa-times-circle"></i></span>
                </div>
            </div>
            <div class="modal-body">
                <h5 class="border-bottom f-koulen pb-2 text-center">
                    <?php echo __('TXT_CONIDTION'); ?>
                </h5>
                <?php echo $settingLang['term']['txt_'.Configure::read('Config.language')]; ?>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0);"  class="f-koulen term-ok" data-dismiss="modal"><?php echo __('TXT_AGREE'); ?></a>
            </div>
        </div>
    </div>
</div>
<div class="member-blog" id="members">
    <div class="container">
        <h3 class="s-title text-center pb-4">
            <?php
            echo __d('home', 'TXT_OUR_TEAM'); ?>
        </h3>
        <div class="row justify-content-center slide-wrap">
            <?php if ($members) : ?>
                <?php foreach ($members as $key => $value) : ?>
                    <div class="col-4 col-sm-3 col-md-4">
                        <div class="member-wrap">
                            <?php
                            echo $this->Html->image(MEMBER_PATH . '/' . $value['Member']['image'], array(
                                'alt' => '',
                                'url' => $value['Member']['link'],
                                'class' => 'img-fluid',
                            )); ?>
                            <section>
                                <h4 style="font-size: 1.3rem;"><?php echo $value['Member']['name'] ?></h4>
                                <p><?php echo $value['Member']['position'] ?></p>
                            </section>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div style="text-align: center;">
            <a href="<?php echo $this->Html->url('/pages/member'); ?>" class="btn btn-outline-primary">More members</a>
        </div>
    </div>
</div>
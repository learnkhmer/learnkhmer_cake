
<div class="head-s-list b-bottom-blue d-flex">
    <h4 class="mb-0 f-koulen txt-blue">
        <?php
        echo __d('worksheet', 'CATEGORY'); ?>
    </h4>
</div>
<div class="row pt-1">
    <div class="col cat-desktop">
        <ul class="list-unstyled book-type accordion" id="category-list">
            <?php
            $this->Category->initialize($categories);
            $this->Category->show();
            ?>
        </ul>
    </div>
    <div class="col cat-mobile">
        <?php
        echo $this->Form->select('category_id', $this->Category->getOption(), array(
            'class' => 'form-control',
            'id' => 'category-id',
            'empty' => array('' => 'Choose category'),
            'esape' => false,
        )); ?>
    </div>
</div>
<?php if (false): ?>
<div class="row my-3">
    <div class="col">
        <?php
        if ($category['CategoryDetail']['download_url']) {
            echo $this->Html->link(__d('worksheet', 'DOWNLOAD_ALL'), $category['CategoryDetail']['download_url'], array(
                'target' => '_blank',
                'class' => 'btn btn-block bg-sea',
            ));
        } else {
            echo $this->Html->link(__d('worksheet', 'DOWNLOAD_ALL'), 'javascript:void(0)', array(
                'class' => 'btn btn-block btn-outline-danger',
                'disabled' => true,
            ));
        }
        ?>
    </div>
</div>
<?php endif; ?>
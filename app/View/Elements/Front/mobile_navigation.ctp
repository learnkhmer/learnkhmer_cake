<div id="mySidenav" class="sidenav">
    <ul class="list-unstyled">
        <li>
            <a href="javascript:void(0)" class="closebtn close-nav">&times;</a>
        </li>
        <li>
            <?php
            echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/', array()); ?>
        </li>
        <li>
            <?php
            echo $this->Html->link(__('FRONTEND_MENU_TXT_BEGINNER'), '/beginners/animation/1', array(
                'class' => ($this->request->controller === 'beginners') ? 'm-active' : '',
            )); ?>
        </li>
        <li>
            <?php
            echo $this->Html->link(__('FRONTEND_MENU_TXT_WORKSHEET'), '/work_sheets/type/13', array(
                'class' => ($this->request->controller === 'work_sheets') ? 'm-active' : '',
            )); ?>
        </li>
        <li>
            <?php
            echo $this->Html->link(__('FRONTEND_MENU_TXT_READING'), '/readings', array(
                'target' => '_blank',
                'class' => ($this->request->controller === 'readings') ? 'm-active' : '',
            )); ?>
        </li>
        <li>
            <?php
            echo $this->Html->link(__('FRONTEND_MENU_TXT_MUSIC'), '/songs', array(
                'class' => ($this->request->controller === 'songs') ? 'm-active' : '',
            )); ?>
        </li>
        <li>
            <?php
            echo $this->Html->link(__('Events'), 'http://event.learnkhmer.online/', array()); ?>
        </li>
    </ul>
</div>
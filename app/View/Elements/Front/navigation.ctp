<nav class="d-nav navbar navbar-expand-lg navbar-light float-right p-0 f-koulen">
<?php
    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/', array(
        'class' => ($this->request->controller === 'home') ? 'navbar-brand m-active' : 'navbar-brand txt-menu-cl',
        'escape' => false,
    )); ?>
    <?php echo $this->Form->button('<span class="navbar-toggler-icon"></span>', [
        'type' => 'button',
        'class' => 'navbar-toggler',
        'data-toggle' => 'collapse',
        'data-target' => '#navbarNavAltMarkup',
        'aria-controls' => 'navbarNavAltMarkup',
        'aria-expanded' => 'false',
        'aria-label' => 'Toggle navigation',
        'escape' => false,
    ]); ?>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <?php
            $class1 = 'nav-item nav-link m-active';
            $class2 = 'nav-item nav-link txt-menu-cl';
            echo $this->Html->link(__('FRONTEND_MENU_TXT_BEGINNER') . ' <span class="sr-only">(current)</span>', '/beginners/animation/1', array(
                'class' => ($this->request->controller === 'beginners') ? $class1 : $class2,
                'escape' => false,
            ));
            echo $this->Html->link(__('FRONTEND_MENU_TXT_WORKSHEET'), '/work_sheets/type/13', array(
                'class' => ($this->request->controller === 'work_sheets') ? $class1 : $class2,
            ));
            echo $this->Html->link(__('FRONTEND_MENU_TXT_READING'), '/readings', array(
                'class' => ($this->request->controller === 'readings') ? $class1 : $class2,
            ));
            echo $this->Html->link(__('FRONTEND_MENU_TXT_MUSIC'), '/songs', array(
                'class' => ($this->request->controller === 'songs') ? $class1 : $class2,
            ));
            echo $this->Html->link(__('Events'), 'http://event.learnkhmer.online', array(
                'target' => '_blank',
                'class' => $class2,
            )); ?>
        </div>
    </div>
</nav>
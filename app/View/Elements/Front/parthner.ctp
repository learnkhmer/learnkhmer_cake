<section class="parthner-blog">
    <h3 class="s-title text-center pb-4">
        <?php
        echo __d('home', 'TXT_OUR_PARTNER'); ?>
    </h3>
    <div class="parthner-wrap">
        <div class="row justify-content-between p-slide">
            <div class="col-2 p-item">
                <?php
                echo $this->Html->image('parthner/kare.png', array(
                    'alt' => 'Ikare',
                    'class' => 'img-fluid',
                )); ?>
            </div>
            <div class="col-2 p-item">
                <?php
                echo $this->Html->image('parthner/learn_khmer.png', array(
                    'alt' => 'Ikare',
                    'class' => 'img-fluid',
                )); ?>
            </div>
            <div class="col-2 p-item">
                <?php
                echo $this->Html->image('parthner/ikare.jpg', array(
                    'alt' => 'Ikare',
                    'class' => 'img-fluid',
                )); ?>
            </div>
            <div class="col-2 p-item">
                <?php
                echo $this->Html->image('parthner/plan.jpg', array(
                    'alt' => 'Ikare',
                    'class' => 'img-fluid',
                )); ?>
            </div>
            <div class="col-2 p-item">
                <?php
                echo $this->Html->image('parthner/makhat.jpg', array(
                    'alt' => 'Ikare',
                    'class' => 'img-fluid',
                )); ?>
            </div>
        </div>
    </div>
</section>

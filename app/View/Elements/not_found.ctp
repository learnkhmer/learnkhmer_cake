<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo $error; ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('ទំព័រដើម', '/'); ?>
                </li>
                
            </ul>
        </div>
    </div>
</section>
<div class="bg-white text-center error-page">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="m-md-auto m-sm-auto">
                    <?php echo $this->Html->image('404.jpg', [
                        'class' => 'img-fluid',
                        'alt' => '404 Page',
                    ]); ?>
                    <p class="txt-blue f-koulen">គេ​ហ​ទំ​ព័រ​នេះ​មិន​មាន​នោះ​ទេ​!</p>
                    <?php
                    echo $this->Html->link('ទំព័រដើម', '/'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

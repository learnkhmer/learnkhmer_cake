<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Learn Khmer Online is accessible to everyone through an online forum.Learn Khmer Online a project will help revolutionize the learning experience for everyone who wishes to learn the language.Learn Khmer Online website is to preserve and enrich Khmer language. Learn Khmer Online will use innovative tools and methodologies that make it easier and more efficient way to learn the language. It will provide learners with more accurate in pronunciation and dialect.Learn Khmer Online project with the hope that the Khmer youth of today will take full advantage of this online forum to learn the language and pass it on to the next generation.">
        <meta name="keyword" content="Khmer Leaning, Cambodia Leanring, Khmer Online Leaning Tools, First Leanring Online Cambodia, Khmer-USA learning online tools">
        <title><?php echo $this->fetch('title'); ?></title>
        <!--FAVOURITE ICON-->
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $this->Html->url('/'); ?>aassets/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $this->Html->url('/'); ?>favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->Html->url('/'); ?>favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $this->Html->url('/'); ?>favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->Html->url('/'); ?>favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo $this->Html->url('/'); ?>favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo $this->Html->url('/'); ?>favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <?php
        echo $this->Html->css(array(
            'fonts',
            '/js/bookblock/css/jquery.jscrollpane.custom.css',
            '/js/bookblock/css/bookblock.css',
            '/js/bookblock/css/custom.css',
            '/js/player/css/mediaelementplayer.css',
        )); ?>
    </head>
    <body>
        <div id="container" class="container">
            <div class="menu-panel">
                <h3>តារាងមាតិកា</h3>
                <ul id="menu-toc" class="menu-toc">
                    <?php
                    foreach ($data as $key => $value) : ?>
                        <li>
                            <?php
                            echo $this->Html->link($value['Reading']['title'], '#item' . $value['Reading']['id']); ?>
                        </li>
                    <?php
                    endforeach; ?>
                </ul>
                <div style="display: none;">
                    <a href="http://tympanus.net/Development/AudioPlayer/">&larr; Previous Demo: Responsive Audio Player</a>
                    <a href="http://tympanus.net/codrops/?p=12795">Back to the Codrops Article</a>
                </div>
            </div>

            <div class="bb-custom-wrapper">
                <div id="bb-bookblock" class="bb-bookblock">
                    <?php
                    foreach ($data as $key => $value) : ?>
                    <div class="bb-item" id="item<?php echo $value['Reading']['id']; ?>">
                        <div class="content">
                            <div class="scroller">
                                <h2 class="f-hanuman"><?php echo $value['Reading']['title']; ?></h2>
                                <?php
                                echo $this->Sound->render($value); ?>
                                <div class="text-lesson f-hanuman">
                                    <?php
                                    foreach ($value['ReadingItem'] as $key1 => $value1) {
                                        if ($value1['is_newline']) {
                                            echo $this->Html->tag('span', '<br />', array(
                                                'data-start' => '0',
                                                'data-end' => '0',
                                            ));
                                        } else {
                                            echo $this->Html->tag('span', h($value1['text']), array(
                                                'data-start' => h($value1['start']),
                                                'data-end' => h($value1['end']),
                                                'class' => $value1['is_space'] ? 'reading-space' : '',
                                            ));
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    endforeach;?>
                </div>
                <nav>
                    <span id="bb-nav-prev">&larr;</span>
                    <span id="bb-nav-next">&rarr;</span>
                </nav>
                <span id="tblcontents" class="menu-button">Table of Contents</span>
                <span class="bb-nav-close"><i class="fa fa-times"></i></span>
                <span class="bb-sound"><i class="fa fa-volume-up" aria-hidden="true"></i></span>
                <audio src="http://file.learnkhmer.online/files/readings/sound/18-01-12-10-10-28151576982871.mp3" preload="auto" class="sound-control" id="sound-control-id"></audio>
            </div>
        </div><!-- /container -->
        <?php
        echo $this->Html->script(array(
            '//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
            'bookblock/js/modernizr.custom.79639.js',
            'bookblock/js/jquery.mousewheel.js',
            'bookblock/js/jquery.jscrollpane.min.js',
            'bookblock/js/jquerypp.custom.js',
            'bookblock/js/jquery.bookblock.js',
            'bookblock/js/page.js',
            'player/js/popcorn.js',
            'player/js/mediaelement-and-player.min.js',
            'player/js/reading.js',
        ));
        ?>
        <script>
            $(function() {
                Page.init();
                Reading.init();
            });
        </script>
    </body>
</html>

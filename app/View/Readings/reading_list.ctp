<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_READING'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_READING'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY'); ?></h4>
                </div>
                <div class="row pt-1">
                    <div class="col">
                        <ul class="list-unstyled book-type" id="cat-list">
                            <?php
                            $arrowRight = '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span>&nbsp;';
                            ?>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_G_1_12'), '/readings', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_NOVEL'), '/novels', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_POEM'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_SLOGAN'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_FAIRY_TALE'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_OTHER'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY_SLOGAN'); ?></h4>
                </div>
                <div class="lst-poetry">
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image('poetry1.jpg', [
                            'class' => 'img-fluid',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image('poetry2.jpg', [
                            'class' => 'img-fluid',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9 grades-list">
                <div class="head-s-list b-bottom-blue d-flex mb-3">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY_G_1_12'); ?></h4>
                </div>
                <div class="row">
                    <div class="col-12 px-2 py-2">
                        <a href="http://learnkhmer.online/">
                            <img class="img-fluid img-center" alt="Learn Khmer Online" src="<?php echo $this->Html->url('/img/worksheet/banner/banner_consonant_ads.png'); ?>">
                        </a>
                    </div>
                </div>
                <div class="row">
                    <?php
                    foreach ($data as $key => $value) : ?>
                    <div class="col-6 col-md-4 px-2 py-2">
                        <!-- <div class="card item" data-id="<?php echo $value['Reading']['id']; ?>">
                            <div class="card-body">
                                <p class="card-text text-center"><?php echo $value['Reading']['title']; ?></p>
                            </div>
                        </div> -->

                        <div class="card border-primary mb-3 item" data-id="<?php echo $value['Reading']['id']; ?>" style="border: 1px dashed #007bff">
                            <div class="card-header mbr-title mbr-fonts-style mbr-bold mbr-black display-5 text-center f-koulen" style="background: #277eb5; color: #fff">ភាសារខ្មែរ-អានសរសេរ</div>
                            <div class="card-body text-primary text-center">
                                <p class="card-text"><?php echo $value['Reading']['title']; ?></p>
                            </div>
                            <div class="card-fotter text-primary text-right">
                                <?php if ($value['Reading']['level_title']) : ?>
                                <div class="page-number">
                                    <?php __d('reading', 'TXT_PAGE'); ?> <?php  $value['Reading']['page']; ?>
                                     <?php echo $value['Reading']['level_title']; ?>

                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <?php 
                    endforeach; ?>
                </div>
                <div class="box-footer clearfix">
                    <?php
                    echo $this->element('/Front/pagination2'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
.page-number {
    border-radius: 0.2em;
    border-top: 1px dashed;
    border-left: 1px dashed;
    width: 40%;
    float: right;
    text-align: center;
    /* background-color: #277eb5; */
    padding-top: 3px;
}

.item {
    border: 1px solid #277eb5;
}

.item:hover {
    cursor: pointer;
    background-color: #277eb4;
    
}

.item:hover p, .item:hover .page-number {
    /* text-decoration: underline; */
    color: #fff !important;
}

.item h3 {
    font-size: 0.9rem;
}

.item p {
    padding-top: 20px;
    padding-bottom: 20px;
}
</style>
<?php
echo $this->Html->script([
    '//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
    'jquery.matchHeight-min',
]);
echo $this->html->css(['//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css']);
?>
<script>
    $(function() {
        $('.item').matchHeight({});
        $('.item').on('click', function() {
            window.location.href = BASE_URL + 'readings/view/' + $(this).data('id');
        });
    });
</script>
<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_READING'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_READING'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY'); ?></h4>
                </div>
                <div class="row pt-1">
                    <div class="col">
                        <ul class="list-unstyled book-type" id="cat-list">
                            <?php
                            $arrowRight = '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span>&nbsp;';
                            ?>
                            <li>
                                <a href="#sound" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sport" class="collapsed">
                                    <span class="txt-cl-blue">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                                    </span>
                                    <?php echo __d('reading', 'CATEGORY_G_1_12'); ?>
                                </a>
                                <ul class="pl-4 collapse list-unstyled" id="sound" data-parent="#cat-list">
                                    <?php
                                    for ($i = 1; $i <= 12; $i++) : ?>
                                        <li class="">
                                            <?php
                                            echo $this->Html->link(
                                                '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span> ថ្នាក់​ទី ' .$i,
                                                '/readings/index/' . $i,
                                                array('escape' => false)
                                            ); ?>
                                        </li>
                                    <?php endfor; ?>
                                </ul>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_NOVEL'), '/readings/novel', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <!-- <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_POEM'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_SLOGAN'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_FAIRY_TALE'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_OTHER'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li> -->
                        </ul>
                    </div>
                </div>
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY_SLOGAN'); ?></h4>
                </div>
                <div class="lst-poetry">
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image('poetry1.jpg', [
                            'class' => 'img-fluid',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image('poetry2.jpg', [
                            'class' => 'img-fluid',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9 grades-list">
                <div class="head-s-list b-bottom-blue d-flex mb-3">
                    <h4 class="mb-0 f-koulen txt-blue title-icon-sound"><span class="bb-sound"><?php echo __d('reading', 'CATEGORY_NOVEL'); ?></span></h4>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div id="demo" class="carousel slide" data-interval="false" data-ride="carousel">
            
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                <?php
                                echo $this->Html->image('reading/TheSoundOfaPiano/TheSoundOfaPiano-001.jpg', [
                                    // 'class' => 'img-fluid',
                                    'alt' => 'Poetry',
                                    'width' => '1200px'
                                ]); ?>  
                                </div>
                                <div class="carousel-item">
                                    <?php
                                echo $this->Html->image('reading/TheSoundOfaPiano/TheSoundOfaPiano-002.jpg', [
                                    // 'class' => 'img-fluid',
                                    'alt' => 'Poetry',
                                    'width' => '1200px'
                                ]); ?> 
                                </div>
                                <div class="carousel-item">
                                <?php
                                echo $this->Html->image('reading/TheSoundOfaPiano/TheSoundOfaPiano-003.jpg', [
                                    // 'class' => 'img-fluid',
                                    'alt' => 'Poetry',
                                    'width' => '1200px'
                                ]); ?> 
                                </div>
                            </div>
                            <div class="gggg">
                                <input type="text" class="form-control" />
                            </div>
                            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a class="carousel-control-next" href="#demo" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<style>
  /* Make the image fully responsive */
  .carousel-inner img {
    width: 100%;
    height: 100%;
  }
  
  .carousel-control-prev, .carousel-control-next {
      background-color: #277eb5;
  }

  .carousel-indicators {
    background-color: #277eb5;
  }
  </style>
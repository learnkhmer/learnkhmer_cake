<div class="card">
    <div class="card-body">
        <?php
        echo $this->Form->create('Reading', array(
            'type' => 'get',
        )); ?>
        <div class="row">
            <div class="col-md-3 col-sm-4 col-6">
                <div class="form-group">
                    <?php
                    echo $this->Form->text('search', array(
                        'class' => 'form-control',
                        'value' => $this->request->query('search'),
                        'placeholder' => __('COMMON_TXT_SEARCH'),
                    )); ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-4 col-3">
                <div class="form-group">
                    <?php
                    echo $this->Form->input('search', array(
                        'class' => 'btn btn-primary',
                        'type' => 'button',
                        'placeholder' => __('COMMON_TXT_SEARCH'),
                        'label' => false,
                    )); ?>
                </div>
            </div>
            <div class="col-md-6 col-sm-4 col-3">
                <div class="form-group">
                    <?php
                    echo $this->Html->link('Create', array(
                        'action' => 'create',
                        'prefix' => 'admin',
                    ), array(
                        'class' => 'btn btn-primary float-right',
                        'escape' => false,
                    )); ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<div class="card">
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Sub Title</th>
                    <th>Level</th>
                    <th>Page</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $key => $value) : ?>
                    <tr>
                        <td><?php echo (($this->params['paging']['Reading']['page'] * LIMIT) - LIMIT + 1) + $key; ?></td>
                        <td><?php echo $value['Reading']['title']; ?></td>
                        <td><?php echo $value['Reading']['sub_title']; ?></td>
                        <td><?php echo $value['Reading']['level']; ?></td>
                        <td><?php echo $value['Reading']['page']; ?></td>
                        <td class="project-actions text-right">
                            <?php
                            echo $this->Html->link('<i class="fas fa-folder"></i> View', '/admin/reading_items/view/' . $value['Reading']['id'], [
                                'class' => 'btn btn-primary btn-sm',
                                'escape' => false,
                            ]); ?>

                            <?php
                            echo $this->Html->link('<i class="fas fa-pencil-alt"></i> Edit', '/admin/readings/edit/' . $value['Reading']['id'], [
                                'class' => 'btn btn-info btn-sm',
                                'escape' => false,
                            ]); ?>

                            <?php
                            echo $this->Form->postLink('<i class="fas fa-trash"></i> Delete', '/admin/readings/delete/' . $value['Reading']['id'], [
                                'class' => 'btn btn-danger btn-sm',
                                'escape' => false,
                                'confirm' => 'Are you sure you want to delete?',
                            ]); ?>
                        </td>
                    </tr>
                <?php
                endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <?php
        echo $this->element('/Backend/pagination'); ?>
    </div>
</div>
<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_READING'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_READING'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY'); ?></h4>
                </div>
                <div class="row pt-1">
                    <div class="col">
                        <ul class="list-unstyled book-type" id="cat-list">
                            <?php
                            $arrowRight = '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span>&nbsp;';
                            ?>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_G_1_12'), '/readings', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_RTB'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_NOVEL'), '/novels', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_POEM'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_SLOGAN'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_FAIRY_TALE'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_OTHER'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY_SLOGAN'); ?></h4>
                </div>
                <div class="lst-poetry">
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image('poetry1.jpg', [
                            'class' => 'img-fluid',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image('poetry2.jpg', [
                            'class' => 'img-fluid',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9 grades-list">
                <div class="head-s-list b-bottom-blue d-flex mb-3">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY_G_1_12'); ?></h4>
                </div>

                <div class="row pt-1">
                    <div class="col-12 mb-3">
                        <a href="http://learnkhmer.online/">
                            <img class="img-fluid img-center" alt="Learn Khmer Online" src="<?php echo $this->Html->url('/img/worksheet/banner/banner_consonant_ads.png'); ?>">
                        </a>
                    </div>
                </div>
                <div class="row">
                    <?php
                    for ($i = 1; $i <= 12; $i++) :
                    if ($i > 8) {
                        continue;
                    }
                    ?>
                    <div class="col-6 col-md-4">
                        <div class="grade-wrap">
                            <?php
                            $url = '/readings/index/1';
                            $class = 'img-fluid';
                            if ($i > 1) {
                                $url = 'javascript:void(0);';
                                // $class = 'img-fluid item-disable';
                            }
                            echo $this->Html->image('readings/book-grade-'.$i.'.jpg', [
                                'class' => $class,
                                'alt' => 'Book Cover',
                                'url' => $url,
                            ]); ?>
                        </div>
                    </div>
                    <?php 
                    endfor; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<style>

.item-disable {
    filter: grayscale(1);
}
</style>
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Learn Khmer Online is accessible to everyone through an online forum.Learn Khmer Online a project will help revolutionize the learning experience for everyone who wishes to learn the language.Learn Khmer Online website is to preserve and enrich Khmer language. Learn Khmer Online will use innovative tools and methodologies that make it easier and more efficient way to learn the language. It will provide learners with more accurate in pronunciation and dialect.Learn Khmer Online project with the hope that the Khmer youth of today will take full advantage of this online forum to learn the language and pass it on to the next generation.">
        <meta name="keyword" content="Khmer Leaning, Cambodia Leanring, Khmer Online Leaning Tools, First Leanring Online Cambodia, Khmer-USA learning online tools">
        <title><?php echo $this->fetch('title'); ?></title>
        <!--FAVOURITE ICON-->
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $this->Html->url('/'); ?>aassets/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $this->Html->url('/'); ?>favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->Html->url('/'); ?>favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $this->Html->url('/'); ?>favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->Html->url('/'); ?>favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo $this->Html->url('/'); ?>favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo $this->Html->url('/'); ?>favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        
        <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jQuery/jquery-1.4.4.min.js"></script>
		<script src="<?php echo $this->Html->base; ?>/js/moleskine_notebook/booklet/jquery.easing.1.3.js" type="text/javascript"></script>
        <script src="<?php echo $this->Html->base; ?>/js/moleskine_notebook/booklet/jquery.booklet.1.1.0.min.js" type="text/javascript"></script>
        
        <link href="<?php echo $this->Html->base; ?>/js/moleskine_notebook/booklet/jquery.booklet.1.1.0.css" type="text/css" rel="stylesheet" media="screen" />
		<link rel="stylesheet" href="<?php echo $this->Html->base; ?>/js/moleskine_notebook/css/style.css" type="text/css" media="screen"/>
    
        <?php
        echo $this->Html->css(array(
            'fonts',
            '/js/player/css/mediaelementplayer.css',
        )); ?>
    </head>

        <script src="<?php echo $this->Html->base; ?>/js/moleskine_notebook/cufon/cufon-yui.js" type="text/javascript"></script>
		<script src="<?php echo $this->Html->base; ?>/js/moleskine_notebook/cufon/ChunkFive_400.font.js" type="text/javascript"></script>
		<script src="<?php echo $this->Html->base; ?>/js/moleskine_notebook/cufon/Note_this_400.font.js" type="text/javascript"></script>
    </head>


    <body>
	<div id="custom-menu"></div>
		<div class="book_wrapper">
			<a id="next_page_button"></a>
			<a id="prev_page_button"></a>
			<div id="loading" class="loading">Loading pages...</div>
			<div id="mybook" style="display:none;">
				<div class="b-load">
                <?php
                    foreach ($data as $key => $value) : ?>
                    <div title="<?php echo $value['Reading']['title']; ?>" class="bb-item" id="item<?php echo $value['Reading']['id']; ?>">
                    <?php
                    echo $this->Sound->render($value); ?>
                    <div>
						<h1 class="f-hanuman"><?php echo $value['Reading']['title']; ?></h1>
						<div class="text-lesson f-hanuman">
                            <?php
                                foreach ($value['ReadingItem'] as $key1 => $value1) {
                                    if ($value1['is_newline']) {
                                        echo $this->Html->tag('span', '<br />', array(
                                            'data-start' => '0',
                                            'data-end' => '0',
                                        ));
                                    } else {
                                        echo $this->Html->tag('span', h($value1['text']), array(
                                            'data-start' => h($value1['start']),
                                            'data-end' => h($value1['end']),
                                            'class' => $value1['is_space'] ? 'reading-space' : '',
                                        ));
                                    }
                                }
                            ?>
                        </div>
					</div>
                </div>
                    <?php
                    endforeach;?>
					<div>
						<img src="<?php echo $this->Html->base; ?>/js/moleskine_notebook/images/14.jpg" alt="" />
						<p>Thank you for reading!</p>
						<a href="<?php echo $this->Html->url('/'); ?>" target="_blank" class="article">Go back</a>
					</div>
				</div>
			</div>
		</div>
        <div>
            <span class="reference">
				<a href="<?php echo $this->Html->url('/'); ?>" target="_blank">Go Back</a>
            </span>
        </div>
		
        <?php
        echo $this->Html->script(array(
            'player/js/popcorn.js',
            'player/js/mediaelement-and-player.min.js',
            'player/js/reading.js',
        ));
        ?>

        <!-- The JavaScript -->

        <script type="text/javascript">
			$(function() {
				var $mybook 		= $('#mybook');
				var $bttn_next		= $('#next_page_button');
				var $bttn_prev		= $('#prev_page_button');
				var $loading		= $('#loading');
				var $mybook_images	= $mybook.find('img');
				var cnt_images		= $mybook_images.length;
				var loaded			= 0;
				//preload all the images in the book,
				//and then call the booklet plugin

				$mybook_images.each(function(){
					var $img 	= $(this);
					var source	= $img.attr('src');
					$('<img/>').load(function(){
						++loaded;
						if(loaded == cnt_images){
							$loading.hide();
							$bttn_next.show();
							$bttn_prev.show();
							$mybook.show().booklet({
								name:               null,                            // name of the booklet to display in the document title bar
								width:              800,                             // container width
								height:             500,                             // container height
								speed:              600,                             // speed of the transition between pages
								direction:          'LTR',                           // direction of the overall content organization, default LTR, left to right, can be RTL for languages which read right to left
								startingPage:       0,                               // index of the first page to be displayed
								easing:             'easeInOutQuad',                 // easing method for complete transition
								easeIn:             'easeInQuad',                    // easing method for first half of transition
								easeOut:            'easeOutQuad',                   // easing method for second half of transition

								closed:             true,                           // start with the book "closed", will add empty pages to beginning and end of book
								closedFrontTitle:   null,                            // used with "closed", "menu" and "pageSelector", determines title of blank starting page
								closedFrontChapter: null,                            // used with "closed", "menu" and "chapterSelector", determines chapter name of blank starting page
								closedBackTitle:    null,                            // used with "closed", "menu" and "pageSelector", determines chapter name of blank ending page
								closedBackChapter:  null,                            // used with "closed", "menu" and "chapterSelector", determines chapter name of blank ending page
								covers:             false,                           // used with  "closed", makes first and last pages into covers, without page numbers (if enabled)

								pagePadding:        10,                              // padding for each page wrapper
								pageNumbers:        true,                            // display page numbers on each page

								hovers:             false,                            // enables preview pageturn hover animation, shows a small preview of previous or next page on hover
								overlays:           false,                            // enables navigation using a page sized overlay, when enabled links inside the content will not be clickable
								tabs:               false,                           // adds tabs along the top of the pages
								tabWidth:           60,                              // set the width of the tabs
								tabHeight:          20,                              // set the height of the tabs
								arrows:             false,                           // adds arrows overlayed over the book edges
								cursor:             'pointer',                       // cursor css setting for side bar areas

								hash:               true,                           // enables navigation using a hash string, ex: #/page/1 for page 1, will affect all booklets with 'hash' enabled
								keyboard:           false,                            // enables navigation with arrow keys (left: previous, right: next)
								next:               $bttn_next,          			// selector for element to use as click trigger for next page
								prev:               $bttn_prev,          			// selector for element to use as click trigger for previous page

								menu: 				'#custom-menu',                           // selector for element to use as the menu area, required for 'pageSelector'
								pageSelector:       true,                           // enables navigation with a dropdown menu of pages, requires 'menu'
								chapterSelector:    false,                           // enables navigation with a dropdown menu of chapters, determined by the "rel" attribute, requires 'menu'

								shadows:            true,                            // display shadows on page animations
								shadowTopFwdWidth:  166,                             // shadow width for top forward anim
								shadowTopBackWidth: 166,                             // shadow width for top back anim
								shadowBtmWidth:     50,                              // shadow width for bottom shadow

								before:             function(){},                    // callback invoked before each page turn animation
								after:              function(){}                     // callback invoked after each page turn animation
							});
							Cufon.refresh();
						}
					}).attr('src',source);
				});
				
			});
        </script>
</html>

<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_READING'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_READING'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY'); ?></h4>
                </div>
                <div class="row pt-1">
                    <div class="col">
                    <ul class="list-unstyled book-type" id="cat-list">
                            <?php
                            $arrowRight = '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span>&nbsp;';
                            ?>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_G_1_12'), '/readings', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_NOVEL'), '/novels', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_POEM'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_SLOGAN'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_FAIRY_TALE'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_OTHER'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY_SLOGAN'); ?></h4>
                </div>
                <div class="lst-poetry">
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image('poetry1.jpg', [
                            'class' => 'img-fluid',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image('poetry2.jpg', [
                            'class' => 'img-fluid',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9 grades-list">
                <div class="row">
                    <div class="col-12 alert" style="border: 1px solid #cce5ff;">
                        <div class="b-bottom-blue mb-3 pb-2 pt-1 row">
                            <div class="col-9">
                                <?php if ($data['Reading']['level_title']) : ?>
                                    <h4 class="mb-0 f-koulen txt-blue title-icon-sound"><?php echo $data['Reading']['level_title']; ?> : <?php echo $data['Reading']['title']; ?></h4>
                                <?php else : ?>
                                    <h4 class="mb-0 f-koulen txt-blue title-icon-sound"><?php echo $data['Reading']['title']; ?></h4>
                                <?php endif; ?>
                            </div>
                            <div class="col-3">
                                <div class="btn-group float-right">
                                    <button type="button" class="f-koulenn btn btn-outline-secondary border-primary bb-sound">
                                        <i class="fa fa-volume-up" aria-hidden="true"></i>
                                    </button>
                                    <button type="button" class="btn btn-outline-secondary font-button plus">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-zoom-in" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                            <path d="M10.344 11.742c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1 6.538 6.538 0 0 1-1.398 1.4z"></path>
                                            <path fill-rule="evenodd" d="M6.5 3a.5.5 0 0 1 .5.5V6h2.5a.5.5 0 0 1 0 1H7v2.5a.5.5 0 0 1-1 0V7H3.5a.5.5 0 0 1 0-1H6V3.5a.5.5 0 0 1 .5-.5z"></path>
                                        </svg>
                                        <!-- <span class="visually-hidden">Button</span> -->
                                    </button>
                                                        
                                    <button type="button" class="btn btn-outline-secondary font-button minus">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-zoom-out" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                                            <path d="M10.344 11.742c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1 6.538 6.538 0 0 1-1.398 1.4z"/>
                                            <path fill-rule="evenodd" d="M3 6.5a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5z"/>
                                        </svg>
                                        <!-- <span class="visually-hidden">Button</span> -->
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div id="bb-bookblock" class="lyrice">
                            <div class="text-lesson f-hanuman">
                                <?php
                                foreach ($data['ReadingItem'] as $key1 => $value1) {
                                    if ($value1['is_newline']) {
                                        echo $this->Html->tag('span', '<br />', array(
                                            'data-start' => '0',
                                            'data-end' => '0',
                                        ));
                                        echo $this->Html->tag('span', ' ', array(
                                            'data-start' => '0',
                                            'data-end' => '0',
                                        ));
                                    } else {
                                        echo $this->Html->tag('span', h($value1['text']), array(
                                            'data-start' => h($value1['start']),
                                            'data-end' => h($value1['end']),
                                            'class' => $value1['is_space'] ? 'reading-space' : '',
                                        ));
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <audio id="sound-control-id" src="<?php echo FILE_DOMAIN.PATH_READING_SOUND. h($data['Reading']['sound']); ?>" preload="auto" class="sound-control"></audio>
                    </div>
                    <div class="col-12">
                    <div class="row mt-4">
                    <?php
                    if (!empty($neighbors['prev'])) : ?>
                        <div class="col-6">
                            <?php
                            echo $this->Html->link('<< '.$neighbors['prev']['Reading']['title'], array(
                                'controller' => 'readings',
                                'action' => 'view', $neighbors['prev']['Reading']['id']
                            ), array(
                                'escape' => false
                            )); ?>
                        </div>
                    <?php else : ?>
                        <div class="col-6">
                            -- No prev
                        </div>
                    <?php endif; ?>


                    <?php
                    if (!empty($neighbors['next'])) : ?>
                        <div class="col-6 text-right">
                            <?php
                            echo $this->Html->link($neighbors['next']['Reading']['title'].' >>', array(
                                'controller' => 'readings',
                                'action' => 'view', $neighbors['next']['Reading']['id']
                            ), array(
                                'escape' => false
                            )); ?>
                        </div>
                    <?php else : ?>
                        <div class="col-6 text-right">
                            -- No next
                        </div>
                    <?php endif; ?>
				</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 alert mt-4" style="border: 1px solid #cce5ff;">
                        <a href="http://learnkhmer.online/">
                            <img class="img-fluid img-center" alt="Learn Khmer Online" src="<?php echo $this->Html->url('/img/worksheet/banner/banner_consonant_ads.png'); ?>">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>

.previous {
  background-color: #f1f1f1;
  color: black;
}

a.next, a.previous {
  text-decoration: none;
  display: inline-block;
  padding: 8px 16px;
}

.next {
  background-color: #04AA6D;
  color: white;
}

.reading-space {
	padding-left: 10px;
}

.title-icon-sound:hover {
	cursor: pointer;
}

.speaking {
	color: #F26531;
}

.text-lesson > span {
    /* padding: 5px; */
    font-size: 1.3rem;
}


</style>
<?php
echo $this->Html->css(array(
	'//use.fontawesome.com/releases/v5.6.3/css/all.css',
	'fonts',
	'/js/player/css/mediaelementplayer.css',
	// '/js/bookblock/css/custom.css',
)); ?>
<?php
echo $this->Html->script(array(
	'player/js/popcorn.js',
	'player/js/mediaelement-and-player.min.js',
	'player/js/reading.js',
));
?>
<script>
	$(function() {
		Reading.init();
	});
    $(".font-button").bind("click", function () {
        var size = parseInt($('.text-lesson > span').css("font-size"));
        if ($(this).hasClass("plus")) {
            size = size + 2;
        } else {
            size = size - 2;
            if (size <= 10) {
                size = 10;
            }
        }
        $('.text-lesson > span').css("font-size", size);
    });

    $(function() {
        //Disable cut copy paste
        $('.lyrice').attr('unselectable','on')
        .css({'-moz-user-select':'-moz-none',
              '-moz-user-select':'none',
              '-o-user-select':'none',
              '-khtml-user-select':'none', /* you could also put this in a class */
              '-webkit-user-select':'none',/* and add the CSS class here instead */
              '-ms-user-select':'none',
              'user-select':'none'
        }).bind('selectstart', function(){ return false; });
    });
    document.addEventListener('contextmenu', function(e) {
        e.preventDefault();
    });
</script>
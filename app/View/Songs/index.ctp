
<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_MUSIC'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_MUSIC'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap customize-paging">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="row pt-1">
                    <div class="col-12">
                        <?php
                        echo $this->Form->create('Category', array(
                            'type' => 'get',
                        )); ?>
                        <div class="form-group mb-2">
                            <?php
                            echo $this->Form->select('singer_id', $singers, array(
                                'empty' => 'Select Singer',
                                'class' => 'form-control',
                                'value' => $this->request->query('singer_id'),
                                'label' => false,
                            )); ?>
                        </div>
                        <div class="form-group mb-2">
                            <?php
                            echo $this->Form->select('category_id', $categories, array(
                                'empty' => 'Select Category',
                                'class' => 'form-control',
                                'value' => $this->request->query('category_id'),
                                'label' => false,
                            )); ?>
                        </div>
                        <?php
                        echo $this->Form->end(); ?>
                    </div>
                </div>
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __('CATEGORY'); ?></h4>
                </div>
            </div>
            <div class="col-12 col-md-9">
                <div class="alert alert-primary"> 
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            echo $this->Form->create('Category', array(
                                'type' => 'get',
                                'class' => 'needs-validation',
                            )); ?>
                            <div class="form-row">
                                <div class="col-md-9">
                                    <?php
                                    echo $this->Form->text('search', array(
                                        'class' => 'form-control',
                                        'placeholder' => __('COMMON_TXT_SEARCH'),
                                    )); ?>
                                </div>
                                <div class="col-md-3">
                                    <?php
                                    echo $this->Form->input('search', array(
                                        'class' => 'btn btn-primary btn-block',
                                        'type' => 'button',
                                        'placeholder' => __('COMMON_TXT_SEARCH'),
                                        'label' => false,
                                    )); ?>
                                </div>
                            </div>
                            <?php
                            echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div>
                <div id="products" class="view-group">
                    <?php
                    if ($data) : ?>
                    <table class="table table-border table-bordered table-hover table-sm table-striped">
                        <tr>
                            <th>#</th>
                            <th><?php echo __('Title'); ?></th>
                            <th><?php echo __('SANG_BY'); ?></th>
                            <th><?php echo __('Artists'); ?></th>
                        </tr>
                        <?php
                        foreach ($data as $key => $value) : ?>
                        <tr>
                            <td><?php echo ++$key; ?></td>
                            <td><a href="<?php echo $this->Html->url('/songs/view/'. $value['Song']['id']); ?>"><?php echo $value['Song']['title']; ?></a></td>
                            <td>
                                <small><?php
                                $singer_name = $value['Singer']['name_en'];
                                if (Configure::read('Config.language') === 'kh') {
                                    $singer_name = $value['Singer']['name_kh'];
                                }
                                echo $singer_name; ?></small>
                            </td>
                            <td><small> <?php echo $value['Song']['written'] ? $value['Song']['written'] : 'N/A'; ?></small></td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                    <?php
                    endif; ?>
                </div>
                <div class="pagination justify-content-end">
                    <ul class="pagination">
                        <?php
                        echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                        echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                        echo $this->Paginator->next('&raquo;', array('tag' => 'li','currentClass' => 'disabled', 'escape' => false), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->script([
    '//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
    'jquery.matchHeight-min',
]);
echo $this->html->css(['//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css']);
?>
<style>
.select2-container .select2-selection--single {
    height: 40px;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 40px;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 36px;
}
</style>
<script>
    $('select').select2();
    $(document).ready(function() {
        $('.keyword').autocomplete({
            source: function(request, response) {
              $.ajax({
                type: 'GET',
                dataType: 'json',
                url: BASE_URL + 'songs/search',
                data: 'keyword=' + request.term,
                success: function(data) {
                  response($.map(data, function(item) {
                      var object = new Object();
                      object.label = item.label;
                      object.value = item.value;
                      return object
                  }));
                }
              });
            },
            select: function (event, ui) {
              window.location.href = BASE_URL + 'songs/view/' + ui.item.value;
             }
        });
        $('.item').matchHeight({});
        $('.item').on('click', function() {
            window.location.href = BASE_URL + 'songs/view/' + $(this).data('id');
        });
    });
</script>

<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_MUSIC'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_MUSIC'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap customize-paging">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __('CATEGORY'); ?></h4>
                </div>
                <div class="row pt-1">
                    <div class="col">
                        <ul class="list-unstyled cate-list mb-0" id="cat-list">
                            <li>
                            <a href="#animation" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sport">
                                    <span class="txt-cl-blue">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                                    </span>
                                    <?php echo __('CATEGORY'); ?>
                                </a>
                                <ul class="pl-4 collapse list-unstyled" id="animation" data-parent="#cat-list">
                                    <?php foreach ($categories as $key => $value) : ?>
                                        <?php
                                        if ($value['Category']['id'] == 170) {
                                            continue;
                                        } ?>
                                        <li class="">
                                            <?php
                                            $category_animate_name = Configure::read('Config.language') === 'kh' ? $value['Category']['name_kh'] : $value['Category']['name_en'];
                                            echo $this->Html->link(
                                                '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span> ' . $category_animate_name,
                                                '/songs?category=' . $value['Category']['id'],
                                                array('escape' => false)
                                            ); ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                            <?php foreach ($categories as $key => $value) : ?>
                                <?php
                                if ($value['Category']['id'] != 170) {
                                    continue;
                                } ?>
                                <li class="">
                                    <?php
                                    $category_animate_name = Configure::read('Config.language') === 'kh' ? $value['Category']['name_kh'] : $value['Category']['name_en'];
                                    echo $this->Html->link(
                                        '<span class="txt-cl-blue">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                                    </span> ' . $category_animate_name,
                                        '/songs/christ',
                                        array('escape' => false)
                                    ); ?>
                                </li>
                            <?php endforeach; ?>
                            <li>
                                <a href="#singers" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="subject">
                                    <span class="txt-cl-blue">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                                    </span>
                                    <?php echo __('SINGER'); ?>
                                </a>
                                <ul class="pl-4 collapse list-unstyled" id="singers" data-parent="#cat-list">
                                    <?php foreach ($singers as $key => $value) : ?>
                                        <li class="">
                                            <?php
                                            $category_animate_name = Configure::read('Config.language') === 'kh' ? $value['Singer']['name_kh'] : $value['Singer']['name_en'];
                                            echo $this->Html->link(
                                                '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span> ' . $category_animate_name,
                                                '/songs?singer=' . $value['Singer']['id'],
                                                array('escape' => false)
                                            ); ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php if (false) : ?>
                <div class="lst-poetry">
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image(ADS_PATH.DS.'2779209.jpg', [
                            'class' => 'img-fluid',
                            'url' => 'http://doungvirakseth.com',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="col-12 col-md-9">
                <div class="alert alert-primary"> 
                    <div class="row">
                        <div class="col-md-12">
                            <form action="<?php echo $this->here; ?>" method="get">
                                <input class="form-control keyword" name="keyword" placeholder="<?php echo __('COMMON_TXT_SEARCH'); ?>">
                            </form>
                        </div>
                    </div>
                </div>
                <div id="products" class="view-group">
                    <?php
                    if ($data) : ?>
                    <table class="table table-border table-bordered table-hover table-sm table-striped">
                        <tr>
                            <th>#</th>
                            <th><?php echo __('Title'); ?></th>
                            <th><?php echo __('SANG_BY'); ?></th>
                            <th><?php echo __('Artists'); ?></th>
                        </tr>
                        <?php
                        foreach ($data as $key => $value) : ?>
                        <tr>
                            <td><?php echo ++$key; ?></td>
                            <td><a href="<?php echo $this->Html->url('/songs/view/'. $value['Song']['id']); ?>"><?php echo $value['Song']['title']; ?></a></td>
                            <td>
                                <small><?php
                                $singer_name = $value['Singer']['name_en'];
                                if (Configure::read('Config.language') === 'kh') {
                                    $singer_name = $value['Singer']['name_kh'];
                                }
                                echo $singer_name; ?></small>
                            </td>
                            <td><small>Mr. Yourn Doung</small></td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                    <?php
                    endif; ?>
                </div>
                <div class="pagination justify-content-end">
                    <ul class="pagination">
                        <?php
                        echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                        echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                        echo $this->Paginator->next('&raquo;', array('tag' => 'li','currentClass' => 'disabled', 'escape' => false), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->script([
    '//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
    'jquery.matchHeight-min',
]);
echo $this->html->css(['//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css']);
?>
<script>
    $(document).ready(function() {
        $('.keyword').autocomplete({
            source: function(request, response) {
              $.ajax({
                type: 'GET',
                dataType: 'json',
                url: BASE_URL + 'songs/search',
                data: 'keyword=' + request.term,
                success: function(data) {
                  response($.map(data, function(item) {
                      var object = new Object();
                      object.label = item.label;
                      object.value = item.value;
                      return object
                  }));
                }
              });
            },
            select: function (event, ui) {
              window.location.href = BASE_URL + 'songs/view/' + ui.item.value;
             }
        });
        $('.item').matchHeight({});
        $('.item').on('click', function() {
            window.location.href = BASE_URL + 'songs/view/' + $(this).data('id');
        });
    });
</script>
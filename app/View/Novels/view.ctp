<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_READING'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_READING'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY'); ?></h4>
                </div>
                <div class="row pt-1">
                    <div class="col">
                    <ul class="list-unstyled book-type" id="cat-list">
                            <?php
                            $arrowRight = '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span>&nbsp;';
                            ?>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_G_1_12'), '/readings', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_NOVEL'), '/novels', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_POEM'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_SLOGAN'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_FAIRY_TALE'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_OTHER'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY_SLOGAN'); ?></h4>
                </div>
                <div class="lst-poetry">
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image('poetry1.jpg', [
                            'class' => 'img-fluid',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image('poetry2.jpg', [
                            'class' => 'img-fluid',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9 grades-list">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert" style="border: 1px solid #cce5ff;">
                            <h5 class="f-koulen"><?php echo $data['ReadingItem']['title']; ?></h5>
                            <hr />
                            <div class="f-hanuman"><?php echo $data['ReadingItem']['description']; ?></div>
                        </div>
                    </div>


                    <div class="col-12">
                            <div class="row mt-4">
                            <?php
                            if (!empty($neighbors['prev'])) : ?>
                                <div class="col-6">
                                    <?php
                                    echo $this->Html->link('<< '.$neighbors['prev']['ReadingItem']['title'], array(
                                        'controller' => 'novels',
                                        'action' => 'view', $neighbors['prev']['ReadingItem']['reading_id'],
                                        '?' => [
                                            'item' => $neighbors['prev']['ReadingItem']['id']
                                        ]
                                    ), array(
                                        'escape' => false
                                    )); ?>
                                </div>
                            <?php else : ?>
                                <div class="col-6">
                                    -- No prev
                                </div>
                            <?php endif; ?>


                            <?php
                            if (!empty($neighbors['next'])) : ?>
                                <div class="col-6 text-right">
                                    <?php
                                    echo $this->Html->link($neighbors['next']['ReadingItem']['title'].' >>', array(
                                        'controller' => 'novels',
                                        'action' => 'view', $neighbors['next']['ReadingItem']['reading_id'],
                                        '?' => [
                                            'item' => $neighbors['next']['ReadingItem']['id']
                                        ]
                                    ), array(
                                        'escape' => false
                                    )); ?>
                                </div>
                            <?php else : ?>
                                <div class="col-6 text-right">
                                    -- No next
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 alert mt-4" style="border: 1px solid #cce5ff;">
                        <a href="http://learnkhmer.online/">
                            <img class="img-fluid img-center" alt="Learn Khmer Online" src="<?php echo $this->Html->url('/img/worksheet/banner/banner_consonant.jpg'); ?>">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function() {
        //Disable cut copy paste
        $('.lyrice').attr('unselectable','on')
        .css({'-moz-user-select':'-moz-none',
              '-moz-user-select':'none',
              '-o-user-select':'none',
              '-khtml-user-select':'none', /* you could also put this in a class */
              '-webkit-user-select':'none',/* and add the CSS class here instead */
              '-ms-user-select':'none',
              'user-select':'none'
        }).bind('selectstart', function(){ return false; });
    });
    document.addEventListener('contextmenu', function(e) {
        e.preventDefault();
    });
</script>
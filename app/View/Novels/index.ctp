<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_READING'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_READING'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY'); ?></h4>
                </div>
                <div class="row pt-1">
                    <div class="col">
                    <ul class="list-unstyled book-type" id="cat-list">
                            <?php
                            $arrowRight = '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span>&nbsp;';
                            ?>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_G_1_12'), '/readings', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_NOVEL'), '/novels', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_POEM'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_SLOGAN'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_FAIRY_TALE'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <li>
                                <?php
                                echo $this->Html->link($arrowRight . __d('reading', 'CATEGORY_OTHER'), 'javascript:void(0);', array(
                                    'escape' => false,
                                )); ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('reading', 'CATEGORY_SLOGAN'); ?></h4>
                </div>
                <div class="lst-poetry">
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image('poetry1.jpg', [
                            'class' => 'img-fluid',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image('poetry2.jpg', [
                            'class' => 'img-fluid',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9 grades-list">
                <div class="head-s-list b-bottom-blue d-flex mb-3">
                    <h4 class="mb-0 f-koulen txt-blue title-icon-sound"><span class="bb-sound"><?php echo __d('reading', 'CATEGORY_NOVEL'); ?></span></h4>
                </div>
                <div class="row">
                   <?php
                   foreach ($data as $key => $value) : ?>
                    <div class="col-4">
                        <?php
                        echo $this->Html->image(FILE_DOMAIN .      PATH_READING_NOVEL . $value['Reading']['photo'], array(
                            'alt' => 'Letter Kor',
                            'class' => 'img-fluid img-thumbnail',
                            'url' => 'view/'.$value['Reading']['id']
                        )); ?>
                    </div>
                   <?php
                   endforeach; ?>
                </div>
        </div>
    </div>
</div>
<style>
  /* Make the image fully responsive */
  .carousel-inner img {
    width: 100%;
    height: 100%;
  }
  
  .carousel-control-prev, .carousel-control-next {
      background-color: #277eb5;
  }

  .carousel-indicators {
    background-color: #277eb5;
  }
  </style>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <i class="fa fa-edit"></i>
                <?php 
                if (false) {
                    echo $this->Html->link('Create', '/admin/novels/create', [
                        'class' => 'box-title',
                    ]);
                }
                ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered data-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo __('ADMIN_CATEGORY_TXT_NAME_KH'); ?></th>
                            <th><?php echo __('ADMIN_CATEGORY_TXT_NAME_EN'); ?></th>
                            <th><?php echo __('COMMON_TXT_OPERATION'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data as $key => $value): ?>
                            <tr>
                                <td><?php echo ++$key; ?></td>
                                <td><?php echo $value['Reading']['title']; ?></td>
                                <td><?php echo $value['Reading']['title_en']; ?></td>
                                <td>
                                    <?php 
                                    echo $this->Html->link('<i class="fa fa-edit"></i> Edit', '/admin/novels/edit/'.$value['Reading']['id'], [
                                        'class' => 'btn btn-info btn-xs',
                                        'escape' => false,
                                    ]); ?>
                                    <?php 
                                    echo $this->Html->link('<i class="fa fa-bars"></i> Page', '/admin/novels/item/'.$value['Reading']['id'], [
                                        'class' => 'btn btn-bitbucket btn-xs',
                                        'escape' => false,
                                    ]); ?>
                                    
                                    <?php if (false) : ?>
                                    <?php 
                                    echo $this->Html->link('<i class="fa fa-trash"></i> Delete', '/admin/novels/delete/'.$value['Reading']['id'], [
                                        'class' => '',
                                        'escape' => false,
                                    ]); ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php
                        endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
  $(function () {
      $('.data-table').DataTable();
  });
</script>

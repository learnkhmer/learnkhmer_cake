<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <i class="fa fa-edit"></i>
                <?php 
                echo $this->Html->link('Create', '/admin/novels/item_create/'. $reading_id, [
                    'class' => 'box-title',
                ]); ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered data-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th><?php echo __('ADMIN_CATEGORY_TXT_NAME_KH'); ?></th>
                            <th><?php echo __('ADMIN_CATEGORY_TXT_NAME_EN'); ?></th>
                            <th><?php echo __('Page'); ?></th>
                            <th><?php echo __('COMMON_TXT_OPERATION'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data as $key => $value): ?>
                            <tr>
                                <td><?php echo ++$key; ?></td>
                                <td><?php echo $value['ReadingItem']['title']; ?></td>
                                <td><?php echo $value['ReadingItem']['title_en']; ?></td>
                                <td><?php echo $value['ReadingItem']['order']; ?></td>
                                <td>
                                    
                                    <?php 
                                    echo $this->Html->link('<i class="fa fa-edit"></i> Edit', '/admin/novels/item_edit/'.$value['ReadingItem']['id'], [
                                        'class' => 'btn btn-bitbucket btn-xs',
                                        'escape' => false,
                                    ]); ?>
                                    <?php 
                                    echo $this->Html->link('<i class="fa fa-trash"></i> Delete', '/admin/novels/item_delete/'.$value['ReadingItem']['id'], [
                                        'class' => 'btn btn-danger btn-xs',
                                        'escape' => false,
                                    ]); ?>
                                </td>
                            </tr>
                        <?php
                        endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
  $(function () {
      $('.data-table').DataTable();
  });
</script>

<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>
            <?php
            echo $this->fetch('title').' | '.SERVICE_NAME; ?>
        </title>
        <?php
        echo $this->Html->css(array(
            '/admin/bootstrap/css/bootstrap.min.css',
            '/js/datatable/jquery.dataTables.min.css',
            '/admin/font-awesome/css/font-awesome.min.css',
            '/admin/css/skins/_all-skins.min.css',
            '/admin/ionicons/css/ionicons.min.css',
            '/admin/js/select2/dist/css/select2.min.css',
            '/js/editor/summernote-master/dist/summernote.min',
            '/admin/css/admin.min.css',
            '/admin/css/custom.css',
            '//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
        )); ?>
        <?php
        echo $this->Html->script(array(
            '//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
            'datatable/jquery.dataTables.min.js',
            '/admin/js/dataTables.bootstrap.min.js',
            '/admin/js/loadingoverlay.min.js',
            '/admin/bootstrap/js/bootstrap.min.js',
            'editor/summernote-master/dist/summernote.min',
            '/admin/js/adminlte.min.js',
            '/admin/js/select2/dist/js/select2.min.js',
            '/admin/js/common.js',
        ));
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script'); ?>
        <script>
        var BASE_URL = '<?php echo $this->Html->url('/'); ?>';
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <a href="<?php echo $this->Html->url('/admin/home'); ?>" class="logo">
                    <span class="logo-mini"><b>L</b>KO</span>
                    <span class="logo-lg"><b>Admin</b></span>
                </a>
                <nav class="navbar navbar-static-top">
                    <?php echo $this->Html->link('<span class="sr-only">Toggle navigation</span>',
                    'javascript:void(0);',
                    array(
                        'class' => 'sidebar-toggle',
                        'data-toggle' => 'push-menu',
                        'role' => 'button',
                        'escape' => false,
                    )); ?>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <?php echo $this->Html->link('<i class="fa fa-sign-out" aria-hidden="true"></i> Logout',
                                '/admin/users/logout',
                                array(
                                    'escape' => false,
                                )); ?>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <?php
            echo $this->element('Backend/admin_navigation'); ?>
            <?php // Content Wrapper. Contains page content ?>
            <div class="content-wrapper">
                <?php
                echo $this->element('Backend/admin_content_header'); ?>
                <section class="content">
                    <?php
                    echo $this->Session->flash();
                    echo $this->fetch('content'); ?>
                </section>
            </div>
        </div>
    </body>
</html>

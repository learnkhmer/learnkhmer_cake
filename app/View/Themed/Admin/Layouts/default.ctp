<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?php
        echo $this->fetch('title') . ' | ' . SERVICE_NAME; ?>
    </title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/toastr/toastr.min.css">

    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/summernote/summernote-bs4.min.css">
    
    <link rel="stylesheet" href="<?php echo $this->Html->url('/admin/css/'); ?>custom.css">

    <!-- jQuery -->
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/jquery-ui/jquery-ui.min.js"></script>
    <?php
    echo $this->element('Admin/Partials/javascript'); ?>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Preloader -->
        <div class="preloader flex-column justify-content-center align-items-center">
            <img class="animation__shake" src="<?php echo $this->Html->url('/img/'); ?>lko_logo.png" alt="AdminLTELogo" height="60" width="60">
        </div>

        <!-- Navbar -->
        <?php
        echo $this->element('Admin/Partials/navbar_top');
        echo $this->element('Admin/Partials/main_sidebar');
        ?>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <?php
            echo $this->element('Admin/Partials/breadcrumb'); ?>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <?php
                    echo $this->Session->flash();
                    echo $this->fetch('content'); ?>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php
        echo $this->element('Admin/Partials/footer'); ?>
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- Bootstrap 4 -->
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/select2/js/select2.full.min.js"></script>
    <!-- Bootstrap Switch -->
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
    <!-- daterangepicker -->
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/moment/moment.min.js"></script>
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/toastr/toastr.min.js"></script>
    <!-- AdminLTE App -->

    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/load-overlay/loadingoverlay.min.js"></script>
    
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>dist/js/adminlte.js"></script>
    <script src="<?php echo $this->Html->url('/admin/js/'); ?>common.js"></script>

    <script>
        $(function() {
            let selected_menu = $('.prefix_id_' + controller);
            selected_menu.addClass('active');
            selected_menu.parent().parent().parent().addClass('menu-open');
            selected_menu.parent().parent().prev().addClass('active');

            selected_menu.parent().parent().parent().parent().parent().addClass('menu-open');
            selected_menu.parent().parent().parent().parent().prev().addClass('active');

            // breadcrumb
            $('.breadcrumb_module').text(controller.replace('_', ''));

            setBreadcrumb(controller);

            if (action != 'admin_index') {
                setBreadcrumb(action_no_prefix);
            }

            //Initialize Select2 Elements
            $('.select2bs4, .select2').select2({
                theme: 'bootstrap4'
            })

            $('input[data-bootstrap-switch]').each(function() {
                $(this).bootstrapSwitch('state', $(this).prop('checked'));
            });

            $('.summernote').summernote();
            bsCustomFileInput.init();
        });

        function setBreadcrumb(text, link)
        {
            if ((text && text != 'dashboard')) {
                text = text.replace('_', '');
                $('.breadcrumb').append($('<li>').attr('class', 'breadcrumb-item').append(text));
            }
        }
    </script>
</body>

</html>
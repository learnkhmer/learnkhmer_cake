<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2>ជំនួយ​</h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('ទំព័រដើម', '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);', array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('ជំនួយ​', 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<script>
    $(function(e) {
        $('.d-amount').click(function(e) {
            let amount = $(this).attr('data-amount');
            $('.amount').val(amount);
            if (amount != 0) {
                $('.amount').attr('type', 'hidden');
                $('.d-amount.btn-donate-active')
                        .removeClass('btn-donate-active');
                $(this).addClass('btn-donate-active');
                return true;
            }
            $('.amount').attr('type', 'number');
        });
    });
</script>


<div class="col-md-12 register-form">
    <div class="row contact-form-styles">
        <div class="col-md-12">
            <h2>Donation complete!</h2>
            <hr />
            <p class="introduction">We are so grateful to you for your generous donation—thanks!</p>
            <?php
            echo $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Go to HOME',
                array('controller' => 'home', 'action' => 'index'),
                array('escape' => false , 'class' => 'btn btn-default'));
            ?>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <?php
        echo $this->Form->create('Member', array(
            'type' => 'get',
        )); ?>
        <div class="row">
            <div class="col-5">
                <div class="form-group">
                    <?php
                    echo $this->Form->text('search', array(
                        'class' => 'form-control',
                        'placeholder' => __('COMMON_TXT_SEARCH'),
                    )); ?>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <?php
                    echo $this->Form->input('search', array(
                        'class' => 'btn btn-primary',
                        'type' => 'button',
                        'placeholder' => __('COMMON_TXT_SEARCH'),
                        'label' => false,
                    )); ?>
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <?php
                    echo $this->Html->link('<i class="fa fa-plus"></i> Create', array(
                        'action' => 'create',
                        'prefix' => 'admin',
                    ), array(
                        'class' => 'btn btn-primary float-right',
                        'escape' => false,
                    )); ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>

<div class="card">
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Donator</th>
                    <th>Amount</th>
                    <th>Purpose</th>
                    <th>Description</th>
                    <th>Source</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $key => $value) : ?>
                    <tr>
                        <td><?php echo (($this->params['paging']['Donation']['page'] * LIMIT) - LIMIT + 1) + $key; ?></td>
                        <td><?php echo $value['Donation']['name']; ?></td>
                        <td><?php echo $value['Donation']['amount'] ? number_format($value['Donation']['amount'], 2) : '0.00'; ?></td>
                        <td><?php echo $value['Donation']['purpose']; ?></td>
                        <td><?php echo $value['Donation']['description']; ?></td>
                        <td><?php echo $value['Donation']['source'] == 1 ? 'BO' : 'IPN'; ?></td>
                        <td class="project-actions text-right">
                            <?php
                            echo $this->Html->link('<i class="fas fa-pencil-alt"></i> Edit', '/admin/donations/edit/' . $value['Donation']['id'], [
                                'class' => 'btn btn-info btn-sm',
                                'escape' => false,
                            ]); ?>

                            <?php
                            echo $this->Form->postLink('<i class="fas fa-trash"></i> Delete', '/admin/donations/delete/' . $value['Donation']['id'], [
                                'class' => 'btn btn-danger btn-sm',
                                'escape' => false,
                                'confirm' => 'Are you sure you want to delete?',
                            ]); ?>
                        </td>
                    </tr>
                <?php
                endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <?php
        echo $this->element('/Backend/pagination'); ?>
    </div>
</div>
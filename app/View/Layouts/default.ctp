<!DOCTYPE html>
<html translate="no">

<head>
    <?php echo $this->Html->charset(); ?>
    <meta name="google" content="notranslate">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Learn Khmer Online is accessible to everyone through an online forum.Learn Khmer Online a project will help revolutionize the learning experience for everyone who wishes to learn the language.Learn Khmer Online website is to preserve and enrich Khmer language. Learn Khmer Online will use innovative tools and methodologies that make it easier and more efficient way to learn the language. It will provide learners with more accurate in pronunciation and dialect.Learn Khmer Online project with the hope that the Khmer youth of today will take full advantage of this online forum to learn the language and pass it on to the next generation.">
    <meta name="keyword" content="Khmer Leaning, Cambodia Leanring, Khmer Online Leaning Tools, First Leanring Online Cambodia, Khmer-USA learning online tools">
    <title><?php echo $this->fetch('title'); ?></title>
    <!--FAVOURITE ICON-->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $this->Html->url('/'); ?>aassets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->Html->url('/'); ?>favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo $this->Html->url('/'); ?>favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->Html->url('/'); ?>favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $this->Html->url('/'); ?>favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->Html->url('/'); ?>favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $this->Html->url('/'); ?>favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo $this->Html->url('/'); ?>favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- FACEBOOK -->
    <meta property="og:image" content="<?php echo $this->Html->url('/'); ?>img/s2.jpg" />
    <meta property="og:url" content="<?php echo $this->Html->url('/', true); ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:site_name" content="Learn Khmer Online" />

    <!-- TWITTER -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="Learn Khmer Online" />

    <meta name="twitter:title" content="Learn Khmer Online For Everyone" />
    <meta name="twitter:description" content="Learn Khmer Online For Everyone" />
    <meta name="twitter:image" content="https://www.ikare.info/wp-content/uploads/2019/03/vanday.4.jpg" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <?php
    echo $this->Html->script(array(
        '//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
        '/js/bootstrap/js/bootstrap.min.js',
        '//cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js',
        '/admin/js/select2/dist/js/select2.min.js',
    )); ?>
    <?php
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script'); ?>
    <?php
    echo $this->Html->css(array(
        '/js/bootstrap/css/bootstrap.min.css',
        '/admin/js/select2/dist/css/select2.min.css',
        'style.css',
        'mbr-additional.css',
        'custom.css',
    )); ?>
    <?php
    echo $this->element('Common/javascript'); ?>
</head>
<?php flush(); ?>

<body>
    <main>
        <div class="container-fluid p-0">
            <!-- Header Page -->
            <?php
            echo $this->element('/Front/header'); ?>
            <!-- Display Fetch Content -->
            <?php
            echo $this->fetch('content'); ?>
            <!--CONTACT-->
            <div class="contact-blog" id="contact">
                <div class="container">
                    <!--CONTACT FORM-->
                    <section>
                        <?php
                        switch ($this->request->controller) {
                            case 'home':
                                echo $this->element('/Front/contact');
                                break;

                            case 'donations':
                                echo $this->element('/Front/donation');
                                break;
                        } ?>
                    </section>
                    <!--OUT PARTNER-->
                    <?php
                    if ($this->request->controller === 'home') {
                        // echo $this->element('/Front/parthner');
                    } ?>
                    <!--FOOTER MENU-->
                    <?php
                    echo $this->element('/Front/footer_menu'); ?>
                </div>
            </div>
            <!--FOOTER-->
            <?php
            echo $this->element('/Front/footer'); ?>
        </div>
        <!--Mobile Navigation-->
        <?php
        echo $this->element('/Front/mobile_navigation'); ?>
        <!--Term and Conditions-->
        <?php
        echo $this->element('/Front/term'); ?>
    </main>
    <?php
    echo $this->Html->script(array(
        'f/home/common',
    )); ?>
</body>

</html>
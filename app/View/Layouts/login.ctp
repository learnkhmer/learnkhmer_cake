<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo SERVICE_NAME; ?></title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $this->Html->url('/js/adminLTE/'); ?>dist/css/adminlte.min.css">
</head>

<body class="hold-transition login-page">
<?php
    echo $this->fetch('content'); ?>
    
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $this->Html->url('/js/adminLTE/'); ?>dist/js/adminlte.min.js"></script>
</body>

</html>
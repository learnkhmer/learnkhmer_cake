<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ចូលក្នុង​ប្រព័ន្ធ</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php
        echo $this->Html->css(array(
            '/admin/bootstrap/css/bootstrap.min.css',
            '/admin/font-awesome/css/font-awesome.min.css',
            '/admin/css/admin.min.css',
            '/admin/ionicons/css/ionicons.min.css',
            '//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
        )); ?>
        <?php
        echo $this->Html->script(array(
            '//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
            '/admin/bootstrap/js/bootstrap.min.js',
        )); ?>
    </head>
    <body class="hold-transition login-page">
    <?php
    echo $this->fetch('content'); ?>
    </body>
</html>

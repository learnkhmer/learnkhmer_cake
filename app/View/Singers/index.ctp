
<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_MUSIC'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_MUSIC'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap customize-paging">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __('SINGER'); ?></h4>
                </div>
                <div class="row pt-1">
                    <div class="col">
                        <ul class="list-unstyled book-type">
                            <?php
                            $arrowRight = '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span>&nbsp;';
                            ?>
                            <?php
                            foreach ($data as $key => $value) : ?>
                            <li>
                                <?php
                                $cat_name = $value['Singer']['name_en'];
                                if (Configure::read('Config.language') === 'kh') {
                                    $cat_name = $value['Singer']['name_kh'];
                                }
                                echo $this->Html->link($arrowRight . $cat_name, '/singers/view/'.$value['Singer']['id'], array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <?php
                            endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="lst-poetry">
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image(ADS_PATH.DS.'2779209.jpg', [
                            'class' => 'img-fluid',
                            'url' => 'http://doungvirakseth.com',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9">
                <div id="products" class="row view-group">
                    <?php
                    foreach ($data as $key => $value) : ?>
                    <div class="card px-3 py-4 col-md-4">
                        <div class="card-wrapper flip-card item" data-id="<?php echo $value['Singer']['id']; ?>">
                            <div class="card-img">
                                <?php
                                $photo = $value['Singer']['photo'] ? SINGER_PATH.DS.$value['Singer']['photo'] : 'no_preview.png';
                                echo $this->Html->image($photo, [
                                    'class' => 'group list-group-image img-fluid',
                                    'alt' => $value['Singer']['name_en']
                                ]); ?>
                            </div>
                            <div class="card-box" style="background-color: #2e5230; padding: 10px 15px 3px 15px;">
                                <h3 class="mbr-title mbr-fonts-style mbr-bold mbr-black display-5 text-center f-koulen" style="color: #fff">
                                    <?php 
                                    $singer_name = $value['Singer']['name_en'];
                                if (Configure::read('Config.language') === 'kh') {
                                    $singer_name = $value['Singer']['name_kh'];
                                }
                                    echo $singer_name; ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <?php
                    endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->script([
    'jquery.matchHeight-min',
]);?>
<script>
    $(document).ready(function() {
        $('.item').matchHeight({});
        $('.item').on('click', function() {
            window.location.href = BASE_URL + 'singers/view/' + $(this).data('id');
        });
    });
</script>
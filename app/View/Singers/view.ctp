

<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_MUSIC'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_MUSIC'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __('SINGER'); ?></h4>
                </div>
                <div class="row pt-1">
                    <div class="col">
                        <ul class="list-unstyled book-type">
                            <?php
                            $arrowRight = '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span>&nbsp;';
                            ?>
                            <?php
                            foreach ($singers as $key => $value) : ?>
                            <li>
                                <?php
                                $cat_name = $value['Singer']['name_en'];
                                if (Configure::read('Config.language') === 'kh') {
                                    $cat_name = $value['Singer']['name_kh'];
                                }
                                echo $this->Html->link($arrowRight . $cat_name, '/singers/view/'.$value['Singer']['id'], array(
                                    'escape' => false,
                                )); ?>
                            </li>
                            <?php
                            endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="lst-poetry">
                    <div class="poetry-wrap">
                        <?php
                        echo $this->Html->image(ADS_PATH.DS.'2779209.jpg', [
                            'class' => 'img-fluid',
                            'url' => 'http://doungvirakseth.com',
                            'alt' => 'Poetry',
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9">
                <div>
                    <div class="row lyrice">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active" data-toggle="tab" href="#tab-kh">Khmer</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" data-toggle="tab" href="#tab-en">English</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab-kh" class="container tab-pane active"><br>
                                    <?php echo $data['Singer']['description']; ?>
                                </div>
                                <div id="tab-en" class="container tab-pane"><br>
                                  <?php echo $data['Singer']['description_en']; ?>
                                </div>
                            </div>
                    </div>
                    <div class="row">
                        <div class="ads-block m-b">
                            <a href="http://learnkhmer.online/">
                                <img class="img-fluid img-center" alt="Learn Khmer Online" src="http://doungvirakseth.com/app/storage/app/public/ads/5d8210841fa881568804996.jpg">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
    <style>
        .tab-content p {
            font-family: 'Hanuman', Fallback, sans-serif;
            font-size: unset;
        }
    </style>

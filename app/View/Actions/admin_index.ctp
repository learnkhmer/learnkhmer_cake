<div class="card">
    <div class="card-body">
        <?php
        echo $this->Form->create('Action', array(
            'type' => 'get',
        )); ?>
        <div class="row">
            <div class="col">
                <?php
                echo $this->Form->text('search', array(
                    'class' => 'form-control',
                    'placeholder' => __('COMMON_TXT_SEARCH'),
                )); ?>
            </div>
            <div class="col">
                <?php
                echo $this->Form->input('search', array(
                    'class' => 'btn btn-primary',
                    'type' => 'button',
                    'placeholder' => __('COMMON_TXT_SEARCH'),
                    'label' => false,
                )); ?>
            </div>
            <div class="col">
                <?php
                echo $this->Html->link('<i class="fa fa-plus"></i> Create', array(
                    'action' => 'create', $module['Module']['id'],
                    'prefix' => 'admin',
                ), array(
                    'class' => 'btn btn-primary float-right',
                    'escape' => false,
                )); ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>


<div class="card">
    <div class="card-header">
        <h3 class="card-title">Action</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
                <tr>
                    <th style="width: 1%">#</th>
                    <th>Module</th>
                    <th>Name</th>
                    <th>RUL</th>
                    <th>Is Menu</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $key => $value) : ?>
                    <tr>
                        <td><?php echo ++$key; ?></td>
                        <td><?php echo ucfirst($module['Module']['name']); ?></td>
                        <td><?php echo $value['Action']['name']; ?></td>
                        <td><?php echo $value['Action']['url']; ?></td>
                        <td><?php echo $value['Action']['is_menu'] ? 'YES' : '--'; ?></td>
                        <td class="project-actions text-right">
                            <?php
                            echo $this->Html->link('<i class="fas fa-pencil-alt"></i> Edit', '/admin/actions/edit/' . $value['Action']['id'], [
                                'class' => 'btn btn-info btn-sm',
                                'escape' => false,
                            ]); ?>

                            <?php
                            echo $this->Form->postLink('<i class="fas fa-trash"></i> Delete', '/admin/actions/delete/' .$value['Action']['id'], [
                                'class' => 'btn btn-danger btn-sm',
                                'escape' => false,
                                'confirm' => 'Are you sure you want to delete?',
                            ]); ?>
                        </td>
                    </tr>
                <?php
                endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
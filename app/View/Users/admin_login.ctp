<div class="login-box">
    <!-- /.login-logo -->
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <h3><?php echo SERVICE_NAME; ?></h3>
        </div>
        <div class="card-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <?php echo $this->Session->flash(); ?>
            <?php
            echo $this->Form->create('User', array(
                'novalidate' => true,
                'autocomplete' => 'off',
                'inputDefaults' => array(
                    'legend' => false,
                    'label' => false,
                    'div' => false
                )
            )); ?>
            <div class="input-group mb-3">
                <?php
                echo $this->Form->email('username', array(
                    'class' => 'form-control',
                    'placeholder' => 'Username',
                    'required' => false,
                )); ?>
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <?php
                echo $this->Form->password('password', array(
                    'class' => 'form-control',
                    'placeholder' => 'Password',
                    'required' => false,
                )); ?>
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <div class="icheck-primary">
                        <input type="checkbox" id="remember">
                        <label for="remember">
                            Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
            <?php
            echo $this->Form->end(); ?>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
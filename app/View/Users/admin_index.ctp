<div class="card">
    <div class="card-body">
        <?php
        echo $this->Form->create('Category', array(
            'type' => 'get',
        )); ?>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-12">
                <div class="form-group">
                    <?php
                    echo $this->Form->input('role', array(
                        'options' => unserialize(ROLE),
                        'empty' => 'Choose Role',
                        'class' => 'form-control',
                        'value' => $this->request->query('role'),
                        'label' => false,
                        'style' => 'width: 100%',
                    )); ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <div class="form-group">
                    <?php
                    echo $this->Form->text('search', array(
                        'class' => 'form-control',
                        'placeholder' => __('COMMON_TXT_SEARCH'),
                    )); ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-6">
                <div class="form-group">
                    <?php
                    echo $this->Form->input('search', array(
                        'class' => 'btn btn-primary',
                        'type' => 'button',
                        'placeholder' => __('COMMON_TXT_SEARCH'),
                        'label' => false,
                    )); ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-6">
                <div class="form-group">
                    <?php
                    echo $this->Html->link('<i class="fa fa-plus"></i> Create', array(
                        'action' => 'create',
                        'prefix' => 'admin',
                    ), array(
                        'class' => 'btn btn-primary float-right',
                        'escape' => false,
                    )); ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>


<div class="card">
    <div class="card-header">
        <h3 class="card-title">User</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
                <tr>
                    <th style="width: 1%">#</th>
                    <th>Username</th>
                    <th>Phone</th>
                    <th>Group</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $key => $value) : ?>
                    <tr>
                        <td><?php echo ++$key; ?></td>
                        <td><?php echo $value['User']['username']; ?></td>
                        <td><?php echo $value['User']['phone']; ?></td>
                        <td><?php echo $value['Group']['name']; ?></td>
                        <td class="project-actions text-right">
                            <?php
                            echo $this->Html->link('<i class="fas fa-folder"></i> View', '/admin/users/view/' . $value['User']['id'], [
                                'class' => 'btn btn-primary btn-sm',
                                'escape' => false,
                            ]); ?>

                            <?php
                            echo $this->Html->link('<i class="fas fa-pencil-alt"></i> Edit', '/admin/users/edit/' . $value['User']['id'], [
                                'class' => 'btn btn-info btn-sm',
                                'escape' => false,
                            ]); ?>

                            <?php
                            echo $this->Form->postLink('<i class="fas fa-trash"></i> Delete', '/admin/users/delete/' . $value['User']['id'], [
                                'class' => 'btn btn-danger btn-sm',
                                'escape' => false,
                                'confirm' => 'Are you sure you want to delete?',
                            ]); ?>
                        </td>
                    </tr>
                <?php
                endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
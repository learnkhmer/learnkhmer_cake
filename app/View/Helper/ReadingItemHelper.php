<?php

class ReadingItemHelper extends AppHelper
{
    var $helpers = array('Html', 'Form');

    public function render($data = null)
    {
        if (empty($this->request->data['ReadingItem'])) {
            echo '<tr>';
            echo $this->Html->tag('td', $this->Form->input('ReadingItem.0.text', array(
                'class' => 'form-control text',
                'placeholder' => 'text',
            )), array(
                //'class' => 'col-sm-3',
            ));
            echo $this->Html->tag('td', $this->Form->input('ReadingItem.0.start', array(
                'class' => 'form-control start',
                'placeholder' => 'start',
            )), array(
                //'class' => 'col-sm-3',
            ));
            echo $this->Html->tag('td', $this->Form->input('ReadingItem.0.end', array(
                'class' => 'form-control end',
                'placeholder' => 'end',
            )), array(
                //'class' => 'col-sm-3',
            ));
            echo $this->Html->tag('td', $this->Form->input('ReadingItem.0.is_space', array(
                'type' => 'checkbox',
                'class' => 'is_space',
            )), array(
                'class' => 'text-center',
            ));
            $order = $this->Form->hidden('ReadingItem.0.order', array(
                'class' => 'order',
            ));
            $mode = $this->Form->hidden('ReadingItem.0.mode', array(
                'value' => 'create',
                 'class' => 'mode',
            ));
            echo $this->Html->tag('td', $order.$mode.$this->Html->link(__('COMMON_TXT_DELETE'), 'javascript: void(0);', array(
                'class' => 'btn btn-danger btn-sm btn-block btn-delete',
                'escape' => false,
            )), array(
                //'class' => 'col-sm-2',
            ));
            echo '</tr>';
            return false;
        }
        
        foreach ($this->request->data['ReadingItem'] as $key => $value) {
            echo '<tr>';
            echo $this->Html->tag('td', $this->Form->input('ReadingItem.'. $key .'.text', array(
                'value' => $value['text'],
                'class' => 'form-control text',
                'placeholder' => 'text',
            )), array(
                //'class' => 'col-sm-3',
            ));
            echo $this->Html->tag('td', $this->Form->input('ReadingItem.'. $key .'.start', array(
                'value' => $value['start'],
                'class' => 'form-control start',
                'placeholder' => 'start',
            )), array(
                //'class' => 'col-sm-3',
            ));
            echo $this->Html->tag('td', $this->Form->input('ReadingItem.'. $key .'.end', array(
                'value' => $value['end'],
                'class' => 'form-control end',
                'placeholder' => 'end',
            )), array(
                //'class' => 'col-sm-3',
            ));
            echo $this->Html->tag('td', $this->Form->input('ReadingItem.'. $key .'.is_space', array(
                'checked' => $value['is_space'] ? true : false,
                'type' => 'checkbox',
                'class' => 'is_space',
            )), array(
                'class' => 'text-center',
            ));
            // hidden
            $id = $this->Form->hidden('ReadingItem.'. $key .'.id', array(
                'value' => isset($value['id']) ? $value['id'] : '',
                'class' => 'id',
            ));
            $order = $this->Form->hidden('ReadingItem.'. $key .'.order', array(
                'value' => $key,
                'class' => 'order',
            ));
            $mode = $this->Form->hidden('ReadingItem.'. $key .'.mode', array(
                'value' => 'edit',
                 'class' => 'mode',
            ));
            echo $this->Html->tag('td', $id.$order.$mode.$this->Html->link(__('COMMON_TXT_DELETE'), 'javascript: void(0);', array(
                'class' => 'btn btn-danger btn-sm btn-block btn-delete',
                'escape' => false,
            )), array(
                //'class' => 'col-sm-2',
            ));
            echo '</tr>';
        }
    }
}

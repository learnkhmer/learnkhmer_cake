<?php

class SoundHelper extends AppHelper
{
    var $helpers = array('Html', 'Form');

    public function render($data = null)
    {
        if ($data['Reading']['sound']) {
            echo $this->Html->tag('span', '', array(
                'data-url' => FILE_DOMAIN.PATH_READING_SOUND. h($data['Reading']['sound']),
                'preload' => 'auto',
                'class' => 'url-audio',
                'style' => 'display:none',
            ));
        }
    }

    public function renderSound()
    {
        $css = $this->Html->css(array('/admin/js/audio/css/audioplayer.css'));
        $js = $this->Html->script(array('/admin/js/audio/js/audioplayer.js'));
        $script= '<script>$( function() { $("audio").audioPlayer(); } );</script>';
        $sound = '';
        if ($this->request->data('Reading.sound')) {
            $sound = FILE_DOMAIN.PATH_READING_SOUND.$this->request->data('Reading.sound');
        }
        $html = '<audio preload="auto" class="audio-wrapper" controls>';
		$html .= '<source src="'.$sound.'">';
        $html .= '</audio>';

        echo $css;
        echo $js;
        echo $html;
        echo $script;
    }

    public function renderUpload()
    {
        $sound = '';
        if ($this->request->data('Reading.sound')) {
            $sound = FILE_DOMAIN.PATH_READING_SOUND.$this->request->data('Reading.sound');
        }
        $html = '<audio preload="auto" class="audio-wrapper" controls>';
		$html .= '<source src="'.$sound.'">';
        $html .= '</audio>';
        $html .= $this->Form->input('sound', array(
            'class' => 'input-file-sound-upload',
            'type' => 'file',
            'accept' => '.mp3',
            'style' => 'display:none;',
        ));

        echo $html;
    }
}

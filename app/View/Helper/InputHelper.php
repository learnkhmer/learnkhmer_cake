
<?php

class InputHelper extends AppHelper
{
 

    var $helpers = array('Html', 'Form');

    public function generate($parent, $arr, $type) {
        $data = '';
        $key = $type === 'eng' ? 'txt_eng' : 'txt_kh';
        $value = $type === 'eng' ? $arr['txt_eng'] : $arr['txt_kh'];
        switch ($arr['type']) {
            case 'input':
                 $data = $this->Form->input($parent.'.'.$key, [
                    'value' => $value,
                    'type' => 'text',
                    'class' => 'form-control',
                ]);
                break;

            default:
                $data = $this->Form->textarea($parent.'.'.$key, [
                    'value' => $value,
                    'class' => 'form-control',
                ]);
                break;
        }
        $type = $this->Form->hidden($parent.'.type', ['value' => $arr['type']]);
        $title = $this->Form->hidden($parent.'.title', ['value' => $arr['title']]);
        return $data.$type.$title;
    }
}

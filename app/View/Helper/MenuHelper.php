<?php

class MenuHelper extends AppHelper
{
    var $helpers = array('Html');

    public function getMenu($modules)
    {
        $html = '';
        if(is_array($modules)) {
            foreach ($modules as $key => $value) {
                $extra_action = '';
                if ($value['Module']['url'] == 'modules') {
                    $extra_action = 'prefix_id_actions';
                }

                if (isset($value['Module']) && !isset($value['Action']) && !isset($value['children'])) {
                    
                    $link = $this->Html->link(
                        '<i class="'.$value['Module']['icon'].'"></i><p>'.$value['Module']['name'].'</p>',
                        '/admin/'.$value['Module']['url'],
                        array(
                            'escape' => false,
                            'class' => 'nav-link '.'prefix_id_'.$value['Module']['url'].' '.$extra_action,
                        )
                    );
                    $html .= '<li class="nav-item">'.$link.'</li>';
                } else {
                    if (isset($value['Action']) && !isset($value['children'])) {
                        $html .= ' <li class="nav-item">';
                        $html .= $this->Html->link(
                            '<i class="'.$value['Module']['icon'].'"></i><p>'.$value['Module']['name'].'<i class="right fas fa-angle-left"></i></p>',
                            '#',
                            array(
                                'escape' => false,
                                'class' => 'nav-link',
                            )
                        );
                        $html .= '<ul class="nav nav-treeview">';
                        foreach ($value['Action'] as $key_action => $value_action) {
                            $link = $this->Html->link(
                                '<i class="nav-icon fas fa-tachometer-alt"></i><p>'.$value_action['name'].'</p>',
                                '/admin/'.$value_action['url'],
                                array(
                                    'escape' => false,
                                    'class' => 'nav-link '.'prefix_id_'.$value['Module']['url'].'-'.$value_action['url'],
                                )
                            );
                            $html .= '<li class="nav-item">'.$link.'</li>';
                        }
                        $html .= '</ul>';
                        $html .= '</li>';
                    } else if (isset($value['children']) && !isset($value['Action'])) {
                        $html .= ' <li class="nav-item">';
                        $html .= $this->Html->link(
                            '<i class="'.$value['Module']['icon'].'"></i><p>'.$value['Module']['name'].'<i class="right fas fa-angle-left"></i></p>',
                            '#',
                            array(
                                'escape' => false,
                                'class' => 'nav-link',
                            )
                        );
                        $html .= '<ul class="nav nav-treeview">';
                        $html .= $this->getMenu($value['children']);
                        $html .= '</ul>';
                        $html .= '</li>';
                    } else if (isset($value['children']) && isset($value['Action'])) {
                        $html .= ' <li class="nav-item">';
                        $html .= $this->Html->link(
                            '<i class="'.$value['Module']['icon'].'"></i><p>'.$value['Module']['name'].'<i class="right fas fa-angle-left"></i></p>',
                            '#',
                            array(
                                'escape' => false,
                                'class' => 'nav-link',
                            )
                        );
                        $html .= '<ul class="nav nav-treeview">';
                        foreach ($value['Action'] as $key_action => $value_action) {
                            $link = $this->Html->link(
                                '<i class="nav-icon fas fa-tachometer-alt"></i><p>'.$value_action['name'].'</p>',
                                '/admin/'.$value_action['url'],
                                array(
                                    'escape' => false,
                                    'class' => 'nav-link '.'prefix_id_'.$value['Module']['url'].'-'.$value_action['url']
                                )
                            );
                            $html .= '<li class="nav-item">'.$link.'</li>';
                        }
                        $html .= $this->getMenu($value['children']);
                        $html .= '</ul>';
                        $html .= '</li>';
                    }
                }
            }
        }
        return $html;
    }
}

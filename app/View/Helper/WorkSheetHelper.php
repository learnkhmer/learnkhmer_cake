<?php

class WorkSheetHelper extends AppHelper
{
    var $helpers = array('Html');

    public function getThumbnailPath($data)
    {
        $image1 = explode('.', $data['thumbnail']);
        $link = $data['file'] ? $data['file'] : $image1[0].'.pdf';
        $google_path = GOOGLE_VIEW_PATH . FILE_DOMAIN . PATH_WORKSHEET_PDF . $link;
        $thumbnail = $this->Html->image(FILE_DOMAIN . PATH_WORKSHEET_IMG . $data['thumbnail'], array(
            'alt' => $data['title'],
            'class' => 'img-fluid',
        ));
        return $this->Html->link($thumbnail, $google_path, array(
            'target' => '_blank',
            'escape' => false,
        ));
    }
}
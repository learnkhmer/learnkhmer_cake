<?php

class ActiveCategoryHelper extends AppHelper
{
    var $helpers = array('Html');

    public function displayAactive($id = null)
    {
        if (!isset($this->request->params['pass'][0])) {
            return 'active';
        }
        if ($this->request->params['pass'][0] === $id) {
            return 'active';
        }
    }
}

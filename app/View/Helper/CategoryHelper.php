<?php

class CategoryHelper extends AppHelper
{
    protected $data;
    protected $children;
    protected $element;
    protected $select_options;
    protected $options = array();

    var $helpers = array('Html');

    public function initialize($data = array())
    {
        $this->setData($data);
    }

    public function setOption($data)
    {
        $this->select_options = $data;
    }

    public function getOption()
    {
        return $this->select_options;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setChildren($data)
    {
        $this->children = $data;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function show()
    {
        if (!$this->getData()) {
            return false;
        }
        foreach ($this->getData() as $key => $value) {
            if (!$value['Category']['parent_id']) {
                $this->options[$value['Category']['id']] = $this->getCategoryName($value['Category']);
                $active = $this->addActiveClass($value['Category']['id']);
                $aria_expaded = 'aria-expanded="false"';
                if ($value['Category']['id'] === $this->request->query('parent_id')) {
                    $aria_expaded = 'aria-expanded="true"';
                }
                $this->element .= '<li class="'.$active.'" '.$aria_expaded.' data-type="parent" data-id="'.$value['Category']['id'].'" data-toggle="collapse" data-target="#collapse-'.$value['Category']['id'].'">';
                $this->element .= $this->parentLink($value);
                $this->element .= '</li>';
                $this->element .= $this->checkChildren($value['children'], $value['Category']['id']);
            }
        }
        $this->setOption($this->options);
        echo $this->element;
    }

    private function checkChildren($data, $parent_id)
    {
        if (!$data) {
            return false;
        }
        $class = $this->addCollapse($parent_id);
        $this->element .= '<ul id="collapse-'.$parent_id.'" class="'.$class.' child-list-wrap" aria-labelledby="heading-'.$parent_id.'" data-parent="#category-list">';
        foreach ($data as $key => $value) {
            $this->options[$value['Category']['id']] = $this->getCategoryName($value['Category']);
            $active = $this->addActiveClass($value['Category']['id']);
            $this->element .= '<li class="'.$active.'" data-type="child" data-id="'.$value['Category']['id'].'">';
            $this->element .= $this->childrenLink($value);
            $this->element .= '</li>';
        }
        $this->element .= '</ul>';
    }

    private function parentLink($data)
    {
        $name = $this->getCategoryName($data['Category']);
        if ($data['children']) {
            $name .= ' <span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span>';
        }
        $link = $this->Html->link($name,
            'javascript:void(0);', array(
            'class' => !$data['children'] ? 'cat-item' : '',
            'data-id' => $data['Category']['id'],
            'data-parent-id' => $data['Category']['parent_id'] ? $data['Category']['parent_id'] : '',
            'escape' => false,
        ));
        return $link;
    }

    private function childrenLink($data)
    {
        $name = $this->getCategoryName($data['Category']);
        $link = $this->Html->link('<span class="txt-cl-blue"><i class="fa fa-caret-right" aria-hidden="true"></i></span> '.$name,
            'javascript:void(0);', array(
            'class' => 'cat-item',
            'data-id' => $data['Category']['id'],
            'data-parent-id' => $data['Category']['parent_id'],
            'escape' => false,
        ));
        return $link;
    }

    private function getCategoryName($data)
    {
        $name = Configure::read('Config.language') === 'kh' ? $data['name_kh'] : $data['name_en'];
        return $name;
    }

    private function addActiveClass($id)
    {
        $active = '';
        if ($this->request->query('cat_id') == $id) {
            $active = 'active';
        }
        if (!$this->request->query('cat_id') && ($this->request->params['pass'][0] == $id)) {
            $active = 'active';
        }
        return $active;
    }

    private function addCollapse($parent_id)
    {
        $class = 'collapse';
        if ($parent_id === $this->request->query('parent_id')) {
            $class = 'collapse show';
        }
        return $class;
    }
}

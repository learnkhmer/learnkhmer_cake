<?php

class ImageHelper extends AppHelper
{
    var $helpers = array('Html', 'Form');

    public function generateFromLink($image = null, $mode = null,  $options = array())
    {
        if (empty($image)) {
            return false;
        }

        // generate thumbs
        switch ($mode) {
            case 'thumbs':
                $explode = $explode = pathinfo($image);
                if (!isset($explode['extension']) || !isset($explode['filename'])) {
                    return false;
                }

                $replace = str_replace('filemanager', 'filemanager/_thumbs', $explode['dirname']);
                $image = $replace.DS.$explode['filename'].'_64x64px.'.$explode['extension'];
                break;
        }
        preg_match('/^.+?\webroot(.+)$/is' , $image, $match);
        if (isset($match[1])) {
            return $this->Html->image($match[1], $options);
        }
    }

    public function getFristImage($text = null, $mode = null, $options = array())
    {
        if ($text) {
            preg_match('/< *img[^>]*src *= *["\']?([^"\']*)/i', $text, $image);
            if (isset($image[1])) {
                return $this->generateFromLink($image[1], $mode, $options);
            }
        }
    }

    public function getPreviewImage($file, $path = null)
    {
        $selected_img = !empty($file) ? $this->Html->url($path.DS.$file) : '';
        $label = $this->Html->tag('label', 'Choose File', [
            'for' => 'image-upload',
            'id' => 'image-label',
        ]);
        $img =  $this->Form->input('tmp_image', [
            'type' => 'file',
            'id' => 'image-upload',
            'accept' => 'image/*'
        ]);
        $html = $this->Html->tag('div', $label.$img, [
            'id' => 'image-preview',
            'style' => 'background-image: url('.$selected_img.')'
        ]);
        return $html;
    }

    public function getCDNPreviewImage($file, $path = null)
    {
        $selected_img = !empty($file) ? FILE_DOMAIN_FILES.DS.$path.DS.$file : '';
        $label = $this->Html->tag('label', 'Choose File', [
            'for' => 'image-upload',
            'id' => 'image-label',
        ]);
        $img =  $this->Form->input('tmp_image', [
            'type' => 'file',
            'id' => 'image-upload',
            'accept' => 'image/*'
        ]);
        $html = $this->Html->tag('div', $label.$img, [
            'id' => 'image-preview',
            'style' => 'background-image: url('.$selected_img.')'
        ]);
        return $html;
    }

    public function getFilePreview($data)
    {
        $input = $this->Form->input('', [
            'name' => null,
            'type' => 'text',
            'class' => 'form-control tmp_mp3_value',
            'readonly' => 'readonly',
            'value' => !empty($this->request->data['Song']['mp3']) ? $this->request->data['Song']['mp3'] : ''
        ]);
        $file = $this->Form->input('tmp_mp3', [
            'type' => 'file',
            'id' => 'audio-upload',
            'class' => 'hidden',
            'accept' => 'mp3'
        ]);
        $btn = $this->Form->button('<i class="fa fa-upload"></i>', [
            'type' => 'button',
            'class' => 'btn btn-info btn-flat',
            'escape' => false,
        ]);
        $span = $this->Html->tag('span', $btn, [
            'class' => 'input-group-btn btn-audio-upload',
        ]);
        $html = $this->Html->tag('div', $input.$file.$span, [
            'class' => 'input-group input-group-sm',
        ]);
        return $html;
    }
    
    public function getPDFPreview($data)
    {
        $input = $this->Form->input('', [
            'name' => null,
            'type' => 'text',
            'class' => 'form-control tmp_pdf_value',
            'readonly' => 'readonly',
            'value' => !empty($this->request->data['Song']['pdf']) ? $this->request->data['Song']['pdf'] : ''
        ]);
        $file = $this->Form->input('tmp_pdf', [
            'type' => 'file',
            'id' => 'pdf-upload',
            'class' => 'hidden',
            // 'accept' => 'pdf'
            'accept' => 'image/*'
        ]);
        $btn = $this->Form->button('<i class="fa fa-upload"></i>', [
            'type' => 'button',
            'class' => 'btn btn-info btn-flat',
            'escape' => false,
        ]);
        $span = $this->Html->tag('span', $btn, [
            'class' => 'input-group-btn btn-pdf-upload',
        ]);
        $html = $this->Html->tag('div', $input.$file.$span, [
            'class' => 'input-group input-group-sm',
        ]);
        return $html;
    }

    public function previewUpload($data = '', $tmp_name = '')
    {
        $input = $this->Form->input('', [
            'name' => null,
            'type' => 'text',
            'class' => 'form-control tmp_name',
            'readonly' => 'readonly',
            'value' => $data,
        ]);
        $file = $this->Form->input($tmp_name, [
            'type' => 'file',
            'id' => false,
            'class' => 'hidden file_upload',
            'accept' => 'image/*'
        ]);
        $btn = $this->Form->button('<i class="fa fa-upload"></i>', [
            'type' => 'button',
            'class' => 'btn btn-info btn-flat',
            'escape' => false,
        ]);
        $span = $this->Html->tag('span', $btn, [
            'class' => 'input-group-btn btn-file-upload',
        ]);
        $html = $this->Html->tag('div', $input.$file.$span, [
            'class' => 'input-group input-group-sm',
        ]);
        return $html;
    }
}

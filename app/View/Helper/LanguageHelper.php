<?php

class LanguageHelper extends AppHelper
{
    var $helpers = array('Html', 'Form');

    public function transtlate($key = null)
    {
        App::import('Model', 'Setting');
        $setting = new Setting();
        $data = $setting->getLang();
        $lang = Configure::read('Config.language') ? Configure::read('Config.language') : 'eng';
        return isset($data[$key]['txt_'.$lang]) ? $data[$key]['txt_'.$lang] : '';
    }
}

<!--LEVEL-->
<div class="level-blog">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-4 col-md-4">
                <div class="text-center">
                    <?php
                    echo $this->Html->image('level_1.png', array(
                        'alt' => 'beginner level',
                        'class' => 'img-fluid',
                    )); ?>
                    <h3 class="txt-blue f-moul">
                        <?php
                        echo __d('home', 'TXT_BEGINNER_LEVEL'); ?>
                    </h3>
                </div>
            </div>
            <div class="col-12 col-sm-4 col-md-4">
                <div class="text-center">
                    <?php
                    echo $this->Html->image('level_2.png', array(
                        'alt' => 'meduim level',
                        'class' => 'img-fluid',
                    )); ?>
                    <h3 class="txt-blue f-moul">
                        <?php
                        echo __d('home', 'TXT_INTERMEDIATE_LEVEL'); ?>
                    </h3>
                </div>
            </div>
            <div class="col-12 col-sm-4 col-md-4">
                <div class="text-center">
                    <?php
                    echo $this->Html->image('level_3.png', array(
                        'alt' => 'high level',
                        'class' => 'img-fluid',
                    )); ?>
                    <h3 class="txt-blue f-moul">
                        <?php
                        echo __d('home', 'TXT_ADVANCE_LEVEL'); ?>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!--OUR GOLD AND MISSION-->
<div class="gold-mission-blog" id="mission-vision">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-12 col-md-6">
                <?php
                echo $this->Html->image('two_students.png', array(
                    'alt' => 'two students',
                    'class' => 'img-fluid',
                )); ?>
            </div>
            <div class="col-12 col-md-6">
                <p class="txt-blue text-justify" style="text-transform: uppercase; font-size: 24px; font-weight: bold; color: #f26630;">
                    <?php
                    echo __d('home', 'TXT_OUR_MISSION'); ?>
                </p>
                <?php echo $this->Language->transtlate('mission'); ?>
                
                <p class="txt-blue text-justify" style="text-transform: uppercase; font-size: 24px; font-weight: bold; color: #f26630;">
                    <?php
                    echo __d('home', 'TXT_OUR_GOAL'); ?>
                </p>
                <ul class="list-unstyled text-left txt-blue">
                    <li>
                        <div class="icon-circle">
                            <span><i class="far fa-check-circle"></i></span>
                        </div>
                        <?php
                        echo __d('home', 'TXT_GM_L3'); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--MEMBERS-->
<?php
echo $this->element('/Front/member'); ?>
<?php
echo $this->Html->css(array(
    '/js/slick/slick.css',
    '/js/slick/slick-theme.css',
)); ?>
<?php
echo $this->Html->script('slick/slick.min', array('blog' => 'false')); ?>
<script>
    $(function(e) {
        $('.slide-wrap').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            autoplay: true,
            autoplaySpeed: 2000,
            prevArrow: false,
            nextArrow: false
        });

        $('.p-slide').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            prevArrow: false,
            nextArrow: false
        });
    });
</script>

<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __d('resource', 'RESOURCE'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);', array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__d('resource', 'RESOURCE'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<section class="rs-blog bg-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h3 class="s-title pb-4">តារាង​ភ្ជាប់​រូប​ភាព​</h3>
                <p>គាំទ្រពួក​យើង​ដើម្បី​ធ្វើ​អោយ​បានល្អជាងមុនពួក​យើង​​ត្រូវ​ការ​ជំនួយ​ពី​អ្នក​ទាំអស់គ្នា​ដើម្បី​បន្តរ​ការងារ​នេះ​អោយ​បាន​គង់វង្សយូរអង្វែង​​និង​ធ្វើ​អោយ​អក្សរខ្មែរ​កាន់​តែ​ទូលំទូលាយជាងមុន។ សូមអគុណសម្រាប់ការ​ចែក​រំលែក ឬ ជំនួន​របស់អ្នក។</p>
            </div>
            <div class="col-12 col-md-6 text-center">
                <a href="<?php echo $this->Html->url('educational_poster_bg.jpg'); ?>" data-fancybox="resource-image">
                    <?php
                    echo $this->Html->image('educational_poster.jpg', array(
                        'alt' => 'Educational Poster',
                        'class' => 'img-fluid rounded-circle border border-blue',
                    )); ?>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="rs-blog bg-light-yellow">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 text-center">
                <a href="<?php echo $this->Html->url('flash_card_bg.jpg'); ?>" data-fancybox="resource-image">
                    <?php
                    echo $this->Html->image('flash_card.jpg', array(
                        'alt' => 'Flash Card',
                        'class' => 'img-fluid rounded-circle border border-blue',
                    )); ?>
                </a>
            </div>
            <div class="col-12 col-md-6">
                <h3 class="s-title pb-4">Flash Card</h3>
                <p>គាំទ្រពួក​យើង​ដើម្បី​ធ្វើ​អោយ​បានល្អជាងមុនពួក​យើង​​ត្រូវ​ការ​ជំនួយ​ពី​អ្នក​ទាំអស់គ្នា​ដើម្បី​បន្តរ​ការងារ​នេះ​អោយ​បាន​គង់វង្សយូរអង្វែង​​និង​ធ្វើ​អោយ​អក្សរខ្មែរ​កាន់​តែ​ទូលំទូលាយជាងមុន។ សូមអគុណសម្រាប់ការ​ចែក​រំលែក ឬ ជំនួន​របស់អ្នក។</p>
            </div>
        </div>
    </div>
</section>
<section class="rs-blog bg-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h3 class="s-title pb-4">DVD</h3>
                <p>គាំទ្រពួក​យើង​ដើម្បី​ធ្វើ​អោយ​បានល្អជាងមុនពួក​យើង​​ត្រូវ​ការ​ជំនួយ​ពី​អ្នក​ទាំអស់គ្នា​ដើម្បី​បន្តរ​ការងារ​នេះ​អោយ​បាន​គង់វង្សយូរអង្វែង​​និង​ធ្វើ​អោយ​អក្សរខ្មែរ​កាន់​តែ​ទូលំទូលាយជាងមុន។ សូមអគុណសម្រាប់ការ​ចែក​រំលែក ឬ ជំនួន​របស់អ្នក។</p>
            </div>
            <div class="col-12 col-md-6 text-center">
                <a href="<?php echo $this->Html->url('dvd_bg.jpg'); ?>" data-fancybox="resource-image">
                    <?php
                    echo $this->Html->image('dvd.jpg', array(
                        'alt' => 'DVD',
                        'class' => 'img-fluid rounded-circle border border-blue',
                    )); ?>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="rs-blog bg-light-green">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 text-center">
                <a href="<?php echo $this->Html->url('educational_game_bg.jpg'); ?>" data-fancybox="resource-image">
                    <?php
                    echo $this->Html->image('educational_game.jpg', array(
                        'alt' => 'Educational Game',
                        'class' => 'img-fluid rounded-circle border border-blue',
                    )); ?>
                </a>
            </div>
            <div class="col-12 col-md-6">
                <h3 class="s-title pb-4">ហ្គេម​អប់រំ</h3>
                <p>គាំទ្រពួក​យើង​ដើម្បី​ធ្វើ​អោយ​បានល្អជាងមុនពួក​យើង​​ត្រូវ​ការ​ជំនួយ​ពី​អ្នក​ទាំអស់គ្នា​ដើម្បី​បន្តរ​ការងារ​នេះ​អោយ​បាន​គង់វង្សយូរអង្វែង​​និង​ធ្វើ​អោយ​អក្សរខ្មែរ​កាន់​តែ​ទូលំទូលាយជាងមុន។ សូមអគុណសម្រាប់ការ​ចែក​រំលែក ឬ ជំនួន​របស់អ្នក។</p>
            </div>
        </div>
    </div>
</section>
<section class="rs-blog bg-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h3 class="s-title pb-4">Manuscript Boards</h3>
                <p>គាំទ្រពួក​យើង​ដើម្បី​ធ្វើ​អោយ​បានល្អជាងមុនពួក​យើង​​ត្រូវ​ការ​ជំនួយ​ពី​អ្នក​ទាំអស់គ្នា​ដើម្បី​បន្តរ​ការងារ​នេះ​អោយ​បាន​គង់វង្សយូរអង្វែង​​និង​ធ្វើ​អោយ​អក្សរខ្មែរ​កាន់​តែ​ទូលំទូលាយជាងមុន។ សូមអគុណសម្រាប់ការ​ចែក​រំលែក ឬ ជំនួន​របស់អ្នក។</p>
            </div>
            <div class="col-12 col-md-6 text-center">
                <a href="<?php echo $this->Html->url('manuscript_board_bg.jpg'); ?>" data-fancybox="resource-image">
                    <?php
                    echo $this->Html->image('manuscript_board.jpg', array(
                        'alt' => 'Manuscript Board',
                        'class' => 'img-fluid rounded-circle border border-blue',
                    )); ?>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="rs-blog bg-light-yellow">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 text-center">
                <a href="<?php echo $this->Html->url('bingo_card_bg.jpg'); ?>" data-fancybox="resource-image">
                    <?php
                    echo $this->Html->image('bingo_card.jpg', array(
                        'alt' => 'Bingo Card',
                        'class' => 'img-fluid rounded-circle border border-blue',
                    )); ?>
                </a>
            </div>
            <div class="col-12 col-md-6">
                <h3 class="s-title pb-4">Bingo</h3>
                <p>គាំទ្រពួក​យើង​ដើម្បី​ធ្វើ​អោយ​បានល្អជាងមុនពួក​យើង​​ត្រូវ​ការ​ជំនួយ​ពី​អ្នក​ទាំអស់គ្នា​ដើម្បី​បន្តរ​ការងារ​នេះ​អោយ​បាន​គង់វង្សយូរអង្វែង​​និង​ធ្វើ​អោយ​អក្សរខ្មែរ​កាន់​តែ​ទូលំទូលាយជាងមុន។ សូមអគុណសម្រាប់ការ​ចែក​រំលែក ឬ ជំនួន​របស់អ្នក។</p>
            </div>
        </div>
    </div>
</section>
<section class="rs-blog bg-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h3 class="s-title pb-4">ពណ៌</h3>
                <p>គាំទ្រពួក​យើង​ដើម្បី​ធ្វើ​អោយ​បានល្អជាងមុនពួក​យើង​​ត្រូវ​ការ​ជំនួយ​ពី​អ្នក​ទាំអស់គ្នា​ដើម្បី​បន្តរ​ការងារ​នេះ​អោយ​បាន​គង់វង្សយូរអង្វែង​​និង​ធ្វើ​អោយ​អក្សរខ្មែរ​កាន់​តែ​ទូលំទូលាយជាងមុន។ សូមអគុណសម្រាប់ការ​ចែក​រំលែក ឬ ជំនួន​របស់អ្នក។</p>
            </div>
            <div class="col-12 col-md-6 text-center">
                <a href="<?php echo $this->Html->url('understand_color_bg.jpg'); ?>" data-fancybox="resource-image">
                    <?php
                    echo $this->Html->image('understand_color.jpg', array(
                        'alt' => 'Understand About Color',
                        'class' => 'img-fluid rounded-circle border border-blue',
                    )); ?>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="rs-blog bg-light-green">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 text-center">
                <a href="<?php echo $this->Html->url('understand_time_bg.jpg'); ?>" data-fancybox="resource-image">
                    <?php
                    echo $this->Html->image('understand_time.jpg', array(
                        'alt' => 'Understand About Time',
                        'class' => 'img-fluid rounded-circle border border-blue',
                    )); ?>
                </a>
            </div>
            <div class="col-12 col-md-6">
                <h3 class="s-title pb-4">ម៉ោង</h3>
                <p>គាំទ្រពួក​យើង​ដើម្បី​ធ្វើ​អោយ​បានល្អជាងមុនពួក​យើង​​ត្រូវ​ការ​ជំនួយ​ពី​អ្នក​ទាំអស់គ្នា​ដើម្បី​បន្តរ​ការងារ​នេះ​អោយ​បាន​គង់វង្សយូរអង្វែង​​និង​ធ្វើ​អោយ​អក្សរខ្មែរ​កាន់​តែ​ទូលំទូលាយជាងមុន។ សូមអគុណសម្រាប់ការ​ចែក​រំលែក ឬ ជំនួន​របស់អ្នក។</p>
            </div>
        </div>
    </div>
</section>
<section class="rs-blog bg-white">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h3 class="s-title pb-4">Puzzle Piece</h3>
                <p>គាំទ្រពួក​យើង​ដើម្បី​ធ្វើ​អោយ​បានល្អជាងមុនពួក​យើង​​ត្រូវ​ការ​ជំនួយ​ពី​អ្នក​ទាំអស់គ្នា​ដើម្បី​បន្តរ​ការងារ​នេះ​អោយ​បាន​គង់វង្សយូរអង្វែង​​និង​ធ្វើ​អោយ​អក្សរខ្មែរ​កាន់​តែ​ទូលំទូលាយជាងមុន។ សូមអគុណសម្រាប់ការ​ចែក​រំលែក ឬ ជំនួន​របស់អ្នក។</p>
            </div>
            <div class="col-12 col-md-6 text-center">
                <a href="<?php echo $this->Html->url('puzzle_bg.jpg'); ?>" data-fancybox="resource-image">
                    <?php
                    echo $this->Html->image('puzzle.jpg', array(
                        'alt' => 'Puzzle',
                        'class' => 'img-fluid rounded-circle border border-blue',
                    )); ?>
                </a>
            </div>
        </div>
    </div>
</section>
<?php
echo $this->Html->css('/js/fancybox/jquery.fancybox.min.css');
echo $this->Html->script('fancybox/jquery.fancybox.min.js', array('blog' => false)); ?>

<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FOOTER_MENU_TXT_REPORT'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);', array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FOOTER_MENU_TXT_REPORT'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="expense1-blog">
    <div class="container">
        <h3 class="s-title text-center pb-4"><?php echo __d('report', 'TOTAL_BUDGET_FOR_BEGINNER_PROJECT'); ?></h3>
        <div class="row">
            <div class="col-12 col-sm-3 col-md-3">
                <section class="text-center f-koulen">
                    <?php
                    echo $this->Html->image('level_1.png', array(
                        'class' => 'img-fluid',
                        'alt' => 'Level 1',
                    )); ?>
                    <h3 class="txt-blue"><?php echo __d('report', 'BEGINNER_K_6'); ?></h3>
                    <p class="pb-0 f-hanuman">
                        <?php echo __d('report', 'BEGINNER_K_6_CONTENT'); ?><br>
                        <span class="txt-yellow"><?php echo __d('report', 'BEGINNER_K_6_BUDGET'); ?></span>
                    </p>
                </section>
            </div>
            <div class="col-12 col-sm-3 col-md-3">
                <section class="text-center f-koulen">
                    <?php
                    echo $this->Html->image('worksheet.png', array(
                        'class' => 'img-fluid',
                        'alt' => 'Level 2',
                    )); ?>
                    <h3 class="txt-blue"><?php echo __d('report', 'WORKSHEET'); ?></h3>
                    <p class="pb-0 f-hanuman">
                        <?php echo __d('report', 'WORKSHEET_CONTENT'); ?><br>
                        <span class="txt-yellow"><?php echo __d('report', 'WORKSHEET_BUDGET'); ?></span>
                    </p>
                </section>
            </div>
            <div class="col-12 col-sm-3 col-md-3">
                <section class="text-center f-koulen">
                    <?php
                    echo $this->Html->image('reading.png', array(
                        'class' => 'img-fluid',
                        'alt' => 'Level 3',
                    )); ?>
                    <h3 class="txt-blue"><?php echo __d('report', 'READING'); ?></h3>
                    <p class="pb-0 f-hanuman">
                        <?php echo __d('report', 'READING_CONTENT'); ?><br>
                        <span class="txt-yellow"><?php echo __d('report', 'READING_BUDGET'); ?></span>
                    </p>
                </section>
            </div>
            <div class="col-12 col-sm-3 col-md-3">
                <section class="text-center f-koulen">
                    <?php
                    echo $this->Html->image('app.png', array(
                        'class' => 'img-fluid',
                        'alt' => 'Level 4',
                    )); ?>
                    <h3 class="txt-blue"><?php echo __d('report', 'CONVERSATION'); ?></h3>
                    <p class="pb-0 f-hanuman">
                        <?php echo __d('report', 'CONVERSATION_CONTENT'); ?><br>
                        <span class="txt-yellow"><?php echo __d('report', 'CONVERSATION_BUDGET'); ?></span>
                    </p>
                </section>
            </div>
        </div>
        <div class="row pt-3">
            <div class="col-12 col text-center">
                <a href="//shorturl.at/pJSW5" class="bg-dakr-blue f-koulen read-more" target="_blank">
                    <?php echo __d('report', 'READ_MORE'); ?>
                </a>
            </div>
        </div>
    </div>
</div>
<?php if (false) : ?>
<div class="expense2-blog text-white">
    <div class="container">
        <h3 class="s-title text-center pb-4 text-white">
            <?php echo __d('report', 'SOUND_AND_ANIMATION_PROGRAM'); ?>
        </h3>
        <p class="text-center">
            <?php echo __d('report', 'SOUND_AND_ANIMATION_PROGRAM_CONTENT'); ?>
        </p>
        <div class="row justify-content-between">
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'CONSONANT'); ?>
            </div>
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'SUB_SCRIPT_CONSONANT'); ?>
            </div>
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'VOWEL'); ?>
            </div>
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'INDEPENDENCE'); ?>
            </div>
        </div>
        <div class="w-100"></div>
        <div class="row justify-content-between">
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'NUMBER'); ?>
            </div>
            <div class="col-6 col-sm-3 col-md-3"></div>
            <div class="col-6 col-sm-3 col-md-3"></div>
            <div class="col-6 col-sm-3 col-md-3"></div>
        </div>
        <div class="row pt-3">
            <div class="col-12 col text-center">
                <a href="https://docs.google.com/document/d/15Av_KiXjcNI20tk62RiXakyad-uXjlehVDYq7XN4SK0/edit?usp=sharing" class="bg-dakr-blue f-koulen read-more" target="_blank">
                    <?php echo __d('report', 'READ_MORE'); ?>
                </a>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<div class="expense3-blog text-white">
    <div class="container">
        <h3 class="s-title text-center pb-4 text-white">
            <?php echo __d('report', 'SOUND_14_GROUPS'); ?>
        </h3>
        <div class="row justify-content-between">
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'DAY'); ?></div>
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'MONTH'); ?></div>
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'YEAR'); ?></div>
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'TIME'); ?></div>
        </div>
        <div class="w-100"></div>
        <div class="row justify-content-between">
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'CURRENCY'); ?></div>
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'COLOR'); ?></div>
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'BODY'); ?></div>
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'FRUIT'); ?></div>
        </div>
        <div class="w-100"></div>
        <div class="row justify-content-between">
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'ANIMAL'); ?></div>
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'BIRD'); ?></div>
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'FISH'); ?></div>
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'INSECT'); ?></div>
        </div>
        <div class="w-100"></div>
        <div class="row justify-content-between">
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'VEGETABLE'); ?>​</div>
            <div class="col-6 col-sm-3 col-md-3"><?php echo __d('report', 'GASTROPOD'); ?></div>
            <div class="col-6 col-sm-3 col-md-3"></div>
            <div class="col-6 col-sm-3 col-md-3"></div>
        </div>
        <div class="row pt-3">
            <div class="col-12 col text-center">
                <a href="https://drive.google.com/file/d/0B4ykE_fR7ksdU1BQTEtYOUFwS2VvZ2llalVrZldVaXZFcjFZ/view?usp=sharing" class="bg-dakr-blue f-koulen read-more" target="_blank">
                    <?php echo __d('report', 'READ_MORE'); ?>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="pt-5">
    <div class="container">
        <!--EVENTS-->
        <section>
            <h3 class="s-title text-center pb-4">
                <?php echo __d('report', 'EXPENSE_AND_REVENUE'); ?>
            </h3>
            <table class="tb-expense table table-bordered f-koulen">
                <tbody>
                    <tr>
                        <td><?php echo __d('report', 'DATE'); ?></td>
                        <td><?php echo __d('report_content', 'DATE_D_15'); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo __d('report', 'TIME'); ?></td>
                        <td><?php echo __d('report_content', 'TIME_D_15'); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo __d('report', 'LOCATION'); ?></td>
                        <td><?php echo __d('report_content', 'LOCATION_D_15'); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo __d('report', 'TICKET_PRICE'); ?></td>
                        <td><?php echo __d('report_content', 'TICKET_PRICE_D_15'); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo __d('report', 'FOOD_AND_BEVERAGE'); ?></td>
                        <td><?php echo __d('report_content', 'FOOD_AND_BEVERAGE_D_15'); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo __d('report', 'SPEND_ON'); ?></td>
                        <td><?php echo __d('report_content', 'SPEND_ON_D_15'); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo __d('report', 'REMAINING_CASE'); ?></td>
                        <td><?php echo __d('report_content', 'REMAINING_CASE_D_15'); ?></td>
                    </tr>
                </tbody>
            </table>
            <table class="tb-expense table table-bordered f-koulen">
                <tbody>
                    <tr>
                        <td><?php echo __d('report', 'DATE'); ?></td>
                        <td><?php echo __d('report_content', 'DATE_D_7'); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo __d('report', 'TIME'); ?></td>
                        <td><?php echo __d('report_content', 'TIME_D_7'); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo __d('report', 'LOCATION'); ?></td>
                        <td><?php echo __d('report_content', 'LOCATION_D_7'); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo __d('report', 'TICKET_PRICE'); ?></td>
                        <td><?php echo __d('report_content', 'TICKET_PRICE_D_7'); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo __d('report', 'FOOD_AND_BEVERAGE'); ?></td>
                        <td><?php echo __d('report_content', 'FOOD_AND_BEVERAGE_D_7'); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo __d('report', 'SPEND_ON'); ?></td>
                        <td><?php echo __d('report_content', 'SPEND_ON_D_7'); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo __d('report', 'REMAINING_CASE'); ?></td>
                        <td><?php echo __d('report_content', 'REMAINING_CASE_D_7'); ?></td>
                    </tr>
                </tbody>
            </table>
        </section>

        <section></section>
    </div>
</div>

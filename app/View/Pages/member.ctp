
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __d('home', 'TXT_OUR_TEAM'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);', array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__d('home', 'TXT_OUR_TEAM'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white about-info" style="padding-bottom: 0 !impotant">
    <h3 class="text-center"><?php echo __d('home', 'TXT_OUR_TEAM'); ?></h3>
</div>
<div class="container">
    <div class="row justify-content-between">
        <?php foreach ($members as $key => $value): ?>
        <div class="col-3 col-sm-3 col-md-3">
            <div class="member-wrap">
                <?php
                echo $this->Html->image(MEMBER_PATH . '/' . $value['Member']['image'], array(
                    'alt' => '',
                    'url' => $value['Member']['link'],
                    'class' => 'img-fluid',
                )); ?>
                <section>
                    <h4 class="name"><?php echo $value['Member']['name'] ?></h4>
                    <p class="position"><?php echo $value['Member']['position'] ?></p>
                </section>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>

<style>
    .name {
        margin-top: 20px;
        font-size: 16px;
        text-transform: uppercase;
    }
    
    .position {
        font-size: 13px;
        color: red;
        text-transform: uppercase;
    }
</style>
<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2>សំណួរ​ និង​ ចម្លើយ​</h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('ទំព័រដើម', '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);', array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('សំណួរ​ និង​ ចម្លើយ​', 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white qa-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 border-right">
                <ul class="nav nav-link list-unstyled mb-0 lst-lesson sticky-top" id="lst-questions">
                    <li class="nav-item">
                        <a class="nav-link" href="#q1">
                            What benefits do I earn from this Learn Khmer Online?
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#q2">
                            Does it cost to become a member?
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#q3">
                            Will my listing be exposed to my target market all over the country?
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#q4">
                            How is learn Khmer better than other business directory?
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#q5">
                            Are there limited advertising spaces? If so, how much?
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#q6">
                            What is the cost of advertising on Learn Khmer Online?
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#q7">
                            Who is your website audience, and where will they see our ads?
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#q8">
                            How can we track how effective our website advertising campaign is?
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-8">
                <div class="answer-wrap" data-spy="scroll" data-target="#lst-questions" data-offset="0">
                    <div id="q1">
                        <h4>What benefits do I earn from this Learn Khmer Online?</h4>
                        <p>
                            Well for start off, CP have some of the coolest gadgets that none other website doesn’t have available in Cambodia yet, such as the live video chat that you can see the person you’re talking to. You can message unlimited SMS to your other party. Stay in tune with the latest news and cool games to keep you occupied. The most benefit you’ll get from is CP busy traffic, which will help to advance your business to higher grounds toward success. Not enough? You can even customize your concepts of advertising and we’ll arrange the way you like it-the way that you are satisfied.
                        </p>
                    </div>
                    <div id="q2">
                        <h4>Does it cost to become a member?</h4>
                        <p>
                            No, it doesn’t cost to become a member. You just simply fill in all the blank spot; especially answering requirement star spot and after pressing enter, you’ve officially a CP member!
                        </p>
                    </div>
                    <div id="q3">
                        <h4>Will my listing be exposed to my target market all over the country?</h4>
                        <p>
                            Most definitely! Not only is it visible in the heart of Phnom Penh city & 24 Provinces, but it’s presence visually, circulated actively in the develop countries such as US, Australia, Canada and in France.
                        </p>
                    </div>
                    <div id="q4">
                        <h4>How is learn Khmer better than other business directory?</h4>
                        <p>
                            There are many special and better-equipped benefits with CP Business Directory. Among the many, one highlight stands out the most about CambodiaPages: you’re business will be connected & visible globally! Businessmen & women who travel to Cambodia who are often from developing countries will be connected with CP. This means with our high traffic, your business revenue will advance and succeed through CP Business Directory!
                        </p>
                    </div>
                    <div id="q5">
                        <h4>Are there limited advertising spaces? If so, how much?</h4>
                        <p>
                            There is no limited advertising space. Advertise as much as you’d like and as many you like.
                        </p>
                    </div>
                    <div id="q6">
                        <h4>What is the cost of advertising on Learn Khmer Online?</h4>
                        <p>
                            This varies depending on which packages you choose from and how much quantities of advertising space you’d like. We’d be more than happy to serve you with ways on going about this procedure through contacting us at our office.
                        </p>
                    </div>
                    <div id="q7">
                        <h4>Who is your website audience, and where will they see our ads?</h4>
                        <p>
                            Our audiences are from everywhere, anywhere, anyplace and anytime. We’re aim to all people in general, open to public, because we believe that in all range of persons, despite of particular class of group of people, will want to establish connection, network, and social activity one way or the other.
                        </p>
                    </div>
                    <div id="q8">
                        <h4>How can we track how effective our website advertising campaign is?</h4>
                        <p>
                            There are a few indicators to help answer this question. 1st and most important is the numbers of company advertising. Another is the ‘Visitors Count’ chart that is visible to inform how many people visited the site. You can also tell by the comments that members & non-members leave their comments on certain topics. Another is through ‘Word of Marketing’ that you hear people talking about the cool gadget toys and cool program that CP having available.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(e) {
        $('body').scrollspy({ target: '#lst-questions' })
    });
</script>

<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __d('about', 'ABOUT'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);', array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__d('about', 'ABOUT'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white about-info">
    <div class="container">
        <h3 class="text-center"><?php echo __d('about', 'INTRODUCTION'); ?></h3>
        <div style="text-align: justify">
            <?php echo $this->Language->transtlate('introduction'); ?>
        </div>
    </div>
</div>
<div class="about-info-1">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-6 col-md-2">
                <div class="about-icon-wrap text-center f-koulen">
                    <p class="mb-0">
                        <span><i class="fas fa-users"></i></span><br>
                        <span class="txt-yellow">35</span>
                    </p>
                    <p class="mb-0 border-top"><?php echo __d('about', 'MEMBER'); ?></p>
                </div>
            </div>
            <div class="col-6 col-md-2">
                <div class="about-icon-wrap text-center f-koulen">
                    <p class="mb-0">
                        <span><i class="fas fa-unlock-alt"></i></span><br>
                        <span class="txt-yellow">3+</span>
                    </p>
                    <p class="mb-0 border-top"><?php echo __d('about', 'PROJECT'); ?></p>
                </div>
            </div>
            <div class="col-12 col-md-auto">
                <div class="about-icon-wrap text-center f-koulen">
                    <p class="mb-0">
                        <span class="txt-yellow">95%</span>
                    </p>
                    <p class="mb-0"><?php echo __d('about', 'NUMBER_OF_KID_SAFTIFY'); ?></p>
                </div>
            </div>
            <div class="col-6 col-md-2">
                <div class="about-icon-wrap text-center f-koulen">
                    <p class="mb-0">
                        <span><i class="fas fa-anchor"></i></span><br>
                        <span class="txt-yellow">5+</span>
                    </p>
                    <p class="mb-0 border-top"><?php echo __d('about', 'PROJECT_IS_DOING'); ?></p>
                </div>
            </div>
            <div class="col-6 col-md-2">
                <div class="about-icon-wrap text-center f-koulen">
                    <p class="mb-0">
                        <span><i class="fas fa-mug-hot"></i></span><br>
                        <span class="txt-yellow">200+</span>
                    </p>
                    <p class="mb-0 border-top"><?php echo __d('about', 'NUMBER_OF_DONATOR'); ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <ul class="f-koulen list-inline list-unstyled m-0 pt-5">
                    <li class="list-inline-item">
                        <?php
                        echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/', array(
                            'class' => 'txt-yellow',
                        )); ?>
                    </li>
                    <li class="list-inline-item">&sol;</li>
                    <li class="list-inline-item">
                        <?php
                        echo $this->Html->link(__d('about', 'CONTACT'), '/#contact', array(
                            'class' => 'txt-yellow',
                        )); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="bg-white about-contact-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <section>
                    <h3 class="s-title txt-yellow"><?php echo __d('about', 'PURPOSE_TITLE'); ?></h3>
                    <div style="text-align: justify"><?php echo $this->Language->transtlate('purpose') ?></div>
                </section>
            </div>
            <?php if (false) : ?>
            <div class="col-12 col-md-4">
                <div class="located-wrap border">
                    <?php
                    echo $this->Html->image('located.jpg', array(
                        'class' => 'img-fluid',
                        'alt' => 'Location',
                    )); ?>
                    <div class="txt-yellow text-center">
                        <p class="mb-0 p-3">
                            <span class="s-title"><i class="fas fa-map-marked-alt"></i>
                            </span><br>
                            <?php echo __d('about', 'ADDRESS'); ?>
                        </p>
                    </div>
                    <div class="p-2">
                        <p class="mb-0">1404 Concordia Ave St. Paul, MN 55104</p>
                        <p class="mb-0"><?php echo __d('about', 'TEL'); ?>: 952-393-7802</p>
                        <p class="mb-0"><?php echo __d('about', 'EMAIL'); ?>: <a href="mailto:<?php echo EMAIL_FROM; ?>"><?php echo EMAIL_FROM; ?></a></p>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <section>
                    <h3 class="s-title txt-yellow"><?php echo __d('about', 'HISTORY_TITLE'); ?></h3>
                    <div style="text-align: justify">
                        <?php echo $this->Language->transtlate('history') ?>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div class="bg-dakr-blue about-version">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12 f-koulen text-center">
                <h3 class="s-title text-white" style="font-size: 30px; text-transform: uppercase;"><?php echo __d('about', 'WHO_MADE_THIS_WEBSITE'); ?> </h3>
                <p class="text-center pt-3" style="font-size: 25px;"><?php echo __d('about', 'WHO_MADE_THIS_WEBSITE_CONTENT'); ?> </p>
            </div>
        </div>
        <div class="row">
            <div class="col-3 col-sm-3 col-md-3">
                <div class="member-wrap">
                        <img src="/img/teams/19-01-31-01-52-49154891757027.jpg" alt="" class="img-fluid"/>                            <section>
                            <h5 class="pt-3 text-center">Samphors</h5>
                        </section>
                    </div>
                </div>
                <div class="col-3 col-sm-3 col-md-3">
                    <div class="member-wrap">
                        <img src="/img/teams/19-07-28-12-30-48156428824865.jpg" alt="" class="img-fluid"/>                            <section>
                        <h5 class="pt-3 text-center">Yaya</h5>
                        </section>
                    </div>
                </div>
                <div class="col-3 col-sm-3 col-md-3">
                    <div class="member-wrap">
                        <img src="/img/teams/19-01-31-01-55-00154891770149.jpg" alt="" class="img-fluid"/>                            <section>
                        <h5 class="pt-3 text-center">Palleak</h5>
                        </section>
                    </div>
                </div>
                <div class="col-3 col-sm-3 col-md-3">
                    <div class="member-wrap">
                        <img src="/img/teams/19-01-31-01-55-41154891774268.jpg" alt="" class="img-fluid"/>                            <section>
                        <h5 class="pt-3 text-center">Rika</h5>
                        </section>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 pt-5">
                <a href="https://www.dropbox.com/s/9ssgnqr69yxz4h6/LearnKhmerOnlineSlidePresentation.pdf?dl=0" class="bg-dakr-blue read-more text-center" target="_blank"><?php echo __d('about', 'READ_MORE'); ?> </a>
                </div>
            </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-12 col-md-12">
            <section style="margin-top: 50px;">
                <h3 class="s-title txt-yellow"><?php echo __d('about', 'MESSAGE_FROM_FOUNDER'); ?></h3>
                <p style="margin-top:30px;">
                    <p style="float: left; margin-right:15px; display: grid">
                    <img class="img-fluid" src="http://learnkhmer.online/img/teams/19-01-31-01-42-34154891695512.jpg" style="width: 130px;"/>
                </p>
                <div style="text-align: justify">
                    <?php echo $this->Language->transtlate('greeting'); ?>
                </div>
            </section>
        </div>
    </div>
</div>

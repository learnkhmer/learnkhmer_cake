<div class="ws-grid">
    <?php
    if ($data):
        foreach ($data as $key => $value):
            ?>
            <div class="ws-grid-item ws-wrapper border border-right rounded">
                <div class="thumbnail position-relative overflow-hidden">
                    <?php
                    echo $this->WorkSheet->getThumbnailPath($value['WorkSheet']); ?>
                </div>
                <?php
                echo $this->element('/Front/asterisk'); ?>
            </div>
        <?php endforeach;
    endif; ?>
</div>
<?php
if ($data) {
    echo $this->element('/Front/pagination');
}
?>
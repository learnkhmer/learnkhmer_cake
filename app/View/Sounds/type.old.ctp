<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_WORKSHEET'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_WORKSHEET'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __d('worksheet', 'CATEGORY'); ?></h4>
                </div>
                <div class="row pt-1">
                    <div class="col">
                        <ul class="list-unstyled book-type">
                            <?php
                            foreach ($categories as $key => $value) : ?>
                            <li class="<?php echo $this->ActiveCategory->displayAactive($value['Category']['id']); ?>">
                                <?php
                                $category_name = Configure::read('Config.language') === 'kh' ? $value['Category']['name_kh'] : $value['Category']['name_en'];
                                echo $this->Html->link('<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span> '.$category_name,
                                '/work_sheets/type/'.$value['Category']['id'], array('escape' => false)); ?>
                            </li>
                            <?php
                            endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="row my-3">
                    <div class="col">
                        <?php
                        if ($category['CategoryDetail']['download_url']) {
                            echo $this->Html->link(__d('worksheet', 'DOWNLOAD_ALL'), $category['CategoryDetail']['download_url'], array(
                                'target' => '_blank',
                                'class' => 'btn btn-block bg-sea',
                            ));
                        } else {
                            echo $this->Html->link(__d('worksheet', 'DOWNLOAD_ALL'), 'javascript:void(0)', array(
                                'class' => 'btn btn-block btn-outline-danger',
                                'disabled' => true,
                            ));
                        }  ?>

                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9 border-blue entry-content">
                <div class="letter-list">
                    <?php
                    if ($data): ?>
                    <div class="row">
                        <?php
                        foreach ($data as $key => $value) : ?>
                        <div class="col-6 col-md-3">
                            <div class="letter-wrap p-1">
                                <?php
                                echo $this->Html->image(FILE_DOMAIN . PATH_WORKSHEET_IMG . $value['WorkSheet']['image_thumbnail'], array(
                                    'alt' => $value['WorkSheet']['title'],
                                    'class' => 'img-fluid',
                                )); ?>
                                <?php
                                $image1 = explode('.', $value['WorkSheet']['image_thumbnail']);
                                $link = ($value['WorkSheet']['link_to_file']) ? ($value['WorkSheet']['link_to_file']) : $image1[0].'.pdf';
                                echo $this->Html->link(__d('worksheet', 'DOWNLOAD'), 'https://docs.google.com/viewer?url='. FILE_DOMAIN . PATH_WORKSHEET_PDF . $link, array(
                                    'class' => 'btn bg-sea btn-sm btn-block',
                                    'target' => '_blank',
                                )); ?>
                            </div>
                        </div>
                        <?php
                        endforeach; ?>
                    </div>
                    <div class="justify-content-center my-3 row">
                        <div class="col-5 d-none">
                            <button class="btn btn-block btn-primary" style="box-shadow: 0 4px #29434f;">បង្ហាញ​បន្ថែម​ទៀត​</button>
                        </div>
                    </div>
                    <?php
                    endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

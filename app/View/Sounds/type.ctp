<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2>
                <?php
                echo __('FRONTEND_MENU_TXT_WORKSHEET'); ?>
            </h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_WORKSHEET'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap pl-3 pr-3">
    <div class="row">
        <div class="col-12 col-md-3">
            <?php
            echo $this->element('/Front/worksheet_category'); ?>
        </div>
        <div class="col-md-9 col-12">
            <div class="ws-banner-wrap">
                <?php
                // echo $this->Html->image('/img/worksheet/banner/banner_consonant.jpg', array(
                //     'class' => 'img-fluid ws-banner',
                //     'alt' => 'Kid Banner',
                // ));
                ?>
            </div>
            <div class="ws-list">
                <div class="ws-grid">
                    <?php
                    if ($data):
                        foreach ($data as $key => $value):
                            ?>
                            <div class="ws-grid-item ws-wrapper border border-right rounded">
                                <div class="thumbnail position-relative overflow-hidden">
                                    <?php
                                    echo $this->WorkSheet->getThumbnailPath($value['WorkSheet']); ?>
                                </div>
                                <?php
                                echo $this->element('/Front/asterisk'); ?>
                            </div>
                        <?php endforeach;
                    endif; ?>
                </div>
                <?php
                if ($data) {
                    echo $this->element('/Front/pagination');
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Html->css(array(
    'worksheet',
)); ?>
<script>
    $(function (e) {
        $('body').on('click', '.cat-item', function (e) {
            e.preventDefault();
            window.history.pushState('', '', '?cat_id='+$(this).attr('data-id')+'&parent_id='+$(this).attr('data-parent-id'));
            $('body').find('.book-type li').removeClass('active');
            $(this).closest('li').addClass('active');
            var params = {
                type: 'GET',
                url: BASE_URL + 'WorkSheets/getType',
                data: { id: $(this).attr('data-id') },
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajax_request(params, function (data) {
                $('body').find('.ws-list')
                        .empty()
                        .html(data);
            });
        });

        $('body').on('change', '#category-id', function (e) {
            var params = {
                type: 'GET',
                url: BASE_URL + 'WorkSheets/getType',
                data: { id: $(this).val() },
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajax_request(params, function (data) {
                $('body').find('.ws-list')
                        .empty()
                        .html(data);
            });
        });

        $('body').on('click', '.page-link', function (e) {
            var cat_id = $('.book-type').find('li.active > a').attr('data-id');
            if ($('meta[name="is_mobile"]').attr('content') == 1) {
                cat_id = ($('#category-id').val() === '') ? 13 : $('#category-id').val();
            }
            var page = $(this).attr('data-page');
            var params = {
                type: 'GET',
                url: BASE_URL + 'WorkSheets/getByPageNumber',
                data: {
                    page: page,
                    cat_id: cat_id
                },
                beforeSend: function () {
                    $.LoadingOverlay('show');
                }
            };
            ajax_request(params, function (data) {
                $('body').find('.ws-list')
                        .empty()
                        .html(data);
            });
        });

        $('body').on('click', '.download-icon', function (e) {
            var content = $(this).closest('.ws-wrapper');
            $(content).find('.thumbnail > a')[0].click();
        });
    });
</script>
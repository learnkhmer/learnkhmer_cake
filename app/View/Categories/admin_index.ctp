<div class="card">
    <div class="card-body">
        <?php
        echo $this->Form->create('Category', array(
            'type' => 'get',
        )); ?>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-12">
                <div class="form-group">
                    <?php
                    echo $this->Form->input('model', array(
                        'options' => unserialize(CATEGORY_MODE),
                        'empty' => 'Select Model',
                        'class' => 'form-control select2',
                        'value' => $this->request->query('model'),
                        'label' => false,
                        'required' => false,
                    )); ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-12">
                <div class="form-group">
                    <?php
                    echo $this->Form->input('category_id', array(
                        'options' => $categories,
                        'empty' => 'Choose Category',
                        'class' => 'form-control select2',
                        'value' => $this->request->query('category_id'),
                        'placeholder' => __('COMMON_TXT_SEARCH'),
                        'label' => false,
                        'style' => 'width: 100%',
                    )); ?>
                </div>
            </div>
            <div class="col-md-2 col-sm-8 col-12">
                <div class="form-group">
                    <?php
                    echo $this->Form->text('search', array(
                        'class' => 'form-control',
                        'value' => $this->request->query('search'),
                        'placeholder' => __('COMMON_TXT_SEARCH'),
                    )); ?>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-6">
                <div class="form-group">
                    <?php
                    echo $this->Form->input('search', array(
                        'class' => 'btn btn-primary',
                        'type' => 'button',
                        'placeholder' => __('COMMON_TXT_SEARCH'),
                        'label' => false,
                    )); ?>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 col-6">
                <div class="form-group">
                    <?php
                    echo $this->Html->link('Create', array(
                        'action' => 'create',
                        'prefix' => 'admin',
                    ), array(
                        'class' => 'btn btn-primary float-right',
                        'escape' => false,
                    )); ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<div class="card">
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Model</th>
                    <th>Parent</th>
                    <th>Name (KH)</th>
                    <th>Name (EN)</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $key => $value) : ?>
                    <tr>
                        <td><?php echo ++$key; ?></td>
                        <td><?php echo ucfirst($value['Category']['model']); ?></td>
                        <td><?php echo $value['Parent']['name_kh'].'<br />'.$value['Parent']['name_en']; ?></td>
                        <td><?php echo $value['Category']['name_kh']; ?></td>
                        <td><?php echo $value['Category']['name_en']; ?></td>
                        <td><span class="badge badge-<?php echo $value['Category']['status'] ? 'success' : 'danger'; ?>">Status</span></td>
                        <td class="project-actions text-right">
                            <?php
                            echo $this->Html->link('<i class="fas fa-pencil-alt"></i> Edit', '/admin/categories/edit/' . $value['Category']['id'], [
                                'class' => 'btn btn-info btn-sm',
                                'escape' => false,
                            ]); ?>

                            <?php
                            echo $this->Form->postLink('<i class="fas fa-trash"></i> Delete', '/admin/categories/delete/' . $value['Category']['id'], [
                                'class' => 'btn btn-danger btn-sm',
                                'escape' => false,
                                'confirm' => 'Are you sure you want to delete?',
                            ]); ?>
                        </td>
                    </tr>
                <?php
                endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <?php
        echo $this->element('/Backend/pagination'); ?>
    </div>
</div>

<?php
if (Configure::read('debug') > 0) {
	echo __d('cake', 'An Internal Error Has Occurred.');
	echo $this->element('exception_stack_trace', array('error' => '500'));
} else {
	$pf = isset($this->request->params['admin']) ? 'admin_not_found' : 'not_found';
	echo $this->element($pf, array('error' => '500'));
}
?>

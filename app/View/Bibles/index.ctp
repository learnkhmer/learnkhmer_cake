
<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_MUSIC'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_MUSIC'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap customize-paging">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __('CATEGORY'); ?></h4>
                </div>
                <div class="row pt-1">
                    <div class="col">
                        <ul class="list-unstyled cate-list mb-0" id="cat-list">
                            <?php foreach (unserialize(BIBLE) as $key => $value) : ?>
                            <li class="">
                                <?php
                                    echo $this->Html->link(
                                        '<span class="txt-cl-blue">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                                    </span> ' . __($value),
                                        '/bibles?type=' .$key,
                                        array('escape' => false)
                                    ); ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9">
                <div class="alert alert-primary"> 
                    <div class="row">
                        <div class="col-md-12">
                            <form action="<?php echo $this->here; ?>" method="get">
                                <input class="form-control keyword" name="keyword" placeholder="<?php echo __('COMMON_TXT_SEARCH'); ?>">
                            </form>
                        </div>
                    </div>
                </div>
                <div id="products" class="row view-group">
                    <?php
                    foreach ($data as $key => $value) : ?>
                    <div class="card px-3 py-4 col-md-4">
                        <div class="card-wrapper flip-card item" data-id="<?php echo $value['Song']['id']; ?>">
                            <div class="card-img">
                                <?php
                                $photo = $value['Singer']['photo'] ? SINGER_PATH.DS.$value['Singer']['photo'] : 'no_preview.png';
                                if ($value['Song']['photo']) {
                                    $photo = SINGER_SONG_PATH.DS.$value['Song']['photo'];
                                }
                                echo $this->Html->image($photo, [
                                    'class' => 'group list-group-image img-fluid',
                                    'alt' => $value['Song']['title']
                                ]); ?>
                            </div>
                            <div class="card-box" style="background-color: #2e5230; padding: 10px 15px 3px 15px;">
                                <h3 class="mbr-title mbr-fonts-style mbr-bold mbr-black display-5 text-center f-koulen" style="color: #fff">
                                    <?php echo $value['Song']['title']; ?>
                                </h3>
                            </div>
                            <div class="card-box">
                                <p class="mbr-card-text mbr-fonts-style align-left display-7 text-center f-khpali">
                                    <?php
                                    $singer_name = $value['Singer']['name_en'];
                                    if (Configure::read('Config.language') === 'kh') {
                                        $singer_name = $value['Singer']['name_kh'];
                                    }
                                    echo __('SANG_BY').': '.$singer_name; ?><br />
                                    <?php echo __('WRITTEN_BY').': '?><?php echo $value['Song']['written'] ? $value['Song']['written'] : 'N/A'; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php
                    endforeach; ?>
                </div>
                <div class="pagination justify-content-end">
                    <ul class="pagination">
                        <?php
                        echo $this->Paginator->prev('&laquo;', array('tag' => 'li', 'escape' => false), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                        echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                        echo $this->Paginator->next('&raquo;', array('tag' => 'li','currentClass' => 'disabled', 'escape' => false), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->script([
    '//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
    'jquery.matchHeight-min',
]);
echo $this->html->css(['//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css']);
?>
<script>
    $(document).ready(function() {
        $('.keyword').autocomplete({
            source: function(request, response) {
              $.ajax({
                type: 'GET',
                dataType: 'json',
                url: BASE_URL + 'songs/search',
                data: 'keyword=' + request.term,
                success: function(data) {
                  response($.map(data, function(item) {
                      var object = new Object();
                      object.label = item.label;
                      object.value = item.value;
                      return object
                  }));
                }
              });
            },
            select: function (event, ui) {
              window.location.href = BASE_URL + 'songs/view/' + ui.item.value;
             }
        });
        $('.item').matchHeight({});
        $('.item').on('click', function() {
            window.location.href = BASE_URL + 'songs/view/' + $(this).data('id');
        });
    });
</script>
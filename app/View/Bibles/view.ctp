
<!--BREADCRUMBS-->
<section class="bread-blog f-koulen">
    <div class="container">
        <div class="crumb-wrap">
            <h2><?php echo __('FRONTEND_MENU_TXT_MUSIC'); ?></h2>
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_HOME'), '/'); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link('<i class="fas fa-minus"></i>', 'javascript:void(0);' , array(
                        'escape' => false,
                    )); ?>
                </li>
                <li class="list-inline-item">
                    <?php
                    echo $this->Html->link(__('FRONTEND_MENU_TXT_MUSIC'), 'javascript:void(0);'); ?>
                </li>
            </ul>
        </div>
    </div>
</section>
<div class="bg-white reading-wrap">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="head-s-list b-bottom-blue d-flex">
                    <h4 class="mb-0 f-koulen txt-blue"><?php echo __('CATEGORY'); ?></h4>
                </div>
                <div class="row pt-1">
                <div class="col">
                        <ul class="list-unstyled cate-list mb-0" id="cat-list">
                            <li>
                            <a href="#animation" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sport" class="collapsed">
                                    <span class="txt-cl-blue">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                                    </span>
                                    <?php echo __('CATEGORY'); ?>
                                </a>
                                <ul class="pl-4 collapse show list-unstyled" id="animation" data-parent="#cat-list">
                                    <?php foreach ($categories as $key => $value) : ?>
                                        <?php
                                        if ($value['Category']['id'] == 170) {
                                            continue;
                                        } ?>
                                        <li class="">
                                            <?php
                                            $category_animate_name = Configure::read('Config.language') === 'kh' ? $value['Category']['name_kh'] : $value['Category']['name_en'];
                                            echo $this->Html->link(
                                                '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span> ' . $category_animate_name,
                                                '/songs?category=' . $value['Category']['id'],
                                                array('escape' => false)
                                            ); ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                            <?php foreach ($categories as $key => $value) : ?>
                                <?php
                                if ($value['Category']['id'] != 170) {
                                    continue;
                                } ?>
                                <li class="">
                                    <?php
                                    $category_animate_name = Configure::read('Config.language') === 'kh' ? $value['Category']['name_kh'] : $value['Category']['name_en'];
                                    echo $this->Html->link(
                                        '<span class="txt-cl-blue">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                                    </span> ' . $category_animate_name,
                                        '/songs?category=' . $value['Category']['id'],
                                        array('escape' => false)
                                    ); ?>
                                </li>
                            <?php endforeach; ?>
                            <li>
                                <a href="#singers" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="subject">
                                    <span class="txt-cl-blue">
                                        <i class="fa fa-plus-square" aria-hidden="true"></i>
                                    </span>
                                    <?php echo __('SINGER'); ?>
                                </a>
                                <ul class="pl-4 collapse list-unstyled" id="singers" data-parent="#cat-list">
                                    <?php foreach ($singers as $key => $value) : ?>
                                        <li class="">
                                            <?php
                                            $category_animate_name = Configure::read('Config.language') === 'kh' ? $value['Singer']['name_kh'] : $value['Singer']['name_en'];
                                            echo $this->Html->link(
                                                '<span class="txt-cl-blue"><i class="fa fa-angle-right" aria-hidden="true"></i></span> ' . $category_animate_name,
                                                '/songs?singer=' . $value['Singer']['id'],
                                                array('escape' => false)
                                            ); ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9">
                <!-- <div class="alert alert-primary"> 
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <form action="<?php echo $this->here; ?>" method="get">
                                <input class="form-control keyword" name="keyword" placeholder="<?php echo __('COMMON_TXT_SEARCH'); ?>">
                            </form>
                        </div>
                    </div>
                </div> -->
                <div>
                    <?php if ($data['Song']['mp3']) : ?>
                        <audio preload="auto" controls>
                            <source src="<?php echo SINGER_SONG_PATH.DS.$data['Song']['mp3']; ?>">;
                        </audio>
                    <?php else : ?>
                        <!-- <div class="alert alert-warning">NO Audio</div> -->
                    <?php endif; ?>
                    <div class="row lyrice">
                        <div class="col-md-12">
                            <div class="alert" style="border: 1px solid #cce5ff;">
                                <h5 class="f-koulen"><?php echo $data['Song']['title']; ?></h5>
                                <hr />
                                <div class="card-img">
                                    <?php
                                    if ($data['Song']['pdf']) {
                                        echo $this->Html->image(SINGER_SONG_PATH.DS.$data['Song']['pdf'], [
                                            'class' => 'group list-group-image img-fluid',
                                            'alt' => $data['Song']['title']
                                        ]);
                                    }?>
                                </div>
                                <div class="f-hanuman"><?php echo $data['Song']['lyrice']; ?></div>
                                <?php if ($data['Song']['pdf']) : ?>
                                <hr />
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php 
                                        $pdf = SINGER_SONG_PATH.DS.$data['Song']['pdf'];
                                        echo $this->Html->link('<span class="fa fa-file-pdf"> Download</span>', $pdf, [
                                            'escape' => false,
                                            'target' => '_blank',
                                            'class' => 'btn btn-danger btn-sm float-right',
                                        ]); ?>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="alert" style="border: 1px solid #cce5ff;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-borderless table-sm" style="margin-bottom: 0px">
                                            <tr>
                                                <td><?php echo __('SANG_BY'); ?></td>
                                                <td><?php echo __('WRITTEN_BY'); ?></td>
                                                <td><?php echo __('CATEGORY'); ?></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <?php
                                                    $singer_url = '/songs/?singer='.$data['Singer']['id'];
                                                    $singer_name = $data['Singer']['name_en'];
                                                    if (Configure::read('Config.language') === 'kh') {
                                                        $singer_name = $data['Singer']['name_kh'];
                                                    }
                                                    echo $this->Html->link($singer_name, $singer_url); ?>
                                                </td>
                                                <td>
                                                    <?php echo $data['Song']['written'] ? $data['Song']['written'] : 'N/A'; ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $category_url = '/songs/?category='.$data['Category']['id'];
                                                    $category_name = $data['Category']['name_en'];
                                                    if (Configure::read('Config.language') === 'kh') {
                                                        $category_name = $data['Category']['name_kh'];
                                                    }
                                                    echo $this->Html->link($category_name, $category_url); ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ads-block m-b">
                            <a href="http://learnkhmer.online/">
                                <img class="img-fluid img-center" alt="Learn Khmer Online" src="<?php echo $this->Html->url('/img/worksheet/banner/banner_consonant.jpg'); ?>">
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="products" class="row view-group">
                                <?php
                                foreach ($related_articles as $key => $value) : ?>
                                <div class="card px-3 py-4 col-md-4">
                                    <div class="card-wrapper flip-card item" data-id="<?php echo $value['Song']['id']; ?>">
                                        <div class="card-img">
                                            <?php
                                            $photo = $value['Singer']['photo'] ? SINGER_PATH.DS.$value['Singer']['photo'] : 'no_preview.png';
                                            echo $this->Html->image($photo, [
                                                'class' => 'group list-group-image img-fluid',
                                                'alt' => $value['Song']['title']
                                            ]); ?>
                                        </div>
                                        <div class="card-box" style="background-color: #2e5230; padding: 10px 15px 3px 15px;">
                                            <h3 class="mbr-title mbr-fonts-style mbr-bold mbr-black display-5 text-center f-koulen" style="color: #fff">
                                                <?php echo $value['Song']['title']; ?>
                                            </h3>
                                        </div>
                                        <div class="card-box">
                                            <p class="mbr-card-text mbr-fonts-style align-left display-7 text-center f-khpali">
                                                <?php
                                                $singer_name = $value['Singer']['name_en'];
                                                if (Configure::read('Config.language') === 'kh') {
                                                    $singer_name = $value['Singer']['name_kh'];
                                                }
                                                echo __('SANG_BY').': '.$singer_name; ?><br />
                                                <?php echo __('WRITTEN_BY').': '?><?php echo $value['Song']['written'] ? $value['Song']['written'] : 'N/A'; ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->script([
    'player/js/audioplayer',
    '//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
    'jquery.matchHeight-min',
]);
echo $this->html->css([
    '/js/player/css/audioplayer',
    '//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css']);
?>
<style>
.lyrice p {
    font-family: 'Hanuman', Fallback, sans-serif !important;
}
.f-hanuman .p1 {
    font-size: 1em !important;
}
img
</style>
<script>
    $(function() {
        $('audio').audioPlayer();
        $('.keyword').autocomplete({
            source: function(request, response) {
              $.ajax({
                type: 'GET',
                dataType: 'json',
                url: BASE_URL + 'songs/search',
                data: 'keyword=' + request.term,
                success: function(data) {
                  response($.map(data, function(item) {
                      var object = new Object();
                      object.label = item.label;
                      object.value = item.value;
                      return object
                  }));
                }
              });
            },
            select: function (event, ui) {
              window.location.href = BASE_URL + 'songs/view/' + ui.item.value;
             }
        });
        //Disable cut copy paste
        $('.lyrice').attr('unselectable','on')
        .css({'-moz-user-select':'-moz-none',
              '-moz-user-select':'none',
              '-o-user-select':'none',
              '-khtml-user-select':'none', /* you could also put this in a class */
              '-webkit-user-select':'none',/* and add the CSS class here instead */
              '-ms-user-select':'none',
              'user-select':'none'
        }).bind('selectstart', function(){ return false; });
    });
    // document.addEventListener('contextmenu', function(e) {
    //     e.preventDefault();
    // });
    $('.item').matchHeight({});
    $('.item').on('click', function() {
        window.location.href = BASE_URL + 'songs/view/' + $(this).data('id');
    });
</script>

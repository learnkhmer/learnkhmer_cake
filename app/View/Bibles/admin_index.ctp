<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <i class="fa fa-edit"></i>
                <?php 
                echo $this->Html->link('Create', '/admin/songs/create', [
                    'class' => 'box-title',
                ]); ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered data-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Singer</th>
                            <th>Category</th>
                            <th><?php echo __('COMMON_TXT_OPERATION'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data as $key => $value): ?>
                            <tr>
                                <td><?php echo ++$key; ?></td>
                                <td><?php echo $value['Song']['title']; ?></td>
                                <td><?php echo $value['Singer']['name_kh']; ?></td>
                                <td><?php echo $value['Category']['name_kh']; ?></td>
                                <td>
                                    <i class="fa fa-edit"></i>
                                    <?php 
                                    echo $this->Html->link('Edit', '/admin/songs/edit/'.$value['Song']['id'], [
                                        'class' => '',
                                    ]); ?>
                                    
                                    <i class="fa fa-trash"></i>
                                    <?php 
                                    echo $this->Html->link('Delete', 'javascript:void(0)', [
                                        'data-id' => $value['Song']['id'],
                                        'class' => 'btn-delete',
                                    ]); ?>
                                </td>
                            </tr>
                        <?php
                        endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.data-table').DataTable();
        $('.btn-delete').click(function(e) {
            var id = $(this).attr('data-id');
            if (confirm('Are you sure you want to delete this?')) {
                $.ajax({
                    type: 'get',
                    url: BASE_URL + 'admin/songs/delete/' + id,
                }).done(function(data) {
                    data = JSON.parse(data);
                    if (data.status === 'success') {
                        location.reload();
                    }
                });
            }
        });
    });
</script>

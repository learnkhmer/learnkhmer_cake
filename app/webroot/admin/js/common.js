function ajax_request(params, callback) {
    $.ajax(params).done(function(data){
        if ((data === '') && (data === 'undefined')) {
            return false;
        }
        if (typeof callback === 'function') {
            callback(JSON.parse(data));
        }
    }).always(function() {
        $.LoadingOverlay('hide');
    });
}

function readURL(input, content) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $(content).find('.img-preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(function () {
    // remove item

    function scrollToBottom() {
        var scrollBottom = Math.max($('.table-head-fixed').height() - $('.reading-item-container').height() + 20, 0);
        $('.reading-item-container').scrollTop(scrollBottom);
    }

    scrollToBottom();

    // $('.table-head-fixed').animate({ scrollTop: 9999 }, 'slow');
    $('.table-sortable').on('click', '.btn-delete', function (e) {
        e.preventDefault();
        var count = $(this).parent().parent().parent().find('tr').not('.remove').length;
        if (count <= 1) {
            return false;
        }
        $(this).parent().parent().css({ 'background': 'pink' });
        var checkType = $(this).parent().find('.id').val();
        if (checkType === undefined) {
            $(this).parent().parent().fadeOut('slow').remove();
        } else {
            $(this).parent().parent().find('.mode').val('delete');
            $(this).parent().parent().fadeOut('slow');
        }
        $(this).parent().parent().addClass('remove');
    });

    // add item
    $('.add-item').click(function (e) {
        e.preventDefault();
        var lastIndex = parseInt($('.order').last().val()) + 1;
        var clone = $('tr:last').clone();
        // remove class id for new item
        clone.find('.id').remove();
        clone.removeClass('remove');
        // set all fields to empty for new item
        clone.find('.text, .start, .end').val('');
        clone.find('.is_space').prop('checked', false);
        // set index for new item
        clone.find('.text').attr('name', 'data[ReadingItem][' + lastIndex + '][text]').prop('type', 'text');
        clone.find('.start').attr('name', 'data[ReadingItem][' + lastIndex + '][start]').prop('type', 'text');
        clone.find('.end').attr('name', 'data[ReadingItem][' + lastIndex + '][end]').prop('type', 'text');
        clone.find('.order').attr('name', 'data[ReadingItem][' + lastIndex + '][order]');
        clone.find('.is_space').attr('name', 'data[ReadingItem][' + lastIndex + '][is_space]');
        clone.find('.is_space').attr('type', 'checkbox');
        clone.find('.mode').attr('name', 'data[ReadingItem][' + lastIndex + '][mode]');
        // set value to order for new item
        clone.find('.order').val(lastIndex);
        // this mode is mostly come with create
        clone.find('.mode').val('create');
        // append new item
        $('.table-sortable').append(clone);
        clone.css({ 'background': 'white' }).fadeIn();
        scrollToBottom();
    });
});

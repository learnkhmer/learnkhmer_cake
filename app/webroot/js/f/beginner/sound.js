$(document).ready(function() {
    $('.collapse').on('shown.bs.collapse', function () {
        $('.vowel-info').removeClass('txt-blue').addClass('txt-red');
    });

    $('.collapse').on('hidden.bs.collapse', function () {
        $('.vowel-info').removeClass('txt-red').addClass('txt-blue');
    });

    // play audio
    $('.btn-audio').click(function(e) {
        var content  = $(this).closest('.letter-wrap').find('audio');
        $(content)[0].play();
    });

    $('.letter-wrap').on('mouseover', function() {
        $(this).find('.btn-audio').trigger('click');
    }).mouseout(function() {
        var sound = $(this).find('audio')[0];
        sound.pause();
        sound.currentTime = 0;
    });
    // end play audoi
});

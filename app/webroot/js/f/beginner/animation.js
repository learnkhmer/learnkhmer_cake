$(document).ready(function() {
    $('.collapse').on('shown.bs.collapse', function () {
        $('.vowel-info').removeClass('txt-blue')
                .addClass('txt-red');
    });

    $('.collapse').on('hidden.bs.collapse', function () {
        $('.vowel-info').removeClass('txt-red')
                .addClass('txt-blue');
    });

    $('[data-fancybox]').fancybox({
        youtube : {
            controls : 0,
            showinfo : 0
        },
        vimeo : {
            color : 'f00'
        }
    });

    $('.youtube-ico').click(function(e) {
        $(this).closest('.vdo-item')
                .find('a').click();
    });

    // play audio
    $('.btn-audio').click(function(e) {
        var content  = $(this).closest('.letter-wrap').find('audio');
        $(content)[0].play();
    });

    $('.letter-wrap').on('mouseover', function() {
        $(this).find('.btn-audio').trigger('click');
    }).mouseout(function() {
        var sound = $(this).find('audio')[0];
        sound.pause();
        sound.currentTime = 0;
    });
    // end play audio

    $('.writing-popup').click(function(e) {
        $.ajax({
            type: 'GET',
            url: BASE_URL + 'beginners/view/',
            dataType: 'html',
            data: { id: $(this).attr('data-id') },
            beforeSend: function() {
                $.LoadingOverlay('show');
            }
        }).done(function(data) {
            if ((data === null) && (data === 'undefined')) {
                return false;
            }
            var modal = $('body').find('#modal-detail');
            $(modal).modal('show');
            $(modal).find('.modal-body').html(data);
        }).always(function() {
            $.LoadingOverlay('hide');
        });
    });

    $('body').on('click', '.close-modal', function(e) {
        var content = $(this).closest('.modal').find('.modal-body').empty();
    });
});

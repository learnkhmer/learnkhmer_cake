$(function () {
    $('form.contact-form-lko').on('submit', function (e) {
        e.preventDefault();
        $.LoadingOverlay('show');
        $.ajax({
            url: BASE_URL + 'contacts/send',
            type:'POST',
            cache: false,
            dataType:'json',
            data: $('form.contact-form-lko').serializeArray(),
            success: function (respond) {
                if (respond.status === STATUS_SUCCESS) {
                    $('form.contact-form-lko .no-error').removeClass('text-danger').addClass('text-success').text(CANTACT_SUCCESS_MESSAGE);
                    $('form.contact-form-lko').trigger('reset');
                } else {
                    $('form.contact-form-lko .no-error').removeClass('text-success').addClass('text-danger').text(CANTACT_ERROR_MESSAGE);
                }
                $.LoadingOverlay('hide');
            }
        });
    });
});

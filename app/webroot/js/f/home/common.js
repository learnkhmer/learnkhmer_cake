$(function() {
    $('.go-top').click(function() {
        $('html, body').animate({scrollTop: 0}, 1000);
    });

    $('.close-nav').click(function() {
        $('#mySidenav').css({
            'width': '0'
        });
        $('body').css({
            'overflow': 'auto',
            'position': 'unset'
        });
        $('#nav-popup').hide();
    });

    $('.open-nav').click(function() {
        $('#mySidenav').css({
            'width': '250px'
        });
        $('body').css({
            'overflow': 'hidden',
            'position': 'fixed'
        });
        $('#nav-popup').show();
    });
});

function ajax_request(params, callback) {
    $.ajax(params).done(function(data){
        if ((data === '') && (data === 'undefined')) {
            return false;
        }
        if (typeof callback === 'function') {
            callback(data);
        }
    }).always(function() {
        $.LoadingOverlay('hide');
    });
}

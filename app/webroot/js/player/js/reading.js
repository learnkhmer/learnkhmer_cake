var Reading = (function() {

        function init()
        {
            initEvents();
        }

        function initEvents()
        {
            $('.bb-sound').click(play);
            $('.bb-nav-close').click(leavePage);
        }

        function play()
        {
            // var content = $('body').find('.jspContainer');
            var content = $('body').find('#bb-bookblock');
            
            var audio = $('.sound-control')[0];
            //console.log(audio);
            if (audio.paused) {
                // generate popcorn to highlight text
                var popcorn = Popcorn('#' + audio.id);
                $(content).find('.text-lesson span').each(function(i) {
                    var start = $(this).attr('data-start');
                    var end = $(this).attr('data-end');
                    popcorn.code({
                        start: start,
                        end: end,
                        onStart: function(options) {
                            highlightText(i, content);
                        },
                        onEnd: function(options) {
                            removeHighlightText(content);
                        }
                    });
                });
                // play audio
                audio.play();
                // change icon
                $('.bb-sound').find('i').removeClass('fa-volume-up').addClass('fa-volume-off');
            } else {
                // pause audio
                audio.pause();
                // change icon
                $('.bb-sound').find('i').removeClass('fa-volume-off').addClass('fa-volume-up');
            }
        }

        function highlightText(num, feature)
        {
            var $currentLine = feature.find('.text-lesson span:eq(' + num + ')');
            $currentLine.addClass('speaking');
        }

        function removeHighlightText(feature)
        {
            feature.find('span.speaking').removeClass('speaking');
        }

        function leavePage()
        {
            window.location.href = document.location.origin + '/readings';
        }

        return { init : init };
})();

var Reading = (function() {

    function init()
    {
        initEvents();
    }

    function initEvents()
    {
        $('#prev_page_button, #next_page_button').click(removeSound);
        // when sound is ended
        // $('.sound-control')[0].addEventListener('ended', function () {
        //     $('i').css({'color' : '#13386a'});
        // }, false);
        // click sound and play
        $('body').off().on('click', '.sound', function() {
            var that = this;
            var audioId = $(that).data('sound-id');
            var audio = $('#' + audioId)[0];
            // remove active
            $('i').css({'color' : '#13386a'});
            // //console.log(audio);
            var content = $(that).closest('.content');
            if (audio.paused) {
                // pause all if audio is playing
                $('body').find('.sound-control').each(function () {
                    if (!$(this)[0].paused) {
                        $(this)[0].pause();
                    }
                });
                // generate popcorn to highlight text
                var popcorn = Popcorn('#' + audioId);
                $(content).find('.text-lesson span').each(function(i) {
                    var start = $(this).attr('data-start');
                    var end = $(this).attr('data-end');
                    popcorn.code({
                        start: start,
                        end: end,
                        onStart: function(options) {
                            highlightText(i, content);
                        },
                        onEnd: function(options) {
                            removeHighlightText(content);
                        }
                    });
                });
                // play audio
                audio.play();
                // change icon
                $(that).find('i').css({'color' : 'red'})
            } else {
                // pause audio
                audio.pause();
                // change icon
                $(that).find('i').css({'color' : '#13386a'})
            }
        });
        $('.bb-nav-close').click(leavePage);
    }

    function removeSound()
    {
        $('i').css({'color' : '#13386a'});
        $('body').find('.sound-control').each(function () {
            var audio = $(this)[0];
            if (!audio.paused) {
                audio.pause();
            }
        });
    }

    function highlightText(num, feature)
    {
        var currentLine = feature.find('.text-lesson span:eq(' + num + ')');
        currentLine.addClass('speaking');
    }

    function removeHighlightText(feature)
    {
        feature.find('span.speaking').removeClass('speaking');
    }

    function leavePage()
    {
        window.location.href = document.location.origin + '/readings';
    }

    return { init : init };
})();

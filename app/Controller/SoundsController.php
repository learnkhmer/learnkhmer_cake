<?php
App::uses('Controller', 'AppController');

class SoundsController extends AppController
{
    public $title_for_layout = 'Basic';
    public $components = array(
        'Image',
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->loadModel('Category');
        $this->loadModel('Beginner');
        $this->Auth->allow(array(
            'view',
            'animation',
            'sound',
        ));
        $this->Security->unlockedActions = array(
            'admin_saveAndUpdate',
        );
    }

    public function admin_index()
    {
        $conditions = [
            'Category.model' => MENU_MODEL_BEGINNER_SOUND,
        ];
        if ($this->request->query('category_id')) {
            $conditions['category_id'] = $this->request->query('category_id');
        }
        $categories = $this->Category->find('list', array(
            'fields' => [
                'Category.id',
                'Category.name_kh',
            ],
            'conditions' => array(
                'Category.model' => MENU_MODEL_BEGINNER_SOUND,
            ),
            'order' => array(
                'Category.order' => 'asc',
            ),
        ));
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'limit' => LIMIT,
            'order' => array(
                'Beginner.order' => 'desc',
            )
        );
        $data = $this->Paginator->paginate('Beginner');
        $this->set([
            'data' => $data,
            'categories' => $categories,
            'title_for_layout' => $this->title_for_layout . ' List',
        ]);
    }

	public function admin_create()
    {
        $this->loadModel('Category');
        $this->request->data = $this->Beginner->create();
        $categories = $this->Category->getAllByModelBeginnerAndType($this->request->query('type'));
        $data1 = array(
            'title_for_layout' => $this->title_for_layout . 'Create',
            'categories' => $categories,
        );
        $this->set($data1);
    }

    public function admin_edit($id = null)
    {
        $data = $this->Beginner->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->loadModel('Category');
        $categories = $this->Category->getAllByModelBeginnerAndType($this->request->query('type'));
        $this->request->data = $data;
        $data1 = array(
            'title_for_layout' => $this->title_for_layout . 'Edit',
            'categories' => $categories,
        );
        $this->set($data1);
    }

    public function admin_delete($id = null)
    {
        if (!$id) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $data = $this->Beginner->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Beginner->id = $data['Beginner']['id'];
        if ($this->Beginner->delete()) {
            $this->redirect(array('action' => $this->request->query('type')));
        }
    }

    public function admin_saveAndUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->entity($this->request->data);
        $this->Beginner->set($data);
        // Check validation
        if (!$this->Beginner->validates()) {
            return json_encode(array(
                'status' => '0',
                'message' => MESSAGE_FAIL,
                'data' => $this->Beginner->validationErrors,
            ));
        }
        if ($this->Beginner->save($data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
                'data' => array('type' => $this->request->data['Beginner']['type']),
            ));
        }
    }

    private function entity($data = null)
    {
        if ($data === null) {
            return array();
        }
        $data = $data['Beginner'];
        if ($data['sound'] === STATUS_UPDATE) {
            $data['sound'] = $this->Image->check_and_upload_file($data['sound_file'], MENU_MODEL_BEGINNER, FILE_TYPE_SOUND);
            unset($data['sound_file']);
        }
        if ($data['sound_2'] === STATUS_UPDATE) {
            $data['sound_2'] = $this->Image->check_and_upload_file($data['sound_file_2'], MENU_MODEL_BEGINNER, FILE_TYPE_SOUND_2);
            unset($data['sound_file_2']);
        }
        if ($data['image_thumbnail'] === STATUS_UPDATE) {
            $data['image_thumbnail'] = $this->Image->check_and_upload_file($data['image_thumbnail_file'], MENU_MODEL_BEGINNER, FILE_TYPE_THUMBNAIL);
            unset($data['image_thumbnail_file']);
        }
        if ($data['image'] === STATUS_UPDATE) {
            $data['image'] = $this->Image->check_and_upload_file($data['image_file'], MENU_MODEL_BEGINNER, FILE_TYPE_IMAGE);
            unset($data['image_file']);
        }
        if ($data['image_letter'] === STATUS_UPDATE) {
            $data['image_letter'] = $this->Image->check_and_upload_file($data['image_letter_file'], MENU_MODEL_BEGINNER, FILE_TYPE_IMAGE_LETTER);
            unset($data['image_letter_file']);
        }
        if ($data['image_drawing'] === STATUS_UPDATE) {
            $data['image_drawing'] = $this->Image->check_and_upload_file($data['image_drawing_file'], MENU_MODEL_BEGINNER, FILE_TYPE_IMAGE_DRAWING);
            unset($data['image_drawing_file']);
        }
        return $data;
    }
}

<?php
App::uses('Controller', 'AppController');

class NovelsController extends AppController
{
    public $title_for_layout = 'Reading';
    public $components = array(
        'Image',
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->loadModel('Reading');
        $this->loadModel('ReadingItem');

        $this->Auth->allow(array(
            'index',
            'view',
        ));
    }

    public function index()
    {
        $data = array(
            'data' => $this->Reading->find('all', [
                'conditions' => [
                    'COALESCE(Reading.status, 0)' => 0,
                    'Reading.type' => 2,
                ],
                'order' => ['id' =>'DESC'],
                'recursive' => false,
            ]),
            'title_for_layout' => $this->title_for_layout . ' List',
        );
        // pr($data);
        $this->set($data);
    }

    public function view($id = null)
    {
        $this->Reading->recursive = -1;
        $reading = $this->Reading->findById($id);
        if (!$reading) {
            throw new NotFoundException();
        }
        $item = $this->request->query('item');
        if ($item) {
            $data = $this->ReadingItem->findById($item);
        } else {
            $data = $this->ReadingItem->find('first', array(
                'order' => array('ReadingItem.order' => 'asc'),
                'conditions' => [
                    'ReadingItem.reading_id' => $reading['Reading']['id'], 
                ]
            ));
        }
        $neighbors = $this->ReadingItem->find('neighbors', array(
            'conditions' => [
                'reading_id' => $reading['Reading']['id']
            ],
            'field' => 'order',
            'value' => $data['ReadingItem']['order']
        ));
        $this->set(compact('data', 'neighbors'));
        $this->set('title_for_layout', $this->title_for_layout.'');
    }

    public function admin_index()
    {
        $data = array(
            'data' => $this->Reading->find('all', [
                'conditions' => [
                    'COALESCE(Reading.status, 0)' => 0,
                    'Reading.type' => 2,
                ],
                'order' => ['id' =>'DESC']
            ]),
            'title_for_layout' => $this->title_for_layout . ' List',
        );
        $this->set($data);
    }

    public function admin_item($reading_id)
    {
        $data = array(
            'data' => $this->ReadingItem->find('all', [
                'conditions' => [
                    'COALESCE(ReadingItem.status, 0)' => 0,
                    'ReadingItem.reading_id' => $reading_id,
                ],
                'order' => ['order' =>'asc']
            ]),
            'reading_id' => $reading_id,
            'title_for_layout' => $this->title_for_layout . ' List',
        );
        $this->set($data);
        $this->render('Item/admin_index');
    }

    public function admin_item_create($reading_id)
    {
        $this->set([
            'title_for_layout' => $this->title_for_layout . 'Create',
            'reading_id' => $reading_id,
        ]);
        $this->render('Item/admin_create');
    }

    public function admin_item_edit($id = null)
    {
        $data = $this->ReadingItem->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->request->data = $data;
        $this->set(['title_for_layout' => $this->title_for_layout . 'Edit']);
        $this->render('Item/admin_edit');
    }


    public function admin_create()
    {
        $this->set(['title_for_layout' => $this->title_for_layout . 'Create']);
    }

    public function admin_edit($id = null)
    {
        $data = $this->Reading->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->request->data = $data;
        $this->set(['title_for_layout' => $this->title_for_layout . 'Edit']);
    }
    
    public function admin_delete($id = null)
    {
        if (!$id) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $data = $this->Reading->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Reading->id = $data['Reading']['id'];
        if ($this->Reading->delete()) {
            $this->redirect(array('action' => 'index'));
        }
    }

    public function admin_item_save()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->ReadingItem->set($this->request->data);
        if (!$this->ReadingItem->validates()) {
            return json_encode(array(
                'status' => 0,
                'message' => MESSAGE_FAIL,
                'data' => $this->ReadingItem->validationErrors,
            ));
        }
        $data = $this->item_entity($this->request->data);
        $this->ReadingItem->id = $this->request->data['ReadingItem']['id'];
        if ($this->ReadingItem->save($data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }

    public function admin_save()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->Reading->set($this->request->data);
        if (!$this->Reading->validates()) {
            return json_encode(array(
                'status' => 0,
                'message' => MESSAGE_FAIL,
                'data' => $this->Reading->validationErrors,
            ));
        }
        $data = $this->entity($this->request->data);
        $this->Reading->id = $this->request->data['Reading']['id'];
        if ($this->Reading->save($data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }

    private function entity($data)
    {
        if (!empty($data['Reading']['tmp_image']['name'])) {
            $data['Reading']['photo'] = $this->Image->check_and_upload_file($data['Reading']['tmp_image'], MENU_MODEL_READING, 'novel');
            return $data;
        }
    }

    private function item_entity($data)
    {
        if (!empty($data['ReadingItem']['tmp_image']['name'])) {
            $data['ReadingItem']['photo'] = $this->Image->check_and_upload_file($data['ReadingItem']['tmp_image'], MENU_MODEL_READING, 'novel');
            return $data;
        }
    }
}

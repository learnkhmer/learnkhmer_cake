<?php
App::uses('Controller', 'AppController');

class MembersController extends AppController
{
    public $title_for_layout = 'Members';
    public $components = array(
        'Image',
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Security->unlockedActions = array(
            'admin_saveAndUpdate',
        );
    }

    public function admin_index()
    {
        $options = array();
        if ($this->request->query('search')) {
            $options[] = array(
                'OR' => array(
                    'Member.phone LIKE' => '%' . trim($this->request->query('search')) . '%',
                    'Member.name LIKE' => '%' . trim($this->request->query('search')) . '%',
                    'Member.position LIKE' => '%' . trim($this->request->query('search')) . '%',
                    'Member.description LIKE' => '%' . trim($this->request->query('search')) . '%',
                    'Member.email LIKE' => '%' . trim($this->request->query('search')) . '%',                ),
            );
        }
        $this->Paginator->settings = array(
            'conditions' => $options,
            'limit' => LIMIT,
            'order' => array(
                'Member.sort' => 'asc',
            )
        );
        $data = array(
            'data' => $this->Paginator->paginate('Member'),
            'title_for_layout' => $this->title_for_layout . ' List',
        );
        $this->set($data);
    }

    public function admin_edit($id = null)
    {
        $data = $this->Member->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->request->data = $data;
        $data = array(
            'title_for_layout' => $this->title_for_layout . 'Edit',
        );
        $this->set($data);
    }

    public function admin_create()
    {
        $data = array(
            'title_for_layout' => $this->title_for_layout . 'Create',
        );
        $this->set($data);
    }

    public function admin_delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException();
        }
        $data = $this->Member->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Member->id = $data['Member']['id'];
        $this->Member->delete();
        $this->redirect(array('action' => 'index'));
    }

    public function admin_save()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->Member->set($this->request->data);
        $data = $this->entity($this->request->data);
        // Check validation
        if (!$this->Member->validates()) {
            return json_encode(array(
                'status' => '0',
                'message' => MESSAGE_FAIL,
                'data' => $this->Member->validationErrors,
            ));
        }
        $this->Member->id = $this->request->data['Member']['id'];
        if ($this->Member->save($data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }

    private function entity($data)
    {
        if (!empty($data['Member']['tmp_photo']['name'])) {
            $path = WWW_ROOT . SINGER_PATH;
            $data['Member']['photo'] = $this->Image->upload($data['Member']['tmp_photo'], $path);
            return $data;
        }
    }
}

<?php
App::uses('Controller', 'AppController');

class SongsController extends AppController
{
    public $title_for_layout = 'Song';
    public $components = array(
        'Image',
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(array(
            'index',
            'view',
            'christ',
        ));
    }

    public function index()
    {
        $this->loadModel('Category');
        $this->loadModel('Singer');
        $categories = $this->Category->find('all', [
            'conditions' => [
                'Category.status' => 1,
                'Category.model' => CATEGORY_SONG,
            ],
            'order' => [
                'Category.order' => 'asc',
            ]
        ]);
        $singers = $this->Singer->find('all', [
            'conditions' => [
                'status' => 0,
            ],
            'order' => [
                'id' => 'asc',
            ],
            'limit' => 5,
        ]);
        $conditions = [
            'Song.status' => 0,
            'Category.model' => CATEGORY_SONG,
        ];
        if ($this->request->query('category')) {
            $conditions[] = [
                'Song.category_id' => $this->request->query('category'),
            ];
        }
        if ($this->request->query('singer')) {
            $conditions[] = [
                'Song.singer_id' => $this->request->query('singer'),
            ];
        }
        if ($this->request->query('keyword')) {
            $conditions[] = [
                'OR' => [
                    'Song.title LIKE' => '%'.$this->request->query('keyword').'%',
                    'Song.written LIKE' => '%'.$this->request->query('keyword').'%',
                    'Song.lyrice LIKE' => '%'.$this->request->query('keyword').'%',
                ],
            ];
        }
        $this->Paginator->settings = array(
            'order' => array(
                'Song.id' => 'desc',
            ),
            'limit' => 20,
            'conditions' => $conditions,
            'contain' => [
                'Category'
            ]
        );
        $data = $this->Paginator->paginate('Song');
        $this->set([
            'categories' => $categories,
            'singers' => $singers,
            'data' => $data,
        ]);
    }

    public function view($id = null)
    {
        $data = $this->Song->findByIdAndStatus($id, 0);
        if (!$data) {
            new NotFoundException();
        }
        $this->set([
            'data' => $data,
            'related_articles' => [],
        ]);
    }

    public function admin_index()
    {
        $conditions = [
            'Song.status' => 0,
            'Category.model' => CATEGORY_SONG,
        ];
        if ($this->request->query('search')) {
            $conditions[] = array(
                'OR' => array(
                    'Song.title LIKE' => '%'.$this->request->query('search').'%',
                    'Song.written LIKE' => '%'.$this->request->query('search').'%',
                    'Song.lyrice LIKE' => '%'.$this->request->query('search').'%',
                )
            );
        }

        if ($this->request->query('singer_id')) {
            $conditions['Song.singer_id'] = $this->request->query('singer_id');
        }

        if ($this->request->query('category_id')) {
            $conditions['Song.category_id'] = $this->request->query('category_id');
        }
        
        $this->loadModel('Singer');
        $this->loadModel('Category');
        $singers = $this->Singer->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'status' => 0,
            ]
        ]);
        $categories = $this->Category->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'Category.model' => CATEGORY_SONG,
                'Category.status' => 1,
                'Category.parent_id' => NULL,
            ],
        ]);

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'limit' => LIMIT,
            'order' => array(
                'Song.id' => 'desc',
            ),
            'contain' => [
                'Category'
            ]
        );
        $data = $this->Paginator->paginate('Song');

        $this->set([
            'singers' => $singers,
            'categories' => $categories,
            'data' => $data,
        ]);
    }

    public function admin_create()
    {
        $this->loadModel('Singer');
        $this->loadModel('Category');
        $singers = $this->Singer->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'status' => 0,
            ]
        ]);
        $categories = $this->Category->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'Category.model' => 'song',
                'Category.status' => 1,
                'Category.parent_id' => NULL,
            ],
        ]);
        $data = [
            'singers' => $singers,
            'categories' => $categories,
            'title_for_layout' => $this->title_for_layout . 'Create'
        ];
        $this->set($data);
    }

    public function admin_edit($id = null)
    {
        $data = $this->Song->find('first', [
            'conditions' => [
                'Category.model' => CATEGORY_SONG,
                'Song.id' => $id,
            ],
            'contain' => [
                'Category'
            ]
        ]);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->loadModel('Singer');
        $this->loadModel('Category');
        $singers = $this->Singer->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'status' => 0,
            ]
        ]);
        $categories = $this->Category->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'Category.model' => 'song',
                'Category.status' => 1,
                'Category.parent_id' => NULL,
            ],
        ]);
        $this->request->data = $data;
        $this->set([
            'singers' => $singers,
            'categories' => $categories,
            'title_for_layout' => $this->title_for_layout . 'Edit'
        ]);
    }

    public function admin_delete($id = null)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $data = $this->Song->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Song->id = $data['Song']['id'];
        $save = [
            'Song' => [
                'status' => 1,
            ],
        ];
        if ($this->Song->save($save)) {
            return json_encode(array(
                'status' => STATUS_SUCCESS,
            ));
        }
    }

    public function admin_save()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->entity($this->request->data);
        $this->Song->set($data);
        if (!$this->Song->validates()) {
            return json_encode(array(
                'status' => 0,
                'message' => MESSAGE_FAIL,
                'data' => $this->Song->validationErrors,
            ));
        }
        $this->Song->id = $this->request->data['Song']['id'];
        if ($this->Song->save($data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }

    private function entity($data)
    {
        if (!empty($data['Song']['tmp_thumnail']['name'])) {
            $path = WWW_ROOT . MUSIC_PAHT_IMAGE;
            $data['Song']['thumnail'] = $this->Image->upload($data['Song']['tmp_thumnail'], $path);
        }
        if (!empty($data['Song']['tmp_audio']['name'])) {
            $path = WWW_ROOT . MUSIC_PAHT_MP3;
            $data['Song']['audio'] = $this->Image->upload($data['Song']['tmp_audio'], $path);
        }
        if (!empty($data['Song']['tmp_chord_image']['name'])) {
            $path = WWW_ROOT . MUSIC_PAHT_IMAGE;
            $data['Song']['chord_image'] = $this->Image->upload($data['Song']['tmp_chord_image'], $path);
        }
        if (!empty($data['Song']['tmp_lyric_image']['name'])) {
            $path = WWW_ROOT . MUSIC_PAHT_IMAGE;
            $data['Song']['lyric_image'] = $this->Image->upload($data['Song']['tmp_lyric_image'], $path);
        }
        return $data;
    }
}

<?php

class IpnComponent extends Component
{
    public function validate_ipn_data($data)
    {
        // change these with your information
        // sandbox for testing or empty ''
        $paypalmode = @$_SERVER['SERVER_NAME'] === 'learnkhmer.online' ? '' : 'sandbox';

        if ($data) {
            if ($paypalmode === 'sandbox') {
                $paypalmode = '.sandbox';
            }

            $req = 'cmd='.urlencode('_notify-validate');

            foreach ($data as $key => $value) {
                $value = urlencode(stripslashes($value));
                $req .= "&$key=$value";
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://www'.$paypalmode.'.paypal.com/cgi-bin/webscr');
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Host: www'.$paypalmode.'.sandbox.paypal.com'));
            $res = curl_exec($ch);
            curl_close($ch);

            if (strcmp($res, 'VERIFIED') === 0) {
                return true;
            }
        }
        return false;
    }

    public function paypal_data($data = null)
    {
        return $data['url']
            .'?cmd=_donations'
            .'&business='.$data['business']
            .'&notify_url='.$data['notify_url']
            .'&item_name='.h($data['item_name'])
            .'&item_number='.$data['item_number']
            .'&description='.$data['description']
            .'&amount='.$data['amount']
            .'&currency_code=USD'
            .'&cancel_return='.$data['cancel_url']
            .'&return='.$data['return'];
    }
}
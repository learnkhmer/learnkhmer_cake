<?php
App::uses('Component', 'Controller');

class ImageComponent extends Component
{
    public $new_file_path;
    public $new_width;
    // 生成された画像サイズ
    public $new_height;
    private $img_fname_arr;
    // [.]で分割されたファイル名-配列
    private $img_ext;
    private $temp_image;
    private $create_img_width;
    // 指定した画像サイズ
    private $create_img_height;
    private $temp_image_width;
    // アップされた画像サイズ
    private $temp_image_height;
    private $img_create;
    private $src_file_realpath;
    private $new_file_realpath;
    protected $rootRealpath;
    protected $url;

    function __construct()
    {
        $this->url          = 'http://'.$_SERVER["SERVER_NAME"];
        //末尾のスラッシュなし
        $this->rootRealpath = $_SERVER["DOCUMENT_ROOT"];
        //末尾のスラッシュなし
    }

    public function upload($file, $path = null)
    {
        if (strlen($file['name']) > 4) {
            if (!is_dir($path)) mkdir($path, 0777, true);
            $temp        = explode('.', $file['name']);
            $newfilename = date('y-m-d-h-i-s').round(microtime(true)).mt_rand(5,100).'.'.end($temp);

            move_uploaded_file($file['tmp_name'], $path.'/'.$newfilename);

            return $newfilename;
        }
    }

    public function disp_resize_img_path($src_img_path, $create_img_width, $create_img_height, $type = 'fit')
    {

        /* realpath変換 */

        if (substr($src_img_path, 0, 1) == "/") {
            $src_file_realpath = $this->rootRealpath.$src_img_path;
        } else {
            $src_file_realpath = str_replace($this->url, $this->rootRealpath, $src_img_path);
        }

        /* ファイルの存在チェック */

        // MODIFIED
        $src_file_realpath = $src_img_path;

        if (!is_file($src_file_realpath)) {
            return 'noimage';
        } else {

            /* 画像情報 */

            $img_fname_arr = explode(".", $src_img_path);

            // 拡張子を除いた画像ファイル名（ファイル名に「.」「.jpg」が入っている時にも対応）
            $img_first_name = $img_fname_arr[0];

            for ($i = 1; $i < (count($img_fname_arr) - 1); $i++) {
                $img_first_name .= ".".$img_fname_arr[$i];
            }

            // 画像拡張子（小文字）
            $img_ext = strtolower($img_fname_arr[count($img_fname_arr) - 1]);

            switch ($type) {
                case 'fit' :
                    $create_img_width  = intval($create_img_width);
                    $create_img_height = intval($create_img_height);
                    $img_create        = (is_numeric($create_img_width) && is_numeric($create_img_height)) ? 'on' : 'off';
                    break;
                case 'resize' :
                    $create_img_width  = intval($create_img_width);
                    $create_img_height = intval($create_img_height);
                    $img_create        = (is_numeric($create_img_width) && is_numeric($create_img_height)) ? 'on' : 'off';
                    break;
                case 'wfit' :
                    $create_img_width  = intval($create_img_width);
                    $create_img_height = 'auto';
                    $img_create        = (is_numeric($create_img_width)) ? 'on' : 'off';
                    break;
                case 'hfit' :
                    $create_img_width  = 'auto';
                    $create_img_height = intval($create_img_height);
                    $img_create        = (is_numeric($create_img_height)) ? 'on' : 'off';
                    break;
                case 'resizefit' :
                    $create_img_width  = intval($create_img_width);
                    $create_img_height = intval($create_img_height);
                    $img_create        = (is_numeric($create_img_width) && is_numeric($create_img_height)) ? 'on' : 'off';
                    break;
                default :
                    $img_create        = 'off';
            }

            // 生成画像パス
            if ($img_create == 'on') {
                // 作成される画像ファイル名
                $new_file_path = $img_first_name.'-'.$type.'-'.$create_img_width.'x'.$create_img_height.'.'.$img_ext;
            } else {
                // 作成されなかった時は元画像
                $new_file_path = $src_img_path;
            }

            /* 画像処理 */

            if (is_file($new_file_path)) {
                return $new_file_path;
            } elseif (!is_file($new_file_path) && ($img_ext == 'jpg' || $img_ext == 'gif' || $img_ext == 'png')) {

                //拡張子により処理を分岐
                switch ($img_ext) {
                    case 'jpg' :
                        $temp_image = ImageCreateFromJPEG($src_file_realpath);
                        break;
                    case 'gif' :
                        $temp_image = ImageCreateFromGIF($src_file_realpath);
                        break;
                    case 'png' :
                        $temp_image = ImageCreateFromPNG($src_file_realpath);
                        break;
                }

                // アップされた画像のサイズ
                // 横幅（px）
                $temp_image_width  = ImageSX($temp_image);
                // 縦幅（px）
                $temp_image_height = ImageSY($temp_image);

                /* - fit - */
                if ($type == 'fit') {

                    // 対象画像-case横長
                    if (($temp_image_width / $temp_image_height) > ($create_img_width / $create_img_height)) {
                        $new_height = $create_img_height;
                        // 縦横比
                        $rate       = $new_height / $temp_image_height;
                        $new_width  = $rate * $temp_image_width;
                        $x          = ($create_img_width - $new_width) / 2;
                        $y          = 0;

                    // 対象画像-case縦長
                    } else {
                        $new_width  = $create_img_width;
                        // 縦横比
                        $rate       = $new_width / $temp_image_width;
                        $new_height = $rate * $temp_image_height;
                        $x          = 0;
                        $y          = ($create_img_height - $new_height) / 2;
                    }

                    // 空画像
                    $new_image = ImageCreateTrueColor($create_img_width, $create_img_height);

                /* - resize - */
                } elseif ($type == 'resize') {

                    // 対象画像-サイズが収まる場合
                    if (($temp_image_width < $create_img_width) && ($temp_image_height < $create_img_height)) {
                        $new_width  = $temp_image_width;
                        $new_height = $temp_image_height;
                        $x          = 0;
                        $y          = 0;

                    // 対象画像-case横長
                    } elseif (($temp_image_width / $temp_image_height) > ($create_img_width / $create_img_height)) {
                        $new_width  = $create_img_width;
                        // 縦横比
                        $rate       = $new_width / $temp_image_width;
                        $new_height = $rate * $temp_image_height;
                        $x          = 0;
                        $y          = 0;

                    // 対象画像-case縦長
                    } else {
                        $new_height = $create_img_height;
                        // 縦横比
                        $rate       = $new_height / $temp_image_height;
                        $new_width  = $rate * $temp_image_width;
                        $x          = 0;
                        $y          = 0;
                    }

                    // 空画像
                    $new_image = ImageCreateTrueColor($new_width, $new_height);

                /* - wfit - */
                } elseif ($type == 'wfit') {

                    // 対象画像 : create_img_widthは数値
                    $new_width  = $create_img_width;
                    $rate       = $new_width / $temp_image_width;
                    // 縦横比
                    $new_height = $rate * $temp_image_height;
                    $x          = 0;
                    $y          = 0;

                    // 空画像
                    $new_image = ImageCreateTrueColor($new_width, $new_height);

                /* - hfit - */
                } elseif ($type == 'hfit') {
                    // 対象画像 : create_img_heightは数値
                    $new_height = $create_img_height;
                    // 縦横比
                    $rate       = $new_height / $temp_image_height;
                    $new_width  = $rate * $temp_image_width;
                    $x          = 0;
                    $y          = 0;

                    // 空画像
                    $new_image = ImageCreateTrueColor($new_width, $new_height);

                /* - resizefit - */
                } elseif ($type == 'resizefit') {

                    // 対象画像-case横長
                    if (($temp_image_width / $temp_image_height) > ($create_img_width / $create_img_height)) {
                        $new_width  = $create_img_width;
                        // 縦横比
                        $rate       = $new_width / $temp_image_width;
                        $new_height = $rate * $temp_image_height;
                        $x          = 0;
                        $y          = 0;

                    // 対象画像-case縦長
                    } else {
                        $new_height = $create_img_height;
                        // 縦横比
                        $rate       = $new_height / $temp_image_height;
                        $new_width  = $rate * $temp_image_width;
                        $x          = 0;
                        $y          = 0;
                    }

                    // 空画像
                    $new_image = ImageCreateTrueColor($new_width, $new_height);
                }

                ImageCopyResampled($new_image, $temp_image, $x, $y, 0, 0, $new_width, $new_height, $temp_image_width, $temp_image_height);

                // realpathで画像生成
                if (substr($new_file_path, 0, 1) == "/") {
                    $new_file_realpath = $this->rootRealpath.$new_file_path;
                } else {
                    $new_file_realpath = str_replace($this->url, $this->rootRealpath, $new_file_path);
                }

                // MODIFIED
                $new_file_realpath = $new_file_path;

                // 3rd引数:クオリティー（0-100）
                ImageJPEG($new_image, $new_file_realpath, 100);

                imagedestroy($temp_image);
                imagedestroy($new_image);

                return $new_file_path;
            } else {
                return $new_file_path;
            }
        }
    }

    public function transfer_by_curl($file_name = null, $model = null, $type = null)
    {
        if ($file_name === null) {
            return false;
        }
        $full_path = WWW_ROOT . 'img/temp/' . $file_name;
        $targetUrl = UPLOAD_API_URL;
        $postData = array(
            'model' => $model,
            'type' => $type,
            'file' => curl_file_create($full_path),
        );
        // Transfer file via curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $targetUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close ($ch);
        $result = json_decode($result);
        if($result->message === STATUS_SUCCESS) {
            unlink($full_path);
        }
        // unlink($full_path);
        // return json_encode([
        //     'status' => STATUS_SUCCESS,
        // ]);
    }

    public function check_and_upload_file($data = null, $model = null, $type = null)
    {
        if ($data === null && !isset($data) && $data['size'] == 0 || $data === '') {
            return '';
        }
        $full_path = WWW_ROOT . 'img/temp';
        $file_name = $this->upload($data, $full_path);
        if (!empty($file_name)) {
            $this->transfer_by_curl($file_name, $model, $type);
        }
        return $file_name;
    }
}

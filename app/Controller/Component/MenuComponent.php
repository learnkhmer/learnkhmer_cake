<?php

class MenuComponent extends Component
{
    public $module_id = [];
    public $action_id = [];

    public function getPermission($modules)
    {
        if(is_array($modules)) {
            if (isset($modules['Module']['id'])) {
                unset($modules['Parent']);
                if (!in_array($modules['Module']['id'], $this->getModuleId())) {
                    unset($modules['Module']);
                    unset($modules['Action']);
                    unset($modules['children']);
                } else {
                    $has_menu = false;
                    foreach ($modules['Action'] as $action_key => $action_value) {
                        if ($action_value['is_menu']) {
                            $has_menu = true;
                        }
                        if (!in_array($action_value['id'], $this->getActionId()) || !$action_value['is_menu'] || $action_value['name'] == 'index') {
                            unset($modules['Action'][$action_key]);
                        }
                    }
                    $modules['Module']['has_menu'] = $has_menu;
                }
            }
            foreach ($modules as $key => $value) {
                $modules[$key] = $this->getPermission($value, $this->getActionId());
            }        
        }
        return $modules;
    }

    public function getMenu($modules)
    {
        if(is_array($modules)) {
            foreach ($modules as $key => $value) {
                if (!$value || (isset($value['Module']['has_menu']) && !$value['Module']['has_menu'])) {
                    unset($modules[$key]);
                } else {
                    $modules[$key] = $this->getMenu($value, $this->getActionId());
                }
            }
        }
        return $modules;
    }

    public function setModuleId($module_id)
    {
        $this->module_id = $module_id;
    }

    public function getModuleId()
    {
        return $this->module_id;
    }

    public function setActionId($action_id)
    {
        $this->action_id = $action_id;
    }

    public function getActionId()
    {
        return $this->action_id;
    }
}
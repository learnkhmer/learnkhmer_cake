<?php
App::uses('AppController', 'Controller');

class ActionsController extends AppController
{
    public $title_for_layout = 'User';

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->loadModel('Module');
    }


    public function admin_index($module_id = null)
    {
        $module = $this->Module->findById($module_id);
        if (!$module) {
            throw new NotFoundException();
        }
        $data = $this->Action->find('all', [
            'conditions' => [
                'Action.module_id' => $module['Module']['id'],
            ],
            'order' => [
                'Action.created' => 'desc',
            ]
        ]);
        $this->set([
            'module' => $module,
            'data' => $data,
            'title_for_layout' => $this->title_for_layout . ' List',
        ]);
    }

    public function admin_create($module_id = null)
    {
        $module = $this->Module->findById($module_id);
        if (!$module) {
            throw new NotFoundException();
        }
        if ($this->request->is('post')) {
            if (isset($this->request->data['Action']['is_menu'])) {
                $this->request->data['Action']['is_menu'] = $this->request->data['Action']['is_menu'] == 'on' ? 1 : 0;
            }
            $this->request->data['Action']['module_id'] = $module['Module']['id'];
            $this->Action->create();
            $save = $this->Action->save($this->request->data);
            if ($save) {
                $this->Session->setFlash(MESSAGE_CREATE, 'success');
                $this->redirect(array('action' => 'index', $save['Action']['module_id']));
            } else {
                $this->Session->setFlash(MESSAGE_FAIL, 'error');
            }
        }
        $this->set([
            'title_for_layout' => $this->title_for_layout . 'Create',
            'module' => $module,
        ]);
    }

    public function admin_edit($id = null)
    {
        $data = $this->Action->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Action->id = $data['Action']['id'];
            if (isset($this->request->data['Action']['is_menu'])) {
                $this->request->data['Action']['is_menu'] = $this->request->data['Action']['is_menu'] == 'on' ? 1 : 0;
            } else {
                $this->request->data['Action']['is_menu'] = 0;
            }
            if ($this->Action->save($this->request->data)) {
                $this->Session->setFlash(__(MESSAGE_UPDATE), 'success');
                $this->redirect(array('action' => 'index', $data['Action']['module_id']));
            } else {
                $this->Session->setFlash(__(MESSAGE_FAIL), 'error');
            }
        } else {
            $this->request->data = $data;
        }
        $this->set([
            'title_for_layout' => $this->title_for_layout . 'Edit',
        ]);
    }

    public function admin_delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException();
        }
        $data = $this->Action->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Action->id = $data['Action']['id'];
        $this->Action->delete();
        $this->redirect(array('action' => 'index', $data['Action']['module_id']));
    }
}

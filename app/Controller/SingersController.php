<?php
App::uses('Controller', 'AppController');

class SingersController extends AppController
{
    public $title_for_layout = 'Singer';
    public $components = array(
        'Image',
    );

    public function admin_index()
    {
        $conditions = [
            'Singer.status' => 1,
        ];
        if ($this->request->query('search')) {
            $conditions[] = array(
                'OR' => array(
                    'Singer.name_en LIKE' => '%'.$this->request->query('search').'%',
                    'Singer.name_kh LIKE' => '%'.$this->request->query('search').'%',
                )
            );
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'limit' => LIMIT,
            'order' => array(
                'Singer.name_en' => 'desc',
            )
        );
        $this->set([
            'data' => $this->Paginator->paginate('Singer'),
        ]);
    }

    public function admin_create()
    {
        $this->set(['title_for_layout' => $this->title_for_layout . 'Create']);
    }

    public function admin_edit($id = null)
    {
        $data = $this->Singer->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->request->data = $data;
        $this->set(['title_for_layout' => $this->title_for_layout . 'Edit']);
    }

    public function admin_delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException();
        }
        $data = $this->Singer->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Singer->id = $data['Singer']['id'];
        $this->Singer->delete();
        $this->redirect(array('action' => 'index'));
    }

    public function admin_save()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->entity($this->request->data);
        $this->Singer->set($this->request->data);
        if (!$this->Singer->validates()) {
            return json_encode(array(
                'status' => 0,
                'message' => MESSAGE_FAIL,
                'data' => $this->Singer->validationErrors,
            ));
        }
        $this->Singer->id = $this->request->data['Singer']['id'];
        if ($this->Singer->save($data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }

    private function entity($data)
    {
        if (!empty($data['Singer']['tmp_photo']['name'])) {
            $path = WWW_ROOT . SINGER_PATH;
            $data['Singer']['photo'] = $this->Image->upload($data['Singer']['tmp_photo'], $path);
            return $data;
        }
    }
}

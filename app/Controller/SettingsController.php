<?php
App::uses('Controller', 'AppController');

class SettingsController extends AppController
{
    public $title_for_layout = 'Setting';

    public function admin_index()
    {
        $data = array(
            'data' => $this->Setting->find('first'),
            'title_for_layout' => $this->title_for_layout . ' List',
        );
        $this->set($data);
    }

    public function admin_save()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        if ($this->Setting->isExist($this->request->data['Setting']['key'])) {
            return json_encode(array(
                'status' => '0',
                'message' => STATUS_FAIL,
            ));
        }
        $data = $this->Setting->getLangColumToArray($this->request->data);
        $save = [
            'Setting' => [
                'lang' => json_encode($data)
            ]
        ];
        $this->Setting->id = 1;
        if ($this->Setting->save($save)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }
    
    public function admin_update()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->request->data;
        unset($data['_wysihtml5_mode']);
        $save = [
            'Setting' => [
                'lang' => json_encode($data)
            ]
        ];
        $this->Setting->id = 1;
        if ($this->Setting->save($save)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }

}

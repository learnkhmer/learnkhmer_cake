<?php
App::uses('AppController', 'Controller');

class MediaController extends AppController
{
    public $title_for_layout = 'Media';

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Security->unlockedActions = array(
            'upload',
        );
    }

    public function upload()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            $result = array(
                'result' => STATUS_FAIL,
                'data' => null,
                'message' => null,
            );
            if (!$_FILES['file']['error']) {
                $path = '';
                switch ($this->request->data('type')) {
                    case MENU_MODEL_BEGINNER:
                        switch ($this->request->data('endpath')) {
                            case 'big':
                                $path = PATH_BEGINNER.'big/';
                                break;

                            case 'category':
                                $path = PATH_BEGINNER.'category/';
                                break;

                            case 'draw':
                                $path = PATH_BEGINNER.'draw/';
                                break;

                            case 'picture':
                                $path = PATH_BEGINNER.'picture/';
                                break;

                            case 'small':
                                $path = PATH_BEGINNER.'small/';
                                break;

                            case 'sound':
                                $path = PATH_BEGINNER.'sound/';
                                break;
                        }

                        break;

                    case MENU_MODEL_READING:
                        switch ($this->request->data('endpath')) {
                            case 'sound':
                                $path = PATH_READING.'sound/';
                                break;
                            default:
                                $path = PATH_READING;
                        }
                        break;

                    case MENU_MODEL_MATH:


                        break;

                     case MENU_MODEL_WORKSHEET:
                         switch ($this->request->data('endpath')) {
                            case 'post':
                                $path = PATH_WORKSHEET.'post/';
                                break;
                             case 'pdf':
                                 $path = PATH_WORKSHEET.'pdf/';
                                 break;
                            case 'sub':
                                $path = PATH_WORKSHEET.'sub/';
                                break;
                        }
                        break;

                    default:

                        break;
                }
                $Image = $this->Components->load('Image');
                $file = $Image->upload($_FILES, WWW_ROOT . $path);
                switch ($this->request->data['mode']) {
                    // only image
                    case 'image':
                        $Image->disp_resize_img_path(WWW_ROOT . $path . $file, 400, 300, 'fit');
                        break;
                }
                $result['result'] = STATUS_SUCCESS;
                $result['data'] = $file;
                
            }
            echo json_encode($result);
        } else {
            throw new NotFoundException();
        }
    }

    public function unlink()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            // physical delete image
            $file_url = WWW_ROOT.'files'.DS.$this->request->data['Offer']['file_name'];
            if (file_exists($file_url)) {
                unlink($file_url);
            }
        }
    }
}

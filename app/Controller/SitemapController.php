<?php
App::uses('AppController', 'Controller');

class SitemapController extends AppController
{
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow();
    }
    
    public function index()
    {
        $this->layout = 'xml';
        $this->RequestHandler->respondAs('xml');
    }
}

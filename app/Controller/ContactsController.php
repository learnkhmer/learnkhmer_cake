
<?php
App::uses('AppController', 'Controller');

class ContactsController extends AppController
{
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(array(
            'send',
        ));
    }

    public function send()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $respond = array(
            'status' => STATUS_SUCCESS,
            'message' => '',
            'data' => '',
        );
        $this->Contact->set($this->request->data);
        if ($this->Contact->validates()) {
            App::uses('CakeEmail', 'Network/Email');
            $CakeEmail = new CakeEmail();
            $CakeEmail->from($this->request->data['Contact']['email']);
            $CakeEmail->to(EMAIL_FROM);
            $CakeEmail->subject(SERVICE_NAME.'-Contact');
            $CakeEmail->emailFormat('text');
            $CakeEmail->template('contact');
            $CakeEmail->viewVars(array(
                'name' => $this->request->data['Contact']['name'],
                'email' => $this->request->data['Contact']['email'],
                'description' => $this->request->data['Contact']['description'],
                )
            );
            $CakeEmail->send();
        } else {
            $respond['status'] = STATUS_FAIL;
        }
        echo json_encode($respond);
    }
}

<?php
App::uses('Controller', 'AppController');

class BeginnersController extends AppController
{
    public $title_for_layout = 'Basic';
    public $components = array(
        'Image',
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(array(
            'view',
            'animation',
            'sound',
        ));
        $this->Security->unlockedActions = array(
            'admin_saveAndUpdate',
        );
    }

    public function animation($id = null)
    {
        $this->renderView($id);
    }

    public function sound($id = null)
    {
        $this->renderView($id);
    }

    public function view()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->Beginner->recursive = -1;
        $data = $this->Beginner->findById($this->request->query('id'));
        $this->layout = 'ajax';
        $this->set('data', $data);
    }

	public function admin_create()
    {
        $this->loadModel('Category');
        $this->request->data = $this->Beginner->create();
        $categories = $this->Category->getAllByModelBeginnerAndType($this->request->query('type'));
        $data1 = array(
            'title_for_layout' => $this->title_for_layout . 'Create',
            'categories' => $categories,
        );
        $this->set($data1);
    }

    public function admin_edit($id = null)
    {
        $data = $this->Beginner->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->loadModel('Category');
        $categories = $this->Category->getAllByModelBeginnerAndType($this->request->query('type'));
        $this->request->data = $data;
        $data1 = array(
            'title_for_layout' => $this->title_for_layout . 'Edit',
            'categories' => $categories,
        );
        $this->set($data1);
    }

    public function admin_delete($id = null)
    {
        if (!$id) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $data = $this->Beginner->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Beginner->id = $data['Beginner']['id'];
        if ($this->Beginner->delete()) {
            $this->redirect(array('action' => $this->request->query('type')));
        }
    }

    public function admin_index()
    {
        $conditions = [
            'Category.id' =
        ];
        $conditions_2 = [
            'Category.model' => MENU_MODEL_BEGINNER,
        ];


        if ($this->request->query('category_id')) {
            $conditions_2['category_id'] = $this->request->query('category_id');
        }
        $this->loadModel('Category');
        $data = $this->Category->find('list', array(
            'conditions' => array(
                'Category.model' => MENU_MODEL_BEGINNER,
            ),
            'order' => array(
                'Category.order' => 'asc',
            ),
        ));
        $data = $this->Beginner->find('all', [
            'conditions' => $conditions_2,
            'order' => array(
                'Beginner.order' => 'asc',
            ),
        ]);
        $this->set([
            'data' => $data,
            'categories' => $categories,
            'title_for_layout' => $this->title_for_layout . ' List',
        ]);
    }

    public function admin_animation()
    {
        $this->adminRenderView(CATEGORY_BEGINNER_TYPE_ANIMATION);
    }

    public function admin_sound()
    {
        $this->adminRenderView(CATEGORY_BEGINNER_TYPE_SOUND);
    }

    public function admin_saveAndUpdate()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->entity($this->request->data);
        $this->Beginner->set($data);
        // Check validation
        if (!$this->Beginner->validates()) {
            return json_encode(array(
                'status' => '0',
                'message' => MESSAGE_FAIL,
                'data' => $this->Beginner->validationErrors,
            ));
        }
        if ($this->Beginner->save($data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
                'data' => array('type' => $this->request->data['Beginner']['type']),
            ));
        }
    }

    private function entity($data = null)
    {
        if ($data === null) {
            return array();
        }
        $data = $data['Beginner'];
        if ($data['sound'] === STATUS_UPDATE) {
            $data['sound'] = $this->Image->check_and_upload_file($data['sound_file'], MENU_MODEL_BEGINNER, FILE_TYPE_SOUND);
            unset($data['sound_file']);
        }
        if ($data['sound_2'] === STATUS_UPDATE) {
            $data['sound_2'] = $this->Image->check_and_upload_file($data['sound_file_2'], MENU_MODEL_BEGINNER, FILE_TYPE_SOUND_2);
            unset($data['sound_file_2']);
        }
        if ($data['image_thumbnail'] === STATUS_UPDATE) {
            $data['image_thumbnail'] = $this->Image->check_and_upload_file($data['image_thumbnail_file'], MENU_MODEL_BEGINNER, FILE_TYPE_THUMBNAIL);
            unset($data['image_thumbnail_file']);
        }
        if ($data['image'] === STATUS_UPDATE) {
            $data['image'] = $this->Image->check_and_upload_file($data['image_file'], MENU_MODEL_BEGINNER, FILE_TYPE_IMAGE);
            unset($data['image_file']);
        }
        if ($data['image_letter'] === STATUS_UPDATE) {
            $data['image_letter'] = $this->Image->check_and_upload_file($data['image_letter_file'], MENU_MODEL_BEGINNER, FILE_TYPE_IMAGE_LETTER);
            unset($data['image_letter_file']);
        }
        if ($data['image_drawing'] === STATUS_UPDATE) {
            $data['image_drawing'] = $this->Image->check_and_upload_file($data['image_drawing_file'], MENU_MODEL_BEGINNER, FILE_TYPE_IMAGE_DRAWING);
            unset($data['image_drawing_file']);
        }
        return $data;
    }

    private function renderView($id = null)
    {
        $this->loadModel('Category');
        $category = $this->Category->getSelectedCategoryAndModel($id, 'beginner');
        if (!$category) {
            throw new NotFoundException();
        }
        $animation = $this->Category->getAllByModelBeginnerAndType(CATEGORY_BEGINNER_TYPE_ANIMATION);
        $sound = $this->Category->getAllByModelBeginnerAndType(CATEGORY_BEGINNER_TYPE_SOUND);
        $data = $this->Beginner->getAllById($id);
        $this->set(compact('animation', 'sound', 'category', 'data'));
        $this->set('title_for_layout', $this->title_for_layout.'');
    }

    private function adminRenderView($type = null)
    {
        if (!$type) {
            throw new NotFoundException();
        }
        $this->loadModel('Category');
        $categories = $this->Category->getAllByModelBeginnerAndType(str_replace('admin_', '', $this->request->action));
        $data = $this->Beginner->find('all', [
            'conditions' => [
                'Category.model' => MENU_MODEL_BEGINNER,
                'Category.type' => $type,
                'Category.id' => $this->request->query('category_id') ? $this->request->query('category_id') : 1
            ],
            'order' => array(
                'Beginner.created' => 'asc',
            ),
        ]);
        $this->set([
            'data' => $data,
            'categories' => $categories,
            'title_for_layout' => $this->title_for_layout . ' List',
        ]);
    }
}

<?php
App::uses('AppController', 'Controller');

class DashboardController extends AppController
{
    public $title_for_layout = 'User';

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function admin_index()
    {
        $this->set([
            'data' => [],
            'title_for_layout' => $this->title_for_layout . ' List',
        ]);
    }
}

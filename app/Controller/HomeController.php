<?php
App::uses('AppController', 'Controller');

class HomeController extends AppController
{
    public $title_for_layout = 'Home';

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(array(
            'index',
            'member'
        ));
    }

    public function index()
    {
        $this->loadModel('Member');
        $members = $this->Member->find('all', array(
            'order' => array(
                'Member.sort' => 'asc',
            ),
            'limit' => 4
        ));
        $data = array(
            'members' => $members,
            'title_for_layout' => $this->title_for_layout,
        );
        $this->set($data);
    }

	public function admin_index()
    {
        $this->set('title_for_layout', $this->title_for_layout.'');
    }

    public function member()
    {
        $this->loadModel('Member');
        $data = $this->Member->find('all', array(
            'order' => array(
                'Member.sort' => 'asc',
            ),
        ));
        $data = array(
            'data' => $data,
            'title_for_layout' => $this->title_for_layout,
        );
        $this->set($data);
    }


}

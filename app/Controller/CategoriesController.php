<?php
App::uses('AppController', 'Controller');

class CategoriesController extends AppController
{
    public $title_for_layout = 'Category';

    public function admin_index()
    {
        $conditions = [];
        $categories = $this->Category->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'Category.status' => 1,
                'COALESCE(Category.parent_id, 0) <>' => 0,
            ],
        ]);

        if ($this->request->query('search')) {
            $conditions[] = array(
                'OR' => array(
                    'Category.name_en LIKE' => '%'.$this->request->query('search').'%',
                    'Category.name_kh LIKE' => '%'.$this->request->query('search').'%',
                    'Category.type LIKE' => '%'.$this->request->query('search').'%',
                    'Category.description LIKE' => '%'.$this->request->query('search').'%',
                )
            );
        }

        if ($this->request->query('model')) {
            $conditions['Category.model'] = $this->request->query('model');
        }

        if ($this->request->query('category_id')) {
            $conditions['Category.id'] = $this->request->query('id');
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'limit' => LIMIT,
            'order' => array(
                'Category.id' => 'desc',
            )
        );
        $this->set([
            'data' => $this->Paginator->paginate('Category'),
            'categories' => $categories,
        ]);
    }

    public function admin_create()
    {
        $parents = $this->Category->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'Category.status' => 1,
            ],
        ]);
        $data = [
            'parents' => $parents,
            'title_for_layout' => $this->title_for_layout.'Create',
        ];
        $this->set($data);
    }

    public function admin_edit($id = null)
    {
        $category = $this->Category->findById($id);
        if (!$category) {
            throw new NotFoundException();
        }
        $parents = $this->Category->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'Category.id <>' => $category['Category']['id'],
            ],
        ]);
        $this->request->data = $category;
        $data = array(
            'parents' => $parents,
            'title_for_layout' => $this->title_for_layout.'Edit',
        );
        $this->set($data);
    }

    public function admin_delete($id = null)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $data = $this->Category->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Category->id = $data['Category']['id'];
        $save = [
            'Category' => [
                'status' => 1,
            ],
        ];
        if ($this->Category->save($save)) {
            return json_encode(array(
                'status' => STATUS_SUCCESS,
            ));
        }
    }

    public function admin_save()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->Category->set($this->request->data);
        if (!$this->Category->validates()) {
            return json_encode(array(
                'status' => 0,
                'message' => MESSAGE_FAIL,
                'data' => $this->Category->validationErrors,
            ));
        }
        if (isset($this->request->data['Category']['status'])) {
            $this->request->data['Category']['status'] = $this->request->data['Category']['status'] == 'on' ? 1 : 0;
        }
        $this->Category->id = $this->request->data['Category']['id'];
        if ($this->Category->save($this->request->data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }
}

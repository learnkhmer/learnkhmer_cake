<?php
App::uses('AppController', 'Controller');

class CategoryDetailsController extends AppController
{
    public $title_for_layout = 'Other';

    public function admin_index()
    {
        $this->loadModel('Category');
        $data = $this->Category->find('first', array(
            'conditions' => array(
                'Category.id' => $this->request->query('category_id'),
                'Category.model' => $this->request->query('model'),
            ),
        ));
        if (!$data) {
            throw new \Exception("Error Processing Request", 1);
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($data['CategoryDetail']['id']) {
                $this->CategoryDetail->id = $data['CategoryDetail']['id'];
            } else {
                $this->CategoryDetail->create();
                $this->request->data['CategoryDetail']['category_id'] = $data['Category']['id'];
            }
            if ($this->CategoryDetail->save($this->request->data)) {
                $this->Session->setFlash(MESSAGE_CREATE, 'success');
                $this->redirect(array(
                    'controller' => 'categories',
                    'action' => 'index',
                    'prefix' => 'admin',
                ));
            } else {
                $this->Session->setFlash(MESSAGE_FAIL, 'error');
            }
        } else {
            $this->request->data = $data;
        }
        $this->set('title_for_layout'.$this->title_for_layout);
    }
}

<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public $components = array(
        'Session',
        'Auth' => array(
            'authError' => LOGIN_ERROR_USER,
            'redirectUrl' => '/',
            'ajaxLogin' => 'ajax_login',
            'authorize' => 'Controller',
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'username'),
                    'scope' => array('User.status' => 1),
                )
            ),
            'unauthorizedRedirect' => '/',
        ),
        'Cookie',
        'RequestHandler',
        'Security' => array(
            'validatePost' => false,
            'csrfExpires' => '+8760 hour',
            'csrfUseOnce' => false,
        ),
        'Paginator',
        'Flash',
    );
    
    public $helpers = array(
        'Session',
        'Html',
        'Form',
        'Js',
        'Flash',
    );

    public function beforeFilter()
    {
        // $this->Security->blackHoleCallback = 'forceSSL';
        // $this->Security->requireSecure();
        Configure::write('debug', DEBUG_MODE !== 'learnkhmer.online' ? 2 : 2);
        if (isset($this->request->params['admin'])) {
            $this->theme = 'Admin';
            Configure::write('Config.language', LOCAL_NAME);
        } else {
            Configure::write('Config.language', $this->Session->read(LOCAL_NAME) ? $this->Session->read(LOCAL_NAME) : LOCAL_NAME);
        }
        $this->loadModel('Setting');
        $settingLang = $this->Setting->getLang();
        $this->set('settingLang', $settingLang);
    }
    
    // public function forceSSL() {
    //     return $this->redirect('https://' . env('SERVER_NAME') . $this->here);
    // }

    public function isAuthorized($user)
    {
        if (empty($this->request->params['admin'])) {
            return true; 
        }
        if (isset($this->request->params['admin'])) {
            return (bool) in_array($user['Group']['name'], [USER_ROLE_SUPER, USER_ROLE_ADMIN]);
        }

        return false;
    }
}

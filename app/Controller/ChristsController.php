<?php
App::uses('Controller', 'AppController');

class ChristsController extends AppController
{
    public $title_for_layout = 'Song';
    public $components = array(
        'Image',
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(array(
            'index',
            'view',
        ));
        $this->loadModel('Song');
    }

    public function index()
    {
        $conditions = [
            'Song.status' => 0,
            'Category.model' => CATEGORY_CHRIST,
        ];
        if ($this->request->query('keyword')) {
            $conditions[] = [
                'OR' => [
                    'Song.title LIKE' => '%'.$this->request->query('keyword').'%',
                    'Song.written LIKE' => '%'.$this->request->query('keyword').'%',
                    'Song.written_year LIKE' => $this->request->query('keyword'),
                    'Song.ref_number LIKE' => $this->request->query('keyword'),
                ],
            ];
        }
        $this->Paginator->settings = array(
            'order' => array(
                'Song.ref_number' => 'asc',
            ),
            'limit' => 20,
            'conditions' => $conditions,
            'contain' => [
                'Category'
            ]
        );
        $data = $this->Paginator->paginate('Song');
        $this->set([
            'data' => $data,
        ]);
    }

    public function view($id = null)
    {
        $data = $this->Song->findByIdAndStatus($id, 0);
        if (!$data) {
            new NotFoundException();
        }
        $this->set([
            'data' => $data,
            'related_articles' => [],
        ]);
    }

    public function admin_index()
    {
        $conditions = [
            'Song.status' => 0,
            'Category.model' => CATEGORY_CHRIST,
            'Category.id' => 170,
        ];

        if ($this->request->query('search')) {
            $conditions[] = [
                'OR' => [
                    'Song.title LIKE' => '%'.$this->request->query('search').'%',
                    'Song.written LIKE' => '%'.$this->request->query('search').'%',
                    'Song.written_year LIKE' => $this->request->query('search'),
                    'Song.ref_number LIKE' => $this->request->query('search'),
                ],
            ];
        }

        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'limit' => LIMIT,
            'order' => array(
                'Song.ref_number' => 'asc',
            ),
            'contain' => [
                'Category'
            ]
        );

        $this->set([
            'data' => $this->Paginator->paginate('Song'),
        ]);
    }

    public function admin_create()
    {
        $this->set([
            'title_for_layout' => $this->title_for_layout . 'Create'
        ]);
    }

    public function admin_edit($id = null)
    {
        $data = $this->Song->find('first', [
            'conditions' => [
                'Category.model' => CATEGORY_CHRIST,
                'Category.id' => 170,
                'Song.id' => $id,
            ],
        ]);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->request->data = $data;
        $this->set([
            'title_for_layout' => $this->title_for_layout . 'Edit'
        ]);
    }

    public function admin_delete($id = null)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $data = $this->Song->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Song->id = $data['Song']['id'];
        $save = [
            'Song' => [
                'status' => 1,
            ],
        ];
        if ($this->Song->save($save)) {
            return json_encode(array(
                'status' => STATUS_SUCCESS,
            ));
        }
    }

    public function admin_save()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->entity($this->request->data);
        $data['Song']['category_id'] =  170;
        $this->Song->set($data);
        if (!$this->Song->validates()) {
            return json_encode(array(
                'status' => 0,
                'message' => MESSAGE_FAIL,
                'data' => $this->Song->validationErrors,
            ));
        }
        $this->Song->id = $this->request->data['Song']['id'];
        if ($this->Song->save($data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }

    private function entity($data)
    {
        if (!empty($data['Song']['tmp_thumnail']['name'])) {
            $path = WWW_ROOT . MUSIC_PAHT_IMAGE;
            $data['Song']['thumnail'] = $this->Image->upload($data['Song']['tmp_thumnail'], $path);
        }
        if (!empty($data['Song']['tmp_audio']['name'])) {
            $path = WWW_ROOT . MUSIC_PAHT_MP3;
            $data['Song']['audio'] = $this->Image->upload($data['Song']['tmp_audio'], $path);
        }
        if (!empty($data['Song']['tmp_chord_image']['name'])) {
            $path = WWW_ROOT . MUSIC_PAHT_IMAGE;
            $data['Song']['chord_image'] = $this->Image->upload($data['Song']['tmp_chord_image'], $path);
        }
        if (!empty($data['Song']['tmp_lyric_image']['name'])) {
            $path = WWW_ROOT . MUSIC_PAHT_IMAGE;
            $data['Song']['lyric_image'] = $this->Image->upload($data['Song']['tmp_lyric_image'], $path);
        }
        return $data;
    }
}

<?php
App::uses('AppController', 'Controller');

class UsersController extends AppController
{
    public $title_for_layout = 'User';

    public $module_id = [];
    public $action_id = [];

    public $components = array('Menu');

    public function beforeFilter()
    {
        parent::beforeFilter();

        $this->loadModel('Group');
        $this->loadModel('Module');
        $this->loadModel('Permission');

        $this->Auth->allow(array(
            'admin_login',
        ));
    }

    public function admin_login()
    {
        if ($this->Auth->loggedIn() && $this->Auth->user('role') === USER_ROLE_ADMIN) {
            $this->redirect('/admin/home');
        }
        $this->layout = 'login';
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                CakeSession::write('Auth.User.menu', json_encode($this->getMenu()));
                $this->redirect('/admin/dashboard');
            } else {
                 $this->Session->setFlash(LOGIN_ERROR_USER, 'error');
            }
        }
    }

    private function getMenu()
    {
        $permissions = $this->Permission->find('all', [
            'conditions' => [
                'Permission.group_id' => $this->Auth->user('group_id'),
            ],
        ]);
        $this->Menu->setModuleId(Hash::extract($permissions, '{n}.Action.module_id'));
        $this->Menu->setActionId(Hash::extract($permissions, '{n}.Permission.action_id'));
        $modules = $this->Module->find('threaded', [
            'order' => [
                'Module.sort' => 'asc',
            ],
        ]);
        return $this->Menu->getMenu($this->Menu->getPermission($modules));
    }

    public function admin_index()
    {
        $data = $this->User->find('all', [
            'conditions' => [
                'User.status <>' => 0,
            ],
            'order' => [
                'User.created' => 'desc',
            ]
        ]);
        $this->set([
            'data' => $data,
            'title_for_layout' => $this->title_for_layout . ' List',
        ]);
    }

    public function admin_create()
    {
        if ($this->request->is('post')) {
            $this->User->create();
            if (isset($this->request->data['User']['status'])) {
                $this->request->data['User']['status'] = $this->request->data['User']['status'] == 'on' ? 1 : 0;
            }
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(MESSAGE_CREATE, 'success');
                $this->redirect('/admin/users');
            } else {
                $this->Session->setFlash(MESSAGE_FAIL, 'error');
            }
        }
        $groups = $this->Group->find('list', [
            'order' => [
                'Group.name' => 'desc',
            ]
        ]);
        $this->set([
            'groups' => $groups,
            'title_for_layout' => $this->title_for_layout . 'Create',
        ]);
    }

    public function admin_edit($id = null)
    {
        if (!$id) {
            $id = $this->Auth->user('id');
        }
        $user = $this->User->findById($id);
        if (!$user) {
            throw new NotFoundException();
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->User->id = $user['User']['id'];
            if (empty($this->request->data['User']['password'])) {
                unset($this->request->data['User']['password']);
            }
            if (empty($this->request->data['User']['password_update'])) {
                unset($this->request->data['User']['password_update']);
            }
            if (isset($this->request->data['User']['status'])) {
                $this->request->data['User']['status'] = $this->request->data['User']['status'] == 'on' ? 1 : 0;
            }
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__(MESSAGE_UPDATE), 'success');
                $this->redirect('/admin/users');
            } else {
                $this->Session->setFlash(__(MESSAGE_FAIL), 'error');
            }
        } else {
            $this->request->data = $user;
            unset($this->request->data['User']['password']);
        }
        $groups = $this->Group->find('list', [
            'order' => [
                'Group.name' => 'desc',
            ]
        ]);
        $this->set([
            'groups' => $groups,
            'title_for_layout' => $this->title_for_layout . 'Edit',
        ]);
    }

    public function admin_status($id = null)
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException();
        }
        $data = $this->User->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->User->id = $data['User']['id'];
        $status = ($data['User']['status'] === 'active' || empty($data['User']['status'])) ? DEACTIVE_STATUS : ACTIVE_STATUS;
        $this->User->save(array('User' => array('status' => $status), false));
        $this->redirect(array('action' => 'index'));
    }

    public function admin_logout()
    {
        $this->redirect($this->Auth->logout());
    }

    // public function admin_delete($id = null)
    // {
    //     if ($this->request->is('ajax')) {
    //         $this->autoRender = false;

    //         $data = $this->User->findById($id);
    //         if (!$data) {
    //             throw new NotFoundException();
    //         }
    //         if ($this->User->is_admin($data['User']['id'])) {
    //             return json_encode(array('deleted' => 'admin'));
    //         }
    //         $this->User->id = $data['User']['id'];
    //         if ($this->request->query('del_type') === DELETE_TYPE_PHYSICAL) {
    //             return json_encode(array('deleted' => $this->User->delete()));
    //         } else {
    //             $uuid = CakeText::uuid();
    //             $data2 = array('email' => $uuid.'+'.$data['User']['email'], 'status' => DELETE_STATUS);
    //             $response = $this->User->save($data2, array('validate' => false)) ? true : false;
    //             return json_encode(array('delete' => $response));
    //         }
    //     } else {
    //         throw new NotFoundException();
    //     }
    // }


    public function admin_delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException();
        }
        $data = $this->User->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->User->id = $data['User']['id'];
        $this->User->delete();
        $this->redirect('/admin/users');
    }
}

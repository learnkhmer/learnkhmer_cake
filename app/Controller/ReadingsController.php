<?php
App::uses('AppController', 'Controller');

class ReadingsController extends AppController
{
    public $title_for_layout = 'Reading';
    public $components = array(
        'Image',
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(array(
            'index',
            'view',
            'view_test',
            'novel'
        ));
        $this->Security->unlockedActions = array(
            'admin_saveAndUpdate',
        );
    }

    public function index($id = null)
    {
        $this->Paginator->settings = array(
            'conditions' => array(
                // 'Reading.category_id' => !$id ? null : $id,
                'Reading.type <>' => 2,
                'Reading.level' => $id,
            ),
            'order' => array(
                'Reading.page' => 'asc',
            ),
            'limit' => 20
        );
        $data = $this->Paginator->paginate('Reading');
        // pr($data);
        $this->set(compact('data'));
        $this->set('title_for_layout', $this->title_for_layout.'');
        if ($data) {
            return $this->render('reading_list');
        }
    }

    public function view($id = null)
    {
        $data = $this->Reading->findById($id);
        $neighbors = $this->Reading->find('neighbors', array(
            'field' => 'page',
            'value' => $data['Reading']['page']
        ));
        // pr($neighbors);
        $this->set(compact('data', 'neighbors'));
        $this->set('title_for_layout', $this->title_for_layout.'');
    }

    public function admin_index()
    {
        $conditions = array(
            'Reading.status' => 1,
        );
        if ($this->request->query('search')) {
            $conditions = array(
                'OR' => array(
                    'Reading.level LIKE' => '%' . trim($this->request->query('search')) . '%',
                    'Reading.page LIKE' => '%' . trim($this->request->query('search')) . '%',
                    'Reading.title LIKE' => '%' . trim($this->request->query('search')) . '%',
                    'Reading.sub_title LIKE' => '%' . trim($this->request->query('search')) . '%',
                ),
            );
        }
        $this->Paginator->settings = array(
            'order' => array(
                'Reading.id' => 'desc',
            ),
            'limit' => 10,
            'conditions' => $conditions,
        );
        $data = $this->Paginator->paginate('Reading');
        $this->set('data', $data);
        $this->set('title_for_layout', $this->title_for_layout.'');
    }

    public function admin_create()
    {
        $this->set('title_for_layout', $this->title_for_layout.'Create');
    }

    public function admin_edit($id = null)
    {
        $data = $this->Reading->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->request->data = $data;
        $this->set('title_for_layout', $this->title_for_layout.'Edit');
    }

    public function admin_delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException();
        }
        $data = $this->Reading->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Reading->id = $data['Reading']['id'];
        $this->Reading->save(array('Reading' => array('status' => 0), false));
        $this->redirect(array('action' => 'index'));
    }

    public function admin_save()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $this->Reading->set($this->request->data);
        $data = $this->entity($this->request->data);
        // Check validation
        if (!$this->Reading->validates()) {
            return json_encode(array(
                'status' => '0',
                'message' => MESSAGE_FAIL,
                'data' => $this->Reading->validationErrors,
            ));
        }
        $this->Reading->id = $this->request->data['Reading']['id'];
        if ($this->Reading->save($data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }

    private function entity($data)
    {
        if (!empty($data['Reading']['tmp_sound']['name'])) {
            $path = WWW_ROOT . PATH_READING_SOUND;
            $data['Reading']['sound'] = $this->Image->upload($data['Reading']['tmp_sound'], $path);
            return $data;
        }
    }
}

<?php
App::uses('AppController', 'Controller');


class ReadingItemsController extends AppController
{
    private $data = array();

    public function admin_view($reading_id = null)
    {
        $this->loadModel('Reading');
        $this->data = $this->Reading->findById($reading_id);
        if (!$this->data) {
            throw new NotFoundException();
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->saveItem();
            $this->Session->setFlash(MESSAGE_CREATE, 'success');
            $this->redirect(array(
                'controller' => 'readings',
                'action' => 'index',
            ));
        } else {
            $this->request->data = $this->data;
        }
    }

    private function saveItem()
    {
        foreach ($this->request->data['ReadingItem'] as $key => $value) {
            switch ($value['mode']) {
                case 'create':
                    $this->ReadingItem->create();
                    unset($value['id']);
                    $value['reading_id'] = $this->data['Reading']['id'];
                    $this->ReadingItem->save(array('ReadingItem' => $value));
                    break;

                case 'edit':
                    $this->ReadingItem->id = $value['id'];
                    $this->ReadingItem->save(array('ReadingItem' => $value));
                    break;

                case 'delete':
                    $this->ReadingItem->id = $value['id'];
                    $this->ReadingItem->delete();
                    break;
             }
        }
    }
}

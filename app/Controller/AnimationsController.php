<?php
App::uses('Controller', 'AppController');

class AnimationsController extends AppController
{
    public $title_for_layout = 'Basic';
    public $components = array(
        'Image',
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->loadModel('Category');
        $this->loadModel('Beginner');
        $this->Auth->allow(array(
            'view',
            'animation',
            'sound',
        ));
        $this->Security->unlockedActions = array(
            'admin_save',
        );
    }

    public function admin_index()
    {
        $conditions = [
            'Category.model' => CATEGORY_ANIMATION,
        ];
        if ($this->request->query('category_id')) {
            $conditions['category_id'] = $this->request->query('category_id');
        }
        $categories = $this->Category->find('list', array(
            'fields' => [
                'Category.id',
                'Category.name_kh',
            ],
            'conditions' => array(
                'Category.model' => CATEGORY_ANIMATION,
            ),
            'order' => array(
                'Category.order' => 'asc',
            ),
        ));
        $this->Paginator->settings = array(
            'conditions' => $conditions,
            'limit' => LIMIT,
            'order' => array(
                'Beginner.order' => 'desc',
            )
        );
        $data = $this->Paginator->paginate('Beginner');
        $this->set([
            'data' => $data,
            'categories' => $categories,
            'title_for_layout' => $this->title_for_layout . ' List',
        ]);
    }

	public function admin_create()
    {
        $this->request->data = $this->Beginner->create();
        $categories = $this->Category->find('list', array(
            'fields' => [
                'Category.id',
                'Category.name_kh',
            ],
            'conditions' => array(
                'Category.model' => CATEGORY_ANIMATION,
            ),
            'order' => array(
                'Category.order' => 'asc',
            ),
        ));
        $data1 = array(
            'title_for_layout' => $this->title_for_layout . 'Create',
            'categories' => $categories,
        );
        $this->set($data1);
    }

    public function admin_edit($id = null)
    {
        $data = $this->Beginner->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $categories = $this->Category->getAllByModelBeginnerAndType($this->request->query('type'));
        $this->request->data = $data;
        $data1 = array(
            'title_for_layout' => $this->title_for_layout . 'Edit',
            'categories' => $categories,
        );
        $this->set($data1);
    }

    public function admin_delete($id = null)
    {
        if (!$id) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $data = $this->Beginner->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Beginner->id = $data['Beginner']['id'];
        if ($this->Beginner->delete()) {
            $this->redirect(array('action' => $this->request->query('type')));
        }
    }

    public function admin_save()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->validateEmtptyResource($this->entity($this->request->data));
        $this->Beginner->set($data);
        // Check validation
        if (!$this->Beginner->validates()) {
            return json_encode(array(
                'status' => '0',
                'message' => MESSAGE_FAIL,
                'data' => $this->Beginner->validationErrors,
            ));
        }
        $this->Beginner->id = $this->request->data['Beginner']['id'];
        if ($this->Beginner->save($data)) {
            // Move file
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }

    private function entity($data)
    {
        if (!empty($data['Beginner']['tmp_thumbnail']['name'])) {
            $path = WWW_ROOT . PATH_BEGINNER_THUMBNAIL;
            $data['Beginner']['thumbnail'] = $this->Image->upload($data['Beginner']['tmp_thumbnail'], $path);
        }
        if (!empty($data['Beginner']['tmp_image']['name'])) {
            $path = WWW_ROOT . PATH_BEGINNER_IMAGE;
            $data['Beginner']['image'] = $this->Image->upload($data['Beginner']['tmp_image'], $path);
        }
        if (!empty($data['Beginner']['tmp_draw']['name'])) {
            $path = WWW_ROOT . PATH_BEGINNER_DRAW;
            $data['Beginner']['draw'] = $this->Image->upload($data['Beginner']['tmp_draw'], $path);
        }
        if (!empty($data['Beginner']['tmp_simple_image']['name'])) {
            $path = WWW_ROOT . PATH_BEGINNER_SIMPLE_IMAGE;
            $data['Beginner']['simple_image'] = $this->Image->upload($data['Beginner']['tmp_simple_image'], $path);
        }
        if (!empty($data['Beginner']['tmp_sound']['name'])) {
            $path = WWW_ROOT . PATH_BEGINNER_SOUND;
            $data['Beginner']['sound'] = $this->Image->upload($data['Beginner']['tmp_sound'], $path);
        }
        if (!empty($data['Beginner']['tmp_simple_sound']['name'])) {
            $path = WWW_ROOT . PATH_BEGINNER_SIMPLE_SOUND;
            $data['Beginner']['simple_sound'] = $this->Image->upload($data['Beginner']['tmp_simple_sound'], $path);
        }
        return $data;
    }

    private function validateEmtptyResource($entity = [])
    {
        pr($entity);exit;
        if (!($entity['Beginner']['id'])) {
            if (!isset($entity['Beginner']['thumbnail']) || !$entity['Beginner']['thumbnail']) {
                $entity['Beginner']['thumbnail'] = '';
            }
            if (!isset($entity['Beginner']['image']) || !$entity['Beginner']['image']) {
                $entity['Beginner']['image'] = '';
            }
            if (!isset($entity['Beginner']['draw']) || !$entity['Beginner']['draw']) {
                $entity['Beginner']['draw'] = '';
            }
            if (!isset($entity['Beginner']['simple_image']) || !$entity['Beginner']['simple_image']) {
                $entity['Beginner']['simple_image'] = '';
            }
            if (!isset($entity['Beginner']['sound']) || !$entity['Beginner']['sound']) {
                $entity['Beginner']['sound'] = '';
            }
            if (!isset($entity['Beginner']['simple_sound']) || !$entity['Beginner']['simple_sound']) {
                $entity['Beginner']['simple_sound'] = '';
            }
        }
        return $entity;
    }
}

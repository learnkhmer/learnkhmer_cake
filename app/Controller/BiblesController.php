<?php
App::uses('Controller', 'AppController');

class BiblesController extends AppController
{
    public $title_for_layout = 'Song';
    public $components = array(
        'Image',
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(array(
            'index',
            'view',
        ));
    }

    public function index()
    {
        $this->loadModel('Song');
        $this->loadModel('Category');
        $this->loadModel('Singer');
        $categories = $this->Category->find('all', [
            'conditions' => [
                'Category.status' => 1,
                'Category.model' => 'song',
            ],
            'order' => [
                'Category.id' => 'asc',
            ]
        ]);
        $singers = $this->Singer->find('all', [
            'conditions' => [
                'status' => 0,
            ],
            'order' => [
                'id' => 'asc',
            ]
        ]);
        $conditions = [
            'Song.status' => 0
        ];
        if ($this->request->query('category')) {
            $conditions[] = [
                'Song.category_id' => $this->request->query('category'),
            ];
        }
        if ($this->request->query('singer')) {
            $conditions[] = [
                'Song.singer_id' => $this->request->query('singer'),
            ];
        }
        if ($this->request->query('keyword')) {
            $conditions[] = [
                'OR' => [
                    'Song.title LIKE' => '%'.$this->request->query('keyword').'%',
                    'Song.written LIKE' => '%'.$this->request->query('keyword').'%',
                    'Song.lyrice LIKE' => '%'.$this->request->query('keyword').'%',
                ],
            ];
        }
        // $data = $this->Song->find('all', [
        //     'conditions' => $conditions,
        //     'order' => [
        //         'Song.id' => 'desc',
        //     ],
        // ]);
        $this->Paginator->settings = array(
            'order' => array(
                'Song.id' => 'asc',
            ),
            'limit' => 9,
            'conditions' => $conditions,
        );
        $data = $this->Paginator->paginate('Song');
        // pr($data);
        $this->set([
            'categories' => $categories,
            'singers' => $singers,
            'data' => $data,
        ]);
    }

    public function view($id = null)
    {
        $this->loadModel('Category');
        $this->loadModel('Singer');
        $categories = $this->Category->find('all', [
            'conditions' => [
                'Category.status' => 1,
                'Category.model' => 'song',
            ],
            'order' => [
                'Category.id' => 'asc',
            ]
        ]);
        $singers = $this->Singer->find('all', [
            'conditions' => [
                'status' => 0,
            ],
            'order' => [
                'id' => 'desc',
            ]
        ]);
        $data = $this->Song->findByIdAndStatus($id, 0);
        if (!$data) {
            new NotFoundException();
        }
        $this->set([
            'categories' => $categories,
            'singers' => $singers,
            'data' => $data,
            'related_articles' => [],
        ]);
    }

    public function admin_index()
    {
        $data = array(
            'data' => $this->Song->find('all', [
                'conditions' => [
                    'Song.status' => 0
                ],
                'order' => [
                    'Song.id' =>'DESC',
                ],
            ]),
            'title_for_layout' => $this->title_for_layout . ' List',
        );
        $this->set($data);
    }

    public function admin_create()
    {
        $this->loadModel('Singer');
        $this->loadModel('Category');
        $singers = $this->Singer->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'status' => 0,
            ]
        ]);
        $categories = $this->Category->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'Category.model' => 'song',
                'Category.status' => 1,
                'Category.parent_id' => NULL,
            ],
        ]);
        $data = [
            'singers' => $singers,
            'categories' => $categories,
            'title_for_layout' => $this->title_for_layout . 'Create'
        ];
        $this->set($data);
    }

    public function admin_edit($id = null)
    {
        $data = $this->Song->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->loadModel('Singer');
        $this->loadModel('Category');
        $singers = $this->Singer->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'status' => 0,
            ]
        ]);
        $categories = $this->Category->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'Category.model' => 'song',
                'Category.status' => 1,
                'Category.parent_id' => NULL,
            ],
        ]);
        $this->request->data = $data;
        $this->set([
            'singers' => $singers,
            'categories' => $categories,
            'title_for_layout' => $this->title_for_layout . 'Edit'
        ]);
    }

    public function admin_delete($id = null)
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $data = $this->Song->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Song->id = $data['Song']['id'];
        $save = [
            'Song' => [
                'status' => 1,
            ],
        ];
        if ($this->Song->save($save)) {
            return json_encode(array(
                'status' => STATUS_SUCCESS,
            ));
        }
    }

    public function admin_save()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->entity($this->request->data);
        $this->Song->set($data);
        if (!$this->Song->validates()) {
            return json_encode(array(
                'status' => 0,
                'message' => MESSAGE_FAIL,
                'data' => $this->Song->validationErrors,
            ));
        }
        $this->Song->id = $this->request->data['Song']['id'];
        if ($this->Song->save($data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }

    private function entity($data)
    {
        if (!empty($data['Song']['tmp_image']['name'])) {
            $path = WWW_ROOT . SINGER_SONG_PATH;
            $data['Song']['photo'] = $this->Image->upload($data['Song']['tmp_image'], $path);
        }
        if (!empty($data['Song']['tmp_mp3']['name'])) {
            $path = WWW_ROOT . SINGER_SONG_PATH;
            $data['Song']['mp3'] = $this->Image->upload($data['Song']['tmp_mp3'], $path);
        }
        if (!empty($data['Song']['tmp_pdf']['name'])) {
            // $path = WWW_ROOT . SINGER_SONG_PATH;
            $path = WWW_ROOT . SINGER_SONG_PATH;
            $data['Song']['pdf'] = $this->Image->upload($data['Song']['tmp_pdf'], $path);
        }
        return $data;
    }
}

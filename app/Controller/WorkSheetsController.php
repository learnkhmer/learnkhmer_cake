<?php
App::uses('Controller', 'AppController');

class WorkSheetsController extends AppController
{
    public $title_for_layout = 'WorkSheet';
    public $components = array(
        'Image',
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->loadModel('Category');
        $this->Auth->allow(array(
            'type',
            'getType',
            'getByPageNumber',
        ));
        $this->Security->unlockedActions = array(
            'admin_save',
        );
    }

    public function type($id = null)
    {
        $category = $this->Category->getSelectedCategoryAndModel($id, MENU_MODEL_WORKSHEET);
        if (!$category) {
            throw new NotFoundException();
        }
        $cat_id = 13;
        if ($this->request->query('cat_id')) {
            $cat_id = $this->request->query('cat_id');
        }
        $this->Paginator->settings = array(
            'limit' => LIMIT_20,
            'conditions' => array(
                'Category.id' => $cat_id,
                'Category.status' => 1,
            ),
            'contain' => array(
                'Category',
            ),
            'order' => array(
                'WorkSheet.order_number' => 'asc',
            ),
        );
        $data = $this->Paginator->paginate('WorkSheet');
        $categories = array();
        $cat_name = Configure::read('Config.language') === 'kh' ? 'Category.name_kh' : 'Category.name_en';
        $categories = $this->Category->find('threaded', array(
            'conditions' => array(
                'Category.model' => MENU_MODEL_WORKSHEET,
                'Category.status' => 1,
            ),
            'order' => array(
                'Category.parent_id' => 'asc',
            ),
            'recursive' => -1,
        ));
        $data1 = array(
            'data' => $data,
            'categories' => $categories,
            'category' => $category,
            'title_for_layout' => $this->title_for_layout.'List',
        );
        $this->set($data1);
    }

    public function getType()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->layout = 'ajax';
        $conditions = array(
            'Category.id' => $this->request->query('id'),
            'Category.status' => 1,
        );
        $this->Paginator->settings = array(
            'limit' => LIMIT_20,
            'conditions' => $conditions,
            'contain' => array(
                'Category',
            ),
            'order' => array(
                'WorkSheet.order_number' => 'asc',
            ),
        );
        $data = $this->Paginator->paginate('WorkSheet');
        $this->set('data', $data);
    }

    public function getByPageNumber()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->layout = 'ajax';
        $this->Paginator->settings = array(
            'limit' => LIMIT_20,
            'conditions' => array(
                'Category.id' => $this->request->query('cat_id'),
            ),
            'contain' => array(
                'Category',
            ),
            'page' => $this->request->query('page'),
            'order' => array(
                'WorkSheet.order_number' => 'asc',
            ),
        );
        $data = $this->Paginator->paginate('WorkSheet');
        $this->set('data', $data);
        $this->render('get_type');
    }

    public function admin_index()
    {
        $conditions = array(
            'Category.model' => MENU_MODEL_WORKSHEET,
        );
        if ($this->request->query('search')) {
            $keyword = trim($this->request->query('search'));
            $conditions = array(
                'OR' => array(
                    'WorkSheet.title LIKE' => '%'.$keyword.'%',
                    'WorkSheet.description LIKE' => '%'.$keyword.'%',
                    'Category.name_en LIKE' => '%'.$keyword.'%',
                    'Category.name_kh LIKE' => '%'.$keyword.'%',
                )
            );
        }

        if ($this->request->query('category_id')) {
            $conditions['WorkSheet.category_id'] = $this->request->query('category_id');
        }

        
        $categories = $this->Category->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'Category.model' => CATEGORY_WORKSHEET,
            ],
        ]);

        $this->Paginator->settings = array('order' => array(
            'WorkSheet.order_number' => 'asc',
            ),
            'limit' => LIMIT,
            'conditions' => $conditions,
            'order' => [
                'WorkSheet.id' => 'desc',
            ]
        );

        $data = $this->Paginator->paginate('WorkSheet');
        $this->set([
            'data' => $data,
            'categories' => $categories,
        ]);
    }

	public function admin_create()
    {
        $categories = $this->Category->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'Category.model' => CATEGORY_WORKSHEET,
            ],
        ]);

        $data = array(
            'categories' => $categories,
            'title_for_layout' => $this->title_for_layout.'Create',
        );
        $this->set($data);
    }

    public function admin_edit($id = null)
    {
        if (!$id) {
            throw new NotFoundException();
        }
        $data = $this->WorkSheet->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $categories = $this->Category->find('list', [
            'fields' => ['id', 'name_kh'],
            'conditions' => [
                'Category.model' => CATEGORY_WORKSHEET,
            ],
        ]);
        $this->request->data = $data;
        $data1 = array(
            'categories' => $categories,
            'title_for_layout' => $this->title_for_layout.'Edit',
        );
        $this->set($data1);
    }

    public function admin_delete($id = null)
    {
        if (!$id) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $data = $this->WorkSheet->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->WorkSheet->id = $data['WorkSheet']['id'];
        if ($this->WorkSheet->delete()) {
            $this->redirect(array('action' => 'index'));
        }
    }

    public function admin_save()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->validateEmtptyResource($this->entity($this->request->data));
        $this->WorkSheet->set($data);
        // Check validation
        if (!$this->WorkSheet->validates()) {
            return json_encode(array(
                'status' => '0',
                'message' => MESSAGE_FAIL,
                'data' => $this->WorkSheet->validationErrors,
            ));
        }
        $this->WorkSheet->id = $this->request->data['WorkSheet']['id'];
        if ($this->WorkSheet->save($data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }

    private function entity($data)
    {
        if (!empty($data['WorkSheet']['tmp_thumbnail']['name'])) {
            $path = WWW_ROOT . PATH_WORKSHEET_IMG;
            $data['WorkSheet']['thumbnail'] = $this->Image->upload($data['WorkSheet']['tmp_thumbnail'], $path);
        }
        if (!empty($data['WorkSheet']['tmp_file']['name'])) {
            $path = WWW_ROOT . PATH_WORKSHEET_PDF;
            $data['WorkSheet']['file'] = $this->Image->upload($data['WorkSheet']['tmp_file'], $path);
        }
        return $data;
    }

    private function validateEmtptyResource($entity = [])
    {
        if (!($entity['WorkSheet']['id'])) {
            if (!isset($entity['WorkSheet']['thumbnail']) || !$entity['WorkSheet']['thumbnail']) {
                $entity['WorkSheet']['thumbnail'] = '';
            }
            if (!isset($entity['WorkSheet']['file']) || !$entity['WorkSheet']['file']) {
                $entity['WorkSheet']['file'] = '';
            }
        }
        return $entity;
    }
}

<?php
App::uses('AppController', 'Controller');

class GroupsController extends AppController
{
    public $title_for_layout = 'User';

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->loadModel('Module');
        $this->loadModel('Permission');
    }


    public function admin_index()
    {
        $data = $this->Group->find('all', [
            'order' => [
                'Group.created' => 'desc',
            ]
        ]);
        $this->set([
            'data' => $data,
            'title_for_layout' => $this->title_for_layout . ' List',
        ]);
    }

    public function admin_create()
    {
        $modules = $this->Module->find('all');
        $permissions = [];
        
        if ($this->request->is('post')) {
            $this->Group->create();
            $this->request->data['Group']['user_id'] = $this->Auth->user('id');
            $save = $this->Group->save($this->request->data);
            if ($save) {
                $permissions = [];
                foreach ($this->request->data['permissions'] as $key => $value) {
                    $permissions[] = [
                        'group_id' => $save['Group']['id'],
                        'action_id' => $value,
                    ];
                }
                if ($permissions) {
                    $this->Permission->saveMany($permissions);
                }
                $this->Session->setFlash(MESSAGE_CREATE, 'success');
                $this->redirect(array('action' => 'admin_index'));
            } else {
                $this->Session->setFlash(MESSAGE_FAIL, 'error');
                $permissions = $this->request->data['permissions'];
            }
        }
        $this->set([
            'title_for_layout' => $this->title_for_layout . 'Create',
            'modules' => $modules,
            'permissions' => $permissions,
        ]);
    }

    public function admin_edit($id = null)
    {
        $data = $this->Group->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }

        $modules = $this->Module->find('all');
        $permissions = $this->Permission->find('list', [
            'fields' => array('Permission.action_id'),
            'conditions' => [
                'Permission.group_id' => $data['Group']['id'],
            ],
            'order' => [
                'Permission.id' => 'desc',
            ],
        ]);
        if ($this->request->is(array('post', 'put'))) {
            $this->Group->id = $data['Group']['id'];
            if ($this->Group->save($this->request->data)) {
                $permissions = [];
                $this->Permission->deleteAll(array('Permission.group_id' => $data['Group']['id']), false);
                foreach ($this->request->data['permissions'] as $key => $value) {
                    $permissions[] = [
                        'group_id' => $data['Group']['id'],
                        'action_id' => $value,
                    ];
                }
                if ($permissions) {
                    $this->Permission->saveMany($permissions);
                }
                $this->Session->setFlash(__(MESSAGE_UPDATE), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__(MESSAGE_FAIL), 'error');
            }
        } else {
            $this->request->data = $data;
        }
        $this->set([
            'title_for_layout' => $this->title_for_layout . 'Edit',
            'modules' => $modules,
            'permissions' => $permissions,
        ]);
    }

    public function admin_delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException();
        }
        $data = $this->Group->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Group->id = $data['Group']['id'];
        $this->Group->delete();
        $this->redirect(array('action' => 'index'));
    }
}

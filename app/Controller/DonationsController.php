<?php
App::uses('AppController', 'Controller');

class DonationsController extends AppController
{
    public $components = array(
        'Image',
        'Ipn',
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow(array(
            'index',
            'ipn',
            'cancel',
        ));

        $this->Security->unlockedActions = array(
            'ipn',
        );
    }

    public function index()
    {
        if ($this->request->is(array('post', 'put'))) {
            $this->Donation->set($this->request->data);
            if ($this->Donation->validates()) {
                $data = array(
                    'business' => PAYPAL_BUSSINESS_EMAIL,
                    'amount' => $this->request->data['Donation']['amount'],
                    'description' => 'ឧបត្ថម​​ទៅលើ​គម្រោង​រៀន​ភាសា​ខ្មែរ​អនឡាញ',
                    'item_name' => 'ឧបត្ថម',
                    'item_number' => '',
                    'user_id' => '',
                    'return' => PAYPAL_RETURN_SUCCESS_URL,
                    'notify_url' => IPN,
                    'cancel_url'=> PAYPAL_CANCEL_URL,
                    'url' => PAYPAL_URL,
                );
                return $this->redirect($this->Ipn->paypal_data($data));
            }
        } else if ($this->request->query('mode') === 'complete') {
            $this->set('title_for_layout', 'Donation is completed');
            $this->render('complete');
        }
        $this->set('title_for_layout', 'Donation');
    }

    public function ipn()
    {
        $this->autoRender = false;
        $this->layout = false;
        $data = array();
        $data = $_POST;
        //pr($data);exit;
        if ($this->Ipn->validate_ipn_data($data)) {
            $donate['Donation'] = array();
            $donate['Donation']['amount'] = doubleval($data['payment_gross']);

            $this->Donation->create();
            if ($this->Donation->save($donate, false)) {
                $this->loadModel('User');
                $donor = $this->User->findById($donate['Donation']['user_id'], null, null, -1);
                $receivers = array(
                    EMAIL_ADMIN,
                    $donate['Donation']['email']
                );
                App::uses('CakeEmail', 'Network/Email');
                $CakeEmail = new CakeEmail();
                // Notify admin and donor
                $CakeEmail->from(EMAIL_FROM_PAYPAL);
                $CakeEmail->to($receivers);
                $CakeEmail->subject(SERVICE_NAME.' Donation success');
                $CakeEmail->emailFormat('text');
                $CakeEmail->template('notify_donation');
                $CakeEmail->viewVars(array('amount' => $donate['Donation']['amount']));

                try {
                    $CakeEmail->send();
                } catch(Exception $e) {
                    $CakeEmail->reset();
                    CakeEmail::deliver(EMAIL_FROM_PAYPAL, SERVICE_NAME.' : EMAIL ERROR - '.$this->request->action, $e->getMessage(), array('from' => EMAIL_FROM_PAYPAL));
                }
                $CakeEmail->reset();
            }
        }
    }

    public function cancel()
    {
        return $this->redirect('/');
    }

    public function admin_index()
    {
        $options = array();
        if ($this->request->query('search')) {
            $options[] = array(
                'OR' => array(
                    'Donation.name LIKE' => '%' . trim($this->request->query('search')) . '%',
                    'Donation.purpose LIKE' => '%' . trim($this->request->query('search')) . '%',
                    'Donation.amount LIKE' => '%' . trim($this->request->query('search')) . '%',
                    'Donation.source LIKE' => '%' . trim($this->request->query('search')) . '%',
                ),
            );
        }
        $this->Paginator->settings = array(
            'conditions' => $options,
            'limit' => LIMIT,
            'order' => array(
                'Donation.created' => 'desc',
            )
        );
        $data = array(
            'data' => $this->Paginator->paginate('Donation'),
            'title_for_layout' => $this->title_for_layout . ' List',
        );
        $this->set($data);
    }

    public function admin_create()
    {
        $this->set('title_for_layout', $this->title_for_layout . 'Create');
    }

    public function admin_edit($id = null)
    {
        if (!$id) {
            $id = $this->Auth->user('id');
        }
        $data = $this->Donation->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->request->data = $data;
        $this->set(['title_for_layout' => $this->title_for_layout . 'Edit']);
    }

    public function admin_save()
    {
        if (!$this->request->is('ajax')) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        $this->layout = 'ajax';
        $data = $this->entity($this->request->data);
        $this->Donation->set($this->request->data);
        if (!$this->Donation->validates()) {
            return json_encode(array(
                'status' => 0,
                'message' => MESSAGE_FAIL,
                'data' => $this->Donation->validationErrors,
            ));
        }
        $this->Donation->id = $this->request->data['Donation']['id'];
        if ($this->Donation->save($data)) {
            return json_encode(array(
                'status' => '1',
                'message' => STATUS_SUCCESS,
            ));
        }
    }

    private function entity($data)
    {
        if (!empty($data['Donation']['tmp_photo']['name'])) {
            $path = WWW_ROOT . DONATATION_PATH;
            $data['Donation']['photo'] = $this->Image->upload($data['Donation']['tmp_photo'], $path);
            return $data;
        }
    }
}

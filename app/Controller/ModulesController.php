<?php
App::uses('AppController', 'Controller');

class ModulesController extends AppController
{
    public $title_for_layout = 'User';

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function admin_index()
    {
        $data = $this->Module->find('all', [
            'order' => [
                'Module.parent_id' => 'desc',
                'Module.sort' => 'asc',
            ]
        ]);
        $this->set([
            'data' => $data,
            'title_for_layout' => $this->title_for_layout . ' List',
        ]);
    }

    public function admin_create()
    {
        if ($this->request->is('post')) {
            $this->Module->create();
            $save = $this->Module->save($this->request->data);
            if ($save) {
                $this->loadModel('Action');
                foreach ($this->request->data['actions'] as $key => $value) {
                    if (isset($value['key'])) {
                        $this->Action->create();
                        $this->Action->save([
                            'module_id' => $save['Module']['id'],
                            'is_menu' => $value['is_menu'],
                            'name' => $value['name'],
                            'url' => $value['name'],
                            'sort' => $key + 1
                        ]);
                    }
                }
                $this->Session->setFlash(MESSAGE_CREATE, 'success');
                $this->redirect(array('action' => 'admin_index'));
            } else {
                $this->Session->setFlash(MESSAGE_FAIL, 'error');
            }
        }
        $parents = $this->Module->find('list', [
            'order' => [
                'Module.created' => 'desc',
            ]
        ]);
        $this->set([
            'title_for_layout' => $this->title_for_layout . 'Create',
            'parents' => $parents,
        ]);
    }

    public function admin_edit($id = null)
    {
        $data = $this->Module->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Module->id = $data['Module']['id'];
            if ($this->Module->save($this->request->data)) {
                $this->Session->setFlash(__(MESSAGE_UPDATE), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__(MESSAGE_FAIL), 'error');
            }
        } else {
            $this->request->data = $data;
        }
        $parents = $this->Module->find('list', [
            'conditions' => [
                'Module.id <>' => $data['Module']['id'],
            ],
            'order' => [
                'Module.created' => 'desc',
            ]
        ]);
        $this->set([
            'title_for_layout' => $this->title_for_layout . 'Edit',
            'parents' => $parents,
        ]);
    }

    public function admin_delete($id = null)
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException();
        }
        $data = $this->Module->findById($id);
        if (!$data) {
            throw new NotFoundException();
        }
        $this->Module->id = $data['Module']['id'];
        $this->Module->delete();
        $this->redirect(array('action' => 'index'));
    }
}
